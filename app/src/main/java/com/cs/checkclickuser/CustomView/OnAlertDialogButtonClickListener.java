/*

cc
Copyright (c) 2016 Inscripts
License: https://www.cometchat.com/legal/license

*/
package com.cs.checkclickuser.CustomView;

import android.view.View;

import android.app.AlertDialog;

public interface OnAlertDialogButtonClickListener {
	public void onButtonClick(AlertDialog alertDialog, View v, int which, int popupId);
}
