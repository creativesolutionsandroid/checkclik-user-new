package com.cs.checkclickuser.Base;

public interface BasePresenter<V> {

    void attach(V baseView);

    void detach();


}
