package com.cs.checkclickuser.Fragements;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Activites.FillterActivity;
import com.cs.checkclickuser.Activites.MapActivity;
import com.cs.checkclickuser.Activites.NotificationsActivity;
import com.cs.checkclickuser.Adapter.ProductAdapter;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BarCodeScanner;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;

public class ProductFragment extends Fragment implements View.OnClickListener {

    View rootView;
    String TAG = "TAG";
    SharedPreferences languagePrefs;
    private ImageView back_btn, fillter, cart, notification, paymentcash, paymentcard;
    private ImageView filterStatusIcon, cartStatusIcon, notifictionStatusIcon;
    TextView cattext;
    private LinearLayout locationLayout;
    EditText seachbar;
    private RecyclerView productlist;
    private ProductAdapter mProductadapter;
    private CardView mapButton;
    public static String filterStatus = "", filterPaymentType = "", filterRatings = "", filterDistance = "",
            filterSort = "";

    String language;
    SharedPreferences userPrefs, LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;

    ArrayList<ProductlistResponce.Stores> storesArrayList = new ArrayList<>();
    ArrayList<ProductlistResponce.JOtherBranch> branchList = new ArrayList<>();
    ArrayList<ProductlistResponce.FilterMainCategory> MainCategory = new ArrayList<>();
    ArrayList<ProductlistResponce.FilterSubCategoryCat> SubCategoryArrayList = new ArrayList<>();

    private FusedLocationProviderClient mFusedLocationClient;
    public static boolean isCurrentLocationSelected = true;
//    private static final int CATEGORY_REQUEST = 1;
    private static final int FILTER_REQUEST = 3;

    public static ArrayList<Integer> filterid = new ArrayList<>();
    public static boolean filter = false;
    private GoogleMap mMap;

    private int TOTAL_STORES_COUNT;
    private int PAGE_SIZE = 25;
    private int pageNumber = 1;
    private int recyclerViewScrollPosition = 0;
    private String searchText = "";
    private LinearLayoutManager mLayoutManager;
    private boolean isSearching = false;

    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;
    private RelativeLayout searchLayout;

    private ImageView scanner;
    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.product_fragment, container, false);
        } else {
            rootView = inflater.inflate(R.layout.product_fragment_arabic, container, false);
        }
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

//        int currentapiVersion1 = Build.VERSION.SDK_INT;
//        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
//            getActivity().getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//                            | View.SYSTEM_UI_FLAG_VISIBLE);
////            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//        }

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        back_btn = (ImageView) rootView.findViewById(R.id.back_btn);
        cart = (ImageView) rootView.findViewById(R.id.product_cart);
        notification = (ImageView) rootView.findViewById(R.id.product_noti);

        notifictionStatusIcon = (ImageView) rootView.findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) rootView.findViewById(R.id.cart_status);
        filterStatusIcon = (ImageView) rootView.findViewById(R.id.filter_status);

        seachbar = (EditText) rootView.findViewById(R.id.product_search);
        locationLayout = (LinearLayout) rootView.findViewById(R.id.location_layout);
        productlist = (RecyclerView) rootView.findViewById(R.id.product_listview);
        cattext = (TextView) rootView.findViewById(R.id.product_category);
        paymentcash = (ImageView) rootView.findViewById(R.id.payment_cash);
        paymentcard = (ImageView) rootView.findViewById(R.id.payment_card);
        fillter = (ImageView) rootView.findViewById(R.id.filter);
        mapButton = (CardView) rootView.findViewById(R.id.map_button);
        scanner=(ImageView) rootView.findViewById(R.id.cameraicon);
        searchLayout = (RelativeLayout) rootView.findViewById(R.id.search_layout);
        tryAgainButton =(Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(getActivity());
                } else {
                    Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                    startActivity(intent);
                }

            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(getActivity());
                    }
                } else {
                    BarCodeScanner.showScannerDialog(getActivity());
                }
            }
        });

        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), MapActivity.class);
                intent.putExtra("array", storesArrayList);
                intent.putExtra("branch_array", branchList);
                intent.putExtra("type", 1);
                getContext().startActivity(intent);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(getActivity());
                } else {
                    Intent intent = new Intent(getActivity(), CartActivity.class);
                    startActivity(intent);
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                Fragment categoryFragment = new ProductCategoryFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
//                getActivity().onBackPressed();
//                Intent intent = new Intent(getContext(), ProductCategoryActivity.class);
//                startActivityForResult(intent, CATEGORY_REQUEST);
            }
        });

        cattext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().onBackPressed();
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                Fragment categoryFragment = new ProductCategoryFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
//                Intent intent = new Intent(getContext(), ProductCategoryActivity.class);
//                startActivityForResult(intent, CATEGORY_REQUEST);
                }
        });

        fillter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FillterActivity.class);
                intent.putExtra("Fillterarry", MainCategory);
                intent.putExtra("Fillterarry1", SubCategoryArrayList);
                intent.putExtra("type", 1);
                startActivityForResult(intent, FILTER_REQUEST);
            }
        });

        seachbar.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        seachbar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchText = seachbar.getText().toString();
                    hideKeyboard(getActivity());
                    String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        recyclerViewScrollPosition = 0;
                        pageNumber = 1;
                        isSearching = true;
                        new GetProductApi().execute();
                    }
                    else {
                        mapButton.setVisibility(View.GONE);
                        productlist.setVisibility(View.GONE);
                        searchLayout.setVisibility(View.GONE);
                        nothingFoundImage.setVisibility(View.VISIBLE);
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        seachbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() == 0) {
                    searchText = "";
                    recyclerViewScrollPosition = 0;
                    pageNumber = 1;
                    isSearching = false;
                    new GetProductApi().execute();
                }
                else if(editable.toString().length() < 3) {
                    searchText = editable.toString();
                }
                else {
                    searchText = editable.toString();
                    recyclerViewScrollPosition = 0;
                    pageNumber = 1;
                    isSearching = true;
                    new GetProductApi().execute();
                }
            }
        });

        productlist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mLayoutManager.findLastVisibleItemPosition() == (storesArrayList.size() - 1) &&
                        (PAGE_SIZE*pageNumber) < TOTAL_STORES_COUNT) {
                    pageNumber = pageNumber + 1;
                    recyclerViewScrollPosition = mLayoutManager.findLastVisibleItemPosition();
                    isSearching = false;
                    new GetProductApi().execute();
                }
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    recyclerViewScrollPosition = 0;
                    pageNumber = 1;
                    isSearching = false;
                    new GetProductApi().execute();
                }
                else {
                    mapButton.setVisibility(View.GONE);
                    productlist.setVisibility(View.GONE);
                    searchLayout.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        locationLayout.setOnClickListener(this);
        return rootView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.location_layout:
//                getActivity().onBackPressed();
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                Fragment categoryFragment = new ProductCategoryFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
//                Intent intent = new Intent(getContext(), ProductCategoryActivity.class);
//                startActivityForResult(intent, CATEGORY_REQUEST);
                break;
        }
    }


    private class GetProductApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            if(!isSearching) {
                Constants.showLoadingDialog(getActivity());
            }
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductlistResponce> call = apiService.getstorslist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductlistResponce>() {
                @Override
                public void onResponse(Call<ProductlistResponce> call, Response<ProductlistResponce> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        ProductlistResponce stores = response.body();
                        if (stores.getStatus()) {
                            MainCategory = stores.getData().getFilterMainCategory();
                            SubCategoryArrayList = stores.getData().getFilterSubCategoryCat();
                            TOTAL_STORES_COUNT = stores.getData().getStoresCount();

                            if(pageNumber == 1) {
                                storesArrayList = stores.getData().getStores();
                            }
                            else {
                                storesArrayList.addAll(stores.getData().getStores());
                            }

                            CacheData.productStoresArrayList = storesArrayList;
                            CacheData.productMainCategory = MainCategory;
                            CacheData.productSubCategoryArrayList= SubCategoryArrayList;
                            CacheData.PRODUCT_TOTAL_STORE_COUNT = TOTAL_STORES_COUNT;
                        }

                        if (storesArrayList.size() > 0) {
                            mapButton.setVisibility(View.VISIBLE);
                            productlist.setVisibility(View.VISIBLE);
                            searchLayout.setVisibility(View.VISIBLE);
                            nothingFoundImage.setVisibility(View.GONE);
                            noDataFoundLayout.setVisibility(View.GONE);
                            mLayoutManager = new LinearLayoutManager(getActivity());
                            productlist.setLayoutManager(mLayoutManager);
                            mProductadapter = new ProductAdapter(getContext(), storesArrayList, 1, branchList,language);
                            productlist.setAdapter(mProductadapter);
                            mProductadapter.notifyDataSetChanged();
                            productlist.smoothScrollToPosition(recyclerViewScrollPosition);

                            if (storesArrayList.get(0).getCartItemCount() != 0) {
                                cartStatusIcon.setVisibility(View.VISIBLE);
                            }
                            if (storesArrayList.get(0).getNotificationCount() != 0) {
                                notifictionStatusIcon.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            mapButton.setVisibility(View.GONE);
                            productlist.setVisibility(View.GONE);
                            searchLayout.setVisibility(View.GONE);
                            noDataFoundLayout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mapButton.setVisibility(View.GONE);
                        productlist.setVisibility(View.GONE);
                        searchLayout.setVisibility(View.GONE);
                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductlistResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    mapButton.setVisibility(View.GONE);
                    productlist.setVisibility(View.GONE);
                    nothingFoundImage.setVisibility(View.VISIBLE);
                    Log.i(TAG, "onFailure: " + t);
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("CategoryId", Constants.productCategoryid);
            parentObj.put("Latitude", Constants.Latitude);
            parentObj.put("Longitude", Constants.Longitude);
            parentObj.put("OrderStatus", filterStatus);
            parentObj.put("PaymentType", filterPaymentType);
            parentObj.put("Ratings", filterRatings);
            parentObj.put("Distance", filterDistance);
            parentObj.put("Sort", filterSort);
            parentObj.put("PageSize", PAGE_SIZE);
            parentObj.put("PageNumber", pageNumber);
            parentObj.put("SearchText", searchText);
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareGetStoresJSON: "+parentObj.toString());
        return parentObj.toString();
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getContext(), perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(getActivity());
                }
                else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), getResources().getString(R.string.camera_permission_denied_ar), Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == CATEGORY_REQUEST && resultCode == RESULT_OK) {
//            cattext.setText(userPrefs.getString("productCategoryname", ""));
//            isSearching = false;
//            new GetProductApi().execute();
//        }
//        else
            if (requestCode == FILTER_REQUEST && resultCode == RESULT_OK){
            isSearching = false;
            new GetProductApi().execute();

               if (!filterStatus.equals("") || !filterPaymentType.equals("") || !filterRatings.equals("") ||
                       !filterDistance.equals("") || !filterSort.equals("")) {
                   filterStatusIcon.setVisibility(View.VISIBLE);
               }
               else {
                   filterStatusIcon.setVisibility(View.INVISIBLE);
               }
        }
    }

    private void setDataFromCache(){
        storesArrayList = CacheData.productStoresArrayList;
        MainCategory = CacheData.productMainCategory;
        SubCategoryArrayList = CacheData.productSubCategoryArrayList;
        TOTAL_STORES_COUNT = CacheData.PRODUCT_TOTAL_STORE_COUNT;
        recyclerViewScrollPosition = 0;
        pageNumber = 1;

        mLayoutManager = new LinearLayoutManager(getActivity());
        productlist.setLayoutManager(mLayoutManager);
        mProductadapter = new ProductAdapter(getContext(), storesArrayList, 1, branchList, language);
        productlist.setAdapter(mProductadapter);
        mProductadapter.notifyDataSetChanged();
        productlist.smoothScrollToPosition(recyclerViewScrollPosition);

        if (storesArrayList.size() > 0) {
            if (storesArrayList.get(0).getCartItemCount() != 0) {
                cartStatusIcon.setVisibility(View.VISIBLE);
            }
            if (storesArrayList.get(0).getNotificationCount() != 0) {
                notifictionStatusIcon.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (CacheData.productStoresArrayList != null) {
            setDataFromCache();
        }
        else {
            cattext.setText(Constants.productCategoryname);
            isSearching = false;
            new GetProductApi().execute();
        }

        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
    }

}







