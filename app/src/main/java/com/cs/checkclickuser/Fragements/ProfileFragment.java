package com.cs.checkclickuser.Fragements;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ChangeLanguage;
import com.cs.checkclickuser.Activites.CometChatActivity;
import com.cs.checkclickuser.Activites.CouponVoucherActivity;
import com.cs.checkclickuser.Activites.FavoritesActivity;
import com.cs.checkclickuser.Activites.OffersActivity;
import com.cs.checkclickuser.Activites.SettingActivity;
import com.cs.checkclickuser.Activites.SplashScreen;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.Signinresponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.cs.checkclickuser.Fragements.ProductCategoryFragment.TAG;
import static com.cs.checkclickuser.Utils.Constants.USERS_IMAGE_URL;

public class ProfileFragment extends Fragment {

    private static final int CAMERA_REQUEST = 4;
    View rootView;
    ImageView settings;
    RelativeLayout favuorites, coupon, offers, chaticon, changelanguage_layout;
    CircleImageView profilepic;
    TextView customername, location_text;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String strMobile, strUsername, strUserid;
    String pictureName = "";
    AlertDialog customDialog;
    HomePageResponse homePageResponse;

    String imageResponse;
    private static final int PICK_IMAGE_FROM_GALLERY = 2;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    boolean isCamera = false;
    Bitmap thumbnail;
    Boolean isImageChanged = false;
    private DefaultHttpClient mHttpClient11;
    Uri imageUri;
    String imagePath;
    private byte[] image1Bytes;
    private String image1Str = "";


    SharedPreferences LanguagePrefs;
    String language;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewCompat.requestApplyInsets(view);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {

            rootView = inflater.inflate(R.layout.profile_fragment, container, false);

        } else {
            rootView = inflater.inflate(R.layout.profile_fragment_ar, container, false);

        }


//        int currentapiVersion1 = Build.VERSION.SDK_INT;
//        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
//            getActivity().getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_VISIBLE);
//            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//        }

        userPrefs = getActivity().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        pictureName = userPrefs.getString("pic", "");
        strUserid = userPrefs.getString("userId", "");

        settings = (ImageView) rootView.findViewById(R.id.settings);
        favuorites = (RelativeLayout) rootView.findViewById(R.id.favourites);
        profilepic = (CircleImageView) rootView.findViewById(R.id.ac_profilepic);
        coupon = (RelativeLayout) rootView.findViewById(R.id.couponicon);
        offers = (RelativeLayout) rootView.findViewById(R.id.offers_layout);
        customername = (TextView) rootView.findViewById(R.id.customername);
        location_text = (TextView) rootView.findViewById(R.id.location_text);
        chaticon = (RelativeLayout) rootView.findViewById(R.id.chaticon);
        changelanguage_layout = (RelativeLayout) rootView.findViewById(R.id.changelanguage_layout);

        if (strUserid.equals("")) {
            if (language.equalsIgnoreCase("En")) {
                customername.setText("" + getResources().getString(R.string.login));
            } else {
                customername.setText("" + getResources().getString(R.string.login_ar));
            }

        } else {
            customername.setText(userPrefs.getString("FirstName", "  ") + " " + userPrefs.getString("LastName", " "));
        }

        if (!userPrefs.getString("pic", "").equalsIgnoreCase("")) {
            Glide.with(ProfileFragment.this)
                    .load(USERS_IMAGE_URL + userPrefs.getString("pic", ""))
                    .into(profilepic);
        }

        offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }
                } else {
                    new GetHomeDataApi().execute();
                }
            }
        });

        if (Constants.DistrictName != null && !Constants.DistrictName.equals("")) {
            if (language.equalsIgnoreCase("En")) {
                location_text.setText(Constants.DistrictName + ", " + Constants.CityName);
            } else {
                location_text.setText(Constants.DistrictNameAr + ", " + Constants.CityNameAr);
            }
        } else {
            if (language.equalsIgnoreCase("En")) {
                location_text.setText(Constants.CityName);
            } else {
                location_text.setText(Constants.CityNameAr);
            }
        }

        if (userPrefs.getString("userId", "").equals("")) {

            settings.setVisibility(View.GONE);

        } else {

            settings.setVisibility(View.VISIBLE);

        }

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }


                } else {

                    Intent intent = new Intent(getActivity(), SettingActivity.class);
                    startActivity(intent);

                }

            }
        });

        favuorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }


                } else {

                    Intent intent = new Intent(getActivity(), FavoritesActivity.class);
                    startActivity(intent);

                }
            }
        });

        coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {

                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }

                } else {

                    Intent a = new Intent(getActivity(), CouponVoucherActivity.class);
                    startActivity(a);

                }
            }
        });

        chaticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }

                } else {
//                    Intent a = new Intent(getActivity(), CometChatActivity.class);
//                    startActivity(a);
                }
            }
        });
        changelanguage_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (userPrefs.getString("userId", "").equals("")) {
//                    if (language.equalsIgnoreCase("En")){
//                        Constants.showTwoButtonAlertDialog(getActivity());
//                    }else {
//                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
//                    }

//                } else {
                Intent a = new Intent(getActivity(), ChangeLanguage.class);
                startActivity(a);
//                }
            }
        });

        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showTwoButtonAlertDialog(getActivity());
                    } else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }


                } else {

                    int currentapiVersion = Build.VERSION.SDK_INT;
                    if (currentapiVersion >= Build.VERSION_CODES.M) {
                        if (!canAccessCamera()) {
                            requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                        }
                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        }
                    }
                    showtwoButtonsAlertDialog();

                }

            }
        });

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (canAccessCamera() && !canAccessStorage()) {
            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
        }
    }

    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog_camera;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView camera = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView gallery = (TextView) dialogView.findViewById(R.id.ngt_btn);
        TextView cancel = (TextView) dialogView.findViewById(R.id.cancel_btn);

        if (language.equals("En")) {
            title.setText("" + getResources().getString(R.string.app_name));
            gallery.setText("" + getResources().getString(R.string.popup_gallary));
            camera.setText("" + getResources().getString(R.string.popup_camera));
            desc.setText("" + getResources().getString(R.string.do_you_want_to_upload_from));
            cancel.setText("" + getResources().getString(R.string.cancel));

        } else {

            title.setText("" + getResources().getString(R.string.app_name));
            gallery.setText("" + getResources().getString(R.string.popup_gallary_ar));
            camera.setText("" + getResources().getString(R.string.popup_camera_ar));
            desc.setText("" + getResources().getString(R.string.do_you_want_to_upload_from_ar));
            cancel.setText("" + getResources().getString(R.string.cancelled_ar));

        }

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                customDialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
                customDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getActivity(), perm));
    }

    public void openGallery() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {

            if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                isCamera = false;
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "" + getResources().getString(R.string.select_picture)), PICK_IMAGE_FROM_GALLERY);
                customDialog.dismiss();
            }
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "" + getResources().getString(R.string.select_picture)), PICK_IMAGE_FROM_GALLERY);
            customDialog.dismiss();
        }
    }


    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera = true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContext().getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, CAMERA_REQUEST);
            }
        } else {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContext().getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, CAMERA_REQUEST);
        }
    }

    public String StoreByteImage() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
        pictureName = "PIC_" + strUserid + "_" + timeStamp + ".jpg";

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);

        image1Bytes = stream.toByteArray();
        image1Str = Base64.encodeToString(image1Bytes, Base64.DEFAULT);

        String extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString();
        File folder = new File(extStorageDirectory, "CheckClik");
        folder.mkdir();

        File sdImageMainDirectory = new File(folder, pictureName);

        FileOutputStream fileOutputStream = null;

        String nameFile = null;

        try {

            BitmapFactory.Options options = new BitmapFactory.Options();

            fileOutputStream = new FileOutputStream(sdImageMainDirectory);

            BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream);

            thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bos);

            bos.flush();

            bos.close();

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
        return sdImageMainDirectory.getAbsolutePath();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri uri = data.getData();

            try {
                thumbnail = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

            convertGalleryImages();
        } else if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
//            Uri uri = data.getData();
            convertCameraImages();
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private Bitmap codec(Bitmap src, Bitmap.CompressFormat format, int quality) {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);
    }

    private void convertGalleryImages() {
        isImageChanged = true;
        thumbnail = Bitmap.createScaledBitmap(thumbnail, 800, 800,
                true);
//        thumbnail = codec(thumbnail, Bitmap.CompressFormat.PNG, 100);

        Drawable drawable = new BitmapDrawable(getResources(), thumbnail);
        profilepic.setImageDrawable(drawable);
        imagePath = StoreByteImage();
        new updateProfileApi().execute();
    }

    private void convertCameraImages() {
        String path = getRealPathFromURI(imageUri);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inScaled = false;

        isImageChanged = true;

        thumbnail = BitmapFactory.decodeFile(path, bmOptions);
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail, 800,
                800, true);
        thumbnail = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        Drawable drawable = new BitmapDrawable(getResources(), thumbnail);
        profilepic.setImageDrawable(drawable);
        imagePath = StoreByteImage();

        new updateProfileApi().execute();
    }

    private class GetHomeDataApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
//            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HomePageResponse> call = apiService.getHomePageDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HomePageResponse>() {
                @Override
                public void onResponse(Call<HomePageResponse> call, Response<HomePageResponse> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        homePageResponse = response.body();
                    }

                    if (homePageResponse != null) {
                        Intent intent = new Intent(getContext(), OffersActivity.class);
                        intent.putExtra("subcategorylist", homePageResponse.getData().getSubcategorylist());
                        intent.putExtra("featuredstores", homePageResponse.getData().getFeaturedstores());
                        intent.putExtra("offers", homePageResponse.getData().getOffers());
                        intent.putExtra("offers2", homePageResponse.getData().getOffers2());
                        intent.putExtra("selected", 1);
                        getContext().startActivity(intent);
                        Log.d(TAG, "offerssize: " + homePageResponse.getData().getSubcategorylist().size());
                    }
                }

                @Override
                public void onFailure(Call<HomePageResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
//                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Latitude", Constants.Latitude);
            parentObj.put("Longitude", Constants.Longitude);
            parentObj.put("CityId", Constants.CityId);
            parentObj.put("DistrictId", Constants.DistrictId);
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class updateProfileApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateProfileJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<Signinresponce> call = apiService.updateProfile(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signinresponce>() {
                @Override
                public void onResponse(Call<Signinresponce> call, Response<Signinresponce> response) {
                    if (response.isSuccessful()) {
                        Signinresponce resetPasswordResponse = response.body();
                        Log.d("TAG", "onResponse: " + response.body().getStatus());
                        try {
                            if (resetPasswordResponse.getStatus()) {
                                userPrefEditor.putString("pic", pictureName);
                                userPrefEditor.commit();

                                Glide.with(ProfileFragment.this)
                                        .load(USERS_IMAGE_URL + userPrefs.getString("pic", ""))
                                        .into(profilepic);
                            } else {
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<Signinresponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.showLoadingDialog(getActivity());
                }
            });
            return null;
        }
    }

    private String prepareUpdateProfileJSON() {
        String appVersion = "";
        JSONObject parentObj = new JSONObject();
        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        try {
            parentObj.put("Id", strUserid);
            parentObj.put("FirstName", userPrefs.getString("FirstName", ""));
            parentObj.put("LastName", userPrefs.getString("LastName", ""));
            parentObj.put("MobileNo", userPrefs.getString("MobileNo", ""));
            parentObj.put("EmailId", userPrefs.getString("FirstName", ""));
            parentObj.put("Language", "En");
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("DeviceVersion", "andr v" + appVersion);
            parentObj.put("DeviceType", "Android");
            parentObj.put("ProfilePhoto", pictureName);
            parentObj.put("Address", "");
            parentObj.put("FlagId", 2);
            parentObj.put("PhotoBytes", image1Str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "preparechangeemail: " + parentObj);
        return parentObj.toString();
    }
}
