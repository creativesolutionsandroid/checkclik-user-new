package com.cs.checkclickuser.Fragements;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Activites.CitySearchAcitivty;
import com.cs.checkclickuser.Activites.NotificationsActivity;
import com.cs.checkclickuser.Activites.OffersActivity;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Activites.SearchActivity;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Adapter.HomeScreenBannersAdapter;
import com.cs.checkclickuser.Adapter.HomeScreenFeatureStoresAdapter;
import com.cs.checkclickuser.Adapter.HomeScreenOffersAdapter;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.Districts;
import com.cs.checkclickuser.Models.HomeCityResponse;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.LocationResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.Models.UrlDecryptResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BarCodeScanner;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.GPSTracker;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.github.demono.AutoScrollViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    View rootView;
    TextView locationText, offersViewAll, featuredViewAll;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    ImageView product,service,home,orders,profile,cart,scanner,notifications;
    ViewPager offerViewPager, featuredStoresPager;
    AutoScrollViewPager bannersViewPager;
    HomePageResponse homePageResponse;
    HomeScreenOffersAdapter mOfferAdapter;
    HomeScreenBannersAdapter mBannerAdapter;
    private HomeScreenFeatureStoresAdapter featureStoresAdapter;
    EditText searchedittex;
    AlertDialog customDialog;
    Double currentLatitude = 0.0, currentLongitude = 0.0;
    private int citySelected = 0, districtSelected = 0;

    String language;
    SharedPreferences LanguagePrefs;
    SharedPreferences userPrefs;


    private static final int CITY_REQUEST = 1001;
    private static final int LOCATION_REQUEST = 1;
    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    SharedPreferences.Editor userPrefsEditor;
    String branchIDfromIntent = "";
    String encryptedURL = "";
    String[] splitUrl;
    String urlType = "";
    private ImageView cartStatusIcon, notifictionStatusIcon;

    private ArrayList<HomePageResponse.Offers> productOffersList;
    private ArrayList<HomePageResponse.Offers2> servicesOffersList;
    Context context;
    String TAG = "TAG";
    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceDetails> serviceProductLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> serviceVariantsLists = new ArrayList<>();
    private String inputStr, serviceStr;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewCompat.requestApplyInsets(view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.home_fragment, container, false);
        } else {
            rootView = inflater.inflate(R.layout.home_fragment_ar1, container, false);
        }

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        int currentapiVersion1 = Build.VERSION.SDK_INT;
        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        locationText = (TextView) rootView.findViewById(R.id.locationtext);
        offerViewPager = (ViewPager) rootView.findViewById(R.id.home_offers_list);
        featuredStoresPager = (ViewPager) rootView.findViewById(R.id.home_featured_list);
        bannersViewPager = (AutoScrollViewPager) rootView.findViewById(R.id.home_banner_list);
        cart =(ImageView) rootView.findViewById(R.id.home_cart);
        searchedittex=(EditText)rootView.findViewById(R.id.search);
        scanner=(ImageView) rootView.findViewById(R.id.cameraicon);
        notifications = (ImageView) rootView.findViewById(R.id.home_noti);

        notifictionStatusIcon = (ImageView) rootView.findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) rootView.findViewById(R.id.cart_status);

        offersViewAll = (TextView) rootView.findViewById(R.id.offers_viewall);
        featuredViewAll = (TextView) rootView.findViewById(R.id.featured_viewall);

        Bundle bundle = this.getArguments();
        try {
            branchIDfromIntent = bundle.getString("id", "");
            encryptedURL = bundle.getString("url", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (encryptedURL != null && encryptedURL.length() > 2) {
            splitUrl = encryptedURL.split("/");
            Log.d(TAG, "type: "+splitUrl[(splitUrl.length - 2)]);
            urlType = splitUrl[(splitUrl.length - 2)];
        }

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    if (language.equals("En")){
                        Constants.showTwoButtonAlertDialog(getActivity());
                    }else {
                        Constants.showTwoButtonAlertDialog_arabic(getActivity());
                    }

                } else {
                    Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(getActivity());
                } else {
                    Intent intent = new Intent(getActivity(), CartActivity.class);
                    startActivity(intent);
                }
            }
        });

        searchedittex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), SearchActivity.class);
                intent.putExtra("search_text", "");
                intent.putExtra("branch_id", 0);
                startActivity(intent);
            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(getActivity());
                    }
                } else {
                    BarCodeScanner.showScannerDialog(getActivity());
                }
            }
        });

//        if(Constants.Latitude == 0 && Constants.Longitude == 0) {
//            int currentapiVersion = Build.VERSION.SDK_INT;
//            if (currentapiVersion >= Build.VERSION_CODES.M) {
//                if (!canAccessLocation()) {
//                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
//                } else {
//                    try {
//                        gps = new GPSTracker(getActivity());
//                        Constants.showLoadingDialog(getActivity());
//                        getGPSCoordinates();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else {
//                try {
//                    gps = new GPSTracker(getActivity());
//                    Constants.showLoadingDialog(getActivity());
//                    getGPSCoordinates();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        else {
            if(Constants.Latitude != 0 && Constants.Longitude != 0) {
            if (CacheData.homePageResponse != null) {
                setDataFromCache();

                if (!branchIDfromIntent.equals("")) {
                    new GetBranchDetailsApi().execute();
                }

                if (!encryptedURL.equals("")) {
                    new GetDecryptedUrlApi().execute();
                }

            }
            else {
                new GetHomeDataApi().execute();
            }

            if (Constants.DistrictName != null && !Constants.DistrictName.equals("")) {
                if (language.equalsIgnoreCase("En")) {
                    locationText.setText(Constants.DistrictName + ", " + Constants.CityName);
                }
                else {
                    locationText.setText(Constants.DistrictNameAr + ", " + Constants.CityNameAr);
                }
            }
            else {
                if (language.equalsIgnoreCase("En")) {
                    locationText.setText(Constants.CityName);
                }
                else {
                    locationText.setText(Constants.CityNameAr);
                }
            }
        }

        locationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                new GetCityListApi().execute();
                Intent intent = new Intent(getActivity(), CitySearchAcitivty.class);
                startActivityForResult(intent, CITY_REQUEST);
            }
        });

        offersViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(homePageResponse != null) {
                    Intent intent = new Intent(getContext(), OffersActivity.class);
                    intent.putExtra("subcategorylist", homePageResponse.getData().getSubcategorylist());
                    intent.putExtra("featuredstores", homePageResponse.getData().getFeaturedstores());
                    intent.putExtra("offers", homePageResponse.getData().getOffers());
                    intent.putExtra("offers2", homePageResponse.getData().getOffers2());
                    intent.putExtra("selected", 1);
                    getContext().startActivity(intent);
                }
            }
        });

        featuredViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(homePageResponse != null) {
                    Intent intent = new Intent(getContext(), OffersActivity.class);
                    intent.putExtra("subcategorylist", homePageResponse.getData().getSubcategorylist());
                    intent.putExtra("featuredstores", homePageResponse.getData().getFeaturedstores());
                    intent.putExtra("offers", homePageResponse.getData().getOffers());
                    intent.putExtra("offers2", homePageResponse.getData().getOffers2());
                    intent.putExtra("selected", 2);
                    getContext().startActivity(intent);
                }
            }
        });

        return rootView;
    }

    private class GetHomeDataApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HomePageResponse> call = apiService.getHomePageDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HomePageResponse>() {
                @Override
                public void onResponse(Call<HomePageResponse> call, Response<HomePageResponse> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        homePageResponse = response.body();
                    }
                    if (homePageResponse != null) {
                        CacheData.homePageResponse = homePageResponse;
                        if (homePageResponse.getData().getOffers().size() > 0 || homePageResponse.getData().getOffers2().size() > 0) {
                            mOfferAdapter = new HomeScreenOffersAdapter(getContext(), homePageResponse.getData().getOffers(),
                                    homePageResponse.getData().getOffers2());
                            offerViewPager.setAdapter(mOfferAdapter);
                        }else {
                            offerViewPager.setAdapter(null);
                        }featureStoresAdapter = new HomeScreenFeatureStoresAdapter(getContext(), homePageResponse.getData().getFeaturedstores());
                        featuredStoresPager.setAdapter(featureStoresAdapter);

                        mBannerAdapter = new HomeScreenBannersAdapter(getContext(), homePageResponse.getData().getHomebanners(), getActivity());
                        bannersViewPager.setAdapter(mBannerAdapter);
                        bannersViewPager.setCycle(true);
                        bannersViewPager.setSlideDuration(7000);
                        bannersViewPager.startAutoScroll();

                        Constants.cartCount = homePageResponse.getData().getUserBlockedStatus().get(0).getCartItemCount();
                        Constants.notificationCount = homePageResponse.getData().getUserBlockedStatus().get(0).getNotificationCount();

                        if (Constants.cartCount != 0) {
                            cartStatusIcon.setVisibility(View.VISIBLE);
                        }
                        if (Constants.notificationCount != 0) {
                            notifictionStatusIcon.setVisibility(View.VISIBLE);
                        }

                        if (!branchIDfromIntent.equals("")) {
                            new GetBranchDetailsApi().execute();
                        }

                        if (!encryptedURL.equals("")) {
                            new GetDecryptedUrlApi().execute();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<HomePageResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Latitude",  Constants.Latitude);
            parentObj.put("Longitude", Constants.Longitude);
            parentObj.put("CityId", Constants.CityId);
            parentObj.put("DistrictId", Constants.DistrictId);
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetLocationApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Constants.showLoadingDialog(getActivity());
            inputStr = prepareGetLocationJSON();
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<LocationResponse> call = apiService.getLocationDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                    Log.i("TAG", "product servies responce " + response);
                    Constants.closeLoadingDialog();

                    if (response.isSuccessful()) {
                        Constants.CityName = response.body().getData().getSelectedcity().get(0).getNameen();
                        Constants.CityNameAr = response.body().getData().getSelectedcity().get(0).getNamear();
                        Constants.CityId = response.body().getData().getSelectedcity().get(0).getId();
                        Constants.DistrictName = response.body().getData().getSelecteddistrict().get(0).getNameen();
                        Constants.DistrictNameAr = response.body().getData().getSelecteddistrict().get(0).getNamear();
                        Constants.DistrictId = response.body().getData().getSelecteddistrict().get(0).getId();
                        Constants.Latitude = currentLatitude;
                        Constants.Longitude = currentLongitude;
                        new GetHomeDataApi().execute();

                        if (Constants.DistrictName != null && !Constants.DistrictName.equals("")) {
                            if (language.equalsIgnoreCase("En")) {
                                locationText.setText(Constants.DistrictName + ", " + Constants.CityName);
                            }
                            else {
                                locationText.setText(Constants.DistrictNameAr + ", " + Constants.CityNameAr);
                            }
                        }
                        else {
                            if (language.equalsIgnoreCase("En")) {
                                locationText.setText(Constants.CityName);
                            }
                            else {
                                locationText.setText(Constants.CityNameAr);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<LocationResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return "";
        }
    }
    private String prepareGetLocationJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private String prepareCityListJSON(){
        JSONObject parentObj = new JSONObject();
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    public void showLoadingDialog(AppCompatActivity context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = context.getLayoutInflater();
        int layout = R.layout.loading_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void closeLoadingDialog(){
        Log.d("TAG", "closeLoadingDialog: ");
        if(customDialog != null) {
            Log.d("TAG", "closeLoadingDialog: "+customDialog);
            customDialog.dismiss();
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(getContext(), perm));
    }

    public void getGPSCoordinates() throws IOException {

        if(gps != null){
            if (gps.canGetLocation()) {
                    currentLatitude = gps.getLatitude();
                    currentLongitude = gps.getLongitude();
                    Constants.Latitude = currentLatitude;
                    Constants.Longitude = currentLongitude;
//                currentLatitude = 24.48;
//                currentLongitude = 48.72;
                new GetLocationApi().execute();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
                Constants.closeLoadingDialog();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    gps = new GPSTracker(getActivity());
                    try {
                        Constants.showLoadingDialog(getActivity());
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.location_permission_denied), Toast.LENGTH_LONG).show();
                }
                break;

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(getActivity());
                }
                else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private class GetCityListApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareCityListJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HomeCityResponse> call = apiService.getCityList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HomeCityResponse>() {
                @Override
                public void onResponse(Call<HomeCityResponse> call, Response<HomeCityResponse> response) {
                    Constants.closeLoadingDialog();
                    if (response.isSuccessful()) {
                        citySelected = 0;
                        districtSelected = 0;
                        filterCities(response.body().getData());
                    }
                }

                @Override
                public void onFailure(Call<HomeCityResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return "";
        }
    }

    private void filterCities(HomeCityResponse.Data data){
        ArrayList<String> cityArray = new ArrayList<>();
        ArrayList<Districts> districtArray = new ArrayList<>();

//        cityArray.add("-- Select City --");
        for (int i = 0; i < data.getCitylist().size(); i++) {
            cityArray.add(data.getCitylist().get(i).getNameen());
            if (data.getCitylist().get(i).getNameen().equals(Constants.CityName)){
                citySelected = i;
            }
        }
        showCitySelectionDialog(data, cityArray, districtArray);
    }

    private void showCitySelectionDialog(final HomeCityResponse.Data data, final ArrayList<String> cityArray, final ArrayList<Districts> districtArray) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_city_selection;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        final Spinner citySpinner = (Spinner) dialogView.findViewById(R.id.city_spinner);
        final Spinner districtSpinner = (Spinner) dialogView.findViewById(R.id.district_spinner);
        final TextView city = (TextView) dialogView.findViewById(R.id.city);
        final TextView district = (TextView) dialogView.findViewById(R.id.districttxt);
        final TextView close = (TextView) dialogView.findViewById(R.id.popup_close);
        final Button doneBtn = (Button) dialogView.findViewById(R.id.done_btn);

        final ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getContext(), R.layout.list_spinner, cityArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }
        };

        final ArrayAdapter<Districts> districtAdapter = new ArrayAdapter<Districts>(getContext(), R.layout.list_spinner, districtArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
//                ((TextView) v).setText(districtArray.get(position).getNameEn());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        citySpinner.setAdapter(cityAdapter);
        districtSpinner.setAdapter(districtAdapter);
        citySpinner.setSelection(citySelected);

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                city.setText(cityArray.get(i));
                citySelected = i;
                districtSelected = 0;

                districtArray.clear();
                Districts districts = new Districts();
                districts.setId(0);
//                districts.setNameEn("All");
//                districts.setNameAr("All");
                districts.setLatitude("0");
                districts.setLongitude("0");
                districtArray.add(districts);
//                if(citySelected != 0) {
                int districtPosition = 0;
                    for (int j = 0; j < data.getDistrictlist().size(); j++) {
                        if (data.getCitylist().get((citySelected)).getId() == data.getDistrictlist().get(j).getCityid()) {
                            Districts districts1 = new Districts();
                            districts1.setId(data.getDistrictlist().get(j).getId());
//                            districts1.setNameEn(data.getDistrictlist().get(j).getNameen());
//                            districts1.setNameAr(data.getDistrictlist().get(j).getNamear());
                            districts1.setLatitude(data.getDistrictlist().get(j).getLatitude());
                            districts1.setLongitude(data.getDistrictlist().get(j).getLongitude());
                            districtArray.add(districts1);
                            districtPosition = districtPosition + 1;

                            if (data.getDistrictlist().get(j).getNameen().equals(Constants.DistrictName)){
                                districtSelected = districtPosition;
                            }
                        }
                    }
//                }

                districtSpinner.setAdapter(districtAdapter);
                districtSpinner.setSelection(districtSelected);
                districtAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        districtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                district.setText(districtArray.get(i).getNameEn());
                districtSelected = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.CityName = data.getCitylist().get(citySelected).getNameen();
                Constants.CityNameAr = data.getCitylist().get(citySelected).getNamear();
                Constants.CityId = data.getCitylist().get(citySelected).getId();
//                Constants.DistrictId = data.getCitylist().get(districtSelected).getId();
                if (districtSelected != 0){
                    Constants.DistrictName = districtArray.get(districtSelected).getNameen();
                    Constants.DistrictNameAr = districtArray.get(districtSelected).getNamear();
                    Constants.DistrictId = data.getCitylist().get(districtSelected).getId();
                    if (language.equalsIgnoreCase("En")) {
                        locationText.setText(Constants.DistrictName + ", " + Constants.CityName);
                    }
                    else {
                        locationText.setText(Constants.DistrictNameAr + ", " + Constants.CityNameAr);
                    }
                    Constants.Latitude = Double.parseDouble(districtArray.get(districtSelected).getLatitude());
                    Constants.Longitude = Double.parseDouble(districtArray.get(districtSelected).getLongitude());
                }
                else {
                    Constants.DistrictName = "";
                    Constants.DistrictId = 0;
                    if (language.equalsIgnoreCase("En")) {
                        locationText.setText(Constants.CityName);
                    }
                    else {
                        locationText.setText(Constants.CityNameAr);
                    }
                    Constants.Latitude = Double.parseDouble(data.getCitylist().get(citySelected).getLatitude());
                    Constants.Longitude = Double.parseDouble(data.getCitylist().get(citySelected).getLongitude());
                }

                userPrefsEditor.putString("city", Constants.CityName);
                userPrefsEditor.putString("city_ar", Constants.CityNameAr);
                userPrefsEditor.putInt("cityId", Constants.CityId);
                userPrefsEditor.putString("district", Constants.DistrictName);
                userPrefsEditor.putString("district_ar", Constants.DistrictNameAr);
                userPrefsEditor.putInt("districtId", Constants.DistrictId);
                userPrefsEditor.putString("lat", Double.toString(Constants.Latitude));
                userPrefsEditor.putString("lng", Double.toString(Constants.Longitude));
                userPrefsEditor.commit();

                CacheData.homePageResponse = null;
                customDialog.dismiss();
                new GetHomeDataApi().execute();
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    private void setDataFromCache(){
        homePageResponse = CacheData.homePageResponse;
        if(homePageResponse.getData().getOffers().size() > 0 || homePageResponse.getData().getOffers2().size() > 0) {
            mOfferAdapter = new HomeScreenOffersAdapter(getContext(), homePageResponse.getData().getOffers(),
                    homePageResponse.getData().getOffers2());
            offerViewPager.setAdapter(mOfferAdapter);
        }
        else {
            offerViewPager.setAdapter(null);
        }

        featureStoresAdapter = new HomeScreenFeatureStoresAdapter(getContext(), homePageResponse.getData().getFeaturedstores());
        featuredStoresPager.setAdapter(featureStoresAdapter);

        mBannerAdapter = new HomeScreenBannersAdapter(getContext(), homePageResponse.getData().getHomebanners(), getActivity());
        bannersViewPager.setAdapter(mBannerAdapter);
        bannersViewPager.setCycle(true);
        bannersViewPager.setSlideDuration(5000);
        bannersViewPager.startAutoScroll();
    }

    private class GetBranchDetailsApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetBranchDetailsJSON();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchDetails> call = apiService.getBranchDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchDetails>() {
                @Override
                public void onResponse(Call<BranchDetails> call, Response<BranchDetails> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        BranchDetails branchResponse = response.body();
                        if (branchResponse.getData().getBranch() != null &&
                                branchResponse.getData().getBranch().size() > 0) {
                            Intent intent = new Intent(getContext(), ProductStoresActivityStep1.class);
                            intent.putExtra("stores", branchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", branchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "vendor_app_intent");
                            getContext().startActivity(intent);
                        }
                        else {
                            Toast.makeText(getContext(), "Store details not available", Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchDetails> call, Throwable t) {
                    Log.e("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetBranchDetailsJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId",  branchIDfromIntent);
            parentObj.put("Type",  "b");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetDecryptedUrlApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(getActivity());
            inputStr = preparedecryptURLJSON();
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UrlDecryptResponse> call = apiService.UrlDecrypt(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UrlDecryptResponse>() {
                @Override
                public void onResponse(Call<UrlDecryptResponse> call, Response<UrlDecryptResponse> response) {

                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: "+response.toString());
                        if (response.body().getStatus() && response.body().getMessage().equals("Success")) {
                            Constants.closeLoadingDialog();
                            UrlDecryptResponse.Data data = response.body().getData();
                            Log.d(TAG, "data.getId(): "+data.getId());
                            if(urlType.equals(IdConstants.URL_SHARE_TYPE_PRODUCT)) {
                                inputStr = prepareProductVariantJSON(data.getId());
                                new GetProductVariantsApi().execute(""+data.getId(), ""+data.getBranchId(), ""+data.getStoreId(), inputStr);
                            }
                            else if(urlType.equals(IdConstants.URL_SHARE_TYPE_SERVICE)) {
                                serviceStr = prepareGetStoresJSON(data.getId(), data.getBranchSubCategoryId(), data.getBranchId());
                                new GetServiceVariantsApi().execute(""+data.getId(), ""+data.getBranchId(), ""+data.getStoreId(), serviceStr);
                            }
                            else if(urlType.equals(IdConstants.URL_SHARE_TYPE_BRANCH)) {
                                new GetBranchDetailsApi().execute();
                            }
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), getActivity());
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<UrlDecryptResponse> call, Throwable t) {
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return "";
        }
    }

    private String preparedecryptURLJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("EncryId",  splitUrl[(splitUrl.length -  1)]);
            if (urlType.equals(IdConstants.URL_SHARE_TYPE_BRANCH)) {
                parentObj.put("Type",  "b");
            }
         } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetProductVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), strings[3]));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d("TAG", "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductList();
                            variantsLists = response.body().getData().getVariantsList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(getContext(), ProductVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("skuid", strings[0]);
                            intent.putExtra("branchId", strings[1]);
                            intent.putExtra("storeid", strings[2]);
                            getContext().startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getContext().getResources().getString(R.string.app_name),
                                    getContext().getResources().getString(R.string.ok), getActivity());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareProductVariantJSON(int ProductInventoryId){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  ProductInventoryId);
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetServiceVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), serviceStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d("TAG", "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            serviceProductLists = response.body().getData().getServiceDetails();
                            serviceVariantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(getContext(), ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", serviceProductLists);
                            intent.putExtra("variantList", serviceVariantsLists);
                            intent.putExtra("storeId", strings[2]);
                            intent.putExtra("skuid",  strings[0]);
                            intent.putExtra("branchId", strings[1]);
                            getContext().startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getContext().getResources().getString(R.string.app_name),
                                    getContext().getResources().getString(R.string.ok), getActivity());
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int ServiceId, int SubCategoryId, int BranchId){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  ServiceId);
            parentObj.put("SubCategoryId",  SubCategoryId);
            parentObj.put("BranchId",  BranchId);
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(homePageResponse!= null && (homePageResponse.getData().getOffers().size() > 0 || homePageResponse.getData().getOffers2().size() > 0)) {
            mOfferAdapter = new HomeScreenOffersAdapter(getContext(), homePageResponse.getData().getOffers(),
                    homePageResponse.getData().getOffers2());
            offerViewPager.setAdapter(mOfferAdapter);
        }
        else {
            offerViewPager.setAdapter(null);
        }

        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (Constants.notificationCount != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }

        if(Constants.Latitude == 0 && Constants.Longitude == 0) {
            int currentapiVersion = Build.VERSION.SDK_INT;
            if (currentapiVersion >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                } else {
                    try {
                        gps = new GPSTracker(getActivity());
                        Constants.showLoadingDialog(getActivity());
                        getGPSCoordinates();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    gps = new GPSTracker(getActivity());
                    Constants.showLoadingDialog(getActivity());
                    getGPSCoordinates();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CITY_REQUEST && resultCode == Activity.RESULT_OK) {
            if (Constants.DistrictName != null && !Constants.DistrictName.equals("")) {
                if (language.equalsIgnoreCase("En")) {
                    locationText.setText(Constants.DistrictName + ", " + Constants.CityName);
                }
                else {
                    locationText.setText(Constants.DistrictNameAr + ", " + Constants.CityNameAr);
                }
            }
            else {
                if (language.equalsIgnoreCase("En")) {
                    locationText.setText(Constants.CityName);
                }
                else {
                    locationText.setText(Constants.CityNameAr);
                }
            }
            CacheData.homePageResponse = null;
//            customDialog.dismiss();
            new GetHomeDataApi().execute();
        }
    }
}
