package com.cs.checkclickuser.Fragements;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Activites.MainActivity;
import com.cs.checkclickuser.Adapter.ServiceCategoryAdapter;
import com.cs.checkclickuser.Models.ProductCategoryResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceCategoryFragment extends Fragment {

    private RecyclerView catlist;
    View rootView;
    public static final String TAG = "TAG";
    ServiceCategoryAdapter mCatAdapter;
    SharedPreferences userPrefs;
    TextView close, title;
    private List<ProductCategoryResponce.MainCategory> catArrayList = new ArrayList<>();

    private View noDataFoundLayout;
    private ImageView nothingFoundImage;
    private Button tryAgainButton;


    SharedPreferences LanguagePrefs;
    String language;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewCompat.requestApplyInsets(view);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {

            rootView = inflater.inflate(R.layout.category_fragment, container, false);

        } else {
            rootView = inflater.inflate(R.layout.category_fragment_ar, container, false);

        }




        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        catlist = (RecyclerView)rootView.findViewById(R.id.catlist);
        close = (TextView) rootView.findViewById(R.id.close);
        title = (TextView) rootView.findViewById(R.id.title);

        if (language.equalsIgnoreCase("En")){
            title.setText(getResources().getString(R.string.services));
        }else {
            title.setText(getResources().getString(R.string.services_ar));
        }


        tryAgainButton =(Button) rootView.findViewById(R.id.try_again);
        nothingFoundImage = (ImageView) rootView.findViewById(R.id.nothing_found);
        noDataFoundLayout = (View) rootView.findViewById(R.id.no_data_found_layout);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = ((AppCompatActivity)getContext()).getSupportFragmentManager();
                Fragment categoryFragment = new HomeFragment();
                Bundle args = new Bundle();
                args.putString("id", "");
                categoryFragment.setArguments(args);
                fragmentManager.beginTransaction().replace(R.id.fragment_layout, categoryFragment).commit();
                MainActivity.currentSelectedTab = 3;
                MainActivity.navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
            }
        });

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new categoryApi().execute();
            }
        });

        new categoryApi().execute();
        return rootView;
    }


    private class categoryApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductCategoryResponce> call = apiService.catlist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductCategoryResponce>() {
                @Override
                public void onResponse(Call<ProductCategoryResponce> call, Response<ProductCategoryResponce> response) {
                    if (response.isSuccessful()) {
                        ProductCategoryResponce ProductCategoryResponce = response.body();
                        try {
                            if (ProductCategoryResponce.getStatus()) {
                                if (ProductCategoryResponce.getData().getMainCategory().size() > 0) {
                                    for (int i = 0; i < ProductCategoryResponce.getData().getMainCategory().size(); i++) {
                                        if (ProductCategoryResponce.getData().getMainCategory().get(i).getCType() == 2) {
                                            ProductCategoryResponce.MainCategory mainCategory = new ProductCategoryResponce.MainCategory();
                                            mainCategory.setId(ProductCategoryResponce.getData().getMainCategory().get(i).getId());
                                            mainCategory.setNameAr(ProductCategoryResponce.getData().getMainCategory().get(i).getNameAr());
                                            mainCategory.setNameEn(ProductCategoryResponce.getData().getMainCategory().get(i).getNameEn());
                                            mainCategory.setIconClass(ProductCategoryResponce.getData().getMainCategory().get(i).getIconClass());
                                            mainCategory.setCType(ProductCategoryResponce.getData().getMainCategory().get(i).getCType());
                                            mainCategory.setUniCode(ProductCategoryResponce.getData().getMainCategory().get(i).getUniCode());
                                            mainCategory.setColorCode(ProductCategoryResponce.getData().getMainCategory().get(i).getColorCode());
                                            catArrayList.add(mainCategory);
                                        }
                                        noDataFoundLayout.setVisibility(View.GONE);
                                        nothingFoundImage.setVisibility(View.GONE);
                                        mCatAdapter = new ServiceCategoryAdapter(getContext(), catArrayList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                        catlist.setLayoutManager(new GridLayoutManager(getContext(), 1));
                                        catlist.setAdapter(mCatAdapter);
                                    }
                                }else {
                                    noDataFoundLayout.setVisibility(View.VISIBLE);
                                }
                            } else {
                                noDataFoundLayout.setVisibility(View.VISIBLE);
                                String failureResponse = ProductCategoryResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            noDataFoundLayout.setVisibility(View.VISIBLE);
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<ProductCategoryResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        nothingFoundImage.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        noDataFoundLayout.setVisibility(View.VISIBLE);
                        Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {

            parentObj.put("Latitude", Constants.Latitude);
            parentObj.put("Longitude", Constants.Longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }


}
