package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OffersSubCategory {

    public String subcategoryimage;
    public String subcategorynamear;
    public String subcategorynameen;
    public int branchsubcategoryid;

    public String getSubcategoryimage() {
        return subcategoryimage;
    }

    public void setSubcategoryimage(String subcategoryimage) {
        this.subcategoryimage = subcategoryimage;
    }

    public String getSubcategorynamear() {
        return subcategorynamear;
    }

    public void setSubcategorynamear(String subcategorynamear) {
        this.subcategorynamear = subcategorynamear;
    }

    public String getSubcategorynameen() {
        return subcategorynameen;
    }

    public void setSubcategorynameen(String subcategorynameen) {
        this.subcategorynameen = subcategorynameen;
    }

    public int getBranchsubcategoryid() {
        return branchsubcategoryid;
    }

    public void setBranchsubcategoryid(int branchsubcategoryid) {
        this.branchsubcategoryid = branchsubcategoryid;
    }
}
