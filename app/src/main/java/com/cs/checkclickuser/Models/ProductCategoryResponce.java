package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductCategoryResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("FilterSubCategoryCat")
        private List<FilterSubCategoryCat> FilterSubCategoryCat;
        @Expose
        @SerializedName("FilterMainCategory")
        private List<FilterMainCategory> FilterMainCategory;
        @Expose
        @SerializedName("Stores")
        private List<String> Stores;
        @Expose
        @SerializedName("MainCategory")
        private List<MainCategory> MainCategory;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;

        public List<FilterSubCategoryCat> getFilterSubCategoryCat() {
            return FilterSubCategoryCat;
        }

        public void setFilterSubCategoryCat(List<FilterSubCategoryCat> FilterSubCategoryCat) {
            this.FilterSubCategoryCat = FilterSubCategoryCat;
        }

        public List<FilterMainCategory> getFilterMainCategory() {
            return FilterMainCategory;
        }

        public void setFilterMainCategory(List<FilterMainCategory> FilterMainCategory) {
            this.FilterMainCategory = FilterMainCategory;
        }

        public List<String> getStores() {
            return Stores;
        }

        public void setStores(List<String> Stores) {
            this.Stores = Stores;
        }

        public List<MainCategory> getMainCategory() {
            return MainCategory;
        }

        public void setMainCategory(List<MainCategory> MainCategory) {
            this.MainCategory = MainCategory;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }
    }

    public static class FilterSubCategoryCat {
        @Expose
        @SerializedName("Value")
        private String Value;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("TypeName")
        private String TypeName;
        @Expose
        @SerializedName("Type")
        private String Type;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("TypeId")
        private int TypeId;

        public String getValue() {
            return Value;
        }

        public void setValue(String Value) {
            this.Value = Value;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getTypeName() {
            return TypeName;
        }

        public void setTypeName(String TypeName) {
            this.TypeName = TypeName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getTypeId() {
            return TypeId;
        }

        public void setTypeId(int TypeId) {
            this.TypeId = TypeId;
        }
    }

    public static class FilterMainCategory {
        @Expose
        @SerializedName("TypeName")
        private String TypeName;
        @Expose
        @SerializedName("Type")
        private String Type;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("TypeId")
        private int TypeId;

        public String getTypeName() {
            return TypeName;
        }

        public void setTypeName(String TypeName) {
            this.TypeName = TypeName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getTypeId() {
            return TypeId;
        }

        public void setTypeId(int TypeId) {
            this.TypeId = TypeId;
        }
    }

    public static class MainCategory {
        @Expose
        @SerializedName("CType")
        private int CType;
        @Expose
        @SerializedName("UniCode")
        private String UniCode;
        @Expose
        @SerializedName("IconClass")
        private String IconClass;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("ColorCode")
        private String ColorCode;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getColorCode() {
            return ColorCode;
        }

        public void setColorCode(String colorCode) {
            ColorCode = colorCode;
        }

        public int getCType() {
            return CType;
        }

        public void setCType(int CType) {
            this.CType = CType;
        }

        public String getUniCode() {
            return UniCode;
        }

        public void setUniCode(String UniCode) {
            this.UniCode = UniCode;
        }

        public String getIconClass() {
            return IconClass;
        }

        public void setIconClass(String IconClass) {
            this.IconClass = IconClass;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
