package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrlEncryptResponse {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("StoreName")
        private String StoreName;
        @Expose
        @SerializedName("StoreType")
        private String StoreType;
        @Expose
        @SerializedName("EncryId")
        private String EncryId;

        public String getStoreName() {
            return StoreName;
        }

        public void setStoreName(String StoreName) {
            this.StoreName = StoreName;
        }

        public String getStoreType() {
            return StoreType;
        }

        public void setStoreType(String StoreType) {
            this.StoreType = StoreType;
        }

        public String getEncryId() {
            return EncryId;
        }

        public void setEncryId(String EncryId) {
            this.EncryId = EncryId;
        }
    }
}
