package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateCartResponse {

//    @Expose
//    @SerializedName("Data")
//    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

//    public Data getData() {
//        return Data;
//    }

//    public void setData(Data Data) {
//        this.Data = Data;
//    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
//        @Expose
//        @SerializedName("CartList")
//        private List<CartList> CartList;
//
//        public List<CartList> getCartList() {
//            return CartList;
//        }
//
//        public void setCartList(List<CartList> CartList) {
//            this.CartList = CartList;
//        }
    }

    public static class CartList {
        @Expose
        @SerializedName("AvailableQuantity")
        private int AvailableQuantity;
        @Expose
        @SerializedName("VatAmountPerItem")
        private double VatAmountPerItem;
        @Expose
        @SerializedName("TotalVatAmount")
        private double TotalVatAmount;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("TotalItemPrice")
        private double TotalItemPrice;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("IsActive")
        private String IsActive;
        @Expose
        @SerializedName("NotReturnable")
        private String NotReturnable;
        @Expose
        @SerializedName("AllowedQty")
        private int AllowedQty;
        @Expose
        @SerializedName("MaxCartQty")
        private int MaxCartQty;
        @Expose
        @SerializedName("MinCartQty")
        private int MinCartQty;
        @Expose
        @SerializedName("ProductAvailabilityRange")
        private int ProductAvailabilityRange;
        @Expose
        @SerializedName("LowStockActivity")
        private int LowStockActivity;
        @Expose
        @SerializedName("DisplayStockQuantity")
        private String DisplayStockQuantity;
        @Expose
        @SerializedName("ReservedQuantity")
        private int ReservedQuantity;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("CartQuantity")
        private int CartQuantity;
        @Expose
        @SerializedName("Date")
        private String Date;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CartId")
        private int CartId;

        public int getAvailableQuantity() {
            return AvailableQuantity;
        }

        public void setAvailableQuantity(int AvailableQuantity) {
            this.AvailableQuantity = AvailableQuantity;
        }

        public double getVatAmountPerItem() {
            return VatAmountPerItem;
        }

        public void setVatAmountPerItem(double VatAmountPerItem) {
            this.VatAmountPerItem = VatAmountPerItem;
        }

        public double getTotalVatAmount() {
            return TotalVatAmount;
        }

        public void setTotalVatAmount(double TotalVatAmount) {
            this.TotalVatAmount = TotalVatAmount;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public double getTotalItemPrice() {
            return TotalItemPrice;
        }

        public void setTotalItemPrice(double TotalItemPrice) {
            this.TotalItemPrice = TotalItemPrice;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getIsActive() {
            return IsActive;
        }

        public void setIsActive(String IsActive) {
            this.IsActive = IsActive;
        }

        public String getNotReturnable() {
            return NotReturnable;
        }

        public void setNotReturnable(String NotReturnable) {
            this.NotReturnable = NotReturnable;
        }

        public int getAllowedQty() {
            return AllowedQty;
        }

        public void setAllowedQty(int AllowedQty) {
            this.AllowedQty = AllowedQty;
        }

        public int getMaxCartQty() {
            return MaxCartQty;
        }

        public void setMaxCartQty(int MaxCartQty) {
            this.MaxCartQty = MaxCartQty;
        }

        public int getMinCartQty() {
            return MinCartQty;
        }

        public void setMinCartQty(int MinCartQty) {
            this.MinCartQty = MinCartQty;
        }

        public int getProductAvailabilityRange() {
            return ProductAvailabilityRange;
        }

        public void setProductAvailabilityRange(int ProductAvailabilityRange) {
            this.ProductAvailabilityRange = ProductAvailabilityRange;
        }

        public int getLowStockActivity() {
            return LowStockActivity;
        }

        public void setLowStockActivity(int LowStockActivity) {
            this.LowStockActivity = LowStockActivity;
        }

        public String getDisplayStockQuantity() {
            return DisplayStockQuantity;
        }

        public void setDisplayStockQuantity(String DisplayStockQuantity) {
            this.DisplayStockQuantity = DisplayStockQuantity;
        }

        public int getReservedQuantity() {
            return ReservedQuantity;
        }

        public void setReservedQuantity(int ReservedQuantity) {
            this.ReservedQuantity = ReservedQuantity;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public int getCartQuantity() {
            return CartQuantity;
        }

        public void setCartQuantity(int CartQuantity) {
            this.CartQuantity = CartQuantity;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }
    }
}
