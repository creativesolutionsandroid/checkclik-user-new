package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocationResponse {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("SelectedDistrict")
        private List<SelectedDistrict> selecteddistrict;
        @Expose
        @SerializedName("SelectedCity")
        private List<SelectedCity> selectedcity;

        public List<SelectedDistrict> getSelecteddistrict() {
            return selecteddistrict;
        }

        public void setSelecteddistrict(List<SelectedDistrict> selecteddistrict) {
            this.selecteddistrict = selecteddistrict;
        }

        public List<SelectedCity> getSelectedcity() {
            return selectedcity;
        }

        public void setSelectedcity(List<SelectedCity> selectedcity) {
            this.selectedcity = selectedcity;
        }
    }

    public static class SelectedDistrict {
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("CityId")
        private int cityid;
        @Expose
        @SerializedName("Id")
        private int id;

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getCityid() {
            return cityid;
        }

        public void setCityid(int cityid) {
            this.cityid = cityid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class SelectedCity {
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
