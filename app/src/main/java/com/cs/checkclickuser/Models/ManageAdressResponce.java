package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public  class ManageAdressResponce implements Serializable {
    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable
    {
        @Expose
        @SerializedName("Status")
        private boolean Status;
        @Expose
        @SerializedName("AddressType")
        private int AddressType;
        @Expose
        @SerializedName("PhoneNumber")
        private String PhoneNumber;
        @Expose
        @SerializedName("Zipcode")
        private String Zipcode;
        @Expose
        @SerializedName("CityId")
        private int CityId;
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("CityNameAr")
        private String CityNameAr;
        @Expose
        @SerializedName("CityNameEn")
        private String CityNameEn;
        @Expose
        @SerializedName("CountryNameAr")
        private String CountryNameAr;
        @Expose
        @SerializedName("CountryNameEn")
        private String CountryNameEn;
        @Expose
        @SerializedName("Address2")
        private String Address2;
        @Expose
        @SerializedName("Address1")
        private String Address1;
        @Expose
        @SerializedName("FullName")
        private String FullName;
        @Expose
        @SerializedName("UserID")
        private int UserID;
        @Expose
        @SerializedName("Id")
        private int Id;

        public boolean getStatus() {
            return Status;
        }

        public void setStatus(boolean Status) {
            this.Status = Status;
        }

        public int getAddressType() {
            return AddressType;
        }

        public void setAddressType(int AddressType) {
            this.AddressType = AddressType;
        }

        public String getPhoneNumber() {
            return PhoneNumber;
        }

        public void setPhoneNumber(String PhoneNumber) {
            this.PhoneNumber = PhoneNumber;
        }

        public String getZipcode() {
            return Zipcode;
        }

        public void setZipcode(String Zipcode) {
            this.Zipcode = Zipcode;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int CityId) {
            this.CityId = CityId;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getCityNameAr() {
            return CityNameAr;
        }

        public void setCityNameAr(String CityNameAr) {
            this.CityNameAr = CityNameAr;
        }

        public String getCityNameEn() {
            return CityNameEn;
        }

        public void setCityNameEn(String CityNameEn) {
            this.CityNameEn = CityNameEn;
        }

        public String getCountryNameAr() {
            return CountryNameAr;
        }

        public void setCountryNameAr(String CountryNameAr) {
            this.CountryNameAr = CountryNameAr;
        }

        public String getCountryNameEn() {
            return CountryNameEn;
        }

        public void setCountryNameEn(String CountryNameEn) {
            this.CountryNameEn = CountryNameEn;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String Address2) {
            this.Address2 = Address2;
        }

        public String getAddress1() {
            return Address1;
        }

        public void setAddress1(String Address1) {
            this.Address1 = Address1;
        }

        public String getFullName() {
            return FullName;
        }

        public void setFullName(String FullName) {
            this.FullName = FullName;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
