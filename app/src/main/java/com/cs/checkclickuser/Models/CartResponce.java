package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class CartResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("UserId")
    private int UserId;
    @Expose
    @SerializedName("ProductInventoryId")
    private int ProductInventoryId;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getProductInventoryId() {
        return ProductInventoryId;
    }

    public void setProductInventoryId(int ProductInventoryId) {
        this.ProductInventoryId = ProductInventoryId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("CouponList")
        private ArrayList<CouponList> CouponList;
        @Expose
        @SerializedName("CartList")
        private ArrayList<CartList> CartList;

        public ArrayList<CouponList> getCouponList() {
            return CouponList;
        }

        public void setCouponList(ArrayList<CouponList> CouponList) {
            this.CouponList = CouponList;
        }

        public ArrayList<CartList> getCartList() {
            return CartList;
        }

        public void setCartList(ArrayList<CartList> CartList) {
            this.CartList = CartList;
        }
    }

    public static class CouponList implements Serializable {
        @Expose
        @SerializedName("Branches")
        private ArrayList<Branches> Branches;
        @Expose
        @SerializedName("Regions")
        private ArrayList<Regions> Regions;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("BackgroundCopy")
        private String BackgroundCopy;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("VoucherAmount")
        private double VoucherAmount;
        @Expose
        @SerializedName("CouponUsedCount")
        private int CouponUsedCount;
        @Expose
        @SerializedName("NTimes")
        private int NTimes;
        @Expose
        @SerializedName("DiscountLimitation")
        private int DiscountLimitation;
        @Expose
        @SerializedName("CouponCode")
        private String CouponCode;
        @Expose
        @SerializedName("IsCouponCodeRequires")
        private boolean IsCouponCodeRequires;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private double MaxDiscountAmount;
        @Expose
        @SerializedName("IsUptoApplicable")
        private boolean IsUptoApplicable;
        @Expose
        @SerializedName("VoucherDiscount")
        private int VoucherDiscount;
        @Expose
        @SerializedName("VoucherNameEn")
        private String VoucherNameEn;
        @Expose
        @SerializedName("UserSlabID")
        private int UserSlabID;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("Id")
        private int Id;


        public ArrayList<Branches> getBranches() {
            return Branches;
        }

        public void setBranches(ArrayList<Branches> Branches) {
            this.Branches = Branches;
        }

        public ArrayList<Regions> getRegions() {
            return Regions;
        }

        public void setRegions(ArrayList<Regions> Regions) {
            this.Regions = Regions;
        }

        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String LogoCopy) {
            this.LogoCopy = LogoCopy;
        }

        public String getBackgroundCopy() {
            return BackgroundCopy;
        }

        public void setBackgroundCopy(String BackgroundCopy) {
            this.BackgroundCopy = BackgroundCopy;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public double getVoucherAmount() {
            return VoucherAmount;
        }

        public void setVoucherAmount(double VoucherAmount) {
            this.VoucherAmount = VoucherAmount;
        }

        public int getCouponUsedCount() {
            return CouponUsedCount;
        }

        public void setCouponUsedCount(int CouponUsedCount) {
            this.CouponUsedCount = CouponUsedCount;
        }

        public int getNTimes() {
            return NTimes;
        }

        public void setNTimes(int NTimes) {
            this.NTimes = NTimes;
        }

        public int getDiscountLimitation() {
            return DiscountLimitation;
        }

        public void setDiscountLimitation(int DiscountLimitation) {
            this.DiscountLimitation = DiscountLimitation;
        }

        public String getCouponCode() {
            return CouponCode;
        }

        public void setCouponCode(String CouponCode) {
            this.CouponCode = CouponCode;
        }

        public boolean getIsCouponCodeRequires() {
            return IsCouponCodeRequires;
        }

        public void setIsCouponCodeRequires(boolean IsCouponCodeRequires) {
            this.IsCouponCodeRequires = IsCouponCodeRequires;
        }

        public double getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(int MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public boolean getIsUptoApplicable() {
            return IsUptoApplicable;
        }

        public void setIsUptoApplicable(boolean IsUptoApplicable) {
            this.IsUptoApplicable = IsUptoApplicable;
        }

        public int getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(int VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public String getVoucherNameEn() {
            return VoucherNameEn;
        }

        public void setVoucherNameEn(String VoucherNameEn) {
            this.VoucherNameEn = VoucherNameEn;
        }

        public int getUserSlabID() {
            return UserSlabID;
        }

        public void setUserSlabID(int UserSlabID) {
            this.UserSlabID = UserSlabID;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Branches implements Serializable{
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class Regions implements Serializable{
        @Expose
        @SerializedName("RegionId")
        private int RegionId;

        public int getRegionId() {
            return RegionId;
        }

        public void setRegionId(int RegionId) {
            this.RegionId = RegionId;
        }
    }

    public static class CartList implements Serializable {
        @Expose
        @SerializedName("VoucherDiscount")
        private double VoucherDiscount;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private double MaxDiscountAmount;
        @Expose
        @SerializedName("CouponId")
        private int CouponId;
        @Expose
        @SerializedName("CheckOutAmount")
        private double CheckOutAmount;
        @Expose
        @SerializedName("BranchAddress")
        private String BranchAddress;
        @Expose
        @SerializedName("ServiceType")
        private int ServiceType;
        @Expose
        @SerializedName("OrderType")
        private int OrderType;
        @Expose
        @SerializedName("CartStatus")
        private int CartStatus;
        @Expose
        @SerializedName("AvailableQuantity")
        private int AvailableQuantity;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("VatAmountPerItem")
        private double VatAmountPerItem;
        @Expose
        @SerializedName("TotalVatAmount")
        private double TotalVatAmount;
        @Expose
        @SerializedName("TotalItemPrice")
        private double TotalItemPrice;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("IsActive")
        private boolean IsActive;
//        @Expose
//        @SerializedName("NotReturnable")
//        private int NotReturnable;
        @Expose
        @SerializedName("AllowedQty")
        private int AllowedQty;
        @Expose
        @SerializedName("MaxCartQty")
        private int MaxCartQty;
        @Expose
        @SerializedName("MinCartQty")
        private int MinCartQty;
        @Expose
        @SerializedName("ProductAvailabilityRange")
        private int ProductAvailabilityRange;
        @Expose
        @SerializedName("LowStockActivity")
        private int LowStockActivity;
//        @Expose
//        @SerializedName("DisplayStockQuantity")
//        private int DisplayStockQuantity;
        @Expose
        @SerializedName("ReservedQuantity")
        private int ReservedQuantity;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("CartQuantity")
        private int CartQuantity;
        @Expose
        @SerializedName("Date")
        private String Date;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("UserId")
        private int UserId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CartId")
        private int CartId;
        @Expose
        @SerializedName("BranchColor")
        private String BranchColor;
        @Expose
        @SerializedName("CityId")
        private int CityId;
        @Expose
        @SerializedName("MinimumOrderValue")
        private float MinimumOrderValue;
        @Expose
        @SerializedName("FreeDeliveryAboveValue")
        private float FreeDeliveryAboveValue;
        @Expose
        @SerializedName("VatPercentage")
        private double VatPercentage;
        @Expose
        @SerializedName("ShipmentCODFee")
        private float ShimentCODFee;
        @Expose
        @SerializedName("DelDeliveryFee")
        private float DelDeliveryFee;
        @Expose
        @SerializedName("ShippingDeliveryFee")
        private float ShippingDeliveryFee;
        @Expose
        @SerializedName("MaxCODAmount")
        private float MaxCODAmount;
        @Expose
        @SerializedName("SubTotal")
        private float SubTotal;
        @Expose
        @SerializedName("DiscountAmount")
        private float DiscountAmount;
        @Expose
        @SerializedName("VAT")
        private float VAT;
        @Expose
        @SerializedName("GrandTotal")
        private float GrandTotal;

        public float getMaxCODAmount() {
            return MaxCODAmount;
        }

        public void setMaxCODAmount(float maxCODAmount) {
            MaxCODAmount = maxCODAmount;
        }

        public float getFreeDeliveryAboveValue() {
            return FreeDeliveryAboveValue;
        }

        public void setFreeDeliveryAboveValue(float freeDeliveryAboveValue) {
            FreeDeliveryAboveValue = freeDeliveryAboveValue;
        }

        public float getSubTotal() {
            return SubTotal;
        }

        public void setSubTotal(float subTotal) {
            SubTotal = subTotal;
        }

        public float getDiscountAmount() {
            return DiscountAmount;
        }

        public void setDiscountAmount(float discountAmount) {
            DiscountAmount = discountAmount;
        }

        public float getVAT() {
            return VAT;
        }

        public void setVAT(float VAT) {
            this.VAT = VAT;
        }

        public float getGrandTotal() {
            return GrandTotal;
        }

        public void setGrandTotal(float grandTotal) {
            GrandTotal = grandTotal;
        }

        public float getDelDeliveryFee() {
            return DelDeliveryFee;
        }

        public void setDelDeliveryFee(float delDeliveryFee) {
            DelDeliveryFee = delDeliveryFee;
        }

        public float getShippingDeliveryFee() {
            return ShippingDeliveryFee;
        }

        public void setShippingDeliveryFee(float shippingDeliveryFee) {
            ShippingDeliveryFee = shippingDeliveryFee;
        }

        public float getShimentCODFee() {
            return ShimentCODFee;
        }

        public void setShimentCODFee(float shimentCODFee) {
            ShimentCODFee = shimentCODFee;
        }

        public double getVatPercentage() {
            return VatPercentage;
        }

        public void setVatPercentage(double vatPercentage) {
            VatPercentage = vatPercentage;
        }

        public float getMinimumOrderValue() {
            return MinimumOrderValue;
        }

        public void setMinimumOrderValue(float minimumOrderValue) {
            MinimumOrderValue = minimumOrderValue;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int cityId) {
            CityId = cityId;
        }

        public String getBranchColor() {
            return BranchColor;
        }

        public void setBranchColor(String branchColor) {
            BranchColor = branchColor;
        }

        public double getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(double VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public double getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(double MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public int getCouponId() {
            return CouponId;
        }

        public void setCouponId(int CouponId) {
            this.CouponId = CouponId;
        }

        public double getCheckOutAmount() {
            return CheckOutAmount;
        }

        public void setCheckOutAmount(double CheckOutAmount) {
            this.CheckOutAmount = CheckOutAmount;
        }

        public String getBranchAddress() {
            return BranchAddress;
        }

        public void setBranchAddress(String BranchAddress) {
            this.BranchAddress = BranchAddress;
        }

        public int getServiceType() {
            return ServiceType;
        }

        public void setServiceType(int ServiceType) {
            this.ServiceType = ServiceType;
        }

        public int getOrderType() {
            return OrderType;
        }

        public void setOrderType(int OrderType) {
            this.OrderType = OrderType;
        }

        public int getCartStatus() {
            return CartStatus;
        }

        public void setCartStatus(int CartStatus) {
            this.CartStatus = CartStatus;
        }

        public int getAvailableQuantity() {
            return AvailableQuantity;
        }

        public void setAvailableQuantity(int AvailableQuantity) {
            this.AvailableQuantity = AvailableQuantity;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public double getVatAmountPerItem() {
            return VatAmountPerItem;
        }

        public void setVatAmountPerItem(double VatAmountPerItem) {
            this.VatAmountPerItem = VatAmountPerItem;
        }

        public double getTotalVatAmount() {
            return TotalVatAmount;
        }

        public void setTotalVatAmount(double TotalVatAmount) {
            this.TotalVatAmount = TotalVatAmount;
        }

        public double getTotalItemPrice() {
            return TotalItemPrice;
        }

        public void setTotalItemPrice(double TotalItemPrice) {
            this.TotalItemPrice = TotalItemPrice;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public boolean getIsActive() {
            return IsActive;
        }

        public void setIsActive(boolean IsActive) {
            this.IsActive = IsActive;
        }

//        public int getNotReturnable() {
//            return NotReturnable;
//        }
//
//        public void setNotReturnable(int NotReturnable) {
//            this.NotReturnable = NotReturnable;
//        }

        public int getAllowedQty() {
            return AllowedQty;
        }

        public void setAllowedQty(int AllowedQty) {
            this.AllowedQty = AllowedQty;
        }

        public int getMaxCartQty() {
            return MaxCartQty;
        }

        public void setMaxCartQty(int MaxCartQty) {
            this.MaxCartQty = MaxCartQty;
        }

        public int getMinCartQty() {
            return MinCartQty;
        }

        public void setMinCartQty(int MinCartQty) {
            this.MinCartQty = MinCartQty;
        }

        public int getProductAvailabilityRange() {
            return ProductAvailabilityRange;
        }

        public void setProductAvailabilityRange(int ProductAvailabilityRange) {
            this.ProductAvailabilityRange = ProductAvailabilityRange;
        }

        public int getLowStockActivity() {
            return LowStockActivity;
        }

        public void setLowStockActivity(int LowStockActivity) {
            this.LowStockActivity = LowStockActivity;
        }

//        public int getDisplayStockQuantity() {
//            return DisplayStockQuantity;
//        }
//
//        public void setDisplayStockQuantity(int DisplayStockQuantity) {
//            this.DisplayStockQuantity = DisplayStockQuantity;
//        }

        public int getReservedQuantity() {
            return ReservedQuantity;
        }

        public void setReservedQuantity(int ReservedQuantity) {
            this.ReservedQuantity = ReservedQuantity;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public int getCartQuantity() {
            return CartQuantity;
        }

        public void setCartQuantity(int CartQuantity) {
            this.CartQuantity = CartQuantity;
        }

        public String getDate() {
            return Date;
        }

        public void setDate(String Date) {
            this.Date = Date;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getCartId() {
            return CartId;
        }

        public void setCartId(int CartId) {
            this.CartId = CartId;
        }
    }
}
