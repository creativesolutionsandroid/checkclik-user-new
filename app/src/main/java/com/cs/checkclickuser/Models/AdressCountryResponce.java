package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;


public class AdressCountryResponce implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("CityList")
        private ArrayList<CityList> CityList;
        @Expose
        @SerializedName("CountryList")
        private ArrayList<CountryList> CountryList;

        public ArrayList<CityList> getCityList() {
            return CityList;
        }

        public void setCityList(ArrayList<CityList> CityList) {
            this.CityList = CityList;
        }

        public ArrayList<CountryList> getCountryList() {
            return CountryList;
        }

        public void setCountryList(ArrayList<CountryList> CountryList) {
            this.CountryList = CountryList;
        }
    }

    public static class CityList implements Serializable{
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("CountryAr")
        private String CountryAr;
        @Expose
        @SerializedName("CountryEn")
        private String CountryEn;
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getCountryAr() {
            return CountryAr;
        }

        public void setCountryAr(String CountryAr) {
            this.CountryAr = CountryAr;
        }

        public String getCountryEn() {
            return CountryEn;
        }

        public void setCountryEn(String CountryEn) {
            this.CountryEn = CountryEn;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class CountryList implements Serializable{
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
