package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BranchDetails {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("Branch")
        private ArrayList<Branch> Branch;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public ArrayList<Branch> getBranch() {
            return Branch;
        }

        public void setBranch(ArrayList<Branch> Branch) {
            this.Branch = Branch;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class Branch implements Serializable {
        @Expose
        @SerializedName("rn")
        private int rn;
        @Expose
        @SerializedName("Shift")
        private String Shift;
        @Expose
        @SerializedName("Favorite")
        private boolean FavoriteId;
        @Expose
        @SerializedName("VendorType")
        private int VendorType;
        @Expose
        @SerializedName("StoreType")
        private int StoreType;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Weekday")
        private int Weekday;
        @Expose
        @SerializedName("EndTime")
        private String EndTime;
        @Expose
        @SerializedName("StartTime")
        private String StartTime;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("Background")
        private String Background;
        @Expose
        @SerializedName("Maroof")
        private String Maroof;
        @Expose
        @SerializedName("Youtube")
        private String Youtube;
        @Expose
        @SerializedName("Snapchat")
        private String Snapchat;
        @Expose
        @SerializedName("Instagram")
        private String Instagram;
        @Expose
        @SerializedName("LinkedIn")
        private String LinkedIn;
        @Expose
        @SerializedName("TwitterHandle")
        private String TwitterHandle;
        @Expose
        @SerializedName("Facebook")
        private String Facebook;
        @Expose
        @SerializedName("BusinessSince")
        private String BusinessSince;
        @Expose
        @SerializedName("IsDelivery")
        private boolean IsDelivery;
        @Expose
        @SerializedName("InStoreService")
        private String InStoreService;
        @Expose
        @SerializedName("ShippingProvider")
        private int ShippingProvider;
        @Expose
        @SerializedName("OrderDone")
        private int OrderDone;
        @Expose
        @SerializedName("BranchColor")
        private String BranchColor;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("IsCreditCardAllowed")
        private String IsCreditCardAllowed;
        @Expose
        @SerializedName("IsCashAllowed")
        private String IsCashAllowed;
        @Expose
        @SerializedName("DeliveryTime")
        private String DeliveryTime;
        @Expose
        @SerializedName("Ratings")
        private float Ratings;
        @Expose
        @SerializedName("ReviewsCount")
        private int ReviewsCount;
        @Expose
        @SerializedName("Reviews")
        private float Reviews;
        @Expose
        @SerializedName("CategoryAr")
        private String CategoryAr;
        @Expose
        @SerializedName("CategoryEn")
        private String CategoryEn;
        @Expose
        @SerializedName("TermsAndConditionsAr")
        private String TermsAndConditionsAr;
        @Expose
        @SerializedName("TermsAndConditionsEn")
        private String TermsAndConditionsEn;
        @Expose
        @SerializedName("StoreImage")
        private String StoreImage;
        @Expose
        @SerializedName("BranchLogoImage")
        private String BranchLogoImage;
        @Expose
        @SerializedName("BackgroundImage")
        private String BackgroundImage;
        @Expose
        @SerializedName("MinimumOrderValue")
        private int MinimumOrderValue;
        @Expose
        @SerializedName("StoreAr")
        private String StoreAr;
        @Expose
        @SerializedName("StoreEn")
        private String StoreEn;
        @Expose
        @SerializedName("BranchAr")
        private String BranchAr;
        @Expose
        @SerializedName("BranchEn")
        private String BranchEn;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("BranchStatus")
        private String BranchStatus;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("ActualDate")
        private String ActualDate;
        @Expose
        @SerializedName("DescriptionEn")
        private String DescriptionEn;
        @Expose
        @SerializedName("DescriptionAr")
        private String DescriptionAr;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("JOtherBranch")
        private ArrayList<ProductlistResponce.JOtherBranch> JOtherBranch;

        public String getDescriptionEn() {
            return DescriptionEn;
        }

        public void setDescriptionEn(String descriptionEn) {
            DescriptionEn = descriptionEn;
        }

        public String getDescriptionAr() {
            return DescriptionAr;
        }

        public void setDescriptionAr(String descriptionAr) {
            DescriptionAr = descriptionAr;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String contactNo) {
            ContactNo = contactNo;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String emailId) {
            EmailId = emailId;
        }

        public int getRn() {
            return rn;
        }

        public void setRn(int rn) {
            this.rn = rn;
        }

        public String getShift() {
            return Shift;
        }

        public void setShift(String Shift) {
            this.Shift = Shift;
        }

        public boolean getFavoriteId() {
            return FavoriteId;
        }

        public void setFavoriteId(boolean FavoriteId) {
            this.FavoriteId = FavoriteId;
        }

        public int getVendorType() {
            return VendorType;
        }

        public void setVendorType(int VendorType) {
            this.VendorType = VendorType;
        }

        public int getStoreType() {
            return StoreType;
        }

        public void setStoreType(int StoreType) {
            this.StoreType = StoreType;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public int getWeekday() {
            return Weekday;
        }

        public void setWeekday(int Weekday) {
            this.Weekday = Weekday;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String Logo) {
            this.Logo = Logo;
        }

        public String getBackground() {
            return Background;
        }

        public void setBackground(String Background) {
            this.Background = Background;
        }

        public String getMaroof() {
            return Maroof;
        }

        public void setMaroof(String Maroof) {
            this.Maroof = Maroof;
        }

        public String getYoutube() {
            return Youtube;
        }

        public void setYoutube(String Youtube) {
            this.Youtube = Youtube;
        }

        public String getSnapchat() {
            return Snapchat;
        }

        public void setSnapchat(String Snapchat) {
            this.Snapchat = Snapchat;
        }

        public String getInstagram() {
            return Instagram;
        }

        public void setInstagram(String Instagram) {
            this.Instagram = Instagram;
        }

        public String getLinkedIn() {
            return LinkedIn;
        }

        public void setLinkedIn(String LinkedIn) {
            this.LinkedIn = LinkedIn;
        }

        public String getTwitterHandle() {
            return TwitterHandle;
        }

        public void setTwitterHandle(String TwitterHandle) {
            this.TwitterHandle = TwitterHandle;
        }

        public String getFacebook() {
            return Facebook;
        }

        public void setFacebook(String Facebook) {
            this.Facebook = Facebook;
        }

        public String getBusinessSince() {
            return BusinessSince;
        }

        public void setBusinessSince(String BusinessSince) {
            this.BusinessSince = BusinessSince;
        }

        public boolean getIsDelivery() {
            return IsDelivery;
        }

        public void setIsDelivery(boolean IsDelivery) {
            this.IsDelivery = IsDelivery;
        }

        public String getInStoreService() {
            return InStoreService;
        }

        public void setInStoreService(String InStoreService) {
            this.InStoreService = InStoreService;
        }

        public int getShippingProvider() {
            return ShippingProvider;
        }

        public void setShippingProvider(int ShippingProvider) {
            this.ShippingProvider = ShippingProvider;
        }

        public int getOrderDone() {
            return OrderDone;
        }

        public void setOrderDone(int OrderDone) {
            this.OrderDone = OrderDone;
        }

        public String getBranchColor() {
            return BranchColor;
        }

        public void setBranchColor(String BranchColor) {
            this.BranchColor = BranchColor;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public String getIsCreditCardAllowed() {
            return IsCreditCardAllowed;
        }

        public void setIsCreditCardAllowed(String IsCreditCardAllowed) {
            this.IsCreditCardAllowed = IsCreditCardAllowed;
        }

        public String getIsCashAllowed() {
            return IsCashAllowed;
        }

        public void setIsCashAllowed(String IsCashAllowed) {
            this.IsCashAllowed = IsCashAllowed;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String DeliveryTime) {
            this.DeliveryTime = DeliveryTime;
        }

        public float getRatings() {
            return Ratings;
        }

        public void setRatings(float Ratings) {
            this.Ratings = Ratings;
        }

        public int getReviewsCount() {
            return ReviewsCount;
        }

        public void setReviewsCount(int ReviewsCount) {
            this.ReviewsCount = ReviewsCount;
        }

        public float getReviews() {
            return Reviews;
        }

        public void setReviews(float Reviews) {
            this.Reviews = Reviews;
        }

        public String getCategoryAr() {
            return CategoryAr;
        }

        public void setCategoryAr(String CategoryAr) {
            this.CategoryAr = CategoryAr;
        }

        public String getCategoryEn() {
            return CategoryEn;
        }

        public void setCategoryEn(String CategoryEn) {
            this.CategoryEn = CategoryEn;
        }

        public String getTermsAndConditionsAr() {
            return TermsAndConditionsAr;
        }

        public void setTermsAndConditionsAr(String TermsAndConditionsAr) {
            this.TermsAndConditionsAr = TermsAndConditionsAr;
        }

        public String getTermsAndConditionsEn() {
            return TermsAndConditionsEn;
        }

        public void setTermsAndConditionsEn(String TermsAndConditionsEn) {
            this.TermsAndConditionsEn = TermsAndConditionsEn;
        }

        public String getStoreImage() {
            return StoreImage;
        }

        public void setStoreImage(String StoreImage) {
            this.StoreImage = StoreImage;
        }

        public String getBranchLogoImage() {
            return BranchLogoImage;
        }

        public void setBranchLogoImage(String BranchLogoImage) {
            this.BranchLogoImage = BranchLogoImage;
        }

        public String getBackgroundImage() {
            return BackgroundImage;
        }

        public void setBackgroundImage(String BackgroundImage) {
            this.BackgroundImage = BackgroundImage;
        }

        public int getMinimumOrderValue() {
            return MinimumOrderValue;
        }

        public void setMinimumOrderValue(int MinimumOrderValue) {
            this.MinimumOrderValue = MinimumOrderValue;
        }

        public String getStoreAr() {
            return StoreAr;
        }

        public void setStoreAr(String StoreAr) {
            this.StoreAr = StoreAr;
        }

        public String getStoreEn() {
            return StoreEn;
        }

        public void setStoreEn(String StoreEn) {
            this.StoreEn = StoreEn;
        }

        public String getBranchAr() {
            return BranchAr;
        }

        public void setBranchAr(String BranchAr) {
            this.BranchAr = BranchAr;
        }

        public String getBranchEn() {
            return BranchEn;
        }

        public void setBranchEn(String BranchEn) {
            this.BranchEn = BranchEn;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getBranchStatus() {
            return BranchStatus;
        }

        public void setBranchStatus(String BranchStatus) {
            this.BranchStatus = BranchStatus;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getActualDate() {
            return ActualDate;
        }

        public void setActualDate(String ActualDate) {
            this.ActualDate = ActualDate;
        }

        public ArrayList<ProductlistResponce.JOtherBranch> getJOtherBranch() {
            return JOtherBranch;
        }

        public void setJOtherBranch(ArrayList<ProductlistResponce.JOtherBranch> JOtherBranch) {
            this.JOtherBranch = JOtherBranch;
        }

    }
}
