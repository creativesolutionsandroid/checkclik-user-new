package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BranchDetailsResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("Branch")
        private ArrayList<Branch> Branch;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public ArrayList<Branch> getBranch() {
            return Branch;
        }

        public void setBranch(ArrayList<Branch> Branch) {
            this.Branch = Branch;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class Branch implements Serializable{
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;
        @Expose
        @SerializedName("InStoreService")
        private String InStoreService;
        @Expose
        @SerializedName("IsDelivery")
        private boolean IsDelivery;
        @Expose
        @SerializedName("JOtherBranches")
        private List<String> JOtherBranches;
        @Expose
        @SerializedName("JOtherBranch")
        private List<String> JOtherBranch;
        @Expose
        @SerializedName("OtherBranches")
        private String OtherBranches;
        @Expose
        @SerializedName("OtherBranch")
        private String OtherBranch;
        @Expose
        @SerializedName("Weekday")
        private int Weekday;
        @Expose
        @SerializedName("WeekNo")
        private int WeekNo;
        @Expose
        @SerializedName("EndTime")
        private String EndTime;
        @Expose
        @SerializedName("StartTime")
        private String StartTime;
        @Expose
        @SerializedName("Shift")
        private String Shift;
        @Expose
        @SerializedName("CurrentDateTime")
        private String CurrentDateTime;
        @Expose
        @SerializedName("EndDateTime")
        private String EndDateTime;
        @Expose
        @SerializedName("StarDateTime")
        private String StarDateTime;
        @Expose
        @SerializedName("ChatData")
        private String ChatData;
        @Expose
        @SerializedName("OrderDone")
        private int OrderDone;
        @Expose
        @SerializedName("DeliveryTime")
        private String DeliveryTime;
        @Expose
        @SerializedName("Ratings")
        private int Ratings;
        @Expose
        @SerializedName("ReviewsCount")
        private int ReviewsCount;
        @Expose
        @SerializedName("Reviews")
        private int Reviews;
        @Expose
        @SerializedName("VendorType")
        private int VendorType;
        @Expose
        @SerializedName("EmailId")
        private String EmailId;
        @Expose
        @SerializedName("ContactNo")
        private String ContactNo;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("Background")
        private String Background;
        @Expose
        @SerializedName("EntityNameAr")
        private String EntityNameAr;
        @Expose
        @SerializedName("EntityNameEn")
        private String EntityNameEn;
        @Expose
        @SerializedName("StoreType")
        private int StoreType;
        @Expose
        @SerializedName("CategoryAr")
        private String CategoryAr;
        @Expose
        @SerializedName("CategoryEn")
        private String CategoryEn;
        @Expose
        @SerializedName("StoreImage")
        private String StoreImage;
        @Expose
        @SerializedName("StoreAr")
        private String StoreAr;
        @Expose
        @SerializedName("StoreEn")
        private String StoreEn;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("Favorite1")
        private boolean Favorite1;
        @Expose
        @SerializedName("RegionId")
        private int RegionId;
        @Expose
        @SerializedName("RegionAr")
        private String RegionAr;
        @Expose
        @SerializedName("RegionEn")
        private String RegionEn;
        @Expose
        @SerializedName("DistrictAr")
        private String DistrictAr;
        @Expose
        @SerializedName("DistrictEn")
        private String DistrictEn;
        @Expose
        @SerializedName("CityAr")
        private String CityAr;
        @Expose
        @SerializedName("CityEn")
        private String CityEn;
        @Expose
        @SerializedName("CountryAr")
        private String CountryAr;
        @Expose
        @SerializedName("CountryEn")
        private String CountryEn;
        @Expose
        @SerializedName("MembershipPlan")
        private int MembershipPlan;
        @Expose
        @SerializedName("is24x7")
        private boolean is24x7;
        @Expose
        @SerializedName("BranchColor")
        private String BranchColor;
        @Expose
        @SerializedName("BranchLogoImage")
        private String BranchLogoImage;
        @Expose
        @SerializedName("BackgroundImage")
        private String BackgroundImage;
        @Expose
        @SerializedName("ReturnPolicyAr")
        private String ReturnPolicyAr;
        @Expose
        @SerializedName("ReturnPolicyEn")
        private String ReturnPolicyEn;
        @Expose
        @SerializedName("TermsAndConditionsAr")
        private String TermsAndConditionsAr;
        @Expose
        @SerializedName("TermsAndConditionsEn")
        private String TermsAndConditionsEn;
        @Expose
        @SerializedName("Maroof")
        private String Maroof;
        @Expose
        @SerializedName("Youtube")
        private String Youtube;
        @Expose
        @SerializedName("Instagram")
        private String Instagram;
        @Expose
        @SerializedName("Snapchat")
        private String Snapchat;
        @Expose
        @SerializedName("LinkedIn")
        private String LinkedIn;
        @Expose
        @SerializedName("TwitterHandle")
        private String TwitterHandle;
        @Expose
        @SerializedName("Facebook")
        private String Facebook;
        @Expose
        @SerializedName("IsCreditCardAllowed")
        private String IsCreditCardAllowed;
        @Expose
        @SerializedName("IsCashAllowed")
        private String IsCashAllowed;
        @Expose
        @SerializedName("ShippingProvider")
        private int ShippingProvider;
        @Expose
        @SerializedName("IsShippingWorldwide")
        private boolean IsShippingWorldwide;
        @Expose
        @SerializedName("FreeDeliveryAboveValue")
        private int FreeDeliveryAboveValue;
        @Expose
        @SerializedName("MinimumOrderValue")
        private int MinimumOrderValue;
        @Expose
        @SerializedName("DeliveryCharges")
        private int DeliveryCharges;
        @Expose
        @SerializedName("PreBookingDeliveryTime")
        private int PreBookingDeliveryTime;
        @Expose
        @SerializedName("PreBookingDeliveryTimeUnit")
        private int PreBookingDeliveryTimeUnit;
        @Expose
        @SerializedName("IsDeliveryCompany")
        private int IsDeliveryCompany;
        @Expose
        @SerializedName("IsDelivery1")
        private boolean IsDelivery1;
        @Expose
        @SerializedName("PreBookingInStoreTime")
        private int PreBookingInStoreTime;
        @Expose
        @SerializedName("PreBookingInStoreTimeUnit")
        private int PreBookingInStoreTimeUnit;
        @Expose
        @SerializedName("InStoreService1")
        private boolean InStoreService1;
        @Expose
        @SerializedName("strBusinessSince")
        private String strBusinessSince;
        @Expose
        @SerializedName("BusinessSince")
        private String BusinessSince;
        @Expose
        @SerializedName("BranchStatus")
        private String BranchStatus;
        @Expose
        @SerializedName("Distance")
        private double Distance;
        @Expose
        @SerializedName("Longitude")
        private String Longitude;
        @Expose
        @SerializedName("Latitude")
        private String Latitude;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("AddressTypeId")
        private int AddressTypeId;
        @Expose
        @SerializedName("ApartmentOfficeNo")
        private String ApartmentOfficeNo;
        @Expose
        @SerializedName("FloorNo")
        private String FloorNo;
        @Expose
        @SerializedName("BuildingNo")
        private String BuildingNo;
        @Expose
        @SerializedName("StreetNo")
        private String StreetNo;
        @Expose
        @SerializedName("ZipCode")
        private String ZipCode;
        @Expose
        @SerializedName("DistrictId")
        private int DistrictId;
        @Expose
        @SerializedName("CityId")
        private int CityId;
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("DescriptionAr")
        private String DescriptionAr;
        @Expose
        @SerializedName("DescriptionEn")
        private String DescriptionEn;
        @Expose
        @SerializedName("BranchAr")
        private String BranchAr;
        @Expose
        @SerializedName("BranchEn")
        private String BranchEn;
        @Expose
        @SerializedName("BranchNameAr")
        private String BranchNameAr;
        @Expose
        @SerializedName("BranchNameEn")
        private String BranchNameEn;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("rn")
        private int rn;

        public boolean getFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean Favorite) {
            this.Favorite = Favorite;
        }

        public String getInStoreService() {
            return InStoreService;
        }

        public void setInStoreService(String InStoreService) {
            this.InStoreService = InStoreService;
        }

        public boolean getIsDelivery() {
            return IsDelivery;
        }

        public void setIsDelivery(boolean IsDelivery) {
            this.IsDelivery = IsDelivery;
        }

        public List<String> getJOtherBranches() {
            return JOtherBranches;
        }

        public void setJOtherBranches(List<String> JOtherBranches) {
            this.JOtherBranches = JOtherBranches;
        }

        public List<String> getJOtherBranch() {
            return JOtherBranch;
        }

        public void setJOtherBranch(List<String> JOtherBranch) {
            this.JOtherBranch = JOtherBranch;
        }

        public String getOtherBranches() {
            return OtherBranches;
        }

        public void setOtherBranches(String OtherBranches) {
            this.OtherBranches = OtherBranches;
        }

        public String getOtherBranch() {
            return OtherBranch;
        }

        public void setOtherBranch(String OtherBranch) {
            this.OtherBranch = OtherBranch;
        }

        public int getWeekday() {
            return Weekday;
        }

        public void setWeekday(int Weekday) {
            this.Weekday = Weekday;
        }

        public int getWeekNo() {
            return WeekNo;
        }

        public void setWeekNo(int WeekNo) {
            this.WeekNo = WeekNo;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getShift() {
            return Shift;
        }

        public void setShift(String Shift) {
            this.Shift = Shift;
        }

        public String getCurrentDateTime() {
            return CurrentDateTime;
        }

        public void setCurrentDateTime(String CurrentDateTime) {
            this.CurrentDateTime = CurrentDateTime;
        }

        public String getEndDateTime() {
            return EndDateTime;
        }

        public void setEndDateTime(String EndDateTime) {
            this.EndDateTime = EndDateTime;
        }

        public String getStarDateTime() {
            return StarDateTime;
        }

        public void setStarDateTime(String StarDateTime) {
            this.StarDateTime = StarDateTime;
        }

        public String getChatData() {
            return ChatData;
        }

        public void setChatData(String ChatData) {
            this.ChatData = ChatData;
        }

        public int getOrderDone() {
            return OrderDone;
        }

        public void setOrderDone(int OrderDone) {
            this.OrderDone = OrderDone;
        }

        public String getDeliveryTime() {
            return DeliveryTime;
        }

        public void setDeliveryTime(String DeliveryTime) {
            this.DeliveryTime = DeliveryTime;
        }

        public int getRatings() {
            return Ratings;
        }

        public void setRatings(int Ratings) {
            this.Ratings = Ratings;
        }

        public int getReviewsCount() {
            return ReviewsCount;
        }

        public void setReviewsCount(int ReviewsCount) {
            this.ReviewsCount = ReviewsCount;
        }

        public int getReviews() {
            return Reviews;
        }

        public void setReviews(int Reviews) {
            this.Reviews = Reviews;
        }

        public int getVendorType() {
            return VendorType;
        }

        public void setVendorType(int VendorType) {
            this.VendorType = VendorType;
        }

        public String getEmailId() {
            return EmailId;
        }

        public void setEmailId(String EmailId) {
            this.EmailId = EmailId;
        }

        public String getContactNo() {
            return ContactNo;
        }

        public void setContactNo(String ContactNo) {
            this.ContactNo = ContactNo;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String Logo) {
            this.Logo = Logo;
        }

        public String getBackground() {
            return Background;
        }

        public void setBackground(String Background) {
            this.Background = Background;
        }

        public String getEntityNameAr() {
            return EntityNameAr;
        }

        public void setEntityNameAr(String EntityNameAr) {
            this.EntityNameAr = EntityNameAr;
        }

        public String getEntityNameEn() {
            return EntityNameEn;
        }

        public void setEntityNameEn(String EntityNameEn) {
            this.EntityNameEn = EntityNameEn;
        }

        public int getStoreType() {
            return StoreType;
        }

        public void setStoreType(int StoreType) {
            this.StoreType = StoreType;
        }

        public String getCategoryAr() {
            return CategoryAr;
        }

        public void setCategoryAr(String CategoryAr) {
            this.CategoryAr = CategoryAr;
        }

        public String getCategoryEn() {
            return CategoryEn;
        }

        public void setCategoryEn(String CategoryEn) {
            this.CategoryEn = CategoryEn;
        }

        public String getStoreImage() {
            return StoreImage;
        }

        public void setStoreImage(String StoreImage) {
            this.StoreImage = StoreImage;
        }

        public String getStoreAr() {
            return StoreAr;
        }

        public void setStoreAr(String StoreAr) {
            this.StoreAr = StoreAr;
        }

        public String getStoreEn() {
            return StoreEn;
        }

        public void setStoreEn(String StoreEn) {
            this.StoreEn = StoreEn;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public boolean getFavorite1() {
            return Favorite1;
        }

        public void setFavorite1(boolean Favorite1) {
            this.Favorite1 = Favorite1;
        }

        public int getRegionId() {
            return RegionId;
        }

        public void setRegionId(int RegionId) {
            this.RegionId = RegionId;
        }

        public String getRegionAr() {
            return RegionAr;
        }

        public void setRegionAr(String RegionAr) {
            this.RegionAr = RegionAr;
        }

        public String getRegionEn() {
            return RegionEn;
        }

        public void setRegionEn(String RegionEn) {
            this.RegionEn = RegionEn;
        }

        public String getDistrictAr() {
            return DistrictAr;
        }

        public void setDistrictAr(String DistrictAr) {
            this.DistrictAr = DistrictAr;
        }

        public String getDistrictEn() {
            return DistrictEn;
        }

        public void setDistrictEn(String DistrictEn) {
            this.DistrictEn = DistrictEn;
        }

        public String getCityAr() {
            return CityAr;
        }

        public void setCityAr(String CityAr) {
            this.CityAr = CityAr;
        }

        public String getCityEn() {
            return CityEn;
        }

        public void setCityEn(String CityEn) {
            this.CityEn = CityEn;
        }

        public String getCountryAr() {
            return CountryAr;
        }

        public void setCountryAr(String CountryAr) {
            this.CountryAr = CountryAr;
        }

        public String getCountryEn() {
            return CountryEn;
        }

        public void setCountryEn(String CountryEn) {
            this.CountryEn = CountryEn;
        }

        public int getMembershipPlan() {
            return MembershipPlan;
        }

        public void setMembershipPlan(int MembershipPlan) {
            this.MembershipPlan = MembershipPlan;
        }

        public boolean getIs24x7() {
            return is24x7;
        }

        public void setIs24x7(boolean is24x7) {
            this.is24x7 = is24x7;
        }

        public String getBranchColor() {
            return BranchColor;
        }

        public void setBranchColor(String BranchColor) {
            this.BranchColor = BranchColor;
        }

        public String getBranchLogoImage() {
            return BranchLogoImage;
        }

        public void setBranchLogoImage(String BranchLogoImage) {
            this.BranchLogoImage = BranchLogoImage;
        }

        public String getBackgroundImage() {
            return BackgroundImage;
        }

        public void setBackgroundImage(String BackgroundImage) {
            this.BackgroundImage = BackgroundImage;
        }

        public String getReturnPolicyAr() {
            return ReturnPolicyAr;
        }

        public void setReturnPolicyAr(String ReturnPolicyAr) {
            this.ReturnPolicyAr = ReturnPolicyAr;
        }

        public String getReturnPolicyEn() {
            return ReturnPolicyEn;
        }

        public void setReturnPolicyEn(String ReturnPolicyEn) {
            this.ReturnPolicyEn = ReturnPolicyEn;
        }

        public String getTermsAndConditionsAr() {
            return TermsAndConditionsAr;
        }

        public void setTermsAndConditionsAr(String TermsAndConditionsAr) {
            this.TermsAndConditionsAr = TermsAndConditionsAr;
        }

        public String getTermsAndConditionsEn() {
            return TermsAndConditionsEn;
        }

        public void setTermsAndConditionsEn(String TermsAndConditionsEn) {
            this.TermsAndConditionsEn = TermsAndConditionsEn;
        }

        public String getMaroof() {
            return Maroof;
        }

        public void setMaroof(String Maroof) {
            this.Maroof = Maroof;
        }

        public String getYoutube() {
            return Youtube;
        }

        public void setYoutube(String Youtube) {
            this.Youtube = Youtube;
        }

        public String getInstagram() {
            return Instagram;
        }

        public void setInstagram(String Instagram) {
            this.Instagram = Instagram;
        }

        public String getSnapchat() {
            return Snapchat;
        }

        public void setSnapchat(String Snapchat) {
            this.Snapchat = Snapchat;
        }

        public String getLinkedIn() {
            return LinkedIn;
        }

        public void setLinkedIn(String LinkedIn) {
            this.LinkedIn = LinkedIn;
        }

        public String getTwitterHandle() {
            return TwitterHandle;
        }

        public void setTwitterHandle(String TwitterHandle) {
            this.TwitterHandle = TwitterHandle;
        }

        public String getFacebook() {
            return Facebook;
        }

        public void setFacebook(String Facebook) {
            this.Facebook = Facebook;
        }

        public String getIsCreditCardAllowed() {
            return IsCreditCardAllowed;
        }

        public void setIsCreditCardAllowed(String IsCreditCardAllowed) {
            this.IsCreditCardAllowed = IsCreditCardAllowed;
        }

        public String getIsCashAllowed() {
            return IsCashAllowed;
        }

        public void setIsCashAllowed(String IsCashAllowed) {
            this.IsCashAllowed = IsCashAllowed;
        }

        public int getShippingProvider() {
            return ShippingProvider;
        }

        public void setShippingProvider(int ShippingProvider) {
            this.ShippingProvider = ShippingProvider;
        }

        public boolean getIsShippingWorldwide() {
            return IsShippingWorldwide;
        }

        public void setIsShippingWorldwide(boolean IsShippingWorldwide) {
            this.IsShippingWorldwide = IsShippingWorldwide;
        }

        public int getFreeDeliveryAboveValue() {
            return FreeDeliveryAboveValue;
        }

        public void setFreeDeliveryAboveValue(int FreeDeliveryAboveValue) {
            this.FreeDeliveryAboveValue = FreeDeliveryAboveValue;
        }

        public int getMinimumOrderValue() {
            return MinimumOrderValue;
        }

        public void setMinimumOrderValue(int MinimumOrderValue) {
            this.MinimumOrderValue = MinimumOrderValue;
        }

        public int getDeliveryCharges() {
            return DeliveryCharges;
        }

        public void setDeliveryCharges(int DeliveryCharges) {
            this.DeliveryCharges = DeliveryCharges;
        }

        public int getPreBookingDeliveryTime() {
            return PreBookingDeliveryTime;
        }

        public void setPreBookingDeliveryTime(int PreBookingDeliveryTime) {
            this.PreBookingDeliveryTime = PreBookingDeliveryTime;
        }

        public int getPreBookingDeliveryTimeUnit() {
            return PreBookingDeliveryTimeUnit;
        }

        public void setPreBookingDeliveryTimeUnit(int PreBookingDeliveryTimeUnit) {
            this.PreBookingDeliveryTimeUnit = PreBookingDeliveryTimeUnit;
        }

        public int getIsDeliveryCompany() {
            return IsDeliveryCompany;
        }

        public void setIsDeliveryCompany(int IsDeliveryCompany) {
            this.IsDeliveryCompany = IsDeliveryCompany;
        }

        public boolean getIsDelivery1() {
            return IsDelivery1;
        }

        public void setIsDelivery1(boolean IsDelivery1) {
            this.IsDelivery1 = IsDelivery1;
        }

        public int getPreBookingInStoreTime() {
            return PreBookingInStoreTime;
        }

        public void setPreBookingInStoreTime(int PreBookingInStoreTime) {
            this.PreBookingInStoreTime = PreBookingInStoreTime;
        }

        public int getPreBookingInStoreTimeUnit() {
            return PreBookingInStoreTimeUnit;
        }

        public void setPreBookingInStoreTimeUnit(int PreBookingInStoreTimeUnit) {
            this.PreBookingInStoreTimeUnit = PreBookingInStoreTimeUnit;
        }

        public boolean getInStoreService1() {
            return InStoreService1;
        }

        public void setInStoreService1(boolean InStoreService1) {
            this.InStoreService1 = InStoreService1;
        }

        public String getStrBusinessSince() {
            return strBusinessSince;
        }

        public void setStrBusinessSince(String strBusinessSince) {
            this.strBusinessSince = strBusinessSince;
        }

        public String getBusinessSince() {
            return BusinessSince;
        }

        public void setBusinessSince(String BusinessSince) {
            this.BusinessSince = BusinessSince;
        }

        public String getBranchStatus() {
            return BranchStatus;
        }

        public void setBranchStatus(String BranchStatus) {
            this.BranchStatus = BranchStatus;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String Longitude) {
            this.Longitude = Longitude;
        }

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String Latitude) {
            this.Latitude = Latitude;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public int getAddressTypeId() {
            return AddressTypeId;
        }

        public void setAddressTypeId(int AddressTypeId) {
            this.AddressTypeId = AddressTypeId;
        }

        public String getApartmentOfficeNo() {
            return ApartmentOfficeNo;
        }

        public void setApartmentOfficeNo(String ApartmentOfficeNo) {
            this.ApartmentOfficeNo = ApartmentOfficeNo;
        }

        public String getFloorNo() {
            return FloorNo;
        }

        public void setFloorNo(String FloorNo) {
            this.FloorNo = FloorNo;
        }

        public String getBuildingNo() {
            return BuildingNo;
        }

        public void setBuildingNo(String BuildingNo) {
            this.BuildingNo = BuildingNo;
        }

        public String getStreetNo() {
            return StreetNo;
        }

        public void setStreetNo(String StreetNo) {
            this.StreetNo = StreetNo;
        }

        public String getZipCode() {
            return ZipCode;
        }

        public void setZipCode(String ZipCode) {
            this.ZipCode = ZipCode;
        }

        public int getDistrictId() {
            return DistrictId;
        }

        public void setDistrictId(int DistrictId) {
            this.DistrictId = DistrictId;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int CityId) {
            this.CityId = CityId;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public String getDescriptionAr() {
            return DescriptionAr;
        }

        public void setDescriptionAr(String DescriptionAr) {
            this.DescriptionAr = DescriptionAr;
        }

        public String getDescriptionEn() {
            return DescriptionEn;
        }

        public void setDescriptionEn(String DescriptionEn) {
            this.DescriptionEn = DescriptionEn;
        }

        public String getBranchAr() {
            return BranchAr;
        }

        public void setBranchAr(String BranchAr) {
            this.BranchAr = BranchAr;
        }

        public String getBranchEn() {
            return BranchEn;
        }

        public void setBranchEn(String BranchEn) {
            this.BranchEn = BranchEn;
        }

        public String getBranchNameAr() {
            return BranchNameAr;
        }

        public void setBranchNameAr(String BranchNameAr) {
            this.BranchNameAr = BranchNameAr;
        }

        public String getBranchNameEn() {
            return BranchNameEn;
        }

        public void setBranchNameEn(String BranchNameEn) {
            this.BranchNameEn = BranchNameEn;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }

        public int getRn() {
            return rn;
        }

        public void setRn(int rn) {
            this.rn = rn;
        }
    }
}
