package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceItemResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("BranchSubCategoryId")
    private int BranchSubCategoryId;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public int getBranchSubCategoryId() {
        return BranchSubCategoryId;
    }

    public void setBranchSubCategoryId(int BranchSubCategoryId) {
        this.BranchSubCategoryId = BranchSubCategoryId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable {
        @Expose
        @SerializedName("TotalRecords")
        private List<TotalRecords> TotalRecords;
        @Expose
        @SerializedName("ServiceList")
        private ArrayList<ServiceList> ServiceList;

        public List<TotalRecords> getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(List<TotalRecords> TotalRecords) {
            this.TotalRecords = TotalRecords;
        }

        public ArrayList<ServiceList> getServiceList() {
            return ServiceList;
        }

        public void setServiceList(ArrayList<ServiceList> ServiceList) {
            this.ServiceList = ServiceList;
        }
    }

    public static class TotalRecords implements Serializable {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }
    }

    public static class ServiceList implements Serializable{
        @Expose
        @SerializedName("SubCatAr")
        private String SubCatAr;
        @Expose
        @SerializedName("SubCatEn")
        private String SubCatEn;
        @Expose
        @SerializedName("SubCatId")
        private int SubCatId;
        @Expose
        @SerializedName("MainCatAr")
        private String MainCatAr;
        @Expose
        @SerializedName("MainCatEn")
        private String MainCatEn;
        @Expose
        @SerializedName("MainCatId")
        private int MainCatId;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("CountingNameAr")
        private String CountingNameAr;
        @Expose
        @SerializedName("CountingNameEn")
        private String CountingNameEn;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private int DiscountValue;
        @Expose
        @SerializedName("AdvId")
        private int AdvId;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("rowNum")
        private int rowNum;

        public String getSubCatAr() {
            return SubCatAr;
        }

        public void setSubCatAr(String SubCatAr) {
            this.SubCatAr = SubCatAr;
        }

        public String getSubCatEn() {
            return SubCatEn;
        }

        public void setSubCatEn(String SubCatEn) {
            this.SubCatEn = SubCatEn;
        }

        public int getSubCatId() {
            return SubCatId;
        }

        public void setSubCatId(int SubCatId) {
            this.SubCatId = SubCatId;
        }

        public String getMainCatAr() {
            return MainCatAr;
        }

        public void setMainCatAr(String MainCatAr) {
            this.MainCatAr = MainCatAr;
        }

        public String getMainCatEn() {
            return MainCatEn;
        }

        public void setMainCatEn(String MainCatEn) {
            this.MainCatEn = MainCatEn;
        }

        public int getMainCatId() {
            return MainCatId;
        }

        public void setMainCatId(int MainCatId) {
            this.MainCatId = MainCatId;
        }

        public boolean getFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean Favorite) {
            this.Favorite = Favorite;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getCountingNameAr() {
            return CountingNameAr;
        }

        public void setCountingNameAr(String CountingNameAr) {
            this.CountingNameAr = CountingNameAr;
        }

        public String getCountingNameEn() {
            return CountingNameEn;
        }

        public void setCountingNameEn(String CountingNameEn) {
            this.CountingNameEn = CountingNameEn;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public int getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(int DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getAdvId() {
            return AdvId;
        }

        public void setAdvId(int AdvId) {
            this.AdvId = AdvId;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }
    }
}
