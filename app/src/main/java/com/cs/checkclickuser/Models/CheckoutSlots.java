package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckoutSlots implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("TimeSlotList")
        private ArrayList<TimeSlotList> TimeSlotList;

        public ArrayList<TimeSlotList> getTimeSlotList() {
            return TimeSlotList;
        }

        public void setTimeSlotList(ArrayList<TimeSlotList> TimeSlotList) {
            this.TimeSlotList = TimeSlotList;
        }
    }

    public static class TimeSlotList implements Serializable{
        @Expose
        @SerializedName("TotalAcceptOrders")
        private int TotalAcceptOrders;
        @Expose
        @SerializedName("Acceptedorders")
        private int Acceptedorders;
        @Expose
        @SerializedName("EndTime")
        private String EndTime;
        @Expose
        @SerializedName("StartTime")
        private String StartTime;
        @Expose
        @SerializedName("TimeSlot")
        private String TimeSlot;
        @Expose
        @SerializedName("SlotId")
        private int SlotId;
        @Expose
        @SerializedName("WeekDayId")
        private int WeekDayId;
        @Expose
        @SerializedName("OrderAcceptedPerHour")
        private int OrderAcceptedPerHour;
        @Expose
        @SerializedName("TimingType")
        private String TimingType;
        @Expose
        @SerializedName("ProductManagementId")
        private int ProductManagementId;
        @Expose
        @SerializedName("Day")
        private String Day;

        public int getTotalAcceptOrders() {
            return TotalAcceptOrders;
        }

        public void setTotalAcceptOrders(int TotalAcceptOrders) {
            this.TotalAcceptOrders = TotalAcceptOrders;
        }

        public int getAcceptedorders() {
            return Acceptedorders;
        }

        public void setAcceptedorders(int Acceptedorders) {
            this.Acceptedorders = Acceptedorders;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getTimeSlot() {
            return TimeSlot;
        }

        public void setTimeSlot(String TimeSlot) {
            this.TimeSlot = TimeSlot;
        }

        public int getSlotId() {
            return SlotId;
        }

        public void setSlotId(int SlotId) {
            this.SlotId = SlotId;
        }

        public int getWeekDayId() {
            return WeekDayId;
        }

        public void setWeekDayId(int WeekDayId) {
            this.WeekDayId = WeekDayId;
        }

        public int getOrderAcceptedPerHour() {
            return OrderAcceptedPerHour;
        }

        public void setOrderAcceptedPerHour(int OrderAcceptedPerHour) {
            this.OrderAcceptedPerHour = OrderAcceptedPerHour;
        }

        public String getTimingType() {
            return TimingType;
        }

        public void setTimingType(String TimingType) {
            this.TimingType = TimingType;
        }

        public int getProductManagementId() {
            return ProductManagementId;
        }

        public void setProductManagementId(int ProductManagementId) {
            this.ProductManagementId = ProductManagementId;
        }

        public String getDay() {
            return Day;
        }

        public void setDay(String Day) {
            this.Day = Day;
        }
    }
}
