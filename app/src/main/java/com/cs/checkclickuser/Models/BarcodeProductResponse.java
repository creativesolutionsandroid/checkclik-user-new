package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BarcodeProductResponse implements Serializable {

    @Expose
    @SerializedName("NameAr")
    private String NameAr;
    @Expose
    @SerializedName("NameEn")
    private String NameEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public String getNameAr() {
        return NameAr;
    }

    public void setNameAr(String NameAr) {
        this.NameAr = NameAr;
    }

    public String getNameEn() {
        return NameEn;
    }

    public void setNameEn(String NameEn) {
        this.NameEn = NameEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
