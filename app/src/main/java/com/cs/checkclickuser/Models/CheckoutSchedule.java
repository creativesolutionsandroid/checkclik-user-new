package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckoutSchedule implements Serializable {

    @Expose
    @SerializedName("RequestDate")
    private String RequestDate;
    @Expose
    @SerializedName("Remarks")
    private String Remarks;
    @Expose
    @SerializedName("CouponId")
    private int CouponId;
    @Expose
    @SerializedName("TimeSlot")
    private String TimeSlot;
    @Expose
    @SerializedName("ShippingProvider")
    private boolean ShippingProvider;
    @Expose
    @SerializedName("IsShippingWorldwide")
    private boolean IsShippingWorldwide;
    @Expose
    @SerializedName("IsDelivery")
    private boolean IsDelivery;
    @Expose
    @SerializedName("InStoreService")
    private boolean InStoreService;
    @Expose
    @SerializedName("ServiceProviderName")
    private String ServiceProviderName;
    @Expose
    @SerializedName("ServiceType")
    private String ServiceType;
    @Expose
    @SerializedName("OrderType")
    private int OrderType;
    @Expose
    @SerializedName("Address")
    private String Address;
    @Expose
    @SerializedName("TotalAmount")
    private double TotalAmount;
    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("valtwo")
    private int valtwo;
    @Expose
    @SerializedName("DeliveryHomeService")
    private String DeliveryHomeService;
    @Expose
    @SerializedName("PreBookingDeliveryTimeUnit")
    private int PreBookingDeliveryTimeUnit;
    @Expose
    @SerializedName("valfirst")
    private int valfirst;
    @Expose
    @SerializedName("PickUpInStoreType")
    private String PickUpInStoreType;
    @Expose
    @SerializedName("PreBookingInStoreTimeUnit")
    private int PreBookingInStoreTimeUnit;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public String getRequestDate() {
        return RequestDate;
    }

    public void setRequestDate(String RequestDate) {
        this.RequestDate = RequestDate;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String Remarks) {
        this.Remarks = Remarks;
    }

    public int getCouponId() {
        return CouponId;
    }

    public void setCouponId(int CouponId) {
        this.CouponId = CouponId;
    }

    public String getTimeSlot() {
        return TimeSlot;
    }

    public void setTimeSlot(String TimeSlot) {
        this.TimeSlot = TimeSlot;
    }

    public boolean getShippingProvider() {
        return ShippingProvider;
    }

    public void setShippingProvider(boolean ShippingProvider) {
        this.ShippingProvider = ShippingProvider;
    }

    public boolean getIsShippingWorldwide() {
        return IsShippingWorldwide;
    }

    public void setIsShippingWorldwide(boolean IsShippingWorldwide) {
        this.IsShippingWorldwide = IsShippingWorldwide;
    }

    public boolean getIsDelivery() {
        return IsDelivery;
    }

    public void setIsDelivery(boolean IsDelivery) {
        this.IsDelivery = IsDelivery;
    }

    public boolean getInStoreService() {
        return InStoreService;
    }

    public void setInStoreService(boolean InStoreService) {
        this.InStoreService = InStoreService;
    }

    public String getServiceProviderName() {
        return ServiceProviderName;
    }

    public void setServiceProviderName(String ServiceProviderName) {
        this.ServiceProviderName = ServiceProviderName;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String ServiceType) {
        this.ServiceType = ServiceType;
    }

    public int getOrderType() {
        return OrderType;
    }

    public void setOrderType(int OrderType) {
        this.OrderType = OrderType;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(double TotalAmount) {
        this.TotalAmount = TotalAmount;
    }

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public int getValtwo() {
        return valtwo;
    }

    public void setValtwo(int valtwo) {
        this.valtwo = valtwo;
    }

    public String getDeliveryHomeService() {
        return DeliveryHomeService;
    }

    public void setDeliveryHomeService(String DeliveryHomeService) {
        this.DeliveryHomeService = DeliveryHomeService;
    }

    public int getPreBookingDeliveryTimeUnit() {
        return PreBookingDeliveryTimeUnit;
    }

    public void setPreBookingDeliveryTimeUnit(int PreBookingDeliveryTimeUnit) {
        this.PreBookingDeliveryTimeUnit = PreBookingDeliveryTimeUnit;
    }

    public int getValfirst() {
        return valfirst;
    }

    public void setValfirst(int valfirst) {
        this.valfirst = valfirst;
    }

    public String getPickUpInStoreType() {
        return PickUpInStoreType;
    }

    public void setPickUpInStoreType(String PickUpInStoreType) {
        this.PickUpInStoreType = PickUpInStoreType;
    }

    public int getPreBookingInStoreTimeUnit() {
        return PreBookingInStoreTimeUnit;
    }

    public void setPreBookingInStoreTimeUnit(int PreBookingInStoreTimeUnit) {
        this.PreBookingInStoreTimeUnit = PreBookingInStoreTimeUnit;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("PoductList")
        private List<PoductList> PoductList;
        @Expose
        @SerializedName("Coupons")
        private List<CouponList> Coupons;
        @Expose
        @SerializedName("DeliveryHomeWeekdays")
        private List<DeliveryHomeWeekdays> DeliveryHomeWeekdays;
        @Expose
        @SerializedName("PickupInServiceWeekdays")
        private List<PickupInServiceWeekdays> PickupInServiceWeekdays;

        public List<PoductList> getPoductList() {
            return PoductList;
        }

        public void setPoductList(List<PoductList> PoductList) {
            this.PoductList = PoductList;
        }

        public List<CouponList> getCoupons() {
            return Coupons;
        }

        public void setCoupons(List<CouponList> Coupons) {
            this.Coupons = Coupons;
        }

        public List<DeliveryHomeWeekdays> getDeliveryHomeWeekdays() {
            return DeliveryHomeWeekdays;
        }

        public void setDeliveryHomeWeekdays(List<DeliveryHomeWeekdays> DeliveryHomeWeekdays) {
            this.DeliveryHomeWeekdays = DeliveryHomeWeekdays;
        }

        public List<PickupInServiceWeekdays> getPickupInServiceWeekdays() {
            return PickupInServiceWeekdays;
        }

        public void setPickupInServiceWeekdays(List<PickupInServiceWeekdays> PickupInServiceWeekdays) {
            this.PickupInServiceWeekdays = PickupInServiceWeekdays;
        }
    }

    public static class PoductList implements Serializable{
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("TotalItemPrice")
        private float TotalItemPrice;
        @Expose
        @SerializedName("CartQuantity")
        private int CartQuantity;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("Image")
        private String Image;

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public float getTotalItemPrice() {
            return TotalItemPrice;
        }

        public void setTotalItemPrice(float TotalItemPrice) {
            this.TotalItemPrice = TotalItemPrice;
        }

        public int getCartQuantity() {
            return CartQuantity;
        }

        public void setCartQuantity(int CartQuantity) {
            this.CartQuantity = CartQuantity;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }
    }

    public static class DeliveryHomeWeekdays implements Serializable{
        @Expose
        @SerializedName("Weekday")
        private String Weekday;
        @Expose
        @SerializedName("WeekdayId")
        private int WeekdayId;

        public String getWeekday() {
            return Weekday;
        }

        public void setWeekday(String Weekday) {
            this.Weekday = Weekday;
        }

        public int getWeekdayId() {
            return WeekdayId;
        }

        public void setWeekdayId(int WeekdayId) {
            this.WeekdayId = WeekdayId;
        }
    }

    public static class PickupInServiceWeekdays implements Serializable{
        @Expose
        @SerializedName("Weekday")
        private String Weekday;
        @Expose
        @SerializedName("WeekdayId")
        private int WeekdayId;

        public String getWeekday() {
            return Weekday;
        }

        public void setWeekday(String Weekday) {
            this.Weekday = Weekday;
        }

        public int getWeekdayId() {
            return WeekdayId;
        }

        public void setWeekdayId(int WeekdayId) {
            this.WeekdayId = WeekdayId;
        }
    }

    public static class CouponList implements Serializable {
        @Expose
        @SerializedName("Branches")
        private ArrayList<CartResponce.Branches> Branches;
        @Expose
        @SerializedName("Regions")
        private ArrayList<CartResponce.Regions> Regions;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("BackgroundCopy")
        private String BackgroundCopy;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("VoucherAmount")
        private double VoucherAmount;
        @Expose
        @SerializedName("CouponUsedCount")
        private int CouponUsedCount;
        @Expose
        @SerializedName("NTimes")
        private int NTimes;
        @Expose
        @SerializedName("DiscountLimitation")
        private int DiscountLimitation;
        @Expose
        @SerializedName("CouponCode")
        private String CouponCode;
        @Expose
        @SerializedName("IsCouponCodeRequires")
        private boolean IsCouponCodeRequires;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private double MaxDiscountAmount;
        @Expose
        @SerializedName("IsUptoApplicable")
        private boolean IsUptoApplicable;
        @Expose
        @SerializedName("VoucherDiscount")
        private int VoucherDiscount;
        @Expose
        @SerializedName("VoucherNameEn")
        private String VoucherNameEn;
        @Expose
        @SerializedName("UserSlabID")
        private int UserSlabID;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("Id")
        private int Id;

        public ArrayList<CartResponce.Branches> getBranches() {
            return Branches;
        }

        public void setBranches(ArrayList<CartResponce.Branches> Branches) {
            this.Branches = Branches;
        }

        public ArrayList<CartResponce.Regions> getRegions() {
            return Regions;
        }

        public void setRegions(ArrayList<CartResponce.Regions> Regions) {
            this.Regions = Regions;
        }

        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String LogoCopy) {
            this.LogoCopy = LogoCopy;
        }

        public String getBackgroundCopy() {
            return BackgroundCopy;
        }

        public void setBackgroundCopy(String BackgroundCopy) {
            this.BackgroundCopy = BackgroundCopy;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public double getVoucherAmount() {
            return VoucherAmount;
        }

        public void setVoucherAmount(double VoucherAmount) {
            this.VoucherAmount = VoucherAmount;
        }

        public int getCouponUsedCount() {
            return CouponUsedCount;
        }

        public void setCouponUsedCount(int CouponUsedCount) {
            this.CouponUsedCount = CouponUsedCount;
        }

        public int getNTimes() {
            return NTimes;
        }

        public void setNTimes(int NTimes) {
            this.NTimes = NTimes;
        }

        public int getDiscountLimitation() {
            return DiscountLimitation;
        }

        public void setDiscountLimitation(int DiscountLimitation) {
            this.DiscountLimitation = DiscountLimitation;
        }

        public String getCouponCode() {
            return CouponCode;
        }

        public void setCouponCode(String CouponCode) {
            this.CouponCode = CouponCode;
        }

        public boolean getIsCouponCodeRequires() {
            return IsCouponCodeRequires;
        }

        public void setIsCouponCodeRequires(boolean IsCouponCodeRequires) {
            this.IsCouponCodeRequires = IsCouponCodeRequires;
        }

        public double getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(int MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public boolean getIsUptoApplicable() {
            return IsUptoApplicable;
        }

        public void setIsUptoApplicable(boolean IsUptoApplicable) {
            this.IsUptoApplicable = IsUptoApplicable;
        }

        public int getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(int VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public String getVoucherNameEn() {
            return VoucherNameEn;
        }

        public void setVoucherNameEn(String VoucherNameEn) {
            this.VoucherNameEn = VoucherNameEn;
        }

        public int getUserSlabID() {
            return UserSlabID;
        }

        public void setUserSlabID(int UserSlabID) {
            this.UserSlabID = UserSlabID;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Branches {
        @Expose
        @SerializedName("BranchId")
        private int BranchId;

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }
    }

    public static class Regions {
        @Expose
        @SerializedName("RegionId")
        private int RegionId;

        public int getRegionId() {
            return RegionId;
        }

        public void setRegionId(int RegionId) {
            this.RegionId = RegionId;
        }
    }
}
