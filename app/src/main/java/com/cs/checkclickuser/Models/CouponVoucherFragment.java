package com.cs.checkclickuser.Models;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.MainActivity;
import com.cs.checkclickuser.Adapter.CouponVoucherAdapter;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponVoucherFragment extends Fragment {
    View rootView;
    ImageView back_Btn;
    RecyclerView couponlist;
    CouponVoucherAdapter mCouponAdapter;
    public static final String TAG = "TAG";
    private ArrayList<CouponResponce.Data> couponArrayList = new ArrayList<>();



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.coupon_fragment, container, false);

        back_Btn=(ImageView)rootView.findViewById(R.id.back_btn);
        couponlist=(RecyclerView)rootView.findViewById(R.id.couponslist);

        back_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
            }
        });

        new categoryApi().execute();

    return rootView;
    }

    private class categoryApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(getActivity());
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(getActivity());
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CouponResponce> call = apiService.getcoupons(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CouponResponce>() {
                @Override
                public void onResponse(Call<CouponResponce> call, Response<CouponResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        CouponResponce CouponResponce = response.body();
                        try {
                            if (response.isSuccessful()) {
                                    CouponResponce coupons = response.body();

                                    if (coupons.getStatus()) {
                                        couponArrayList = coupons.getData();
                                        Log.d(TAG, "coupon arry size: "+couponArrayList.size());

                                        mCouponAdapter = new CouponVoucherAdapter(getContext(), couponArrayList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                        couponlist.setLayoutManager(new GridLayoutManager(getContext(),1 ));
                                        couponlist.setAdapter(mCouponAdapter);
                                    }
                                }
                            else {
                                String failureResponse = CouponResponce.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), getActivity());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<CouponResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(getActivity(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(getActivity(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {
//
            parentObj.put("StoreId", 38);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

}
