package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeCityResponse {


    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("DistrictList")
        private List<DistrictList> districtlist;
        @Expose
        @SerializedName("CityList")
        private List<CityList> citylist;

        public List<DistrictList> getDistrictlist() {
            return districtlist;
        }

        public void setDistrictlist(List<DistrictList> districtlist) {
            this.districtlist = districtlist;
        }

        public List<CityList> getCitylist() {
            return citylist;
        }

        public void setCitylist(List<CityList> citylist) {
            this.citylist = citylist;
        }
    }

    public static class DistrictList {
        @Expose
        @SerializedName("CityAr")
        private String cityar;
        @Expose
        @SerializedName("CityEn")
        private String cityen;
        @Expose
        @SerializedName("CityId")
        private int cityid;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getCityar() {
            return cityar;
        }

        public void setCityar(String cityar) {
            this.cityar = cityar;
        }

        public String getCityen() {
            return cityen;
        }

        public void setCityen(String cityen) {
            this.cityen = cityen;
        }

        public int getCityid() {
            return cityid;
        }

        public void setCityid(int cityid) {
            this.cityid = cityid;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class CityList {
        @Expose
        @SerializedName("CreatedBy")
        private int createdby;
        @Expose
        @SerializedName("CreatedOn")
        private String createdon;
        @Expose
        @SerializedName("IsActive")
        private boolean isactive;
        @Expose
        @SerializedName("CountryAr")
        private String countryar;
        @Expose
        @SerializedName("CountryEn")
        private String countryen;
        @Expose
        @SerializedName("CountryId")
        private int countryid;
        @Expose
        @SerializedName("RegionAr")
        private String regionar;
        @Expose
        @SerializedName("RegionEn")
        private String regionen;
        @Expose
        @SerializedName("RegionId")
        private int regionid;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Id")
        private int id;

        public int getCreatedby() {
            return createdby;
        }

        public void setCreatedby(int createdby) {
            this.createdby = createdby;
        }

        public String getCreatedon() {
            return createdon;
        }

        public void setCreatedon(String createdon) {
            this.createdon = createdon;
        }

        public boolean getIsactive() {
            return isactive;
        }

        public void setIsactive(boolean isactive) {
            this.isactive = isactive;
        }

        public String getCountryar() {
            return countryar;
        }

        public void setCountryar(String countryar) {
            this.countryar = countryar;
        }

        public String getCountryen() {
            return countryen;
        }

        public void setCountryen(String countryen) {
            this.countryen = countryen;
        }

        public int getCountryid() {
            return countryid;
        }

        public void setCountryid(int countryid) {
            this.countryid = countryid;
        }

        public String getRegionar() {
            return regionar;
        }

        public void setRegionar(String regionar) {
            this.regionar = regionar;
        }

        public String getRegionen() {
            return regionen;
        }

        public void setRegionen(String regionen) {
            this.regionen = regionen;
        }

        public int getRegionid() {
            return regionid;
        }

        public void setRegionid(int regionid) {
            this.regionid = regionid;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
