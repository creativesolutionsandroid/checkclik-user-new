package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;


public class SearchListResponce {
    @Expose
    @SerializedName("Data")
    private ArrayList<String> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<String> getData() {
        return Data;
    }

    public void setData(ArrayList<String> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }
}
