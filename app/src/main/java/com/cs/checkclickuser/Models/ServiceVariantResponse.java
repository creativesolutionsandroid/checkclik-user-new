package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceVariantResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable{
        @Expose
        @SerializedName("ServiceTypeList")
        private ArrayList<ServiceTypeList> ServiceTypeList;
        @Expose
        @SerializedName("ServiceDetails")
        private ArrayList<ServiceDetails> ServiceDetails;

        public ArrayList<ServiceTypeList> getServiceTypeList() {
            return ServiceTypeList;
        }

        public void setServiceTypeList(ArrayList<ServiceTypeList> ServiceTypeList) {
            this.ServiceTypeList = ServiceTypeList;
        }

        public ArrayList<ServiceDetails> getServiceDetails() {
            return ServiceDetails;
        }

        public void setServiceDetails(ArrayList<ServiceDetails> ServiceDetails) {
            this.ServiceDetails = ServiceDetails;
        }
    }

    public static class ServiceTypeList implements Serializable{
        @Expose
        @SerializedName("JServiceProvidersJson")
        private ArrayList<JServiceProvidersJson> JServiceProvidersJson;
        @Expose
        @SerializedName("ServiceProvidersJson")
        private String ServiceProvidersJson;
        @Expose
        @SerializedName("ServiceType")
        private String ServiceType;
        @Expose
        @SerializedName("ServiceTypeId")
        private int ServiceTypeId;

        public ArrayList<JServiceProvidersJson> getJServiceProvidersJson() {
            return JServiceProvidersJson;
        }

        public void setJServiceProvidersJson(ArrayList<JServiceProvidersJson> JServiceProvidersJson) {
            this.JServiceProvidersJson = JServiceProvidersJson;
        }

        public String getServiceProvidersJson() {
            return ServiceProvidersJson;
        }

        public void setServiceProvidersJson(String ServiceProvidersJson) {
            this.ServiceProvidersJson = ServiceProvidersJson;
        }

        public String getServiceType() {
            return ServiceType;
        }

        public void setServiceType(String ServiceType) {
            this.ServiceType = ServiceType;
        }

        public int getServiceTypeId() {
            return ServiceTypeId;
        }

        public void setServiceTypeId(int ServiceTypeId) {
            this.ServiceTypeId = ServiceTypeId;
        }
    }

    public static class JServiceProvidersJson implements Serializable{
        @Expose
        @SerializedName("ServiceProviderName")
        private String ServiceProviderName;
        @Expose
        @SerializedName("ServiceProviderId")
        private int ServiceProviderId;

        public String getServiceProviderName() {
            return ServiceProviderName;
        }

        public void setServiceProviderName(String ServiceProviderName) {
            this.ServiceProviderName = ServiceProviderName;
        }

        public int getServiceProviderId() {
            return ServiceProviderId;
        }

        public void setServiceProviderId(int ServiceProviderId) {
            this.ServiceProviderId = ServiceProviderId;
        }
    }

    public static class ServiceDetails implements Serializable{
        @Expose
        @SerializedName("JRelatedServices")
        private List<JRelatedServices> JRelatedServices;
        @Expose
        @SerializedName("JReviews")
        private List<String> JReviews;
        @Expose
        @SerializedName("JImages")
        private List<JImages> JImages;
        @Expose
        @SerializedName("SubCatAr")
        private String SubCatAr;
        @Expose
        @SerializedName("SubCatEn")
        private String SubCatEn;
        @Expose
        @SerializedName("SubCatId")
        private int SubCatId;
        @Expose
        @SerializedName("MainCatAr")
        private String MainCatAr;
        @Expose
        @SerializedName("MainCatEn")
        private String MainCatEn;
        @Expose
        @SerializedName("MainCatId")
        private int MainCatId;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("UserReviews")
        private int UserReviews;
        @Expose
        @SerializedName("PurchaseCount")
        private int PurchaseCount;
        @Expose
        @SerializedName("Reviews")
        private String Reviews;
        @Expose
        @SerializedName("RelatedService")
        private String RelatedService;
        @Expose
        @SerializedName("Images")
        private String Images;
        @Expose
        @SerializedName("ServiceCountingNameAr")
        private String ServiceCountingNameAr;
        @Expose
        @SerializedName("ServiceCountingNameEn")
        private String ServiceCountingNameEn;
        @Expose
        @SerializedName("ServiceCountingMeasure")
        private int ServiceCountingMeasure;
        @Expose
        @SerializedName("advId")
        private int advId;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private int DiscountValue;
        @Expose
        @SerializedName("MetaDescriptionAr")
        private String MetaDescriptionAr;
        @Expose
        @SerializedName("MetaDescriptionEn")
        private String MetaDescriptionEn;
        @Expose
        @SerializedName("DescriptionAr")
        private String DescriptionAr;
        @Expose
        @SerializedName("DescriptionEn")
        private String DescriptionEn;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("CartItemCount")
        private int CartItemCount;
        @Expose
        @SerializedName("NotificationCount")
        private int NotificationCount;

        public int getCartItemCount() {
            return CartItemCount;
        }

        public void setCartItemCount(int cartItemCount) {
            CartItemCount = cartItemCount;
        }

        public int getNotificationCount() {
            return NotificationCount;
        }

        public void setNotificationCount(int notificationCount) {
            NotificationCount = notificationCount;
        }
        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String logoCopy) {
            LogoCopy = logoCopy;
        }

        public List<JRelatedServices> getJRelatedServices() {
            return JRelatedServices;
        }

        public void setJRelatedServices(List<JRelatedServices> JRelatedServices) {
            this.JRelatedServices = JRelatedServices;
        }

        public List<String> getJReviews() {
            return JReviews;
        }

        public void setJReviews(List<String> JReviews) {
            this.JReviews = JReviews;
        }

        public List<JImages> getJImages() {
            return JImages;
        }

        public void setJImages(List<JImages> JImages) {
            this.JImages = JImages;
        }

        public String getSubCatAr() {
            return SubCatAr;
        }

        public void setSubCatAr(String SubCatAr) {
            this.SubCatAr = SubCatAr;
        }

        public String getSubCatEn() {
            return SubCatEn;
        }

        public void setSubCatEn(String SubCatEn) {
            this.SubCatEn = SubCatEn;
        }

        public int getSubCatId() {
            return SubCatId;
        }

        public void setSubCatId(int SubCatId) {
            this.SubCatId = SubCatId;
        }

        public String getMainCatAr() {
            return MainCatAr;
        }

        public void setMainCatAr(String MainCatAr) {
            this.MainCatAr = MainCatAr;
        }

        public String getMainCatEn() {
            return MainCatEn;
        }

        public void setMainCatEn(String MainCatEn) {
            this.MainCatEn = MainCatEn;
        }

        public int getMainCatId() {
            return MainCatId;
        }

        public void setMainCatId(int MainCatId) {
            this.MainCatId = MainCatId;
        }

        public boolean getFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean Favorite) {
            this.Favorite = Favorite;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public int getUserReviews() {
            return UserReviews;
        }

        public void setUserReviews(int UserReviews) {
            this.UserReviews = UserReviews;
        }

        public int getPurchaseCount() {
            return PurchaseCount;
        }

        public void setPurchaseCount(int PurchaseCount) {
            this.PurchaseCount = PurchaseCount;
        }

        public String getReviews() {
            return Reviews;
        }

        public void setReviews(String Reviews) {
            this.Reviews = Reviews;
        }

        public String getRelatedService() {
            return RelatedService;
        }

        public void setRelatedService(String RelatedService) {
            this.RelatedService = RelatedService;
        }

        public String getImages() {
            return Images;
        }

        public void setImages(String Images) {
            this.Images = Images;
        }

        public String getServiceCountingNameAr() {
            return ServiceCountingNameAr;
        }

        public void setServiceCountingNameAr(String ServiceCountingNameAr) {
            this.ServiceCountingNameAr = ServiceCountingNameAr;
        }

        public String getServiceCountingNameEn() {
            return ServiceCountingNameEn;
        }

        public void setServiceCountingNameEn(String ServiceCountingNameEn) {
            this.ServiceCountingNameEn = ServiceCountingNameEn;
        }

        public int getServiceCountingMeasure() {
            return ServiceCountingMeasure;
        }

        public void setServiceCountingMeasure(int ServiceCountingMeasure) {
            this.ServiceCountingMeasure = ServiceCountingMeasure;
        }

        public int getAdvId() {
            return advId;
        }

        public void setAdvId(int advId) {
            this.advId = advId;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public int getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(int DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public String getMetaDescriptionAr() {
            return MetaDescriptionAr;
        }

        public void setMetaDescriptionAr(String MetaDescriptionAr) {
            this.MetaDescriptionAr = MetaDescriptionAr;
        }

        public String getMetaDescriptionEn() {
            return MetaDescriptionEn;
        }

        public void setMetaDescriptionEn(String MetaDescriptionEn) {
            this.MetaDescriptionEn = MetaDescriptionEn;
        }

        public String getDescriptionAr() {
            return DescriptionAr;
        }

        public void setDescriptionAr(String DescriptionAr) {
            this.DescriptionAr = DescriptionAr;
        }

        public String getDescriptionEn() {
            return DescriptionEn;
        }

        public void setDescriptionEn(String DescriptionEn) {
            this.DescriptionEn = DescriptionEn;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }
    }

    public static class JRelatedServices implements Serializable{
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private int DiscountValue;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public int getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(int DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }
    }

    public static class JImages implements Serializable{
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
