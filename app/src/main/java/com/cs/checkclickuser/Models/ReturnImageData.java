package com.cs.checkclickuser.Models;

public class ReturnImageData {

    private int position;
    private int qty;
    private String imageName1;
    private String imageData1;
    private String imageName2;
    private String imageData2;
    private String imageName3;
    private String imageData3;
    private String imageName4;
    private String imageData4;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getImageName1() {
        return imageName1;
    }

    public void setImageName1(String imageName1) {
        this.imageName1 = imageName1;
    }

    public String getImageData1() {
        return imageData1;
    }

    public void setImageData1(String imageData1) {
        this.imageData1 = imageData1;
    }

    public String getImageName2() {
        return imageName2;
    }

    public void setImageName2(String imageName2) {
        this.imageName2 = imageName2;
    }

    public String getImageData2() {
        return imageData2;
    }

    public void setImageData2(String imageData2) {
        this.imageData2 = imageData2;
    }

    public String getImageName3() {
        return imageName3;
    }

    public void setImageName3(String imageName3) {
        this.imageName3 = imageName3;
    }

    public String getImageData3() {
        return imageData3;
    }

    public void setImageData3(String imageData3) {
        this.imageData3 = imageData3;
    }

    public String getImageName4() {
        return imageName4;
    }

    public void setImageName4(String imageName4) {
        this.imageName4 = imageName4;
    }

    public String getImageData4() {
        return imageData4;
    }

    public void setImageData4(String imageData4) {
        this.imageData4 = imageData4;
    }
}
