package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethods {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("MessageAr")
    private String MessageAr;
    @Expose
    @SerializedName("MessageEn")
    private String MessageEn;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessageAr() {
        return MessageAr;
    }

    public void setMessageAr(String MessageAr) {
        this.MessageAr = MessageAr;
    }

    public String getMessageEn() {
        return MessageEn;
    }

    public void setMessageEn(String MessageEn) {
        this.MessageEn = MessageEn;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("ExpectedDate")
        private String ExpectedDate;
        @Expose
        @SerializedName("CityName")
        private String CityName;
        @Expose
        @SerializedName("IsShipment")
        private int IsShipment;
        @Expose
        @SerializedName("PaymentData")
        private List<PaymentData> PaymentData;

        public String getExpectedDate() {
            return ExpectedDate;
        }

        public void setExpectedDate(String ExpectedDate) {
            this.ExpectedDate = ExpectedDate;
        }

        public String getCityName() {
            return CityName;
        }

        public void setCityName(String CityName) {
            this.CityName = CityName;
        }

        public int getIsShipment() {
            return IsShipment;
        }

        public void setIsShipment(int IsShipment) {
            this.IsShipment = IsShipment;
        }

        public List<PaymentData> getPaymentData() {
            return PaymentData;
        }

        public void setPaymentData(List<PaymentData> PaymentData) {
            this.PaymentData = PaymentData;
        }
    }

    public static class PaymentData {
        @Expose
        @SerializedName("IsApplePayAllowed")
        private boolean IsApplePayAllowed;
        @Expose
        @SerializedName("IsMadaCardAllowed")
        private boolean IsMadaCardAllowed;
        @Expose
        @SerializedName("IsCcPayLater")
        private boolean IsCcPayLater;
        @Expose
        @SerializedName("IsCcPayNow")
        private boolean IsCcPayNow;
        @Expose
        @SerializedName("IsCreditCardAllowed")
        private boolean IsCreditCardAllowed;
        @Expose
        @SerializedName("IsCashAllowed")
        private boolean IsCashAllowed;
        @Expose
        @SerializedName("CityId")
        private int CityId;
        @Expose
        @SerializedName("CountryId")
        private int CountryId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Id")
        private int Id;

        public boolean getIsApplePayAllowed() {
            return IsApplePayAllowed;
        }

        public void setIsApplePayAllowed(boolean IsApplePayAllowed) {
            this.IsApplePayAllowed = IsApplePayAllowed;
        }

        public boolean getIsMadaCardAllowed() {
            return IsMadaCardAllowed;
        }

        public void setIsMadaCardAllowed(boolean IsMadaCardAllowed) {
            this.IsMadaCardAllowed = IsMadaCardAllowed;
        }

        public boolean getIsCcPayLater() {
            return IsCcPayLater;
        }

        public void setIsCcPayLater(boolean IsCcPayLater) {
            this.IsCcPayLater = IsCcPayLater;
        }

        public boolean getIsCcPayNow() {
            return IsCcPayNow;
        }

        public void setIsCcPayNow(boolean IsCcPayNow) {
            this.IsCcPayNow = IsCcPayNow;
        }

        public boolean getIsCreditCardAllowed() {
            return IsCreditCardAllowed;
        }

        public void setIsCreditCardAllowed(boolean IsCreditCardAllowed) {
            this.IsCreditCardAllowed = IsCreditCardAllowed;
        }

        public boolean getIsCashAllowed() {
            return IsCashAllowed;
        }

        public void setIsCashAllowed(boolean IsCashAllowed) {
            this.IsCashAllowed = IsCashAllowed;
        }

        public int getCityId() {
            return CityId;
        }

        public void setCityId(int CityId) {
            this.CityId = CityId;
        }

        public int getCountryId() {
            return CountryId;
        }

        public void setCountryId(int CountryId) {
            this.CountryId = CountryId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
