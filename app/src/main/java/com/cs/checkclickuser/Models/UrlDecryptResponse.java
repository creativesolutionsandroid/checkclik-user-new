package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UrlDecryptResponse {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int BranchSubCategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("Id")
        private int Id;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int storeId) {
            StoreId = storeId;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public int getBranchSubCategoryId() {
            return BranchSubCategoryId;
        }

        public void setBranchSubCategoryId(int BranchSubCategoryId) {
            this.BranchSubCategoryId = BranchSubCategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
