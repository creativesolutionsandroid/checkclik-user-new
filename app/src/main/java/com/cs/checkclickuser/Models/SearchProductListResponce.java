package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchProductListResponce {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("OriginalResultsCount")
        private int OriginalResultsCount;
        @Expose
        @SerializedName("MaxPrice")
        private float MaxPrice;
        @Expose
        @SerializedName("MinPrice")
        private float MinPrice;
        @Expose
        @SerializedName("StaticFacets")
        private StaticFacets StaticFacets;
        @Expose
        @SerializedName("Facets")
        private List<Facet> Facets;
        @Expose
        @SerializedName("DidYouMean")
        private List<String> DidYouMean;
        @Expose
        @SerializedName("OriginalQuery")
        private OriginalQuery OriginalQuery;
        @Expose
        @SerializedName("Status")
        private int Status;
        @Expose
        @SerializedName("QueryTime")
        private int QueryTime;
        @Expose
        @SerializedName("NGroups")
        private int NGroups;
        @Expose
        @SerializedName("NumFound")
        private int NumFound;
        @Expose
        @SerializedName("Results")
        private List<Result> Results;


        public int getOriginalResultsCount() {
            return OriginalResultsCount;
        }

        public void setOriginalResultsCount(int OriginalResultsCount) {
            this.OriginalResultsCount = OriginalResultsCount;
        }

        public float getMaxPrice() {
            return MaxPrice;
        }

        public void setMaxPrice(float MaxPrice) {
            this.MaxPrice = MaxPrice;
        }

        public float getMinPrice() {
            return MinPrice;
        }

        public void setMinPrice(float MinPrice) {
            this.MinPrice = MinPrice;
        }

        public StaticFacets getStaticFacets() {
            return StaticFacets;
        }

        public void setStaticFacets(StaticFacets StaticFacets) {
            this.StaticFacets = StaticFacets;
        }

        public List<Facet> getFacets() {
            return Facets;
        }

        public void setFacets(List<Facet> Facets) {
            this.Facets = Facets;
        }

        public List<String> getDidYouMean() {
            return DidYouMean;
        }

        public void setDidYouMean(List<String> DidYouMean) {
            this.DidYouMean = DidYouMean;
        }

        public OriginalQuery getOriginalQuery() {
            return OriginalQuery;
        }

        public void setOriginalQuery(OriginalQuery OriginalQuery) {
            this.OriginalQuery = OriginalQuery;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int Status) {
            this.Status = Status;
        }

        public int getQueryTime() {
            return QueryTime;
        }

        public void setQueryTime(int QueryTime) {
            this.QueryTime = QueryTime;
        }

        public int getNGroups() {
            return NGroups;
        }

        public void setNGroups(int NGroups) {
            this.NGroups = NGroups;
        }

        public int getNumFound() {
            return NumFound;
        }

        public void setNumFound(int NumFound) {
            this.NumFound = NumFound;
        }

        public List<Result> getResults() {
            return Results;
        }

        public void setResults(List<Result> Results) {
            this.Results = Results;
        }
    }

    public static class StaticFacets {
        @Expose
        @SerializedName("MaxPrice")
        private int MaxPrice;
        @Expose
        @SerializedName("MinPrice")
        private int MinPrice;
        @Expose
        @SerializedName("IsDCPL")
        private boolean IsDCPL;
        @Expose
        @SerializedName("IsDCPN")
        private boolean IsDCPN;
        @Expose
        @SerializedName("IsCCPL")
        private boolean IsCCPL;
        @Expose
        @SerializedName("IsCCPN")
        private boolean IsCCPN;
        @Expose
        @SerializedName("IsCOD")
        private boolean IsCOD;
        @Expose
        @SerializedName("IsShipping")
        private boolean IsShipping;
        @Expose
        @SerializedName("IsDelivery")
        private boolean IsDelivery;
        @Expose
        @SerializedName("IsPickup")
        private boolean IsPickup;

        public int getMaxPrice() {
            return MaxPrice;
        }

        public void setMaxPrice(int MaxPrice) {
            this.MaxPrice = MaxPrice;
        }

        public int getMinPrice() {
            return MinPrice;
        }

        public void setMinPrice(int MinPrice) {
            this.MinPrice = MinPrice;
        }

        public boolean getIsDCPL() {
            return IsDCPL;
        }

        public void setIsDCPL(boolean IsDCPL) {
            this.IsDCPL = IsDCPL;
        }

        public boolean getIsDCPN() {
            return IsDCPN;
        }

        public void setIsDCPN(boolean IsDCPN) {
            this.IsDCPN = IsDCPN;
        }

        public boolean getIsCCPL() {
            return IsCCPL;
        }

        public void setIsCCPL(boolean IsCCPL) {
            this.IsCCPL = IsCCPL;
        }

        public boolean getIsCCPN() {
            return IsCCPN;
        }

        public void setIsCCPN(boolean IsCCPN) {
            this.IsCCPN = IsCCPN;
        }

        public boolean getIsCOD() {
            return IsCOD;
        }

        public void setIsCOD(boolean IsCOD) {
            this.IsCOD = IsCOD;
        }

        public boolean getIsShipping() {
            return IsShipping;
        }

        public void setIsShipping(boolean IsShipping) {
            this.IsShipping = IsShipping;
        }

        public boolean getIsDelivery() {
            return IsDelivery;
        }

        public void setIsDelivery(boolean IsDelivery) {
            this.IsDelivery = IsDelivery;
        }

        public boolean getIsPickup() {
            return IsPickup;
        }

        public void setIsPickup(boolean IsPickup) {
            this.IsPickup = IsPickup;
        }
    }

    public static class OriginalQuery {
        @Expose
        @SerializedName("StaticFacets")
        private StaticFacets StaticFacets;
        @Expose
        @SerializedName("Facets")
        private List<Facets> Facets;
        @Expose
        @SerializedName("Language")
        private String Language;
        @Expose
        @SerializedName("Location")
        private String Location;
        @Expose
        @SerializedName("Rows")
        private int Rows;
        @Expose
        @SerializedName("Start")
        private int Start;
        @Expose
        @SerializedName("Query")
        private String Query;

        public StaticFacets getStaticFacets() {
            return StaticFacets;
        }

        public void setStaticFacets(StaticFacets StaticFacets) {
            this.StaticFacets = StaticFacets;
        }

        public List<Facets> getFacets() {
            return Facets;
        }

        public void setFacets(List<Facets> Facets) {
            this.Facets = Facets;
        }

        public String getLanguage() {
            return Language;
        }

        public void setLanguage(String Language) {
            this.Language = Language;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public int getRows() {
            return Rows;
        }

        public void setRows(int Rows) {
            this.Rows = Rows;
        }

        public int getStart() {
            return Start;
        }

        public void setStart(int Start) {
            this.Start = Start;
        }

        public String getQuery() {
            return Query;
        }

        public void setQuery(String Query) {
            this.Query = Query;
        }
    }
    public static class Facets {
        @Expose
        @SerializedName("SelectedFacets")
        private List<String> SelectedFacets;
        @Expose
        @SerializedName("FacetsAr")
        private String FacetsAr;
        @Expose
        @SerializedName("FacetsEn")
        private String FacetsEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public List<String> getSelectedFacets() {
            return SelectedFacets;
        }

        public void setSelectedFacets(List<String> SelectedFacets) {
            this.SelectedFacets = SelectedFacets;
        }

        public String getFacetsAr() {
            return FacetsAr;
        }

        public void setFacetsAr(String FacetsAr) {
            this.FacetsAr = FacetsAr;
        }

        public String getFacetsEn() {
            return FacetsEn;
        }

        public void setFacetsEn(String FacetsEn) {
            this.FacetsEn = FacetsEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public class OtherFields {

        @SerializedName("IsDCPN")
        @Expose
        private Boolean isDCPN;
        @SerializedName("IsDCPL")
        @Expose
        private Boolean isDCPL;
        @SerializedName("Size#2_facet")
        @Expose
        private String size2Facet;
        @SerializedName("Size#2_facet_ar")
        @Expose
        private String size2FacetAr;
        @SerializedName("MinDeliveryTime")
        @Expose
        private Integer minDeliveryTime;
        @SerializedName("ManufacturerEn")
        @Expose
        private String manufacturerEn;
        @SerializedName("MainCategoryEn")
        @Expose
        private List<String> mainCategoryEn = null;
        @SerializedName("MainCategoryAr")
        @Expose
        private List<String> mainCategoryAr = null;
        @SerializedName("IsCCPL")
        @Expose
        private Boolean isCCPL;
        @SerializedName("IsCCPN")
        @Expose
        private Boolean isCCPN;
        @SerializedName("BranchSubCategoryEn")
        @Expose
        private String branchSubCategoryEn;
        @SerializedName("SkuId")
        @Expose
        private String skuId;
        @SerializedName("BranchSubCategoryAr")
        @Expose
        private String branchSubCategoryAr;
        @SerializedName("IsDelivery")
        @Expose
        private Boolean isDelivery;
        @SerializedName("Color#3_facet_ar")
        @Expose
        private String color3FacetAr;
        @SerializedName("Color#3_facet")
        @Expose
        private String color3Facet;
        @SerializedName("CreatedOn")
        @Expose
        private String createdOn;
        @SerializedName("IsShipping")
        @Expose
        private Boolean isShipping;
        @SerializedName("DeliveryCharge")
        @Expose
        private Integer deliveryCharge;
        @SerializedName("ManufacturerAr")
        @Expose
        private String manufacturerAr;
        @SerializedName("BranchCategoryAr")
        @Expose
        private String branchCategoryAr;
        @SerializedName("MinPickupTime")
        @Expose
        private Integer minPickupTime;
        @SerializedName("location")
        @Expose
        private List<String> location = null;
        @SerializedName("BranchCategoryEn")
        @Expose
        private String branchCategoryEn;
        @SerializedName("IsCOD")
        @Expose
        private Boolean isCOD;
        @SerializedName("IsPickup")
        @Expose
        private Boolean isPickup;
        @SerializedName("MinOrderValue")
        @Expose
        private Boolean minOrderValue;
        @SerializedName("_version_")
        @Expose
        private long version;

        public Boolean getIsDCPN() {
            return isDCPN;
        }

        public void setIsDCPN(Boolean isDCPN) {
            this.isDCPN = isDCPN;
        }

        public Boolean getIsDCPL() {
            return isDCPL;
        }

        public void setIsDCPL(Boolean isDCPL) {
            this.isDCPL = isDCPL;
        }

        public String getSize2Facet() {
            return size2Facet;
        }

        public void setSize2Facet(String size2Facet) {
            this.size2Facet = size2Facet;
        }

        public String getSize2FacetAr() {
            return size2FacetAr;
        }

        public void setSize2FacetAr(String size2FacetAr) {
            this.size2FacetAr = size2FacetAr;
        }

        public Integer getMinDeliveryTime() {
            return minDeliveryTime;
        }

        public void setMinDeliveryTime(Integer minDeliveryTime) {
            this.minDeliveryTime = minDeliveryTime;
        }

        public String getManufacturerEn() {
            return manufacturerEn;
        }

        public void setManufacturerEn(String manufacturerEn) {
            this.manufacturerEn = manufacturerEn;
        }

        public List<String> getMainCategoryEn() {
            return mainCategoryEn;
        }

        public void setMainCategoryEn(List<String> mainCategoryEn) {
            this.mainCategoryEn = mainCategoryEn;
        }

        public List<String> getMainCategoryAr() {
            return mainCategoryAr;
        }

        public void setMainCategoryAr(List<String> mainCategoryAr) {
            this.mainCategoryAr = mainCategoryAr;
        }

        public Boolean getIsCCPL() {
            return isCCPL;
        }

        public void setIsCCPL(Boolean isCCPL) {
            this.isCCPL = isCCPL;
        }

        public Boolean getIsCCPN() {
            return isCCPN;
        }

        public void setIsCCPN(Boolean isCCPN) {
            this.isCCPN = isCCPN;
        }

        public String getBranchSubCategoryEn() {
            return branchSubCategoryEn;
        }

        public void setBranchSubCategoryEn(String branchSubCategoryEn) {
            this.branchSubCategoryEn = branchSubCategoryEn;
        }

        public String getSkuId() {
            return skuId;
        }

        public void setSkuId(String skuId) {
            this.skuId = skuId;
        }

        public String getBranchSubCategoryAr() {
            return branchSubCategoryAr;
        }

        public void setBranchSubCategoryAr(String branchSubCategoryAr) {
            this.branchSubCategoryAr = branchSubCategoryAr;
        }

        public Boolean getIsDelivery() {
            return isDelivery;
        }

        public void setIsDelivery(Boolean isDelivery) {
            this.isDelivery = isDelivery;
        }

        public String getColor3FacetAr() {
            return color3FacetAr;
        }

        public void setColor3FacetAr(String color3FacetAr) {
            this.color3FacetAr = color3FacetAr;
        }

        public String getColor3Facet() {
            return color3Facet;
        }

        public void setColor3Facet(String color3Facet) {
            this.color3Facet = color3Facet;
        }

        public String getCreatedOn() {
            return createdOn;
        }

        public void setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
        }

        public Boolean getIsShipping() {
            return isShipping;
        }

        public void setIsShipping(Boolean isShipping) {
            this.isShipping = isShipping;
        }

        public Integer getDeliveryCharge() {
            return deliveryCharge;
        }

        public void setDeliveryCharge(Integer deliveryCharge) {
            this.deliveryCharge = deliveryCharge;
        }

        public String getManufacturerAr() {
            return manufacturerAr;
        }

        public void setManufacturerAr(String manufacturerAr) {
            this.manufacturerAr = manufacturerAr;
        }

        public String getBranchCategoryAr() {
            return branchCategoryAr;
        }

        public void setBranchCategoryAr(String branchCategoryAr) {
            this.branchCategoryAr = branchCategoryAr;
        }

        public Integer getMinPickupTime() {
            return minPickupTime;
        }

        public void setMinPickupTime(Integer minPickupTime) {
            this.minPickupTime = minPickupTime;
        }

        public List<String> getLocation() {
            return location;
        }

        public void setLocation(List<String> location) {
            this.location = location;
        }

        public String getBranchCategoryEn() {
            return branchCategoryEn;
        }

        public void setBranchCategoryEn(String branchCategoryEn) {
            this.branchCategoryEn = branchCategoryEn;
        }

        public Boolean getIsCOD() {
            return isCOD;
        }

        public void setIsCOD(Boolean isCOD) {
            this.isCOD = isCOD;
        }

        public Boolean getIsPickup() {
            return isPickup;
        }

        public void setIsPickup(Boolean isPickup) {
            this.isPickup = isPickup;
        }

        public Boolean getMinOrderValue() {
            return minOrderValue;
        }

        public void setMinOrderValue(Boolean minOrderValue) {
            this.minOrderValue = minOrderValue;
        }

        public long getVersion() {
            return version;
        }

        public void setVersion(long version) {
            this.version = version;
        }
    }


    public class Result {

        @SerializedName("ProductSKUId")
        @Expose
        private String productSKUId;
        @SerializedName("ProductId")
        @Expose
        private String productId;
        @SerializedName("ItemType")
        @Expose
        private String itemType;
        @SerializedName("ProductBranchId")
        @Expose
        private Integer productBranchId;
        @SerializedName("ProductTypeBranchId")
        @Expose
        private String productTypeBranchId;
        @SerializedName("TagsAr")
        @Expose
        private List<String> tagsAr = null;
        @SerializedName("MetaDescriptionAr")
        @Expose
        private String metaDescriptionAr;
        @SerializedName("DescriptionEn")
        @Expose
        private String descriptionEn;
        @SerializedName("NameEn")
        @Expose
        private String nameEn;
        @SerializedName("TagsEn")
        @Expose
        private List<String> tagsEn = null;
        @SerializedName("MetaDescriptionEn")
        @Expose
        private String metaDescriptionEn;
        @SerializedName("DescriptionAr")
        @Expose
        private String descriptionAr;
        @SerializedName("NameAr")
        @Expose
        private String nameAr;
        @SerializedName("BranchId")
        @Expose
        private Integer branchId;
        @SerializedName("BranchNameEn")
        @Expose
        private String branchNameEn;
        @SerializedName("BranchNameAr")
        @Expose
        private String branchNameAr;
        @SerializedName("ProductMainImage")
        @Expose
        private String productMainImage;
        @SerializedName("SellingPrice")
        @Expose
        private float sellingPrice;
        @SerializedName("DiscountType")
        @Expose
        private Integer discountType;
        @SerializedName("DiscountValue")
        @Expose
        private float discountValue;
        @SerializedName("Price")
        @Expose
        private float price;
        @SerializedName("InStock")
        @Expose
        private Boolean inStock;
        @SerializedName("StoreId")
        @Expose
        private Integer storeId;
        @SerializedName("StoreNameEn")
        @Expose
        private String storeNameEn;
        @SerializedName("StoreNameAr")
        @Expose
        private String storeNameAr;
        @SerializedName("DistToStoreBranch")
        @Expose
        private Object distToStoreBranch;
        @SerializedName("UPCBarcode")
        @Expose
        private String uPCBarcode;
        @SerializedName("ServiceType")
        @Expose
        private Integer serviceType;
        @SerializedName("score")
        @Expose
        private Object score;
        @SerializedName("OtherFields")
        @Expose
        private OtherFields otherFields;
        @SerializedName("StoreItemsCount")
        @Expose
        private Integer storeItemsCount;
        @SerializedName("StaticFacets")
        @Expose
        private Object staticFacets;

        public String getProductSKUId() {
            return productSKUId;
        }

        public void setProductSKUId(String productSKUId) {
            this.productSKUId = productSKUId;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getItemType() {
            return itemType;
        }

        public void setItemType(String itemType) {
            this.itemType = itemType;
        }

        public Integer getProductBranchId() {
            return productBranchId;
        }

        public void setProductBranchId(Integer productBranchId) {
            this.productBranchId = productBranchId;
        }

        public String getProductTypeBranchId() {
            return productTypeBranchId;
        }

        public void setProductTypeBranchId(String productTypeBranchId) {
            this.productTypeBranchId = productTypeBranchId;
        }

        public List<String> getTagsAr() {
            return tagsAr;
        }

        public void setTagsAr(List<String> tagsAr) {
            this.tagsAr = tagsAr;
        }

        public String getMetaDescriptionAr() {
            return metaDescriptionAr;
        }

        public void setMetaDescriptionAr(String metaDescriptionAr) {
            this.metaDescriptionAr = metaDescriptionAr;
        }

        public String getDescriptionEn() {
            return descriptionEn;
        }

        public void setDescriptionEn(String descriptionEn) {
            this.descriptionEn = descriptionEn;
        }

        public String getNameEn() {
            return nameEn;
        }

        public void setNameEn(String nameEn) {
            this.nameEn = nameEn;
        }

        public List<String> getTagsEn() {
            return tagsEn;
        }

        public void setTagsEn(List<String> tagsEn) {
            this.tagsEn = tagsEn;
        }

        public String getMetaDescriptionEn() {
            return metaDescriptionEn;
        }

        public void setMetaDescriptionEn(String metaDescriptionEn) {
            this.metaDescriptionEn = metaDescriptionEn;
        }

        public String getDescriptionAr() {
            return descriptionAr;
        }

        public void setDescriptionAr(String descriptionAr) {
            this.descriptionAr = descriptionAr;
        }

        public String getNameAr() {
            return nameAr;
        }

        public void setNameAr(String nameAr) {
            this.nameAr = nameAr;
        }

        public Integer getBranchId() {
            return branchId;
        }

        public void setBranchId(Integer branchId) {
            this.branchId = branchId;
        }

        public String getBranchNameEn() {
            return branchNameEn;
        }

        public void setBranchNameEn(String branchNameEn) {
            this.branchNameEn = branchNameEn;
        }

        public String getBranchNameAr() {
            return branchNameAr;
        }

        public void setBranchNameAr(String branchNameAr) {
            this.branchNameAr = branchNameAr;
        }

        public String getProductMainImage() {
            return productMainImage;
        }

        public void setProductMainImage(String productMainImage) {
            this.productMainImage = productMainImage;
        }

        public float getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(float sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public Integer getDiscountType() {
            return discountType;
        }

        public void setDiscountType(Integer discountType) {
            this.discountType = discountType;
        }

        public float getDiscountValue() {
            return discountValue;
        }

        public void setDiscountValue(float discountValue) {
            this.discountValue = discountValue;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }

        public Boolean getInStock() {
            return inStock;
        }

        public void setInStock(Boolean inStock) {
            this.inStock = inStock;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public String getStoreNameEn() {
            return storeNameEn;
        }

        public void setStoreNameEn(String storeNameEn) {
            this.storeNameEn = storeNameEn;
        }

        public String getStoreNameAr() {
            return storeNameAr;
        }

        public void setStoreNameAr(String storeNameAr) {
            this.storeNameAr = storeNameAr;
        }

        public Object getDistToStoreBranch() {
            return distToStoreBranch;
        }

        public void setDistToStoreBranch(Object distToStoreBranch) {
            this.distToStoreBranch = distToStoreBranch;
        }

        public String getUPCBarcode() {
            return uPCBarcode;
        }

        public void setUPCBarcode(String uPCBarcode) {
            this.uPCBarcode = uPCBarcode;
        }

        public Integer getServiceType() {
            return serviceType;
        }

        public void setServiceType(Integer serviceType) {
            this.serviceType = serviceType;
        }

        public Object getScore() {
            return score;
        }

        public void setScore(Object score) {
            this.score = score;
        }

        public OtherFields getOtherFields() {
            return otherFields;
        }

        public void setOtherFields(OtherFields otherFields) {
            this.otherFields = otherFields;
        }

        public Integer getStoreItemsCount() {
            return storeItemsCount;
        }

        public void setStoreItemsCount(Integer storeItemsCount) {
            this.storeItemsCount = storeItemsCount;
        }

        public Object getStaticFacets() {
            return staticFacets;
        }

        public void setStaticFacets(Object staticFacets) {
            this.staticFacets = staticFacets;
        }
    }

    public class Facet {

        @SerializedName("Id")
        @Expose
        private Long id;
        @SerializedName("FacetEn")
        @Expose
        private String facetEn;
        @SerializedName("FacetAr")
        @Expose
        private Object facetAr;
        @SerializedName("FacetFilters")
        @Expose
        private List<FacetFilter> facetFilters = null;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getFacetEn() {
            return facetEn;
        }

        public void setFacetEn(String facetEn) {
            this.facetEn = facetEn;
        }

        public Object getFacetAr() {
            return facetAr;
        }

        public void setFacetAr(Object facetAr) {
            this.facetAr = facetAr;
        }

        public List<FacetFilter> getFacetFilters() {
            return facetFilters;
        }

        public void setFacetFilters(List<FacetFilter> facetFilters) {
            this.facetFilters = facetFilters;
        }
    }

    public class FacetFilter {

        @SerializedName("FacetTextId")
        @Expose
        private Long facetTextId;
        @SerializedName("FacetText")
        @Expose
        private String facetText;
        @SerializedName("Count")
        @Expose
        private Long count;
        @SerializedName("Selected")
        @Expose
        private Boolean selected;

        public Long getFacetTextId() {
            return facetTextId;
        }

        public void setFacetTextId(Long facetTextId) {
            this.facetTextId = facetTextId;
        }

        public String getFacetText() {
            return facetText;
        }

        public void setFacetText(String facetText) {
            this.facetText = facetText;
        }

        public Long getCount() {
            return count;
        }

        public void setCount(Long count) {
            this.count = count;
        }

        public Boolean getSelected() {
            return selected;
        }

        public void setSelected(Boolean selected) {
            this.selected = selected;
        }

    }
}
