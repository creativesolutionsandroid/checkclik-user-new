package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductVariantResponse implements Serializable {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("ProductInventoryId")
    private int ProductInventoryId;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public int getProductInventoryId() {
        return ProductInventoryId;
    }

    public void setProductInventoryId(int ProductInventoryId) {
        this.ProductInventoryId = ProductInventoryId;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data implements Serializable  {
        @Expose
        @SerializedName("VariantsList")
        private ArrayList<VariantsList> VariantsList;
        @Expose
        @SerializedName("ProductList")
        private ArrayList<ProductList> ProductList;

        public ArrayList<VariantsList> getVariantsList() {
            return VariantsList;
        }

        public void setVariantsList(ArrayList<VariantsList> VariantsList) {
            this.VariantsList = VariantsList;
        }

        public ArrayList<ProductList> getProductList() {
            return ProductList;
        }

        public void setProductList(ArrayList<ProductList> ProductList) {
            this.ProductList = ProductList;
        }
    }

    public static class VariantsList implements Serializable  {
        @Expose
        @SerializedName("jvariants")
        private ArrayList<Jvariants> jvariants;
        @Expose
        @SerializedName("variants")
        private String variants;
        @Expose
        @SerializedName("VariantNameEn")
        private String VariantNameEn;
        @Expose
        @SerializedName("VariantNameAr")
        private String VariantNameAr;
        @Expose
        @SerializedName("OptionId")
        private int OptionId;

        public ArrayList<Jvariants> getJvariants() {
            return jvariants;
        }

        public void setJvariants(ArrayList<Jvariants> jvariants) {
            this.jvariants = jvariants;
        }

        public String getVariants() {
            return variants;
        }

        public void setVariants(String variants) {
            this.variants = variants;
        }

        public String getVariantNameEn() {
            return VariantNameEn;
        }

        public void setVariantNameEn(String VariantNameEn) {
            this.VariantNameEn = VariantNameEn;
        }

        public String getVariantNameAr() {
            return VariantNameAr;
        }

        public void setVariantNameAr(String VariantNameAr) {
            this.VariantNameAr = VariantNameAr;
        }

        public int getOptionId() {
            return OptionId;
        }

        public void setOptionId(int OptionId) {
            this.OptionId = OptionId;
        }
    }

    public static class Jvariants implements Serializable  {
        @Expose
        @SerializedName("ValueAr")
        private String ValueAr;
        @Expose
        @SerializedName("ValueEn")
        private String ValueEn;
        @Expose
        @SerializedName("OptionId")
        private int OptionId;

        public String getValueAr() {
            return ValueAr;
        }

        public void setValueAr(String ValueAr) {
            this.ValueAr = ValueAr;
        }

        public String getValueEn() {
            return ValueEn;
        }

        public void setValueEn(String ValueEn) {
            this.ValueEn = ValueEn;
        }

        public int getOptionId() {
            return OptionId;
        }

        public void setOptionId(int OptionId) {
            this.OptionId = OptionId;
        }
    }

    public static class ProductList implements Serializable  {
        @Expose
        @SerializedName("JRelatedProducts")
        private ArrayList<JRelatedProducts> JRelatedProducts;
        @Expose
        @SerializedName("JReviews")
        private ArrayList<JReviews> JReviews;
        @Expose
        @SerializedName("JImages")
        private ArrayList<JImages> JImages;
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<JProductVariant> JProductVariant;
        @Expose
        @SerializedName("MetaDescriptionAr")
        private String MetaDescriptionAr;
        @Expose
        @SerializedName("MetaDescriptionEn")
        private String MetaDescriptionEn;
        @Expose
        @SerializedName("TagsAr")
        private String TagsAr;
        @Expose
        @SerializedName("TagsEn")
        private String TagsEn;
        @Expose
        @SerializedName("DescriptionAr1")
        private String DescriptionAr1;
        @Expose
        @SerializedName("DescriptionEn1")
        private String DescriptionEn1;
        @Expose
        @SerializedName("SubCatAr")
        private String SubCatAr;
        @Expose
        @SerializedName("SubCatEn")
        private String SubCatEn;
        @Expose
        @SerializedName("SubCatId")
        private int SubCatId;
        @Expose
        @SerializedName("MainCatAr")
        private String MainCatAr;
        @Expose
        @SerializedName("MainCatEn")
        private String MainCatEn;
        @Expose
        @SerializedName("MainCatId")
        private int MainCatId;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;
        @Expose
        @SerializedName("OrderId")
        private int OrderId;
        @Expose
        @SerializedName("AvgUserReviews")
        private float AvgUserReviews;
        @Expose
        @SerializedName("UserReviews")
        private int UserReviews;
        @Expose
        @SerializedName("PurchaseCount")
        private int PurchaseCount;
        @Expose
        @SerializedName("RelatedProducts")
        private String RelatedProducts;
        @Expose
        @SerializedName("Reviews")
        private String Reviews;
        @Expose
        @SerializedName("DescriptionAr")
        private String DescriptionAr;
        @Expose
        @SerializedName("DescriptionEn")
        private String DescriptionEn;
        @Expose
        @SerializedName("AvailableQuantity")
        private int AvailableQuantity;
        @Expose
        @SerializedName("CartQuantity")
        private int CartQuantity;
        @Expose
        @SerializedName("ProductCountingNo")
        private int ProductCountingNo;
        @Expose
        @SerializedName("Condition")
        private int Condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String UPCBarcode;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private float DiscountValue;
        @Expose
        @SerializedName("DiscountType")
        private int DiscountType;
        @Expose
        @SerializedName("Images")
        private String Images;
        @Expose
        @SerializedName("ProductVariants")
        private String ProductVariants;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("NotReturnable")
        private boolean NotReturnable;
        @Expose
        @SerializedName("AllowedQty")
        private int AllowedQty;
        @Expose
        @SerializedName("MaxCartQty")
        private int MaxCartQty;
        @Expose
        @SerializedName("MinCartQty")
        private int MinCartQty;
        @Expose
        @SerializedName("ProductAvailabilityRange")
        private int ProductAvailabilityRange;
        @Expose
        @SerializedName("LowStockActivity")
        private int LowStockActivity;
        @Expose
        @SerializedName("DisplayStockQuantity")
        private boolean DisplayStockQuantity;
        @Expose
        @SerializedName("ReservedQuantity")
        private int ReservedQuantity;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("InventoryMethod")
        private int InventoryMethod;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("ProductInventoryId")
        private int ProductInventoryId;
        @Expose
        @SerializedName("BranchColor")
        private String BranchColor;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CartItemCount")
        private int CartItemCount;
        @Expose
        @SerializedName("NotificationCount")
        private int NotificationCount;

        public int getCartItemCount() {
            return CartItemCount;
        }

        public void setCartItemCount(int cartItemCount) {
            CartItemCount = cartItemCount;
        }

        public int getNotificationCount() {
            return NotificationCount;
        }

        public void setNotificationCount(int notificationCount) {
            NotificationCount = notificationCount;
        }
        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int storeId) {
            StoreId = storeId;
        }

        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String logoCopy) {
            LogoCopy = logoCopy;
        }

        public String getBranchColor() {
            return BranchColor;
        }

        public void setBranchColor(String branchColor) {
            BranchColor = branchColor;
        }

        public ArrayList<JRelatedProducts> getJRelatedProducts() {
            return JRelatedProducts;
        }

        public void setJRelatedProducts(ArrayList<JRelatedProducts> JRelatedProducts) {
            this.JRelatedProducts = JRelatedProducts;
        }

        public ArrayList<JReviews> getJReviews() {
            return JReviews;
        }

        public void setJReviews(ArrayList<JReviews> JReviews) {
            this.JReviews = JReviews;
        }

        public ArrayList<JImages> getJImages() {
            return JImages;
        }

        public void setJImages(ArrayList<JImages> JImages) {
            this.JImages = JImages;
        }

        public ArrayList<JProductVariant> getJProductVariant() {
            return JProductVariant;
        }

        public void setJProductVariant(ArrayList<JProductVariant> JProductVariant) {
            this.JProductVariant = JProductVariant;
        }

        public String getMetaDescriptionAr() {
            return MetaDescriptionAr;
        }

        public void setMetaDescriptionAr(String MetaDescriptionAr) {
            this.MetaDescriptionAr = MetaDescriptionAr;
        }

        public String getMetaDescriptionEn() {
            return MetaDescriptionEn;
        }

        public void setMetaDescriptionEn(String MetaDescriptionEn) {
            this.MetaDescriptionEn = MetaDescriptionEn;
        }

        public String getTagsAr() {
            return TagsAr;
        }

        public void setTagsAr(String TagsAr) {
            this.TagsAr = TagsAr;
        }

        public String getTagsEn() {
            return TagsEn;
        }

        public void setTagsEn(String TagsEn) {
            this.TagsEn = TagsEn;
        }

        public String getDescriptionAr1() {
            return DescriptionAr1;
        }

        public void setDescriptionAr1(String DescriptionAr1) {
            this.DescriptionAr1 = DescriptionAr1;
        }

        public String getDescriptionEn1() {
            return DescriptionEn1;
        }

        public void setDescriptionEn1(String DescriptionEn1) {
            this.DescriptionEn1 = DescriptionEn1;
        }

        public String getSubCatAr() {
            return SubCatAr;
        }

        public void setSubCatAr(String SubCatAr) {
            this.SubCatAr = SubCatAr;
        }

        public String getSubCatEn() {
            return SubCatEn;
        }

        public void setSubCatEn(String SubCatEn) {
            this.SubCatEn = SubCatEn;
        }

        public int getSubCatId() {
            return SubCatId;
        }

        public void setSubCatId(int SubCatId) {
            this.SubCatId = SubCatId;
        }

        public String getMainCatAr() {
            return MainCatAr;
        }

        public void setMainCatAr(String MainCatAr) {
            this.MainCatAr = MainCatAr;
        }

        public String getMainCatEn() {
            return MainCatEn;
        }

        public void setMainCatEn(String MainCatEn) {
            this.MainCatEn = MainCatEn;
        }

        public int getMainCatId() {
            return MainCatId;
        }

        public void setMainCatId(int MainCatId) {
            this.MainCatId = MainCatId;
        }

        public boolean getFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean Favorite) {
            this.Favorite = Favorite;
        }

        public int getOrderId() {
            return OrderId;
        }

        public void setOrderId(int OrderId) {
            this.OrderId = OrderId;
        }

        public float getAvgUserReviews() {
            return AvgUserReviews;
        }

        public void setAvgUserReviews(float AvgUserReviews) {
            this.AvgUserReviews = AvgUserReviews;
        }

        public int getUserReviews() {
            return UserReviews;
        }

        public void setUserReviews(int UserReviews) {
            this.UserReviews = UserReviews;
        }

        public int getPurchaseCount() {
            return PurchaseCount;
        }

        public void setPurchaseCount(int PurchaseCount) {
            this.PurchaseCount = PurchaseCount;
        }

        public String getRelatedProducts() {
            return RelatedProducts;
        }

        public void setRelatedProducts(String RelatedProducts) {
            this.RelatedProducts = RelatedProducts;
        }

        public String getReviews() {
            return Reviews;
        }

        public void setReviews(String Reviews) {
            this.Reviews = Reviews;
        }

        public String getDescriptionAr() {
            return DescriptionAr;
        }

        public void setDescriptionAr(String DescriptionAr) {
            this.DescriptionAr = DescriptionAr;
        }

        public String getDescriptionEn() {
            return DescriptionEn;
        }

        public void setDescriptionEn(String DescriptionEn) {
            this.DescriptionEn = DescriptionEn;
        }

        public int getAvailableQuantity() {
            return AvailableQuantity;
        }

        public void setAvailableQuantity(int AvailableQuantity) {
            this.AvailableQuantity = AvailableQuantity;
        }

        public int getCartQuantity() {
            return CartQuantity;
        }

        public void setCartQuantity(int CartQuantity) {
            this.CartQuantity = CartQuantity;
        }

        public int getProductCountingNo() {
            return ProductCountingNo;
        }

        public void setProductCountingNo(int ProductCountingNo) {
            this.ProductCountingNo = ProductCountingNo;
        }

        public int getCondition() {
            return Condition;
        }

        public void setCondition(int Condition) {
            this.Condition = Condition;
        }

        public String getUPCBarcode() {
            return UPCBarcode;
        }

        public void setUPCBarcode(String UPCBarcode) {
            this.UPCBarcode = UPCBarcode;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public float getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(float DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getDiscountType() {
            return DiscountType;
        }

        public void setDiscountType(int DiscountType) {
            this.DiscountType = DiscountType;
        }

        public String getImages() {
            return Images;
        }

        public void setImages(String Images) {
            this.Images = Images;
        }

        public String getProductVariants() {
            return ProductVariants;
        }

        public void setProductVariants(String ProductVariants) {
            this.ProductVariants = ProductVariants;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public boolean getNotReturnable() {
            return NotReturnable;
        }

        public void setNotReturnable(boolean NotReturnable) {
            this.NotReturnable = NotReturnable;
        }

        public int getAllowedQty() {
            return AllowedQty;
        }

        public void setAllowedQty(int AllowedQty) {
            this.AllowedQty = AllowedQty;
        }

        public int getMaxCartQty() {
            return MaxCartQty;
        }

        public void setMaxCartQty(int MaxCartQty) {
            this.MaxCartQty = MaxCartQty;
        }

        public int getMinCartQty() {
            return MinCartQty;
        }

        public void setMinCartQty(int MinCartQty) {
            this.MinCartQty = MinCartQty;
        }

        public int getProductAvailabilityRange() {
            return ProductAvailabilityRange;
        }

        public void setProductAvailabilityRange(int ProductAvailabilityRange) {
            this.ProductAvailabilityRange = ProductAvailabilityRange;
        }

        public int getLowStockActivity() {
            return LowStockActivity;
        }

        public void setLowStockActivity(int LowStockActivity) {
            this.LowStockActivity = LowStockActivity;
        }

        public boolean getDisplayStockQuantity() {
            return DisplayStockQuantity;
        }

        public void setDisplayStockQuantity(boolean DisplayStockQuantity) {
            this.DisplayStockQuantity = DisplayStockQuantity;
        }

        public int getReservedQuantity() {
            return ReservedQuantity;
        }

        public void setReservedQuantity(int ReservedQuantity) {
            this.ReservedQuantity = ReservedQuantity;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public int getInventoryMethod() {
            return InventoryMethod;
        }

        public void setInventoryMethod(int InventoryMethod) {
            this.InventoryMethod = InventoryMethod;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getProductInventoryId() {
            return ProductInventoryId;
        }

        public void setProductInventoryId(int ProductInventoryId) {
            this.ProductInventoryId = ProductInventoryId;
        }
    }

    public static class JRelatedProducts implements Serializable  {
        @Expose
        @SerializedName("SubCatAr")
        private String SubCatAr;
        @Expose
        @SerializedName("SubCatEn")
        private String SubCatEn;
        @Expose
        @SerializedName("SubCatId")
        private int SubCatId;
        @Expose
        @SerializedName("MainCatAr")
        private String MainCatAr;
        @Expose
        @SerializedName("MainCatEn")
        private String MainCatEn;
        @Expose
        @SerializedName("MainCatId")
        private int MainCatId;
        @Expose
        @SerializedName("Condition")
        private int Condition;
        @Expose
        @SerializedName("UPCBarcode")
        private String UPCBarcode;
        @Expose
        @SerializedName("ProductSkuId")
        private String ProductSkuId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StockQuantity")
        private int StockQuantity;
        @Expose
        @SerializedName("SellingPrice")
        private float SellingPrice;
        @Expose
        @SerializedName("Price")
        private float Price;
        @Expose
        @SerializedName("DiscountValue")
        private int DiscountValue;
        @Expose
        @SerializedName("DiscountType")
        private int DiscountType;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("ProductNameAr")
        private String ProductNameAr;
        @Expose
        @SerializedName("ProductNameEn")
        private String ProductNameEn;
        @Expose
        @SerializedName("ProductId")
        private int ProductId;
        @Expose
        @SerializedName("id")
        private int id;

        public String getSubCatAr() {
            return SubCatAr;
        }

        public void setSubCatAr(String SubCatAr) {
            this.SubCatAr = SubCatAr;
        }

        public String getSubCatEn() {
            return SubCatEn;
        }

        public void setSubCatEn(String SubCatEn) {
            this.SubCatEn = SubCatEn;
        }

        public int getSubCatId() {
            return SubCatId;
        }

        public void setSubCatId(int SubCatId) {
            this.SubCatId = SubCatId;
        }

        public String getMainCatAr() {
            return MainCatAr;
        }

        public void setMainCatAr(String MainCatAr) {
            this.MainCatAr = MainCatAr;
        }

        public String getMainCatEn() {
            return MainCatEn;
        }

        public void setMainCatEn(String MainCatEn) {
            this.MainCatEn = MainCatEn;
        }

        public int getMainCatId() {
            return MainCatId;
        }

        public void setMainCatId(int MainCatId) {
            this.MainCatId = MainCatId;
        }

        public int getCondition() {
            return Condition;
        }

        public void setCondition(int Condition) {
            this.Condition = Condition;
        }

        public String getUPCBarcode() {
            return UPCBarcode;
        }

        public void setUPCBarcode(String UPCBarcode) {
            this.UPCBarcode = UPCBarcode;
        }

        public String getProductSkuId() {
            return ProductSkuId;
        }

        public void setProductSkuId(String ProductSkuId) {
            this.ProductSkuId = ProductSkuId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStockQuantity() {
            return StockQuantity;
        }

        public void setStockQuantity(int StockQuantity) {
            this.StockQuantity = StockQuantity;
        }

        public float getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(float SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public float getPrice() {
            return Price;
        }

        public void setPrice(float Price) {
            this.Price = Price;
        }

        public int getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(int DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getDiscountType() {
            return DiscountType;
        }

        public void setDiscountType(int DiscountType) {
            this.DiscountType = DiscountType;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public String getProductNameAr() {
            return ProductNameAr;
        }

        public void setProductNameAr(String ProductNameAr) {
            this.ProductNameAr = ProductNameAr;
        }

        public String getProductNameEn() {
            return ProductNameEn;
        }

        public void setProductNameEn(String ProductNameEn) {
            this.ProductNameEn = ProductNameEn;
        }

        public int getProductId() {
            return ProductId;
        }

        public void setProductId(int ProductId) {
            this.ProductId = ProductId;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class JReviews  implements Serializable {
        @Expose
        @SerializedName("RemainingRating")
        private int RemainingRating;
        @Expose
        @SerializedName("ReviewDate")
        private String ReviewDate;
        @Expose
        @SerializedName("ReviewId")
        private int ReviewId;
        @Expose
        @SerializedName("Rating")
        private int Rating;
        @Expose
        @SerializedName("Comments")
        private String Comments;
        @Expose
        @SerializedName("DisplayName")
        private String DisplayName;
        @Expose
        @SerializedName("UserId")
        private int UserId;

        public int getRemainingRating() {
            return RemainingRating;
        }

        public void setRemainingRating(int RemainingRating) {
            this.RemainingRating = RemainingRating;
        }

        public String getReviewDate() {
            return ReviewDate;
        }

        public void setReviewDate(String ReviewDate) {
            this.ReviewDate = ReviewDate;
        }

        public int getReviewId() {
            return ReviewId;
        }

        public void setReviewId(int ReviewId) {
            this.ReviewId = ReviewId;
        }

        public int getRating() {
            return Rating;
        }

        public void setRating(int Rating) {
            this.Rating = Rating;
        }

        public String getComments() {
            return Comments;
        }

        public void setComments(String Comments) {
            this.Comments = Comments;
        }

        public String getDisplayName() {
            return DisplayName;
        }

        public void setDisplayName(String DisplayName) {
            this.DisplayName = DisplayName;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }
    }

    public static class JImages implements Serializable  {
        @Expose
        @SerializedName("ImageId")
        private int ImageId;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("ProductInventoryId")
        private int ProductInventoryId;

        public int getImageId() {
            return ImageId;
        }

        public void setImageId(int ImageId) {
            this.ImageId = ImageId;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public int getProductInventoryId() {
            return ProductInventoryId;
        }

        public void setProductInventoryId(int ProductInventoryId) {
            this.ProductInventoryId = ProductInventoryId;
        }
    }

    public static class JProductVariant implements Serializable  {
        @Expose
        @SerializedName("ValueAr")
        private String ValueAr;
        @Expose
        @SerializedName("ValueEn")
        private String ValueEn;
        @Expose
        @SerializedName("OptionId")
        private int OptionId;
        @Expose
        @SerializedName("ProductInventoryId")
        private int ProductInventoryId;

        public String getValueAr() {
            return ValueAr;
        }

        public void setValueAr(String ValueAr) {
            this.ValueAr = ValueAr;
        }

        public String getValueEn() {
            return ValueEn;
        }

        public void setValueEn(String ValueEn) {
            this.ValueEn = ValueEn;
        }

        public int getOptionId() {
            return OptionId;
        }

        public void setOptionId(int OptionId) {
            this.OptionId = OptionId;
        }

        public int getProductInventoryId() {
            return ProductInventoryId;
        }

        public void setProductInventoryId(int ProductInventoryId) {
            this.ProductInventoryId = ProductInventoryId;
        }
    }
}
