package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class HomePageResponse {

    @Expose
    @SerializedName("Data")
    private Data data;
    @Expose
    @SerializedName("Message")
    private String message;
    @Expose
    @SerializedName("Status")
    private boolean status;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public static class UserBlockedStatus implements Serializable {
        @Expose
        @SerializedName("IsBlocked")
        private int IsBlocked;
        @Expose
        @SerializedName("MessageEn")
        private String MessageEn;
        @Expose
        @SerializedName("MessageAr")
        private String MessageAr;
        @Expose
        @SerializedName("NotificationCount")
        private int NotificationCount;
        @Expose
        @SerializedName("CartItemCount")
        private int CartItemCount;

        public int getIsBlocked() {
            return IsBlocked;
        }

        public void setIsBlocked(int isBlocked) {
            IsBlocked = isBlocked;
        }

        public String getMessageEn() {
            return MessageEn;
        }

        public void setMessageEn(String messageEn) {
            MessageEn = messageEn;
        }

        public String getMessageAr() {
            return MessageAr;
        }

        public void setMessageAr(String messageAr) {
            MessageAr = messageAr;
        }

        public int getNotificationCount() {
            return NotificationCount;
        }

        public void setNotificationCount(int notificationCount) {
            NotificationCount = notificationCount;
        }

        public int getCartItemCount() {
            return CartItemCount;
        }

        public void setCartItemCount(int cartItemCount) {
            CartItemCount = cartItemCount;
        }
    }

    public static class Data  implements Serializable{
        @Expose
        @SerializedName("FeaturedStores")
        private ArrayList<Featuredstores> featuredstores;
        @Expose
        @SerializedName("SubCategoryList")
        private ArrayList<Subcategorylist> subcategorylist;
        @Expose
        @SerializedName("Offers2")
        private ArrayList<Offers2> offers2;
        @Expose
        @SerializedName("Offers")
        private ArrayList<Offers> offers;
        @Expose
        @SerializedName("HomeBanners")
        private ArrayList<Homebanners> homebanners;
        @Expose
        @SerializedName("UserBlockedStatus")
        private ArrayList<UserBlockedStatus> UserBlockedStatus;

        public ArrayList<HomePageResponse.UserBlockedStatus> getUserBlockedStatus() {
            return UserBlockedStatus;
        }

        public void setUserBlockedStatus(ArrayList<HomePageResponse.UserBlockedStatus> userBlockedStatus) {
            UserBlockedStatus = userBlockedStatus;
        }

        public ArrayList<Featuredstores> getFeaturedstores() {
            return featuredstores;
        }

        public void setFeaturedstores(ArrayList<Featuredstores> featuredstores) {
            this.featuredstores = featuredstores;
        }

        public ArrayList<Subcategorylist> getSubcategorylist() {
            return subcategorylist;
        }

        public void setSubcategorylist(ArrayList<Subcategorylist> subcategorylist) {
            this.subcategorylist = subcategorylist;
        }

        public ArrayList<Offers2> getOffers2() {
            return offers2;
        }

        public void setOffers2(ArrayList<Offers2> offers2) {
            this.offers2 = offers2;
        }

        public ArrayList<Offers> getOffers() {
            return offers;
        }

        public void setOffers(ArrayList<Offers> offers) {
            this.offers = offers;
        }

        public ArrayList<Homebanners> getHomebanners() {
            return homebanners;
        }

        public void setHomebanners(ArrayList<Homebanners> homebanners) {
            this.homebanners = homebanners;
        }
    }

    public static class Featuredstores implements Serializable {
        @Expose
        @SerializedName("Shift")
        private String shift;
        @Expose
        @SerializedName("Favorite")
        private boolean favoriteid;
        @Expose
        @SerializedName("VendorType")
        private int vendortype;
        @Expose
        @SerializedName("StoreType")
        private int storetype;
        @Expose
        @SerializedName("Longitude")
        private String longitude;
        @Expose
        @SerializedName("Latitude")
        private String latitude;
        @Expose
        @SerializedName("Weekday")
        private int weekday;
        @Expose
        @SerializedName("EndTime")
        private String endtime;
        @Expose
        @SerializedName("StartTime")
        private String starttime;
        @Expose
        @SerializedName("Address")
        private String address;
        @Expose
        @SerializedName("Logo")
        private String logo;
        @Expose
        @SerializedName("Background")
        private String background;
        @Expose
        @SerializedName("Maroof")
        private String maroof;
        @Expose
        @SerializedName("Youtube")
        private String youtube;
        @Expose
        @SerializedName("Snapchat")
        private String snapchat;
        @Expose
        @SerializedName("Instagram")
        private String instagram;
        @Expose
        @SerializedName("LinkedIn")
        private String linkedin;
        @Expose
        @SerializedName("TwitterHandle")
        private String twitterhandle;
        @Expose
        @SerializedName("Facebook")
        private String facebook;
        @Expose
        @SerializedName("BusinessSince")
        private String businesssince;
        @Expose
        @SerializedName("IsDelivery")
        private boolean isdelivery;
        @Expose
        @SerializedName("InStoreService")
        private String instoreservice;
        @Expose
        @SerializedName("ShippingProvider")
        private int shippingprovider;
        @Expose
        @SerializedName("BranchColor")
        private String branchcolor;
        @Expose
        @SerializedName("DeliveryCharges")
        private int deliverycharges;
        @Expose
        @SerializedName("IsCreditCardAllowed")
        private String iscreditcardallowed;
        @Expose
        @SerializedName("IsCashAllowed")
        private String iscashallowed;
        @Expose
        @SerializedName("DeliveryTime")
        private String deliverytime;
        @Expose
        @SerializedName("OrderDone")
        private int orderdone;
        @Expose
        @SerializedName("Ratings")
        private int ratings;
        @Expose
        @SerializedName("ReviewsCount")
        private int reviewscount;
        @Expose
        @SerializedName("Reviews")
        private float reviews;
        @Expose
        @SerializedName("CategoryAr")
        private String categoryar;
        @Expose
        @SerializedName("CategoryEn")
        private String categoryen;
        @Expose
        @SerializedName("TermsAndConditionsAr")
        private String termsandconditionsar;
        @Expose
        @SerializedName("TermsAndConditionsEn")
        private String termsandconditionsen;
        @Expose
        @SerializedName("StoreImage")
        private String storeimage;
        @Expose
        @SerializedName("BranchLogoImage")
        private String branchlogoimage;
        @Expose
        @SerializedName("BackgroundImage")
        private String backgroundimage;
        @Expose
        @SerializedName("MinimumOrderValue")
        private int minimumordervalue;
        @Expose
        @SerializedName("StoreAr")
        private String storear;
        @Expose
        @SerializedName("StoreEn")
        private String storeen;
        @Expose
        @SerializedName("BranchAr")
        private String branchar;
        @Expose
        @SerializedName("BranchEn")
        private String branchen;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("StoreId")
        private int storeid;
        @Expose
        @SerializedName("BranchStatus")
        private String branchstatus;
        @Expose
        @SerializedName("WeekNo")
        private int weekno;
        @Expose
        @SerializedName("CurrentDateTime")
        private String currentdatetime;
        @Expose
        @SerializedName("EndDateTime")
        private String enddatetime;
        @Expose
        @SerializedName("StarDateTime")
        private String stardatetime;
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("ActualDate")
        private String actualdate;

        public String getShift() {
            return shift;
        }

        public void setShift(String shift) {
            this.shift = shift;
        }

        public boolean getFavoriteid() {
            return favoriteid;
        }

        public void setFavoriteid(boolean favoriteid) {
            this.favoriteid = favoriteid;
        }

        public int getVendortype() {
            return vendortype;
        }

        public void setVendortype(int vendortype) {
            this.vendortype = vendortype;
        }

        public int getStoretype() {
            return storetype;
        }

        public void setStoretype(int storetype) {
            this.storetype = storetype;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public int getWeekday() {
            return weekday;
        }

        public void setWeekday(int weekday) {
            this.weekday = weekday;
        }

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getBackground() {
            return background;
        }

        public void setBackground(String background) {
            this.background = background;
        }

        public String getMaroof() {
            return maroof;
        }

        public void setMaroof(String maroof) {
            this.maroof = maroof;
        }

        public String getYoutube() {
            return youtube;
        }

        public void setYoutube(String youtube) {
            this.youtube = youtube;
        }

        public String getSnapchat() {
            return snapchat;
        }

        public void setSnapchat(String snapchat) {
            this.snapchat = snapchat;
        }

        public String getInstagram() {
            return instagram;
        }

        public void setInstagram(String instagram) {
            this.instagram = instagram;
        }

        public String getLinkedin() {
            return linkedin;
        }

        public void setLinkedin(String linkedin) {
            this.linkedin = linkedin;
        }

        public String getTwitterhandle() {
            return twitterhandle;
        }

        public void setTwitterhandle(String twitterhandle) {
            this.twitterhandle = twitterhandle;
        }

        public String getFacebook() {
            return facebook;
        }

        public void setFacebook(String facebook) {
            this.facebook = facebook;
        }

        public String getBusinesssince() {
            return businesssince;
        }

        public void setBusinesssince(String businesssince) {
            this.businesssince = businesssince;
        }

        public boolean getIsdelivery() {
            return isdelivery;
        }

        public void setIsdelivery(boolean isdelivery) {
            this.isdelivery = isdelivery;
        }

        public String getInstoreservice() {
            return instoreservice;
        }

        public void setInstoreservice(String instoreservice) {
            this.instoreservice = instoreservice;
        }

        public int getShippingprovider() {
            return shippingprovider;
        }

        public void setShippingprovider(int shippingprovider) {
            this.shippingprovider = shippingprovider;
        }

        public String getBranchcolor() {
            return branchcolor;
        }

        public void setBranchcolor(String branchcolor) {
            this.branchcolor = branchcolor;
        }

        public int getDeliverycharges() {
            return deliverycharges;
        }

        public void setDeliverycharges(int deliverycharges) {
            this.deliverycharges = deliverycharges;
        }

        public String getIscreditcardallowed() {
            return iscreditcardallowed;
        }

        public void setIscreditcardallowed(String iscreditcardallowed) {
            this.iscreditcardallowed = iscreditcardallowed;
        }

        public String getIscashallowed() {
            return iscashallowed;
        }

        public void setIscashallowed(String iscashallowed) {
            this.iscashallowed = iscashallowed;
        }

        public String getDeliverytime() {
            return deliverytime;
        }

        public void setDeliverytime(String deliverytime) {
            this.deliverytime = deliverytime;
        }

        public int getOrderdone() {
            return orderdone;
        }

        public void setOrderdone(int orderdone) {
            this.orderdone = orderdone;
        }

        public int getRatings() {
            return ratings;
        }

        public void setRatings(int ratings) {
            this.ratings = ratings;
        }

        public int getReviewscount() {
            return reviewscount;
        }

        public void setReviewscount(int reviewscount) {
            this.reviewscount = reviewscount;
        }

        public float getReviews() {
            return reviews;
        }

        public void setReviews(float reviews) {
            this.reviews = reviews;
        }

        public String getCategoryar() {
            return categoryar;
        }

        public void setCategoryar(String categoryar) {
            this.categoryar = categoryar;
        }

        public String getCategoryen() {
            return categoryen;
        }

        public void setCategoryen(String categoryen) {
            this.categoryen = categoryen;
        }

        public String getTermsandconditionsar() {
            return termsandconditionsar;
        }

        public void setTermsandconditionsar(String termsandconditionsar) {
            this.termsandconditionsar = termsandconditionsar;
        }

        public String getTermsandconditionsen() {
            return termsandconditionsen;
        }

        public void setTermsandconditionsen(String termsandconditionsen) {
            this.termsandconditionsen = termsandconditionsen;
        }

        public String getStoreimage() {
            return storeimage;
        }

        public void setStoreimage(String storeimage) {
            this.storeimage = storeimage;
        }

        public String getBranchlogoimage() {
            return branchlogoimage;
        }

        public void setBranchlogoimage(String branchlogoimage) {
            this.branchlogoimage = branchlogoimage;
        }

        public String getBackgroundimage() {
            return backgroundimage;
        }

        public void setBackgroundimage(String backgroundimage) {
            this.backgroundimage = backgroundimage;
        }

        public int getMinimumordervalue() {
            return minimumordervalue;
        }

        public void setMinimumordervalue(int minimumordervalue) {
            this.minimumordervalue = minimumordervalue;
        }

        public String getStorear() {
            return storear;
        }

        public void setStorear(String storear) {
            this.storear = storear;
        }

        public String getStoreen() {
            return storeen;
        }

        public void setStoreen(String storeen) {
            this.storeen = storeen;
        }

        public String getBranchar() {
            return branchar;
        }

        public void setBranchar(String branchar) {
            this.branchar = branchar;
        }

        public String getBranchen() {
            return branchen;
        }

        public void setBranchen(String branchen) {
            this.branchen = branchen;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public String getBranchstatus() {
            return branchstatus;
        }

        public void setBranchstatus(String branchstatus) {
            this.branchstatus = branchstatus;
        }

        public int getWeekno() {
            return weekno;
        }

        public void setWeekno(int weekno) {
            this.weekno = weekno;
        }

        public String getCurrentdatetime() {
            return currentdatetime;
        }

        public void setCurrentdatetime(String currentdatetime) {
            this.currentdatetime = currentdatetime;
        }

        public String getEnddatetime() {
            return enddatetime;
        }

        public void setEnddatetime(String enddatetime) {
            this.enddatetime = enddatetime;
        }

        public String getStardatetime() {
            return stardatetime;
        }

        public void setStardatetime(String stardatetime) {
            this.stardatetime = stardatetime;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getActualdate() {
            return actualdate;
        }

        public void setActualdate(String actualdate) {
            this.actualdate = actualdate;
        }
    }

    public static class Subcategorylist implements Serializable {

        @Expose
        @SerializedName("ColorCode")
        private String ColorCode;
        @Expose
        @SerializedName("UniCode")
        private String UniCode;
        @Expose
        @SerializedName("IconClass")
        private String IconClass;
        @Expose
        @SerializedName("Type")
        private int Type;
        @Expose
        @SerializedName("CategoryTypeId")
        private int CategoryTypeId;
        @Expose
        @SerializedName("NameAr")
        private String NameAr;
        @Expose
        @SerializedName("NameEn")
        private String NameEn;
        @Expose
        @SerializedName("Id")
        private int Id;

        public String getColorCode() {
            return ColorCode;
        }

        public void setColorCode(String ColorCode) {
            this.ColorCode = ColorCode;
        }

        public String getUniCode() {
            return UniCode;
        }

        public void setUniCode(String UniCode) {
            this.UniCode = UniCode;
        }

        public String getIconClass() {
            return IconClass;
        }

        public void setIconClass(String IconClass) {
            this.IconClass = IconClass;
        }

        public int getType() {
            return Type;
        }

        public void setType(int Type) {
            this.Type = Type;
        }

        public int getCategoryTypeId() {
            return CategoryTypeId;
        }

        public void setCategoryTypeId(int CategoryTypeId) {
            this.CategoryTypeId = CategoryTypeId;
        }

        public String getNameAr() {
            return NameAr;
        }

        public void setNameAr(String NameAr) {
            this.NameAr = NameAr;
        }

        public String getNameEn() {
            return NameEn;
        }

        public void setNameEn(String NameEn) {
            this.NameEn = NameEn;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }

    public static class Offers2 implements Serializable {
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<String> jproductvariant;
        @Expose
        @SerializedName("JCategories")
        private ArrayList<JCategories2> jCategories;
        @Expose
        @SerializedName("rnk")
        private int rnk;
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("ProductVariant")
        private String productvariant;
        @Expose
        @SerializedName("Image")
        private String image;
        @Expose
        @SerializedName("SubCategoryImage")
        private String subcategoryimage;
        @Expose
        @SerializedName("SubCategoryNameAr")
        private String subcategorynamear;
        @Expose
        @SerializedName("SubCategoryNameEn")
        private String subcategorynameen;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int branchsubcategoryid;
        @Expose
        @SerializedName("SubCategoryId")
        private int subcategoryid;
        @Expose
        @SerializedName("CategoryId")
        private int categoryid;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("StoreId")
        private int storeid;
        @Expose
        @SerializedName("CountingNameAr")
        private String countingnamear;
        @Expose
        @SerializedName("EndDate")
        private String enddate;
        @Expose
        @SerializedName("StartDate")
        private String startdate;
        @Expose
        @SerializedName("CountingNameEn")
        private String countingnameen;
        @Expose
        @SerializedName("Price")
        private double price;
        @Expose
        @SerializedName("DiscountType")
        private int discounttype;
        @Expose
        @SerializedName("DiscountValue")
        private float discountvalue;
        @Expose
        @SerializedName("Discount")
        private float discount;
        @Expose
        @SerializedName("SellingPrice")
        private double sellingprice;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean isvatapplicable;
        @Expose
        @SerializedName("ServiceNameAr")
        private String servicenamear;
        @Expose
        @SerializedName("ServiceNameEn")
        private String servicenameen;
        @Expose
        @SerializedName("ServiceId")
        private int serviceid;
        @Expose
        @SerializedName("Id")
        private int id;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("MainCategoryNameEn")
        private String MainCategoryNameEn;
        @Expose
        @SerializedName("MainCategoryNameAr")
        private String MainCategoryNameAr;
        @Expose
        @SerializedName("MainCategoryImage")
        private String MainCategoryImage;
        @Expose
        @SerializedName("BranchMainCategoryId")
        private int BranchMainCategoryId;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;

        public ArrayList<JCategories2> getjCategories() {
            return jCategories;
        }

        public void setjCategories(ArrayList<JCategories2> jCategories) {
            this.jCategories = jCategories;
        }

        public boolean isFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean favorite) {
            Favorite = favorite;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }

        public String getMainCategoryNameEn() {
            return MainCategoryNameEn;
        }

        public void setMainCategoryNameEn(String mainCategoryNameEn) {
            MainCategoryNameEn = mainCategoryNameEn;
        }

        public String getMainCategoryNameAr() {
            return MainCategoryNameAr;
        }

        public void setMainCategoryNameAr(String mainCategoryNameAr) {
            MainCategoryNameAr = mainCategoryNameAr;
        }

        public String getMainCategoryImage() {
            return MainCategoryImage;
        }

        public void setMainCategoryImage(String mainCategoryImage) {
            MainCategoryImage = mainCategoryImage;
        }

        public int getBranchMainCategoryId() {
            return BranchMainCategoryId;
        }

        public void setBranchMainCategoryId(int branchMainCategoryId) {
            BranchMainCategoryId = branchMainCategoryId;
        }

        public ArrayList<String> getJproductvariant() {
            return jproductvariant;
        }

        public void setJproductvariant(ArrayList<String> jproductvariant) {
            this.jproductvariant = jproductvariant;
        }

        public int getRnk() {
            return rnk;
        }

        public void setRnk(int rnk) {
            this.rnk = rnk;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getProductvariant() {
            return productvariant;
        }

        public void setProductvariant(String productvariant) {
            this.productvariant = productvariant;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getSubcategoryimage() {
            return subcategoryimage;
        }

        public void setSubcategoryimage(String subcategoryimage) {
            this.subcategoryimage = subcategoryimage;
        }

        public String getSubcategorynamear() {
            return subcategorynamear;
        }

        public void setSubcategorynamear(String subcategorynamear) {
            this.subcategorynamear = subcategorynamear;
        }

        public String getSubcategorynameen() {
            return subcategorynameen;
        }

        public void setSubcategorynameen(String subcategorynameen) {
            this.subcategorynameen = subcategorynameen;
        }

        public int getBranchsubcategoryid() {
            return branchsubcategoryid;
        }

        public void setBranchsubcategoryid(int branchsubcategoryid) {
            this.branchsubcategoryid = branchsubcategoryid;
        }

        public int getSubcategoryid() {
            return subcategoryid;
        }

        public void setSubcategoryid(int subcategoryid) {
            this.subcategoryid = subcategoryid;
        }

        public int getCategoryid() {
            return categoryid;
        }

        public void setCategoryid(int categoryid) {
            this.categoryid = categoryid;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public String getCountingnamear() {
            return countingnamear;
        }

        public void setCountingnamear(String countingnamear) {
            this.countingnamear = countingnamear;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public String getCountingnameen() {
            return countingnameen;
        }

        public void setCountingnameen(String countingnameen) {
            this.countingnameen = countingnameen;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getDiscounttype() {
            return discounttype;
        }

        public void setDiscounttype(int discounttype) {
            this.discounttype = discounttype;
        }

        public float getDiscountvalue() {
            return discountvalue;
        }

        public void setDiscountvalue(float discountvalue) {
            this.discountvalue = discountvalue;
        }

        public float getDiscount() {
            return discount;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public double getSellingprice() {
            return sellingprice;
        }

        public void setSellingprice(double sellingprice) {
            this.sellingprice = sellingprice;
        }

        public boolean getIsvatapplicable() {
            return isvatapplicable;
        }

        public void setIsvatapplicable(boolean isvatapplicable) {
            this.isvatapplicable = isvatapplicable;
        }

        public String getServicenamear() {
            return servicenamear;
        }

        public void setServicenamear(String servicenamear) {
            this.servicenamear = servicenamear;
        }

        public String getServicenameen() {
            return servicenameen;
        }

        public void setServicenameen(String servicenameen) {
            this.servicenameen = servicenameen;
        }

        public int getServiceid() {
            return serviceid;
        }

        public void setServiceid(int serviceid) {
            this.serviceid = serviceid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class JCategories implements Serializable {
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int categoryId) {
            CategoryId = categoryId;
        }
    }

    public static class JCategories2 implements Serializable {
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int categoryId) {
            CategoryId = categoryId;
        }
    }

    public static class Offers implements Serializable {
        @Expose
        @SerializedName("JProductVariant")
        private ArrayList<Jproductvariant> jproductvariant;
        @Expose
        @SerializedName("JCategories")
        private ArrayList<JCategories> jCategories;
        @Expose
        @SerializedName("rnk")
        private int rnk;
        @Expose
        @SerializedName("Distance")
        private double distance;
        @Expose
        @SerializedName("BranchNameAr")
        private String branchnamear;
        @Expose
        @SerializedName("BranchNameEn")
        private String branchnameen;
        @Expose
        @SerializedName("Condition")
        private int condition;
        @Expose
        @SerializedName("InventoryId")
        private int inventoryid;
        @Expose
        @SerializedName("ProductSkuId")
        private String productskuid;
        @Expose
        @SerializedName("StockQuantity")
        private int stockquantity;
        @Expose
        @SerializedName("UPCBarcode")
        private String upcbarcode;
        @Expose
        @SerializedName("SubCategoryImage")
        private String subcategoryimage;
        @Expose
        @SerializedName("SubCategoryNameAr")
        private String subcategorynamear;
        @Expose
        @SerializedName("SubCategoryNameEn")
        private String subcategorynameen;
        @Expose
        @SerializedName("BranchSubCategoryId")
        private int branchsubcategoryid;
        @Expose
        @SerializedName("BranchCategoryId")
        private int branchcategoryid;
        @Expose
        @SerializedName("BranchId")
        private int branchid;
        @Expose
        @SerializedName("ProductVariant")
        private String productvariant;
        @Expose
        @SerializedName("SellingPrice")
        private double sellingprice;
        @Expose
        @SerializedName("DiscountValue")
        private float discountvalue;
        @Expose
        @SerializedName("Discount")
        private float discount;
        @Expose
        @SerializedName("Price")
        private double price;
        @Expose
        @SerializedName("DiscountType")
        private int discounttype;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean isvatapplicable;
        @Expose
        @SerializedName("StoreId")
        private int storeid;
        @Expose
        @SerializedName("CountryId")
        private int countryid;
        @Expose
        @SerializedName("EndDate")
        private String enddate;
        @Expose
        @SerializedName("StartDate")
        private String startdate;
        @Expose
        @SerializedName("ProductId")
        private int productid;
        @Expose
        @SerializedName("Percentage")
        private int percentage;
        @Expose
        @SerializedName("Image")
        private String image;
        @Expose
        @SerializedName("BannerImageAr")
        private String bannerimagear;
        @Expose
        @SerializedName("BannerImageEn")
        private String bannerimageen;
        @Expose
        @SerializedName("OfferType")
        private int offertype;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("Id")
        private int id;
        @Expose
        @SerializedName("Logo")
        private String Logo;
        @Expose
        @SerializedName("MainCategoryNameEn")
        private String MainCategoryNameEn;
        @Expose
        @SerializedName("MainCategoryNameAr")
        private String MainCategoryNameAr;
        @Expose
        @SerializedName("MainCategoryImage")
        private String MainCategoryImage;
        @Expose
        @SerializedName("BranchMainCategoryId")
        private int BranchMainCategoryId;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;

        public ArrayList<JCategories> getjCategories() {
            return jCategories;
        }

        public void setjCategories(ArrayList<JCategories> jCategories) {
            this.jCategories = jCategories;
        }

        public boolean isFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean favorite) {
            Favorite = favorite;
        }

        public String getLogo() {
            return Logo;
        }

        public void setLogo(String logo) {
            Logo = logo;
        }

        public String getMainCategoryNameEn() {
            return MainCategoryNameEn;
        }

        public void setMainCategoryNameEn(String mainCategoryNameEn) {
            MainCategoryNameEn = mainCategoryNameEn;
        }

        public String getMainCategoryNameAr() {
            return MainCategoryNameAr;
        }

        public void setMainCategoryNameAr(String mainCategoryNameAr) {
            MainCategoryNameAr = mainCategoryNameAr;
        }

        public String getMainCategoryImage() {
            return MainCategoryImage;
        }

        public void setMainCategoryImage(String mainCategoryImage) {
            MainCategoryImage = mainCategoryImage;
        }

        public int getBranchMainCategoryId() {
            return BranchMainCategoryId;
        }

        public void setBranchMainCategoryId(int branchMainCategoryId) {
            BranchMainCategoryId = branchMainCategoryId;
        }

        public ArrayList<Jproductvariant> getJproductvariant() {
            return jproductvariant;
        }

        public void setJproductvariant(ArrayList<Jproductvariant> jproductvariant) {
            this.jproductvariant = jproductvariant;
        }

        public int getRnk() {
            return rnk;
        }

        public void setRnk(int rnk) {
            this.rnk = rnk;
        }

        public double getDistance() {
            return distance;
        }

        public void setDistance(double distance) {
            this.distance = distance;
        }

        public String getBranchnamear() {
            return branchnamear;
        }

        public void setBranchnamear(String branchnamear) {
            this.branchnamear = branchnamear;
        }

        public String getBranchnameen() {
            return branchnameen;
        }

        public void setBranchnameen(String branchnameen) {
            this.branchnameen = branchnameen;
        }

        public int getCondition() {
            return condition;
        }

        public void setCondition(int condition) {
            this.condition = condition;
        }

        public int getInventoryid() {
            return inventoryid;
        }

        public void setInventoryid(int inventoryid) {
            this.inventoryid = inventoryid;
        }

        public String getProductskuid() {
            return productskuid;
        }

        public void setProductskuid(String productskuid) {
            this.productskuid = productskuid;
        }

        public int getStockquantity() {
            return stockquantity;
        }

        public void setStockquantity(int stockquantity) {
            this.stockquantity = stockquantity;
        }

        public String getUpcbarcode() {
            return upcbarcode;
        }

        public void setUpcbarcode(String upcbarcode) {
            this.upcbarcode = upcbarcode;
        }

        public String getSubcategoryimage() {
            return subcategoryimage;
        }

        public void setSubcategoryimage(String subcategoryimage) {
            this.subcategoryimage = subcategoryimage;
        }

        public String getSubcategorynamear() {
            return subcategorynamear;
        }

        public void setSubcategorynamear(String subcategorynamear) {
            this.subcategorynamear = subcategorynamear;
        }

        public String getSubcategorynameen() {
            return subcategorynameen;
        }

        public void setSubcategorynameen(String subcategorynameen) {
            this.subcategorynameen = subcategorynameen;
        }

        public int getBranchsubcategoryid() {
            return branchsubcategoryid;
        }

        public void setBranchsubcategoryid(int branchsubcategoryid) {
            this.branchsubcategoryid = branchsubcategoryid;
        }

        public int getBranchcategoryid() {
            return branchcategoryid;
        }

        public void setBranchcategoryid(int branchcategoryid) {
            this.branchcategoryid = branchcategoryid;
        }

        public int getBranchid() {
            return branchid;
        }

        public void setBranchid(int branchid) {
            this.branchid = branchid;
        }

        public String getProductvariant() {
            return productvariant;
        }

        public void setProductvariant(String productvariant) {
            this.productvariant = productvariant;
        }

        public double getSellingprice() {
            return sellingprice;
        }

        public void setSellingprice(double sellingprice) {
            this.sellingprice = sellingprice;
        }

        public float getDiscountvalue() {
            return discountvalue;
        }

        public void setDiscountvalue(float discountvalue) {
            this.discountvalue = discountvalue;
        }

        public float getDiscount() {
            return discount;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getDiscounttype() {
            return discounttype;
        }

        public void setDiscounttype(int discounttype) {
            this.discounttype = discounttype;
        }

        public boolean getIsvatapplicable() {
            return isvatapplicable;
        }

        public void setIsvatapplicable(boolean isvatapplicable) {
            this.isvatapplicable = isvatapplicable;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public int getCountryid() {
            return countryid;
        }

        public void setCountryid(int countryid) {
            this.countryid = countryid;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public int getPercentage() {
            return percentage;
        }

        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getBannerimagear() {
            return bannerimagear;
        }

        public void setBannerimagear(String bannerimagear) {
            this.bannerimagear = bannerimagear;
        }

        public String getBannerimageen() {
            return bannerimageen;
        }

        public void setBannerimageen(String bannerimageen) {
            this.bannerimageen = bannerimageen;
        }

        public int getOffertype() {
            return offertype;
        }

        public void setOffertype(int offertype) {
            this.offertype = offertype;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Jproductvariant implements Serializable {
        @Expose
        @SerializedName("ValueAr")
        private String valuear;
        @Expose
        @SerializedName("ValueEn")
        private String valueen;
        @Expose
        @SerializedName("OptionId")
        private int optionid;
        @Expose
        @SerializedName("ProductInventoryId")
        private int productinventoryid;

        public String getValuear() {
            return valuear;
        }

        public void setValuear(String valuear) {
            this.valuear = valuear;
        }

        public String getValueen() {
            return valueen;
        }

        public void setValueen(String valueen) {
            this.valueen = valueen;
        }

        public int getOptionid() {
            return optionid;
        }

        public void setOptionid(int optionid) {
            this.optionid = optionid;
        }

        public int getProductinventoryid() {
            return productinventoryid;
        }

        public void setProductinventoryid(int productinventoryid) {
            this.productinventoryid = productinventoryid;
        }
    }

    public static class Homebanners  implements Serializable{
        @Expose
        @SerializedName("storeId")
        private int storeid;
        @Expose
        @SerializedName("WeekNo")
        private int weekno;
        @Expose
        @SerializedName("IsUptoApplicable")
        private boolean isuptoapplicable;
        @Expose
        @SerializedName("CountryId")
        private int countryid;
        @Expose
        @SerializedName("EndDate")
        private String enddate;
        @Expose
        @SerializedName("StartDate")
        private String startdate;
        @Expose
        @SerializedName("ProductId")
        private int productid;
        @Expose
        @SerializedName("Percentage")
        private int percentage;
        @Expose
        @SerializedName("BannerImageAr")
        private String bannerimagear;
        @Expose
        @SerializedName("BannerImageEn")
        private String bannerimageen;
        @Expose
        @SerializedName("OfferType")
        private int offertype;
        @Expose
        @SerializedName("NameAr")
        private String namear;
        @Expose
        @SerializedName("NameEn")
        private String nameen;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("latitude")
        private double latitude;
        @Expose
        @SerializedName("longitude")
        private double longitude;
        @Expose
        @SerializedName("Id")
        private int id;

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String storeNameEn) {
            StoreNameEn = storeNameEn;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getStoreid() {
            return storeid;
        }

        public void setStoreid(int storeid) {
            this.storeid = storeid;
        }

        public int getWeekno() {
            return weekno;
        }

        public void setWeekno(int weekno) {
            this.weekno = weekno;
        }

        public boolean getIsuptoapplicable() {
            return isuptoapplicable;
        }

        public void setIsuptoapplicable(boolean isuptoapplicable) {
            this.isuptoapplicable = isuptoapplicable;
        }

        public int getCountryid() {
            return countryid;
        }

        public void setCountryid(int countryid) {
            this.countryid = countryid;
        }

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getStartdate() {
            return startdate;
        }

        public void setStartdate(String startdate) {
            this.startdate = startdate;
        }

        public int getProductid() {
            return productid;
        }

        public void setProductid(int productid) {
            this.productid = productid;
        }

        public int getPercentage() {
            return percentage;
        }

        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }

        public String getBannerimagear() {
            return bannerimagear;
        }

        public void setBannerimagear(String bannerimagear) {
            this.bannerimagear = bannerimagear;
        }

        public String getBannerimageen() {
            return bannerimageen;
        }

        public void setBannerimageen(String bannerimageen) {
            this.bannerimageen = bannerimageen;
        }

        public int getOffertype() {
            return offertype;
        }

        public void setOffertype(int offertype) {
            this.offertype = offertype;
        }

        public String getNamear() {
            return namear;
        }

        public void setNamear(String namear) {
            this.namear = namear;
        }

        public String getNameen() {
            return nameen;
        }

        public void setNameen(String nameen) {
            this.nameen = nameen;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
