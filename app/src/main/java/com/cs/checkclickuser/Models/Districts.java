package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Districts {

    @Expose
    @SerializedName("CityAr")
    private String cityar;
    @Expose
    @SerializedName("CityEn")
    private String cityen;
    @Expose
    @SerializedName("CityId")
    private int cityid;
    @Expose
    @SerializedName("Longitude")
    private String longitude;
    @Expose
    @SerializedName("Latitude")
    private String latitude;
    @Expose
    @SerializedName("NameAr")
    private String namear;
    @Expose
    @SerializedName("NameEn")
    private String nameen;
    @Expose
    @SerializedName("Id")
    private int id;

    public String getCityar() {
        return cityar;
    }

    public void setCityar(String cityar) {
        this.cityar = cityar;
    }

    public String getCityen() {
        return cityen;
    }

    public void setCityen(String cityen) {
        this.cityen = cityen;
    }

    public int getCityid() {
        return cityid;
    }

    public void setCityid(int cityid) {
        this.cityid = cityid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getNamear() {
        return namear;
    }

    public void setNamear(String namear) {
        this.namear = namear;
    }

    public String getNameen() {
        return nameen;
    }

    public void setNameen(String nameen) {
        this.nameen = nameen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
