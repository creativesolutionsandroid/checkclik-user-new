package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class CouponResponce {

    @Expose
    @SerializedName("Data")
    private ArrayList<Data> Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public ArrayList<Data> getData() {
        return Data;
    }

    public void setData(ArrayList<Data> Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;
        @Expose
        @SerializedName("RowRank")
        private int RowRank;
        @Expose
        @SerializedName("Branches")
        private String Branches;
        @Expose
        @SerializedName("Regions")
        private String Regions;
        @Expose
        @SerializedName("LogoCopy")
        private String LogoCopy;
        @Expose
        @SerializedName("BackgroundCopy")
        private String BackgroundCopy;
        @Expose
        @SerializedName("StoreNameAr")
        private String StoreNameAr;
        @Expose
        @SerializedName("StoreNameEn")
        private String StoreNameEn;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("InvoiceNo")
        private String InvoiceNo;
        @Expose
        @SerializedName("PaymentStatus")
        private boolean PaymentStatus;
        @Expose
        @SerializedName("VoucherAmount")
        private int VoucherAmount;
        @Expose
        @SerializedName("NTimes")
        private int NTimes;
        @Expose
        @SerializedName("DiscountLimitation")
        private int DiscountLimitation;
        @Expose
        @SerializedName("CouponCode")
        private String CouponCode;
        @Expose
        @SerializedName("IsCouponCodeRequires")
        private boolean IsCouponCodeRequires;
        @Expose
        @SerializedName("MaxDiscountAmount")
        private int MaxDiscountAmount;
        @Expose
        @SerializedName("IsUptoApplicable")
        private boolean IsUptoApplicable;
        @Expose
        @SerializedName("VoucherDiscount")
        private int VoucherDiscount;
        @Expose
        @SerializedName("VoucherNameAr")
        private String VoucherNameAr;
        @Expose
        @SerializedName("VoucherNameEn")
        private String VoucherNameEn;
        @Expose
        @SerializedName("UserSlabID")
        private int UserSlabID;
        @Expose
        @SerializedName("EndDate")
        private String EndDate;
        @Expose
        @SerializedName("StartDate")
        private String StartDate;
        @Expose
        @SerializedName("Id")
        private int Id;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }

        public int getRowRank() {
            return RowRank;
        }

        public void setRowRank(int RowRank) {
            this.RowRank = RowRank;
        }

        public String getBranches() {
            return Branches;
        }

        public void setBranches(String Branches) {
            this.Branches = Branches;
        }

        public String getRegions() {
            return Regions;
        }

        public void setRegions(String Regions) {
            this.Regions = Regions;
        }

        public String getLogoCopy() {
            return LogoCopy;
        }

        public void setLogoCopy(String LogoCopy) {
            this.LogoCopy = LogoCopy;
        }

        public String getBackgroundCopy() {
            return BackgroundCopy;
        }

        public void setBackgroundCopy(String BackgroundCopy) {
            this.BackgroundCopy = BackgroundCopy;
        }

        public String getStoreNameAr() {
            return StoreNameAr;
        }

        public void setStoreNameAr(String StoreNameAr) {
            this.StoreNameAr = StoreNameAr;
        }

        public String getStoreNameEn() {
            return StoreNameEn;
        }

        public void setStoreNameEn(String StoreNameEn) {
            this.StoreNameEn = StoreNameEn;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getInvoiceNo() {
            return InvoiceNo;
        }

        public void setInvoiceNo(String InvoiceNo) {
            this.InvoiceNo = InvoiceNo;
        }

        public boolean getPaymentStatus() {
            return PaymentStatus;
        }

        public void setPaymentStatus(boolean PaymentStatus) {
            this.PaymentStatus = PaymentStatus;
        }

        public int getVoucherAmount() {
            return VoucherAmount;
        }

        public void setVoucherAmount(int VoucherAmount) {
            this.VoucherAmount = VoucherAmount;
        }

        public int getNTimes() {
            return NTimes;
        }

        public void setNTimes(int NTimes) {
            this.NTimes = NTimes;
        }

        public int getDiscountLimitation() {
            return DiscountLimitation;
        }

        public void setDiscountLimitation(int DiscountLimitation) {
            this.DiscountLimitation = DiscountLimitation;
        }

        public String getCouponCode() {
            return CouponCode;
        }

        public void setCouponCode(String CouponCode) {
            this.CouponCode = CouponCode;
        }

        public boolean getIsCouponCodeRequires() {
            return IsCouponCodeRequires;
        }

        public void setIsCouponCodeRequires(boolean IsCouponCodeRequires) {
            this.IsCouponCodeRequires = IsCouponCodeRequires;
        }

        public int getMaxDiscountAmount() {
            return MaxDiscountAmount;
        }

        public void setMaxDiscountAmount(int MaxDiscountAmount) {
            this.MaxDiscountAmount = MaxDiscountAmount;
        }

        public boolean getIsUptoApplicable() {
            return IsUptoApplicable;
        }

        public void setIsUptoApplicable(boolean IsUptoApplicable) {
            this.IsUptoApplicable = IsUptoApplicable;
        }

        public int getVoucherDiscount() {
            return VoucherDiscount;
        }

        public void setVoucherDiscount(int VoucherDiscount) {
            this.VoucherDiscount = VoucherDiscount;
        }

        public String getVoucherNameAr() {
            return VoucherNameAr;
        }

        public void setVoucherNameAr(String VoucherNameAr) {
            this.VoucherNameAr = VoucherNameAr;
        }

        public String getVoucherNameEn() {
            return VoucherNameEn;
        }

        public void setVoucherNameEn(String VoucherNameEn) {
            this.VoucherNameEn = VoucherNameEn;
        }

        public int getUserSlabID() {
            return UserSlabID;
        }

        public void setUserSlabID(int UserSlabID) {
            this.UserSlabID = UserSlabID;
        }

        public String getEndDate() {
            return EndDate;
        }

        public void setEndDate(String EndDate) {
            this.EndDate = EndDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setStartDate(String StartDate) {
            this.StartDate = StartDate;
        }

        public int getId() {
            return Id;
        }

        public void setId(int Id) {
            this.Id = Id;
        }
    }
}
