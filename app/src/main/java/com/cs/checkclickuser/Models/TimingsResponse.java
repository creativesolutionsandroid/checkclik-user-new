package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TimingsResponse {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("TimingSlots")
        private ArrayList<TimingSlots> TimingSlots;
        @Expose
        @SerializedName("DisableDays")
        private ArrayList<DisableDays> DisableDays;

        public ArrayList<TimingSlots> getTimingSlots() {
            return TimingSlots;
        }

        public void setTimingSlots(ArrayList<TimingSlots> TimingSlots) {
            this.TimingSlots = TimingSlots;
        }

        public ArrayList<DisableDays> getDisableDays() {
            return DisableDays;
        }

        public void setDisableDays(ArrayList<DisableDays> DisableDays) {
            this.DisableDays = DisableDays;
        }
    }

    public static class TimingSlots {
        @Expose
        @SerializedName("WeekdayId")
        private int WeekdayId;
        @Expose
        @SerializedName("Weekday")
        private String Weekday;
        @Expose
        @SerializedName("EndTime")
        private String EndTime;
        @Expose
        @SerializedName("StartTime")
        private String StartTime;
        @Expose
        @SerializedName("TimeSlot")
        private String TimeSlot;
        @Expose
        @SerializedName("TimeSlotId")
        private int TimeSlotId;

        public int getWeekdayId() {
            return WeekdayId;
        }

        public void setWeekdayId(int WeekdayId) {
            this.WeekdayId = WeekdayId;
        }

        public String getWeekday() {
            return Weekday;
        }

        public void setWeekday(String Weekday) {
            this.Weekday = Weekday;
        }

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String EndTime) {
            this.EndTime = EndTime;
        }

        public String getStartTime() {
            return StartTime;
        }

        public void setStartTime(String StartTime) {
            this.StartTime = StartTime;
        }

        public String getTimeSlot() {
            return TimeSlot;
        }

        public void setTimeSlot(String TimeSlot) {
            this.TimeSlot = TimeSlot;
        }

        public int getTimeSlotId() {
            return TimeSlotId;
        }

        public void setTimeSlotId(int TimeSlotId) {
            this.TimeSlotId = TimeSlotId;
        }
    }

    public static class DisableDays {
        @Expose
        @SerializedName("Weekday")
        private String Weekday;
        @Expose
        @SerializedName("WeekdayId")
        private int WeekdayId;

        public String getWeekday() {
            return Weekday;
        }

        public void setWeekday(String Weekday) {
            this.Weekday = Weekday;
        }

        public int getWeekdayId() {
            return WeekdayId;
        }

        public void setWeekdayId(int WeekdayId) {
            this.WeekdayId = WeekdayId;
        }
    }
}
