package com.cs.checkclickuser.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServiceDealsResponse {

    @Expose
    @SerializedName("Data")
    private Data Data;
    @Expose
    @SerializedName("Message")
    private String Message;
    @Expose
    @SerializedName("Status")
    private boolean Status;

    public Data getData() {
        return Data;
    }

    public void setData(Data Data) {
        this.Data = Data;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public boolean getStatus() {
        return Status;
    }

    public void setStatus(boolean Status) {
        this.Status = Status;
    }

    public static class Data {
        @Expose
        @SerializedName("TotalCount")
        private List<TotalCount> TotalCount;
        @Expose
        @SerializedName("Deals")
        private ArrayList<Deals> Deals;
        @Expose
        @SerializedName("Branch")
        private int Branch;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;

        public List<TotalCount> getTotalCount() {
            return TotalCount;
        }

        public void setTotalCount(List<TotalCount> TotalCount) {
            this.TotalCount = TotalCount;
        }

        public ArrayList<Deals> getDeals() {
            return Deals;
        }

        public void setDeals(ArrayList<Deals> Deals) {
            this.Deals = Deals;
        }

        public int getBranch() {
            return Branch;
        }

        public void setBranch(int Branch) {
            this.Branch = Branch;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }
    }

    public static class TotalCount {
        @Expose
        @SerializedName("TotalRecords")
        private int TotalRecords;

        public int getTotalRecords() {
            return TotalRecords;
        }

        public void setTotalRecords(int TotalRecords) {
            this.TotalRecords = TotalRecords;
        }
    }

    public static class Deals {
        @Expose
        @SerializedName("JProductVariant")
        private List<String> JProductVariant;
        @Expose
        @SerializedName("ProductVariant")
        private String ProductVariant;
        @Expose
        @SerializedName("Image")
        private String Image;
        @Expose
        @SerializedName("Favorite")
        private boolean Favorite;
        @Expose
        @SerializedName("SubCategoryId")
        private int SubCategoryId;
        @Expose
        @SerializedName("CategoryId")
        private int CategoryId;
        @Expose
        @SerializedName("BranchId")
        private int BranchId;
        @Expose
        @SerializedName("StoreId")
        private int StoreId;
        @Expose
        @SerializedName("CountingNameAr")
        private String CountingNameAr;
        @Expose
        @SerializedName("CountingNameEn")
        private String CountingNameEn;
        @Expose
        @SerializedName("Price")
        private double Price;
        @Expose
        @SerializedName("DiscountValue")
        private int DiscountValue;
        @Expose
        @SerializedName("SellingPrice")
        private int SellingPrice;
        @Expose
        @SerializedName("IsVatApplicable")
        private boolean IsVatApplicable;
        @Expose
        @SerializedName("ServiceNameAr")
        private String ServiceNameAr;
        @Expose
        @SerializedName("ServiceNameEn")
        private String ServiceNameEn;
        @Expose
        @SerializedName("ServiceId")
        private int ServiceId;
        @Expose
        @SerializedName("rowNum")
        private int rowNum;

        public List<String> getJProductVariant() {
            return JProductVariant;
        }

        public void setJProductVariant(List<String> JProductVariant) {
            this.JProductVariant = JProductVariant;
        }

        public String getProductVariant() {
            return ProductVariant;
        }

        public void setProductVariant(String ProductVariant) {
            this.ProductVariant = ProductVariant;
        }

        public String getImage() {
            return Image;
        }

        public void setImage(String Image) {
            this.Image = Image;
        }

        public boolean getFavorite() {
            return Favorite;
        }

        public void setFavorite(boolean Favorite) {
            this.Favorite = Favorite;
        }

        public int getSubCategoryId() {
            return SubCategoryId;
        }

        public void setSubCategoryId(int SubCategoryId) {
            this.SubCategoryId = SubCategoryId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public int getBranchId() {
            return BranchId;
        }

        public void setBranchId(int BranchId) {
            this.BranchId = BranchId;
        }

        public int getStoreId() {
            return StoreId;
        }

        public void setStoreId(int StoreId) {
            this.StoreId = StoreId;
        }

        public String getCountingNameAr() {
            return CountingNameAr;
        }

        public void setCountingNameAr(String CountingNameAr) {
            this.CountingNameAr = CountingNameAr;
        }

        public String getCountingNameEn() {
            return CountingNameEn;
        }

        public void setCountingNameEn(String CountingNameEn) {
            this.CountingNameEn = CountingNameEn;
        }

        public double getPrice() {
            return Price;
        }

        public void setPrice(double Price) {
            this.Price = Price;
        }

        public int getDiscountValue() {
            return DiscountValue;
        }

        public void setDiscountValue(int DiscountValue) {
            this.DiscountValue = DiscountValue;
        }

        public int getSellingPrice() {
            return SellingPrice;
        }

        public void setSellingPrice(int SellingPrice) {
            this.SellingPrice = SellingPrice;
        }

        public boolean getIsVatApplicable() {
            return IsVatApplicable;
        }

        public void setIsVatApplicable(boolean IsVatApplicable) {
            this.IsVatApplicable = IsVatApplicable;
        }

        public String getServiceNameAr() {
            return ServiceNameAr;
        }

        public void setServiceNameAr(String ServiceNameAr) {
            this.ServiceNameAr = ServiceNameAr;
        }

        public String getServiceNameEn() {
            return ServiceNameEn;
        }

        public void setServiceNameEn(String ServiceNameEn) {
            this.ServiceNameEn = ServiceNameEn;
        }

        public int getServiceId() {
            return ServiceId;
        }

        public void setServiceId(int ServiceId) {
            this.ServiceId = ServiceId;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }
    }
}
