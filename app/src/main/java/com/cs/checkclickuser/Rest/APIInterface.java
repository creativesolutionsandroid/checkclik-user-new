package com.cs.checkclickuser.Rest;


import com.cs.checkclickuser.Models.AddAddressResponse;
import com.cs.checkclickuser.Models.AddToCartResponse;
import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.ApplyCoupon;
import com.cs.checkclickuser.Models.BankDetails;
import com.cs.checkclickuser.Models.BarcodeProductResponse;
import com.cs.checkclickuser.Models.BasicResponse;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.BranchDetailsResponse;
import com.cs.checkclickuser.Models.BranchGroupId;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.ChangeEmailResponce;
import com.cs.checkclickuser.Models.ChangeMobileOtp;
import com.cs.checkclickuser.Models.CheckOutId;
import com.cs.checkclickuser.Models.CheckoutSchedule;
import com.cs.checkclickuser.Models.CheckoutSlots;
import com.cs.checkclickuser.Models.EditAddressResponse;
import com.cs.checkclickuser.Models.EditMobileNoResponse;
import com.cs.checkclickuser.Models.ForgetPasswordOTPResponse;
import com.cs.checkclickuser.Models.GetOrderDetails;
import com.cs.checkclickuser.Models.InsertOrderResponse;
import com.cs.checkclickuser.Models.MyCards;
import com.cs.checkclickuser.Models.Notifications;
import com.cs.checkclickuser.Models.PaymentMethods;
import com.cs.checkclickuser.Models.ProductCategoryResponce;
import com.cs.checkclickuser.Models.ChangePasswordResponce;
import com.cs.checkclickuser.Models.CouponResponce;
import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.HomeCityResponse;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.LocationResponse;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.Models.ProductStoreReviews;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.Models.PushNotificationResponce;
import com.cs.checkclickuser.Models.PushNotificationsUpdateResponce;
import com.cs.checkclickuser.Models.ResetPasswordResponse;
import com.cs.checkclickuser.Models.ResetpasswordResponce;
import com.cs.checkclickuser.Models.Response;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.Models.SearchListResponce;
import com.cs.checkclickuser.Models.SearchProductListResponce;
import com.cs.checkclickuser.Models.ServiceDealsResponse;
import com.cs.checkclickuser.Models.ServiceItemResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.Models.ShipmentBasicResponse;
import com.cs.checkclickuser.Models.Signinresponce;
import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.Models.TimingsResponse;
import com.cs.checkclickuser.Models.UpdateCartResponse;
import com.cs.checkclickuser.Models.UpdateEmailIdResponse;
import com.cs.checkclickuser.Models.UpdateProfileResponse;
import com.cs.checkclickuser.Models.UrlDecryptResponse;
import com.cs.checkclickuser.Models.UrlEncryptResponse;
import com.cs.checkclickuser.Models.VerifyEmailResponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("api/CustomerRegistrationAPI/NewGetMobileOTP")
    Call<VerifyMobileResponse> verfiyMobileNumber(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUserRegistration")
    Call<Signupresponse> userRegistration(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUserValidation")
    Call<Signinresponce> userLogin(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewVerifyEmail")
    Call<VerifyEmailResponse> VerifyEmail(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewResetPassword")
    Call<ResetpasswordResponce> resetPassword(@Body RequestBody body);

//    @POST("api/CustomerAPI/NewGetMainCategoryAndStore")
//    Call<ProductlistResponce> getproductlist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetMainCategoryAndStore")
    Call<ProductCategoryResponce> catlist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetBranchMainSubCatgory")
    Call<ProductstoreResponce> getstorelist(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetStoreBranchDeals")
    Call<DealsResponce> dealsresponce(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetStoreBranchDeals")
    Call<ServiceDealsResponse> serviceDealsResponce(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetHomePageDetails")
    Call<HomePageResponse> getHomePageDetails(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetLocationByLatLng")
    Call<LocationResponse> getLocationDetails(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetCountryDistricts")
    Call<HomeCityResponse> getCityList(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewResetPassword")
    Call<ChangePasswordResponce> getchagepassword(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetDeleteUserFavorite")
    Call<FavStoresResponce> getfavorites(@Body RequestBody body);

    @POST("api/CustomerAPI/NewAddUpdateCustomerAddress")
    Call<ManageAdressResponce> getaddress(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/GetUpdatePNConfiguration")
    Call<PushNotificationResponce> getpushnotification(@Body RequestBody body);

    @POST("api/OrdersAPI/NewGetAllCoupons")
    Call<CouponResponce> getcoupons(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUpdateProfile")
    Call<UpdateProfileResponse> getupdateprofile(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUpdateEmail")
    Call<UpdateEmailIdResponse> getupdateEmailId(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUpdateMobile")
    Call<EditMobileNoResponse> getupdateMobileNo(@Body RequestBody body);


//    @POST("api/CustomerAPI/NewAddUpdateCustomerAddressapi/CustomerAPI/NewAddUpdateCustomerAddress")
//    Call<AddAddressResponce> getaddaddress(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsListForCustomers")
    Call<ProductItemResponse> getProductList(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetServicesListForCustomers")
    Call<ServiceItemResponse> getServiceList(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetProductsDetailsForCustomers")
    Call<ProductVariantResponse> getProductVariants(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetServicesDetailsForCustomers")
    Call<ServiceVariantResponse> getServiceVariants(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewAddProductstoCart")
    Call<AddToCartResponse> addToCart(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/UserHistoryOrders")
    Call<OrderPendingResponce> getorderpending(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetUserReviews")
    Call<ReviewResponce> getreview(@Body RequestBody body);

    @POST("api/OrdersAPI/NewCancelOrder")
    Call<OrderCancelResponce> getcancl(@Body RequestBody body);

    @POST("api/OrdersAPI/NewInsertReturnRequest")
    Call<BasicResponse> returnRequest(@Body RequestBody body);

    @POST("api/OrdersAPI/NewOrderPartialAccept")
    Call<OrderCancelResponce> getaccepet(@Body RequestBody body);

    @POST("api/CountryAPI/NewGetCountryCity")
    Call<AdressCountryResponce> getcounrylist();

    @POST("api/ProductsInfoAPI/NewGetCartItems")
    Call<CartResponce> getcart(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddCouponToCart")
    Call<ApplyCoupon> applyCoupon(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetMainCategoryAndStore")
    Call<ProductlistResponce> getstorslist(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetDeleteUserFavorite")
    Call<Response> NewAddGetDeleteUserFavorite(@Body RequestBody body);

    @POST("api/OrdersAPI/NewAddGetUserReviews")
    Call<ProductStoreReviews> ProductStoreReviews(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetTimingsDetailsofServiceProviders")
    Call<TimingsResponse> getTimingsOfSP(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewUpdateCartDetails")
    Call<UpdateCartResponse> updateCart(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetBranchDetailsForCheckOut")
    Call<CheckoutSchedule> getCheckoutSchedule(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetTimeSlotsDayWise")
    Call<CheckoutSlots> getCheckoutSlots(@Body RequestBody body);

    @POST("api/OrdersAPI/NewPlaceOrder")
    Call<InsertOrderResponse> insertOrder(@Body RequestBody body);

    @POST("api/OrdersAPI/NewOrderPartialReject")
    Call<OrderCancelResponce> getreject(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewSetNewPassword")
    Call<ResetPasswordResponse> getrestPass(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewVerifyForgotPasswordOTP")
    Call<ForgetPasswordOTPResponse> getforgetpass(@Body RequestBody body);

    @POST("api/CustomerAPI/NewAddUpdateCustomerAddress")
    Call<AddAddressResponse> getAddAddress(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetCustomerAddress")
    Call<EditAddressResponse> getEditAddress(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/GetUpdatePNConfiguration")
    Call<PushNotificationResponce> getpnotifaction(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/GetUpdatePNConfiguration")
    Call<PushNotificationsUpdateResponce> getpushnotifactionupdate(@Body RequestBody body);

    @POST("api/SearchAPI/Search")
    Call<SearchProductListResponce> getsearchlist(@Body RequestBody body);

    @POST("api/SearchAPI/Autocomplete")
    Call<SearchListResponce> getsearcresult(@Query("query") String query, @Query("language") String language);

    @POST("api/VendorOrderManagementAPI/NewGetOrderDetails")
    Call<GetOrderDetails> NewGetOrderDetails(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/NewGetOrderBranchPayment")
    Call<PaymentMethods> NewGetOrderBranchPayment(@Body RequestBody body);

    @POST("api/SMSAAPI/AddShipment")
    Call<ShipmentBasicResponse> AddShipment(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewVerifyEmail")
    Call<ChangeEmailResponce> getemailchange(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewUpdateProfile")
    Call<Signinresponce> updateProfile(@Body RequestBody body);

    @POST("api/CustomerRegistrationAPI/NewGetMobileOTP")
    Call<ChangeMobileOtp> getmobilechangeotp(@Body RequestBody body);

//    @POST("api/CustomerRegistrationAPI/NewUpdateMobile")
//    Call<ChangePhoneResponce> getmobilechange(@Body RequestBody body);

    @POST("api/UtilsAPI/ValidateGroupUserChatId")
    Call<BranchGroupId> getBranchGroupId(@Body RequestBody body);

    @POST("api/UtilsAPI/SaveGroupUserChatId")
    Call<BasicResponse> SaveGroupUserChatId(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetBaseBranchDetails")
    Call<BranchDetailsResponse> NewGetBaseBranchDetails(@Body RequestBody body);

    @POST("api/ProductsInfoAPI/GetProductNameByBarcode")
    Call<BarcodeProductResponse> GetProductNameByBarcode(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetBaseBranchDetails")
    Call<BranchDetails> getBranchDetails(@Body RequestBody body);

    @POST("api/UtilsAPI/UrlEncrypt")
    Call<UrlEncryptResponse> UrlEncrypt(@Body RequestBody body);

    @POST("api/UtilsAPI/UrlDecrypt")
    Call<UrlDecryptResponse> UrlDecrypt(@Body RequestBody body);

    @POST("api/CustomerAPI/NewGetNotifications")
    Call<Notifications> getNotifications(@Body RequestBody body);

    //    Hyperpay
    @POST("api/HyperpayAPI/GetNewCheckoutId")
    Call<CheckOutId> generateCheckOutId(@Body RequestBody body);

    @POST("api/HyperpayAPI/GetPaymentStatus")
    Call<MyCards> getPaymentStatus(@Body RequestBody body);


    @POST("api/BranchAPI/NewGetStandardBanks")
    Call<ArrayList<BankDetails>> getStandardBanks(@Body RequestBody body);

}
