package com.cs.checkclickuser.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Vibrator;
import androidx.annotation.NonNull;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import android.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Activites.SearchActivity;
import com.cs.checkclickuser.Models.BarcodeProductResponse;
import com.cs.checkclickuser.Models.BranchDetailsResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarCodeScanner {
    public static AlertDialog customDialog;
    public static int scanType = 2; // 1 = barcode, 2 = storecode

    public static void showScannerDialog(Activity activity){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = activity.getLayoutInflater();
        int layout = R.layout.scanner_bottam_layout;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);

        CodeScannerView scannerView = dialogView.findViewById(R.id.cPreview);
        final ImageView downArrow = (ImageView) dialogView.findViewById(R.id.downarrow);
        final ImageView upArrow = (ImageView) dialogView.findViewById(R.id.uparrow);
        final LinearLayout scannerTypeLayout = (LinearLayout) dialogView.findViewById(R.id.scanner_type_layout);
        final RelativeLayout barCodeLayout = (RelativeLayout) dialogView.findViewById(R.id.barcode_layout);
        final RelativeLayout qrCodeLayout = (RelativeLayout) dialogView.findViewById(R.id.qrcode_layout);
        final View barCodeLine = (View) dialogView.findViewById(R.id.barcode_line);
        final View qrCodeLine = (View) dialogView.findViewById(R.id.qrcode_line);
        final TextView title = (TextView) dialogView.findViewById(R.id.title);
        final ImageView close_button = (ImageView) dialogView.findViewById(R.id.close_button);

        final CodeScanner mCodeScanner = new CodeScanner(activity, scannerView);
        mCodeScanner.startPreview();

        scanType = 2;

        barCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barCodeLine.setVisibility(View.VISIBLE);
                qrCodeLine.setVisibility(View.INVISIBLE);
                mCodeScanner.startPreview();
                title.setText("Barcode Scanner");
                scanType = 1;
            }
        });

        qrCodeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barCodeLine.setVisibility(View.INVISIBLE);
                qrCodeLine.setVisibility(View.VISIBLE);
                mCodeScanner.startPreview();
                title.setText("Storecode Scanner");
                scanType = 2;
            }
        });

        upArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upArrow.setVisibility(View.GONE);
                scannerTypeLayout.setVisibility(View.VISIBLE);
            }
        });

        downArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scannerTypeLayout.setVisibility(View.GONE);
                upArrow.setVisibility(View.VISIBLE);
            }
        });

        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Vibrator vb = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
                        vb.vibrate(100);
                        if (scanType == 1) {
                            Constants.showLoadingDialog(activity);
                            getProductDetails(activity, result.getText());
                        }
                        else {
                            Constants.showLoadingDialog(activity);
                            getBranchDetails(activity, result.getText());
                        }
                    }
                });
            }
        });

        customDialog = dialogBuilder.create();
        customDialog.show();

        close_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
    }

    public static void getBranchDetails(Activity activity, String BranchId){
        String inputStr = null;
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId", BranchId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        inputStr = parentObj.toString();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(activity);
        APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<BranchDetailsResponse> call = apiService.NewGetBaseBranchDetails(
                RequestBody.create(MediaType.parse("application/json"), inputStr));
        call.enqueue(new Callback<BranchDetailsResponse>() {
            @Override
            public void onResponse(Call<BranchDetailsResponse> call, Response<BranchDetailsResponse> response) {
                if (response.isSuccessful()) {
                    BranchDetailsResponse BranchResponse = response.body();
                    try {
                        if (BranchResponse.getStatus()) {
                            customDialog.dismiss();
                            Intent intent = new Intent(activity, ProductStoresActivityStep1.class);
                            intent.putExtra("stores", BranchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", BranchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "search");
                            activity.startActivity(intent);
                        } else {
                            String failureResponse = BranchResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, activity.getResources().getString(R.string.error),
                                    activity.getResources().getString(R.string.ok), activity);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Constants.closeLoadingDialog();
            }

            @Override
            public void onFailure(Call<BranchDetailsResponse> call, Throwable t) {
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(activity, R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Constants.closeLoadingDialog();
            }
        });
    }

    public static void getProductDetails(Activity activity, String productId){
        String inputStr = null;
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UPCBarCode", productId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        inputStr = parentObj.toString();

        final String networkStatus = NetworkUtil.getConnectivityStatusString(activity);
        APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<BarcodeProductResponse> call = apiService.GetProductNameByBarcode(
                RequestBody.create(MediaType.parse("application/json"), inputStr));
        call.enqueue(new Callback<BarcodeProductResponse>() {
            @Override
            public void onResponse(Call<BarcodeProductResponse> call, Response<BarcodeProductResponse> response) {
                if (response.isSuccessful()) {
                    BarcodeProductResponse BranchResponse = response.body();
                    try {
                        if (BranchResponse.getStatus()) {
                            customDialog.dismiss();
                            Intent intent = new Intent(activity, SearchActivity.class);
                            intent.putExtra("text", BranchResponse.getNameEn());
                            intent.putExtra("text", BranchResponse.getNameEn());
                            activity.startActivity(intent);
                        } else {
                            customDialog.dismiss();
                            showScannerDialog(activity);
                            String failureResponse = "Not Available";
                            Constants.showOneButtonAlertDialog(failureResponse, activity.getResources().getString(R.string.error),
                                    activity.getResources().getString(R.string.ok), activity);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Constants.closeLoadingDialog();
            }

            @Override
            public void onFailure(Call<BarcodeProductResponse> call, Throwable t) {
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    Toast.makeText(activity, R.string.str_connection_error, Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(activity, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                }
                Constants.closeLoadingDialog();
            }
        });
    }
}
