package com.cs.checkclickuser.Utils;

public class IdConstants {

    // Main Category types
    public static int PRODUCT = 1;
    public static int SERVICE = 2;


    // Checkout - OrderTypes
    public static int PRODUCT_PICKUP = 1;
    public static int PRODUCT_DELIVERY = 2;
    public static int PRODUCT_SHIPPING = 3;
    public static int SERVICE_HOME = 4;
    public static int SERVICE_STORE = 5;


    //Favourite Ids
    public static int TYPE_INSERT_FAV = 2;
    public static int TYPE_DELETE_FAV = 3;

    public static int STATUS_ID_PRODUCT = 1;
    public static int STATUS_ID_SERVICE = 2;
    public static int STATUS_ID_STORE = 3;


    //URL Sharing for store/products
    public static String URL_SHARE_TYPE_BRANCH = "b";
    public static String URL_SHARE_TYPE_PRODUCT = "p";
    public static String URL_SHARE_TYPE_SERVICE = "s";


}
