package com.cs.checkclickuser.Utils;

import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.Models.ServiceDealsResponse;

import java.util.ArrayList;

public class CacheData {

    // Product fragment data
    public static ArrayList<ProductlistResponce.Stores> productStoresArrayList;
    public static ArrayList<ProductlistResponce.FilterMainCategory> productMainCategory;
    public static ArrayList<ProductlistResponce.FilterSubCategoryCat> productSubCategoryArrayList;
    public static int PRODUCT_TOTAL_STORE_COUNT;

    // Service fragment data
    public static ArrayList<ProductlistResponce.Stores> serviceStoresArrayList;
    public static ArrayList<ProductlistResponce.FilterMainCategory> serviceMainCategory;
    public static ArrayList<ProductlistResponce.FilterSubCategoryCat> serviceSubCategoryArrayList;
    public static int SERVICE_TOTAL_STORE_COUNT;

    // Home fragment data
    public static HomePageResponse homePageResponse;

    // Product Main Category screen(Step 2)
    public static ArrayList<DealsResponce.Deals> storeDealsList;
    public static ArrayList<ServiceDealsResponse.Deals> serviceDealsList;
    public static ProductstoreResponce.Data storeArrayList;

    public static ArrayList<String> favProductId = new ArrayList<>();
}
