package com.cs.checkclickuser.Utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public abstract class AutoActivityLifecycleCallback implements Application.ActivityLifecycleCallbacks {
    private final AppCompatActivity mTargetActivity;

    public AutoActivityLifecycleCallback(AppCompatActivity var1) {
        this.mTargetActivity = var1;
    }

    public void onActivityCreated(Activity var1, Bundle var2) {
    }

    public void onActivityStarted(Activity var1) {
    }

    public void onActivityResumed(Activity var1) {
    }

    public void onActivityPaused(Activity var1) {
    }

    public void onActivityStopped(Activity var1) {
    }

    public void onActivitySaveInstanceState(Activity var1, Bundle var2) {
    }

    public void onActivityDestroyed(Activity var1) {
        if (var1 == this.mTargetActivity) {
            this.mTargetActivity.getApplication().unregisterActivityLifecycleCallbacks(this);
            this.onTargetActivityDestroyed();
        }

    }

    protected abstract void onTargetActivityDestroyed();
}
