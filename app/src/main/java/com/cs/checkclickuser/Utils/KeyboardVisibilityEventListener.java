package com.cs.checkclickuser.Utils;

public interface KeyboardVisibilityEventListener {
    void onVisibilityChanged(boolean var1);
}