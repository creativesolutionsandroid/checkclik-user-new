package com.cs.checkclickuser.Utils;

public class BranchColor {

    private String appColor;

    public String BranchColor(String colorFromService) {

        switch (colorFromService) {
            case "#606060" :
                    appColor = "606060";
                break;
            case "#D61967" :
                appColor = "d61967";
                break;
            case "#BE2321" :
                appColor = "be2321";
                break;
            case "#918055" :
                appColor = "918055";
                break;
            case "#000000" :
                appColor = "000000";
                break;
            case "#5DBCBE" :
                appColor = "5dbcbe";
                break;
            case "#531D7F" :
                appColor = "531d7f";
                break;
            case "#F9B5CC" :
                appColor = "f9b5cc";
                break;
            case "#3E2722" :
                appColor = "3e2722";
                break;
            case "#003580" :
                appColor = "003580";
                break;
            case "#FF6F00" :
                appColor = "ff6f00";
                break;
            default:
                appColor = "606060";
                break;
        }

        return appColor;
    }

}
