package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

public class HomeScreenFeatureStoresAdapter extends PagerAdapter {
    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<HomePageResponse.Featuredstores> featuredstores = new ArrayList<>();
    int pos = 0;
    LayoutInflater mLayoutInflater;

    public HomeScreenFeatureStoresAdapter(Context context, ArrayList<HomePageResponse.Featuredstores> featuredstores) {
        this.context = context;
        this.featuredstores = featuredstores;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 4f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return (featuredstores.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.featured_store, container, false);

        ImageView logo =(ImageView)itemView.findViewById(R.id.logo);


        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(context).inflate(R.layout.bubble_featured_stores, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(context, bubbleLayout);

        LinearLayout layout = (LinearLayout) bubbleLayout.findViewById(R.id.layout);
        TextView rating = (TextView) bubbleLayout.findViewById(R.id.rating);
        TextView storeName = (TextView) bubbleLayout.findViewById(R.id.store_name);
        CamomileSpinner spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
        spinner.start();

        spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL + featuredstores.get(position).getLogo())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }
                }).into(logo);


        rating.setText(""+featuredstores.get(position).getReviews());
        storeName.setText(featuredstores.get(position).getBranchen());

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                intent.putExtra("stores", featuredstores);
                intent.putExtra("pos", position);
                intent.putExtra("class", "home");
                context.startActivity(intent);
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            volatile int i = 0;
            boolean bubblePopupShown = false;
            @Override
            public void onClick(View view) {
                i++;
                Handler handler = new Handler();

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (i == 1) {
                            //single click logic
                            i = 0;
                            if(!bubblePopupShown) {
                                bubbleLayout.setVisibility(View.VISIBLE);
                                int[] location = new int[2];
                                view.getLocationInWindow(location);
                                bubbleLayout.setArrowDirection(ArrowDirection.BOTTOM_CENTER);
                                popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, (location[0] - (view.getWidth() / 2)), (location[1] - (view.getHeight() / 2)));
                                bubblePopupShown = true;
                            }
                            else {
                                bubbleLayout.setVisibility(View.GONE);
                                bubblePopupShown = false;
                            }
                        }
                    }
                };

                if (i == 1) {
                    handler.postDelayed(r, 300);
                } else if (i == 2) {
                    handler.removeCallbacks(r);
                    i = 0;
                    //Double click logic
                     Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                     intent.putExtra("stores", featuredstores);
                     intent.putExtra("pos", position);
                     intent.putExtra("class", "home");
                     context.startActivity(intent);
                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
