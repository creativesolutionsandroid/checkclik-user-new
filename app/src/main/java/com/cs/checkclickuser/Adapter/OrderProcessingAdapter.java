package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.CustomListView;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderProcessingAdapter extends RecyclerView.Adapter<OrderProcessingAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    int pos = 0;
    String language;
    String itemimage, itemimage1, itemimage2;

    public OrderProcessingAdapter(Context context, ArrayList<OrderPendingResponce.Data> pendingArrayList, String language) {
        this.context = context;
        this.language = language;
        this.pendingArrayList = pendingArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_processing_adapter, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_processing_adapter_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (language.equalsIgnoreCase("En")) {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.number_of_units) + " : " + pendingArrayList.get(position).getItems().size() + " " + context.getResources().getString(R.string.item));
            holder.orderno.setText("" + context.getResources().getString(R.string.order_no) + " :" + pendingArrayList.get(position).getInvoiceNo());
            holder.pendingmaintext.setText("" + context.getResources().getString(R.string.processing));
        } else {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.item_ar) + " " + pendingArrayList.get(position).getItems().size() + " : " + context.getResources().getString(R.string.number_of_units_ar));
            holder.orderno.setText("" + pendingArrayList.get(position).getInvoiceNo() + ": " + context.getResources().getString(R.string.order_no_ar));
            holder.pendingmaintext.setText("" + context.getResources().getString(R.string.processing_ar));
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM d, yyyy", Locale.US);

        Date order_date = null;
        String str_order_date;

        try {
            order_date = simpleDateFormat.parse(pendingArrayList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        str_order_date = simpleDateFormat1.format(order_date);

        if (language.equalsIgnoreCase("En")) {
            holder.order_date.setText("" + context.getResources().getString(R.string.order_date) + " " + str_order_date);
        } else {
            holder.order_date.setText("" + str_order_date + " " + context.getResources().getString(R.string.order_date_ar));
        }

        String[] mimage = null;
        mimage = pendingArrayList.get(position).getMultipleimage().split(",");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        if (mimage.length == 3 || mimage.length > 3) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.VISIBLE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];
            itemimage2 = mimage[2];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image1);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage2)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image2);

        } else if (mimage.length == 2) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .into(holder.image1);

        } else if (mimage.length == 1) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.GONE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = pendingArrayList.get(position).getItems().get(0).getImage();

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);
        }

        holder.orderstatus.setColorFilter(context.getResources().getColor(R.color.blue));

        if (position == 0) {
            holder.startingLayout.setPadding(0, 20, 0, 0);
        } else {
            holder.startingLayout.setPadding(0, 70, 0, 0);
        }

        ProcessingOrderItemsList mAdapter = new ProcessingOrderItemsList(context, pendingArrayList.get(position).getItems(), pendingArrayList.get(position).getOrderStatus(), language);
        holder.list_item.setAdapter(mAdapter);

        holder.orderstatuslayout.setVisibility(View.VISIBLE);

        if (pendingArrayList.get(position).getOrderType() == IdConstants.PRODUCT_PICKUP) {
            holder.dispatchlayout.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                holder.footerText4.setText("" + context.getResources().getString(R.string.picked_up));
            } else {
                holder.footerText4.setText("" + context.getResources().getString(R.string.picked_up_ar));
            }
        } else {
            holder.dispatchlayout.setVisibility(View.VISIBLE);
        }

        if (pendingArrayList.get(position).getOrderStatus() == 2 || pendingArrayList.get(position).getOrderStatus() == 5) {
            holder.footerimage1.setImageResource(R.drawable.acceptedgreen);
            holder.footerimage2.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage3.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        } else if (pendingArrayList.get(position).getOrderStatus() == 7 || pendingArrayList.get(position).getOrderStatus() == 8) {
            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.acceptedgreen);
            holder.footerimage3.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        } else if (pendingArrayList.get(position).getOrderStatus() == 9 || pendingArrayList.get(position).getOrderStatus() == 10) {
            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage3.setImageResource(R.drawable.acceptedgreen);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        } else if (pendingArrayList.get(position).getOrderStatus() == 15 || pendingArrayList.get(position).getOrderStatus() == 17) {
            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage3.setImageResource(R.drawable.acceptedgreen);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        } else if (pendingArrayList.get(position).getOrderStatus() == 11) {
            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage3.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage4.setImageResource(R.drawable.acceptedgreen);
        } else if (pendingArrayList.get(position).getOrderStatus() == 19) {
            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.acceptedgreen);
            holder.footerimage3.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        } else {
            holder.footerimage1.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage2.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage3.setImageResource(R.drawable.roundmark_gray2x);
            holder.footerimage4.setImageResource(R.drawable.roundmark_gray2x);
        }

        for (int i = 0; i < pendingArrayList.get(position).getTrackingDetails().size(); i++) {
            if (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 2 || pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 5) { // APPROVED
                holder.footerdate1.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
            } else if (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 7) { // PACKED
                holder.footerdate2.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                if (language.equalsIgnoreCase("En")) {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.packed));
                } else {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.packed_ar));
                }
            } else if ((pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 9) || (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 10)) { // DISPATCHED
                holder.footerdate3.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                if (language.equalsIgnoreCase("En")) {
                    holder.footerText3.setText("" + context.getResources().getString(R.string.dispatched_caps));
                } else {
                    holder.footerText3.setText("" + context.getResources().getString(R.string.dispatched_caps_ar));
                }
            } else if ((pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 15) || (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 17)) { // CANCEL REQUEST
                if (pendingArrayList.get(position).getOrderType() == 2) {
                    holder.footerdate3.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                    if (language.equalsIgnoreCase("En")) {
                        holder.footerText3.setText("" + context.getResources().getString(R.string.cancel_caps));
                    } else {
                        holder.footerText3.setText("" + context.getResources().getString(R.string.cancel_caps_ar));
                    }
                } else {
                    holder.footerdate4.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                    if (language.equalsIgnoreCase("En")) {
                        holder.footerText4.setText("" + context.getResources().getString(R.string.cancel_caps));
                    } else {
                        holder.footerText4.setText("" + context.getResources().getString(R.string.cancel_caps_ar));
                    }
                }
            } else if (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 11) { // PICKED UP, DELIVERED
                holder.footerdate4.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
            } else if (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 18) { // RESCHEDULE REQUEST
                holder.footerdate2.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                if (language.equalsIgnoreCase("En")) {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.reschedule_caps));
                } else {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.reschedule_caps_ar));
                }
            } else if (pendingArrayList.get(position).getTrackingDetails().get(i).getId() == 19) { // RESCHEDULE ACCEPT
                holder.footerdate2.setText(getFormattedDate(pendingArrayList.get(position).getTrackingDetails().get(i).getCreatedOn()));
                if (language.equalsIgnoreCase("En")) {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.reschedule_caps));
                } else {
                    holder.footerText2.setText("" + context.getResources().getString(R.string.reschedule_caps_ar));
                }
            } else {
                // Todo handle else case
            }
        }

        if (pendingArrayList.get(position).getOrderStatus() == 18) {
            holder.scheduledlayout.setVisibility(View.VISIBLE);
            holder.deliveylayout.setVisibility(View.GONE);

            String rescheduldate = pendingArrayList.get(position).getExpectingDelivery();
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            SimpleDateFormat sdf2 = new SimpleDateFormat("MMM,dd,yyyy", Locale.US);
            rescheduldate.replace(rescheduldate, "yyyy-MM-dd");
            try {
                Date datetime = format2.parse(rescheduldate);
                rescheduldate = sdf2.format(datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (language.equalsIgnoreCase("En")) {
                holder.scheduldate.setText("" + "Your order has been Re-Scheduled for" + " " + rescheduldate + " " +
                        pendingArrayList.get(position).getTimeSlot());
            } else {
                holder.scheduldate.setText("" + pendingArrayList.get(position).getTimeSlot() + " " + rescheduldate + " " +
                        context.getResources().getString(R.string.your_order_has_been_rescheduled_for_ar));
            }
        } else {
            holder.deliveylayout.setVisibility(View.VISIBLE);
            holder.scheduledlayout.setVisibility(View.GONE);
        }

        holder.accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getacceptedapi().execute();

            }
        });
        holder.reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getcancelApi().execute();

            }
        });
    }

    @Override
    public int getItemCount() {
        return pendingArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView orderno, pendingmaintext, no_of_items, order_date;
        ImageView orderstatus;
        CustomListView list_item;
        LinearLayout datelayout, buttonlayout;

        LinearLayout deliveylayout;
        TextView scheduldate;

        ImageView footerimage1;
        ImageView footerimage2;
        ImageView footerimage3;
        ImageView footerimage4;

        Button accept_btn;
        Button reject_btn;
        TextView footerText1;
        TextView footerText2;
        TextView footerText3;
        TextView footerText4;

        TextView footerdate1;
        TextView footerdate2;
        TextView footerdate3;
        TextView footerdate4;

        LinearLayout layout;
        RelativeLayout scheduledlayout;
        LinearLayout orderstatuslayout;
        RelativeLayout acceptedlayout;
        RelativeLayout packedlayout;
        RelativeLayout dispatchlayout;
        RelativeLayout deliverylayout;
        RelativeLayout viewlayout, startingLayout;
        TextView qty;

        ImageView image, image1, image2;
        RelativeLayout item1Layout, item2Layout, item3Layout;
        CamomileSpinner spinner, spinner1, spinner2;

        public MyViewHolder(View itemView) {
            super(itemView);

            qty = itemView.findViewById(R.id.products);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
            orderno = (TextView) itemView.findViewById(R.id.orderno);
            orderstatus = (ImageView) itemView.findViewById(R.id.greenimage);
            datelayout = (LinearLayout) itemView.findViewById(R.id.datelayout);
            buttonlayout = (LinearLayout) itemView.findViewById(R.id.buttonlayout);
            pendingmaintext = (TextView) itemView.findViewById(R.id.pendingstatus);
            no_of_items = (TextView) itemView.findViewById(R.id.no_of_items);
            list_item = (CustomListView) itemView.findViewById(R.id.relativelist);

            deliveylayout = itemView.findViewById(R.id.deliveylayout);
            scheduldate = itemView.findViewById(R.id.scheduldate);

            footerimage1 = itemView.findViewById(R.id.accepetedimage);
            footerimage2 = itemView.findViewById(R.id.packedimage);
            footerimage3 = itemView.findViewById(R.id.dispatchimage);
            footerimage4 = itemView.findViewById(R.id.deliviedimage);

            accept_btn = itemView.findViewById(R.id.accept_btn);
            reject_btn = itemView.findViewById(R.id.reject_btn);

            footerText1 = itemView.findViewById(R.id.accepted_text);
            footerText2 = itemView.findViewById(R.id.packedtext);
            footerText3 = itemView.findViewById(R.id.dispatchedtext);
            footerText4 = itemView.findViewById(R.id.deliveredtext);

            footerdate1 = itemView.findViewById(R.id.accepetdate);
            footerdate2 = itemView.findViewById(R.id.packeddate);
            footerdate3 = itemView.findViewById(R.id.dispatcheddate);
            footerdate4 = itemView.findViewById(R.id.delivereddate);

            layout = itemView.findViewById(R.id.layout);
            scheduledlayout = itemView.findViewById(R.id.scheduledlayout);
            orderstatuslayout = itemView.findViewById(R.id.orderstatuslayout);
            acceptedlayout = itemView.findViewById(R.id.accepetlayout);
            packedlayout = itemView.findViewById(R.id.packedlayout);
            dispatchlayout = itemView.findViewById(R.id.dispatchlayout);
            deliverylayout = itemView.findViewById(R.id.deliverylayout);
            viewlayout = itemView.findViewById(R.id.viewlayout);
            startingLayout = (RelativeLayout) itemView.findViewById(R.id.starting_layout);

            image = (ImageView) itemView.findViewById(R.id.image);
            image1 = (ImageView) itemView.findViewById(R.id.image1);
            image2 = (ImageView) itemView.findViewById(R.id.image2);
            item1Layout = (RelativeLayout) itemView.findViewById(R.id.item1_layout);
            item2Layout = (RelativeLayout) itemView.findViewById(R.id.item2_layout);
            item3Layout = (RelativeLayout) itemView.findViewById(R.id.item3_layout);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
            spinner1 = (CamomileSpinner) itemView.findViewById(R.id.spinner1);
            spinner2 = (CamomileSpinner) itemView.findViewById(R.id.spinner2);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "processing");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    context.startActivity(intent);
                }
            });

            viewlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "processing");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    context.startActivity(intent);
                }
            });

            list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "processing");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }

    private String prepareJson(int pos) {

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", pendingArrayList.get(pos).getUserId());
            parentObj.put("OrderId", pendingArrayList.get(pos).getOrderId());
            parentObj.put("Reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareOrderCancelJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class getacceptedapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
//            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getaccepet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if (response.isSuccessful()) {
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            } else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
//                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
//                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class getcancelApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
//            Constants.showLoadingDialog();
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getreject(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if (response.isSuccessful()) {
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            } else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
//                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String getFormattedDate(String inputDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        inputDate.replace(inputDate, "yyyy-MM-dd");
        try {
            Date datetime = format.parse(inputDate);
            inputDate = sdf.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return inputDate;
    }

    /*Old logic removed and commented 24-12-2020*/

//    if (pendingArrayList.get(position).getOrderStatus() == 2 || pendingArrayList.get(position).getOrderStatus() == 5) {
//            // Approved
//            holder.footerimage1.setImageResource(R.drawable.acceptedgreen);
//
//            String date = pendingArrayList.get(position).getTrackingDetails().get(1).getCreatedOn();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            SimpleDateFormat sdf = new SimpleDateFormat("MMM,dd,yyyy",Locale.US);
//            date.replace(date,"yyyy-MM-dd");
//            try {
//                Date datetime = format.parse(date);
//                date = sdf.format(datetime);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//           holder.footerdate1.setText(""+date);
//
//        }
//        else if (pendingArrayList.get(position).getOrderStatus() == 7) {
//            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage2.setImageResource(R.drawable.acceptedgreen);
//            holder.footerText2.setText("PACKED");
//
//            String date = pendingArrayList.get(position).getTrackingDetails().get(1).getCreatedOn();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            SimpleDateFormat sdf = new SimpleDateFormat("MMM,dd,yyyy",Locale.US);
//            date.replace(date,"yyyy-MM-dd");
//            try {
//                Date datetime = format.parse(date);
//                date = sdf.format(datetime);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            holder.footerdate1.setText(""+date);
//
//            String date1 = pendingArrayList.get(position).getTrackingDetails().get(2).getCreatedOn();
//            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//            SimpleDateFormat sdf1 = new SimpleDateFormat("MMM,dd,yyyy",Locale.US);
//            date1.replace(date1,"yyyy-MM-dd");
//            try {
//                Date datetime = format1.parse(date1);
//                date1 = sdf1.format(datetime);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            holder.footerdate2.setText(""+date1);
//
//        } else if (pendingArrayList.get(position).getOrderStatus() == 9
//                || pendingArrayList.get(position).getOrderStatus() == 10) {
//
//            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage3.setImageResource(R.drawable.acceptedgreen);
//
//            String date1 = pendingArrayList.get(position).getTrackingDetails().get(2).getCreatedOn();
//            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        }
//        else if (pendingArrayList.get(position).getOrderStatus() == 15
//                || pendingArrayList.get(position).getOrderStatus() == 17) {
//
//            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage3.setImageResource(R.drawable.acceptedgreen);
//            String date1 = pendingArrayList.get(position).getTrackingDetails().get(2).getCreatedOn();
//            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        }
//        else if (pendingArrayList.get(position).getOrderStatus() == 11) {
//            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage2.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage3.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage4.setImageResource(R.drawable.acceptedgreen);
//        } else if (pendingArrayList.get(position).getOrderStatus() == 19) {
//            holder.footerimage1.setImageResource(R.drawable.rightmark_gray2x);
//            holder.footerimage2.setImageResource(R.drawable.acceptedgreen);
//        }
}
