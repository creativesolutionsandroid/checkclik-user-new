package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Activites.OrderRetunActivity;
import com.cs.checkclickuser.Activites.ProductRatingActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class OrderDetailsListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int orderStatus;
    int ordertype, branchid;
    ArrayList<OrderPendingResponce.Items> pendingArrayList = new ArrayList<>();
    String language;

    public OrderDetailsListAdapter(Context context, ArrayList<OrderPendingResponce.Items> orderList, int orderStatus, int ordertype, int branchid, String language) {
        this.language = language;
        this.context = context;
        this.pendingArrayList = orderList;
        this.orderStatus = orderStatus;
        this.branchid = branchid;
        this.ordertype = ordertype;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pendingArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView storename, iteamcount, qty, price, retunbtn, retunstatus, ratingtext;
        ImageView storeimage;
        RelativeLayout rejectedLayout;
        RatingBar ratingbar;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        if (language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.details_adapter_list, null);
        } else {
            convertView = inflater.inflate(R.layout.details_adapter_list_arabic, null);
        }

        holder.ratingbar = (RatingBar) convertView.findViewById(R.id.ratingbar);

        holder.ratingtext = (TextView) convertView.findViewById(R.id.productrate);
        holder.storename = (TextView) convertView.findViewById(R.id.storename);
        holder.iteamcount = (TextView) convertView.findViewById(R.id.iteamcount);
        holder.qty = (TextView) convertView.findViewById(R.id.products);
        holder.price = (TextView) convertView.findViewById(R.id.price);
        holder.retunbtn = (TextView) convertView.findViewById(R.id.retun);
        holder.retunstatus = (TextView) convertView.findViewById(R.id.retunstatus);
        holder.storeimage = (ImageView) convertView.findViewById(R.id.storeimage);
        holder.rejectedLayout = (RelativeLayout) convertView.findViewById(R.id.rejectedlayout);

        if (language.equalsIgnoreCase("En")) {
            holder.storename.setText(pendingArrayList.get(position).getNameEn());
        } else {
            holder.storename.setText(pendingArrayList.get(position).getNameAr());
        }
        holder.iteamcount.setText("" + pendingArrayList.get(position).getQty());

        if (!pendingArrayList.get(position).getIsAccepted()) {
            holder.rejectedLayout.setVisibility(View.VISIBLE);
        } else {
            holder.rejectedLayout.setVisibility(View.GONE);
        }

//        Log.i("TAG", "getView: " + pendingArrayList.get(position).getRating() + " " + pendingArrayList.get(position).getIsAccepted()
//                + " " + pendingArrayList.get(position).getNotReturnable());
//
//        if (pendingArrayList.get(position).getRating() == 0 && pendingArrayList.get(position).getIsAccepted()) {
//            holder.ratingtext.setVisibility(View.VISIBLE);
//            holder.ratingbar.setVisibility(View.GONE);
//        } else if (pendingArrayList.get(position).getRating() > 0 && pendingArrayList.get(position).getIsAccepted()
//                && !pendingArrayList.get(position).getNotReturnable()) {
//            holder.ratingbar.setVisibility(View.VISIBLE);
//            holder.ratingtext.setVisibility(View.GONE);
//            holder.ratingbar.setRating(pendingArrayList.get(position).getRating());
//        } else if (pendingArrayList.get(position).getIsAccepted() && pendingArrayList.get(position).getRating() == 0 ) {
//            if (pendingArrayList.get(position).getNotReturnable()) {
//
//            }
//
//        }
//        else {
//            holder.ratingbar.setVisibility(View.GONE);
//            holder.ratingtext.setVisibility(View.GONE);
//        }
//
//        if (!pendingArrayList.get(position).getNotReturnable() && pendingArrayList.get(position).getIsAccepted()) {
////            if (ordertype == 2) {
//            if (pendingArrayList.get(position).getReturnRequestStatus() == 1) {
////                holder.return_pending_text.setVisibility(View.VISIBLE);
//                holder.ratingbar.setVisibility(View.GONE);
//                holder.ratingtext.setVisibility(View.GONE);
//            } else {
////                holder.return_pending_text.setVisibility(View.GONE);
//            }
//        }
//        if (orderStatus == 16 || orderStatus == 3) {
//            holder.ratingtext.setVisibility(View.INVISIBLE);
//        }

//        else if (pendingArrayList.get(position).getNotReturnable() && pendingArrayList.get(position).getIsAccepted()) {
//            holder.return_pending_text.setVisibility(View.GONE);
//        }
//        else {
//            holder.return_pending_text.setVisibility(View.GONE);
//        }

//        if (!pendingArrayList.get(position).getIsAccepted()) {
//            holder.rejecatedlayout.setVisibility(View.VISIBLE);
//        }
//        else {
//            holder.rejecatedlayout.setVisibility(View.GONE);
//        }

//        holder.ratingtext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, ProductRatingActivity.class);
//                intent.putExtra("Order", pendingArrayList);
//                intent.putExtra("Orderpos", position);
//                intent.putExtra("Type", ordertype);
//                intent.putExtra("OrderId", pendingArrayList.get(position).getOrderId());
//                intent.putExtra("Ids", pendingArrayList.get(position).getSKUId());
//                intent.putExtra("BranchId", branchid);
//                context.startActivity(intent);
//            }
//        });

//        if (pendingArrayList.get(position).getReturnRequestStatus()==1){
//            holder.retunstatus.setVisibility(View.VISIBLE);
//            holder.retunbtn.setVisibility(View.GONE);
//            holder.retunstatus.setText("Return Pending");
//        }
//
//        if (ordertype == 1 || ordertype == 2 || ordertype == 3) {
//            if (!pendingArrayList.get(position).getNotReturnable()) {
//                if (ordertype != 2) {
//                    if (pendingArrayList.get(position).getReturnRequestStatus() == 1) {
//                        holder.retunstatus.setVisibility(View.VISIBLE);
//                        holder.retunbtn.setVisibility(View.GONE);
//                        holder.retunstatus.setText("Return Pending");
//                    }
//                    else if (pendingArrayList.get(position).getReturnDays() >= pendingArrayList.get(position).getRemainingDays()) {
//                        holder.retunbtn.setVisibility(View.VISIBLE);
//                        holder.retunstatus.setVisibility(View.GONE);
//                    }
//                }
//            }
//        }

//        if (ordertype){
//            if (pendingArrayList.get(position).getReturnRequestStatus()!=1){
//                Log.d("TAG", "getView: "+ordertype);
//                if (pendingArrayList.get(position).getReturnDays() >= pendingArrayList.get(position).getRemainingDays()) {
//                    holder.retunbtn.setVisibility(View.VISIBLE);
//                }
//                else {
//                    holder.retunbtn.setVisibility(View.GONE);
//                }
//            }
//        }
        holder.retunbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderRetunActivity.class);
                intent.putExtra("Order", pendingArrayList);
                intent.putExtra("Orderpos", position);
                context.startActivity(intent);
            }
        });

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL + pendingArrayList.get(position).getImage())
                .into(holder.storeimage);
        if (language.equalsIgnoreCase("En")) {
            holder.qty.setText("" + context.getResources().getString(R.string.qty) + " " + pendingArrayList.get(position).getQty());
        } else {
            holder.qty.setText("" + pendingArrayList.get(position).getQty() + " " + context.getResources().getString(R.string.qty_ar));
        }
        holder.price.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(position).getPrice()));
        return convertView;
    }
}
