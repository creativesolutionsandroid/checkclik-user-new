package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.CouponResponce;
import com.cs.checkclickuser.R;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ApplyCouponAdapter extends RecyclerView.Adapter< ApplyCouponAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private Activity activity;
    private ArrayList<CartResponce.CouponList> couponArrayList = new ArrayList<>();
    int pos = 0;
    SharedPreferences LanguagePrefs;
    String language;

    public ApplyCouponAdapter(Context context, Activity activity, ArrayList<CartResponce.CouponList> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.couponArrayList = storeArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.coupon_adapter, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.coupon_adapter_ar, parent, false);
            return new MyViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (language.equals("En")){

            holder.mainstore.setText(couponArrayList.get(position).getStoreNameEn());
            holder.offerrate.setText(couponArrayList.get(position).getVoucherNameEn());
            holder.code.setText("" + context.getResources().getString(R.string.code) + " : "+couponArrayList.get(position).getCouponCode());

        }else {

            holder.mainstore.setText(couponArrayList.get(position).getStoreNameAr());
            holder.offerrate.setText(couponArrayList.get(position).getVoucherNameEn());
            holder.code.setText(couponArrayList.get(position).getCouponCode() + " : "+"" + context.getResources().getString(R.string.code_ar));

        }
        //        holder.available.setText(couponArrayList.get(position).getData().get(position).get());

        String date = couponArrayList.get(position).getEndDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd,MMM,yyyy",Locale.US); ;

        try {
            Date datetime = format.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "date: "+date);
        holder.validity.setText(""+date);

        Glide.with(context)
                .load(STORE_IMAGE_URL+couponArrayList.get(position).getLogoCopy())
                .into(holder.storeimage);

//        holder.validity.setText(couponArrayList.get(position).getEndDate());

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "couponlistsize: "+couponArrayList.size());
        return couponArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, code, available, validity,offerrate;
        com.cs.checkclickuser.CustomView.CircleImageView storeimage;
        LinearLayout layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.storename);
            code = (TextView) itemView.findViewById(R.id.code);
            available = (TextView) itemView.findViewById(R.id.expirdate);
            validity = (TextView) itemView.findViewById(R.id.date);
            offerrate=(TextView)itemView.findViewById(R.id.offerrate);
            layout=(LinearLayout) itemView.findViewById(R.id.storelayout);

            storeimage = (com.cs.checkclickuser.CustomView.CircleImageView) itemView.findViewById(R.id.storeimage);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra("coupon_position", getAdapterPosition());
                    activity.setResult(Activity.RESULT_OK, intent);
                    activity.finish();
                }
            });
        }
    }
}
