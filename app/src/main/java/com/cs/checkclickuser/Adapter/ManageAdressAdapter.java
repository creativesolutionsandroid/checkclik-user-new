package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.cs.checkclickuser.Activites.AddAdressActivity;
import com.cs.checkclickuser.Activites.EditiAddressActivity;
import com.cs.checkclickuser.Models.AddAddressResponse;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageAdressAdapter extends RecyclerView.Adapter<ManageAdressAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private ArrayList<ManageAdressResponce.Data> addressArrayList = new ArrayList<>();
    int pos = 0;
    boolean clickable;
    int id = 0, countryId = 0;
    SharedPreferences userPrefs;
    String userId;
    SharedPreferences LanguagePrefs;
    String language;
    private ManageAdressAdapter mAddressAdapter;


    public ManageAdressAdapter(Context context, int countryId, ArrayList<ManageAdressResponce.Data> addressArrayList, boolean clickable, AppCompatActivity activity) {
        this.context = context;
        this.activity = activity;
        this.countryId = countryId;
        this.clickable = clickable;
        this.addressArrayList = addressArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.manageadress_adapter, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.manageadress_adapter_ar, parent, false);
            return new MyViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        holder.buildingname.setText(addressArrayList.get(position).getFullName());
        holder.adressline.setText(addressArrayList.get(position).getAddress1() + ", " + addressArrayList.get(position).getAddress2() + ", " + addressArrayList.get(position).getCityNameEn() + ", " + addressArrayList.get(position).getCountryNameEn() + ", " + addressArrayList.get(position).getZipcode());
        holder.phonenumber.setText("" + context.getResources().getString(R.string.phone_number) + " : " + addressArrayList.get(position).getPhoneNumber());

        if (addressArrayList.get(position).getAddressType() == 1) {
            if (language.equals("En")){
                holder.adresstype.setText("" + context.getResources().getString(R.string.home));
            }else {
                holder.adresstype.setText("" + context.getResources().getString(R.string.home_ar));
            }

        } else {
            if (language.equals("En")){
                holder.adresstype.setText("" + context.getResources().getString(R.string.office));
            }else {
                holder.adresstype.setText("" + context.getResources().getString(R.string.office_ar));
            }

        }
    }

    @Override
    public int getItemCount() {
        return addressArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView buildingname, adressline, phonenumber, adresstype;
        Button edit, delete;
        LinearLayout layout;


        public MyViewHolder(View itemView) {
            super(itemView);
            buildingname = (TextView) itemView.findViewById(R.id.housename);
            adressline = (TextView) itemView.findViewById(R.id.address);
            phonenumber = (TextView) itemView.findViewById(R.id.phonenumber);
            adresstype = (TextView) itemView.findViewById(R.id.addresstype);
            edit = (Button) itemView.findViewById(R.id.edit_btn);
            delete = (Button) itemView.findViewById(R.id.delet_btn);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickable) {
                        if (countryId == addressArrayList.get(getAdapterPosition()).getCountryId()) {
                            Intent intent = new Intent();
                            intent.putExtra("name", addressArrayList.get(getAdapterPosition()).getFullName());
                            intent.putExtra("address", addressArrayList.get(getAdapterPosition()).getAddress1() + "," + addressArrayList.get(getAdapterPosition()).getAddress2() + "," + addressArrayList.get(getAdapterPosition()).getZipcode());
                            intent.putExtra("mobile", addressArrayList.get(getAdapterPosition()).getPhoneNumber());
                            intent.putExtra("id", addressArrayList.get(getAdapterPosition()).getId());

                            if (addressArrayList.get(getAdapterPosition()).getAddressType() == 1) {
                                intent.putExtra("type", "Home");
                            } else {
                                intent.putExtra("type", "Office");
                            }
                            ((Activity) context).setResult(Activity.RESULT_OK, intent);
                            ((Activity) context).finish();
                        } else {
                            Constants.showOneButtonAlertDialog("" + context.getResources().getString(R.string.please_select_address_for) + " " + addressArrayList.get(getAdapterPosition()).getCountryNameEn(),
                                    context.getResources().getString(R.string.app_name), context.getResources().getString(R.string.ok), activity);
                        }
                    }
                }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, EditiAddressActivity.class);
                    intent.putExtra("addressarry", addressArrayList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   id = addressArrayList.get(getAdapterPosition()).getId();
                    String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new DeleteAddressApi().execute();
                    } else {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }
    }

    private String prepareGetAddressJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("Id", id);
            parentObj.put("UserId", userId);
            parentObj.put("FlagId", 3);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class DeleteAddressApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        AlertDialog customDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetAddressJSON();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = activity.getLayoutInflater();
            int layout = R.layout.loading_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<AddAddressResponse> call = apiService.getAddAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddAddressResponse>() {
                @Override
                public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        AddAddressResponse resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {

                                new GetstoreApi().execute();
                                notifyDataSetChanged();

//                                Toast.makeText(EditProfileActivity.this, "", Toast.LENGTH_SHORT).show();
                            } else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.error),
                                        context.getResources().getString(R.string.ok), activity);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        if (customDialog != null) {
                            customDialog.dismiss();
                        }
                    }

                }

                @Override
                public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    if (customDialog != null) {
                        customDialog.dismiss();
                    }

                }
            });
            return null;
        }
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        AlertDialog customDialog1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = activity.getLayoutInflater();
            int layout = R.layout.loading_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog1 = dialogBuilder.create();
            customDialog1.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog1.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = activity.getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ManageAdressResponce> call = apiService.getaddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ManageAdressResponce>() {
                @Override
                public void onResponse(Call<ManageAdressResponce> call, Response<ManageAdressResponce> response) {
                    Log.d(TAG, "product servies responce: " + response);


                    if (response.isSuccessful()) {
                        ManageAdressResponce stores = response.body();

                        if (stores.getStatus()) {
                            addressArrayList = stores.getData();
                            notifyDataSetChanged();

                        }
                        Log.d(TAG, "arry list size 1" + addressArrayList.size());
                    }

                    if (customDialog1 != null) {
                        customDialog1.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<ManageAdressResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    if (customDialog1 != null) {
                        customDialog1.dismiss();
                    }

                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FlagId", 1);
            parentObj.put("UserId", userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

}
