package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreenOffersAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private ArrayList<HomePageResponse.Offers> productOffersList;
    private ArrayList<HomePageResponse.Offers2> servicesOffersList;
    Context context;
    String TAG = "TAG";
    SharedPreferences userPrefs;
    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceDetails> serviceProductLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> serviceVariantsLists = new ArrayList<>();
    private String inputStr, serviceStr;
    LinearLayout discountLayout;

    SharedPreferences LanguagePrefs;
    String language;

    public HomeScreenOffersAdapter(Context context, ArrayList<HomePageResponse.Offers> productOffersList,
                                   ArrayList<HomePageResponse.Offers2> servicesOffersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.productOffersList = productOffersList;
        this.servicesOffersList = servicesOffersList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 2.3f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        Log.d("TAG", "getCount: "+(productOffersList.size() + servicesOffersList.size()));
        return (productOffersList.size() + servicesOffersList.size());
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = null;
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")){

            itemView = mLayoutInflater.inflate(R.layout.list_homescreen_offers, container, false);
        }else {
            itemView = mLayoutInflater.inflate(R.layout.list_homescreen_offers_ar, container, false);
        }

        ImageView productImage = (ImageView) itemView.findViewById(R.id.image);
        TextView discount_percent = (TextView) itemView.findViewById(R.id.discount_percent);
        TextView title = (TextView) itemView.findViewById(R.id.title);
        TextView discountedPrice = (TextView) itemView.findViewById(R.id.discounted_price);
        TextView price = (TextView) itemView.findViewById(R.id.price);
        ImageView favIcon = (ImageView) itemView.findViewById(R.id.fav_icon);
        ImageView logo = (ImageView) itemView.findViewById(R.id.logo);
        RelativeLayout layout = (RelativeLayout) itemView.findViewById(R.id.main_layout);
        RelativeLayout discountLayout = (RelativeLayout) itemView.findViewById(R.id.discount_layout);
        CamomileSpinner spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
        spinner.start();

        spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        discountedPrice.setPaintFlags(discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if(position < productOffersList.size()) {
            if(productOffersList.get(position).getDiscountvalue() > 0) {
                discountLayout.setVisibility(View.VISIBLE);
                if (productOffersList.get(position).getDiscounttype() == 1) {
                    if (language.equalsIgnoreCase("En")){
                    discount_percent.setText((int) productOffersList.get(position).getDiscountvalue() + "% " + context.getResources().getString(R.string.off_non_caps));
                    }else {
                        discount_percent.setText((int) productOffersList.get(position).getDiscountvalue() + "% " + context.getResources().getString(R.string.off_non_caps_ar));
                    }

                } else {
                    if (language.equalsIgnoreCase("En")){
                       discount_percent.setText(productOffersList.get(position).getDiscountvalue() + " " + context.getResources().getString(R.string.off_non_caps));
                    }{
                        discount_percent.setText(productOffersList.get(position).getDiscountvalue() + " " + context.getResources().getString(R.string.off_non_caps_ar));
                    }
                }
            }
            else {
                discountLayout.setVisibility(View.GONE);
            }
            title.setText(productOffersList.get(position).getNameen());
            discountedPrice.setText("SAR "+Constants.priceFormat1.format(productOffersList.get(position).getSellingprice()));
            price.setText("SAR "+Constants.priceFormat1.format(productOffersList.get(position).getPrice()));

            if (productOffersList.get(position).isFavorite() ||
                    (CacheData.favProductId != null && CacheData.favProductId.contains(productOffersList.get(position).getProductskuid()))) {
                favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
            } else {
                favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.STORE_IMAGE_URL + productOffersList.get(position).getLogo())
                    .into(logo);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + productOffersList.get(position).getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(productImage);
        }
        else {
            int pos = position - productOffersList.size();
            if(servicesOffersList.get(pos).getDiscounttype() == 1) {
                if (language.equalsIgnoreCase("En")){
                 discount_percent.setText((int)servicesOffersList.get(pos).getDiscountvalue()+"% " + context.getResources().getString(R.string.off_non_caps));
                }else {
                    discount_percent.setText((int)servicesOffersList.get(pos).getDiscountvalue()+"% " + context.getResources().getString(R.string.off_non_caps));
                }

            }
            else {
                if (language.equalsIgnoreCase("En")){
                discount_percent.setText(servicesOffersList.get(pos).getDiscountvalue()+" " + context.getResources().getString(R.string.off_non_caps));
                }else {
                    discount_percent.setText(servicesOffersList.get(pos).getDiscountvalue()+" " + context.getResources().getString(R.string.off_non_caps_ar));
                }

            }
            title.setText(servicesOffersList.get(pos).getServicenameen());
            discountedPrice.setText("SAR "+Constants.priceFormat1.format(servicesOffersList.get(pos).getSellingprice()));
            price.setText("SAR "+Constants.priceFormat1.format(servicesOffersList.get(pos).getPrice()));

            if (servicesOffersList.get(pos).isFavorite() ||
                    (CacheData.favProductId != null && CacheData.favProductId.contains(servicesOffersList.get(pos).getServiceid()))) {
                favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
            } else {
                favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.STORE_IMAGE_URL + servicesOffersList.get(pos).getLogo())
                    .into(logo);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + servicesOffersList.get(pos).getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(productImage);

            Log.d("TAG", "image: "+servicesOffersList.get(pos).getImage());
        }

        favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject parentObj = new JSONObject();
                if(position < productOffersList.size()) {
                    try {
                        if(productOffersList.get(position).isFavorite()) {
                            parentObj.put("Type", 3);
                        }
                        else {
                            parentObj.put("Type", 2);
                        }
                        parentObj.put("StatusId",  "1");
                        parentObj.put("Ids", productOffersList.get(position).getProductid());
                        parentObj.put("UserId",userPrefs.getString("userId", ""));
                        new InsertStoreFav().execute(parentObj.toString(), "1", ""+position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        int pos = position - productOffersList.size();
                        if(servicesOffersList.get(pos).isFavorite()) {
                            parentObj.put("Type", 3);
                        }
                        else {
                            parentObj.put("Type", 2);
                        }
                        parentObj.put("StatusId",  "1");
                        parentObj.put("Ids", servicesOffersList.get(pos).getServiceid());
                        parentObj.put("UserId",userPrefs.getString("userId", ""));
                        new InsertStoreFav().execute(parentObj.toString(), "2", ""+pos);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position < productOffersList.size()) {
                    inputStr = prepareProductVariantJSON(position);
                    new GetProductVariantsApi().execute("" + position);
                }
                else {
                    inputStr = prepareGetStoresJSON(position);
                    new GetServiceVariantsApi().execute("" + (position - productOffersList.size()));
                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

    private class GetProductVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductList();
                            variantsLists = response.body().getData().getVariantsList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ProductVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("skuid", productOffersList.get(Integer.parseInt(strings[0])).getProductskuid());
                            intent.putExtra("storeid", productOffersList.get(Integer.parseInt(strings[0])).getStoreid());
                            intent.putExtra("branchId", productOffersList.get(Integer.parseInt(strings[0])).getBranchid());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareProductVariantJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  productOffersList.get(position).getInventoryid());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetServiceVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            serviceProductLists = response.body().getData().getServiceDetails();
                            serviceVariantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", serviceProductLists);
                            intent.putExtra("variantList", serviceVariantsLists);
                            intent.putExtra("storeId", servicesOffersList.get(Integer.parseInt(strings[0])).getStoreid());
                            intent.putExtra("skuid", servicesOffersList.get(Integer.parseInt(strings[0])).getServiceid());
                            intent.putExtra("branchId", servicesOffersList.get(Integer.parseInt(strings[0])).getBranchid());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        position = position - productOffersList.size();
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  servicesOffersList.get(position).getServiceid());
            parentObj.put("SubCategoryId",  servicesOffersList.get(position).getSubcategoryid());
            parentObj.put("BranchId",  servicesOffersList.get(position).getBranchid());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog((Activity)context);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), strings[0]));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if (strings[1].equals("1")) {
                                if (productOffersList.get(Integer.parseInt(strings[2])).isFavorite()) {
                                    productOffersList.get(Integer.parseInt(strings[2])).setFavorite(false);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid())) {
                                        CacheData.favProductId.remove(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid());
                                    }
                                } else {
                                    productOffersList.get(Integer.parseInt(strings[2])).setFavorite(true);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid())) {
                                        CacheData.favProductId.remove(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid());
                                    }
                                }
                            }
                            else {
                                if (servicesOffersList.get(Integer.parseInt(strings[2])).isFavorite()) {
                                    servicesOffersList.get(Integer.parseInt(strings[2])).setFavorite(false);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid())) {
                                        CacheData.favProductId.remove(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid());
                                    }
                                } else {
                                    servicesOffersList.get(Integer.parseInt(strings[2])).setFavorite(true);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid())) {
                                        CacheData.favProductId.remove(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid());
                                    }
                                }
                            }
                            notifyDataSetChanged();
                            Constants.showOneButtonAlertDialog(stores.getMessage(), context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), (Activity)context);
                        }
                    }
                    else {
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}
