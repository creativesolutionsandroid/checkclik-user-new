package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductItemCategoriesActivityStep3;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.DecimalFormat;

public class ProductShopAdapter extends RecyclerView.Adapter< ProductShopAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ProductstoreResponce.Data storeArrayList;
    int storeId;
    int type;
    SharedPreferences LanguagePrefs;
    String language;


    public ProductShopAdapter(Context context, ProductstoreResponce.Data storesArrayList, int storeId, int type){
        this.context = context;
        this.storeId = storeId;
        this.type = type;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shopadapter, parent, false);

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        ProductstoreResponce.MainCategory storeDetails = storeArrayList.getMainCategory().get(position);

        if (language.equals("En")){
            holder.store_name.setText(storeDetails.getNameEn());
        }else {

            holder.store_name.setText(storeDetails.getNameAr());
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL +storeDetails.getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.store_image);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.getMainCategory().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name;
        ImageView store_image;
        RelativeLayout storelayout;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);

            store_name = (TextView) itemView.findViewById(R.id.stoename);
            store_image = (ImageView) itemView.findViewById(R.id.storepic);
            storelayout= (RelativeLayout)itemView.findViewById(R.id.storelayout);
            spinner=(CamomileSpinner) itemView.findViewById(R.id.spinner) ;

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductItemCategoriesActivityStep3.class);
                    intent.putExtra("stores", storeArrayList);
                    intent.putExtra("pos",getAdapterPosition());
                    intent.putExtra("storeId",storeId);
                    intent.putExtra("type",type);
                    context.startActivity(intent);
                }
            });
        }
    }
}
