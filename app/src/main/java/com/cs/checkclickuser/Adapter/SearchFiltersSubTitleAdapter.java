package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Models.SearchProductListResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.CustomListView;

import java.util.ArrayList;
import java.util.List;

public class SearchFiltersSubTitleAdapter extends BaseAdapter {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private List<SearchProductListResponce.FacetFilter> filtersList = new ArrayList<>();
    String inputStr;
    public LayoutInflater inflater;

    public SearchFiltersSubTitleAdapter(Context context, List<SearchProductListResponce.FacetFilter> storeArrayList) {
        this.context = context;
        this.filtersList = storeArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return filtersList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        CheckBox subTtitle;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_search_filter_subtitle, null);

            holder.subTtitle = (CheckBox) convertView.findViewById(R.id.checkbox);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.subTtitle.setText(filtersList.get(position).getFacetText());

        if (filtersList.get(position).getSelected()) {
            holder.subTtitle.setChecked(true);
        }
        else {
            holder.subTtitle.setChecked(false);
        }

        return convertView;
    }
}
