package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;

public class FavProductsAdapter  extends RecyclerView.Adapter< FavProductsAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private ArrayList<FavStoresResponce.ProductList> storeArrayList = new ArrayList<>();
    private String inputStr;
    SharedPreferences userPrefs;

    SharedPreferences LanguagePrefs;
    String language;

    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers> productOffersList;

    public FavProductsAdapter(Context context, ArrayList<FavStoresResponce.ProductList> storeArrayList) {
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_items, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_items_ar, parent, false);
            return new MyViewHolder(itemView);
        }


    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


        holder.mrpprice.setText("SAR "+ Constants.priceFormat1.format(storeArrayList.get(position).getPrice()));
        holder.ourprice.setText("SAR "+ Constants.priceFormat1.format(storeArrayList.get(position).getSellingPrice()));
        if (language.equalsIgnoreCase("En")){
            holder.itemname.setText(storeArrayList.get(position).getNameEn());
            holder.discount.setText("" + storeArrayList.get(position).getDiscountValue() + "% OFF");
        }else {
            holder.itemname.setText(storeArrayList.get(position).getNameAr());
            holder.discount.setText("" + storeArrayList.get(position).getDiscountValue() + "% إيقاف");
        }


        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL + storeArrayList.get(position).getImage())
                .into(holder.storeimage);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemname, mrpprice, ourprice, discount;
        ImageView storeimage;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.title);
            mrpprice = (TextView) itemView.findViewById(R.id.discounted_price);
            ourprice = (TextView) itemView.findViewById(R.id.price);
            discount = (TextView) itemView.findViewById(R.id.discount_percent);
            storeimage = (ImageView)itemView.findViewById(R.id.image);
            storelayout =(RelativeLayout)itemView.findViewById(R.id.main_layout);

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inputStr = prepareProductVariantJSON(getAdapterPosition());
                    new GetProductVariantsApi().execute("" + getAdapterPosition());
                }
            });
        }
    }

    private String prepareProductVariantJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  storeArrayList.get(position).getId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetProductVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductList();
                            variantsLists = response.body().getData().getVariantsList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ProductVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("skuid", storeArrayList.get(Integer.parseInt(strings[0])).getProductSkuId());
                            intent.putExtra("storeid", storeArrayList.get(Integer.parseInt(strings[0])).getStoreId());
                            intent.putExtra("branchId", storeArrayList.get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}
