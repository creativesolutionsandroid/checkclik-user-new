package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;

import java.util.ArrayList;


public class Variants2Adapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    private ArrayList<ProductVariantResponse.Jvariants> variantsLists = new ArrayList<>();

    SharedPreferences LanguagePrefs;
    String language;


    public Variants2Adapter(Context context, ArrayList<ProductVariantResponse.Jvariants> variantsLists) {
        this.context = context;
        this.variantsLists = variantsLists;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public int getCount() {
        return variantsLists.size();
    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView variant;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = LanguagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_variant_grid_item, null);

            } else {
                convertView = inflater.inflate(R.layout.list_variant_grid_item_ar, null);

            }


            holder.variant = (TextView) convertView.findViewById(R.id.variant);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equals("En")){
            holder.variant.setText(variantsLists.get(position).getValueEn());
        }else {
            holder.variant.setText(variantsLists.get(position).getValueAr());
        }


        if(variantsLists.get(position).getValueEn().equals(ProductVariantsActivityStep5.variant2Pos)) {
            holder.variant.setTextColor(context.getResources().getColor(R.color.white));
            holder.variant.setBackground(context.getResources().getDrawable(R.drawable.shape_variant_selected));
        }
        else {
            holder.variant.setTextColor(context.getResources().getColor(R.color.grey_in_variants));
            holder.variant.setBackground(context.getResources().getDrawable(R.drawable.shape_variant_unselected));
        }

        holder.variant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProductVariantsActivityStep5.variant2Pos = variantsLists.get(position).getValueEn();
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

}
