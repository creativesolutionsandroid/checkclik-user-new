package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OrderRetunActivity;
import com.cs.checkclickuser.Activites.OrderRetunDetailsActivity;
import com.cs.checkclickuser.Activites.ProductRatingActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;

import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;

public class OrderDetailsHistoryListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int orderStatus, branchid;
    int ordertype;
    ArrayList<OrderPendingResponce.Items> pendingArrayList = new ArrayList<>();

    public OrderDetailsHistoryListAdapter(Context context, ArrayList<OrderPendingResponce.Items> orderList, int orderStatus, int ordertype, int branchid, String language) {
        this.context = context;
        this.branchid = branchid;
        this.pendingArrayList = orderList;
        this.orderStatus = orderStatus;
        this.ordertype = ordertype;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return pendingArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView storename, iteamcount, qty, price, retunbtn, retunstatus, ratingtext;
        ImageView storeimage;
        RelativeLayout rejectedLayout;
        RatingBar ratingbar;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        convertView = inflater.inflate(R.layout.details_adapter_list, null);

        holder.ratingbar = (RatingBar) convertView.findViewById(R.id.ratingbar);

        holder.ratingtext = (TextView) convertView.findViewById(R.id.productrate);
        holder.storename = (TextView) convertView.findViewById(R.id.storename);
        holder.iteamcount = (TextView) convertView.findViewById(R.id.iteamcount);
        holder.qty = (TextView) convertView.findViewById(R.id.products);
        holder.price = (TextView) convertView.findViewById(R.id.price);
        holder.retunbtn = (TextView) convertView.findViewById(R.id.retun);
        holder.retunstatus = (TextView) convertView.findViewById(R.id.retunstatus);
        holder.storeimage = (ImageView) convertView.findViewById(R.id.storeimage);
        holder.storename.setText(pendingArrayList.get(position).getNameEn());
        holder.iteamcount.setText("" + pendingArrayList.get(position).getQty());
        holder.rejectedLayout = (RelativeLayout) convertView.findViewById(R.id.rejectedlayout);

//        if (pendingArrayList.get(position).getReturnRequestStatus()==1){
//            holder.retunstatus.setVisibility(View.VISIBLE);
//            holder.retunbtn.setVisibility(View.GONE);
//            holder.retunstatus.setText("Return Pending");
//        }

        if (!pendingArrayList.get(position).getIsAccepted()) {
            holder.rejectedLayout.setVisibility(View.VISIBLE);
        } else {
            holder.rejectedLayout.setVisibility(View.GONE);
        }

        if (pendingArrayList.get(position).getReturnRequestStatus() == 1) {
            holder.retunstatus.setVisibility(View.VISIBLE);
            holder.retunbtn.setVisibility(View.GONE);
            holder.retunstatus.setText("" + context.getResources().getString(R.string.return_pending));
        }

        if (pendingArrayList.get(position).getRating() == 0 && pendingArrayList.get(position).getIsAccepted()) {
            holder.ratingtext.setVisibility(View.VISIBLE);
            holder.ratingbar.setVisibility(View.GONE);
        } else if (pendingArrayList.get(position).getRating() > 0 && pendingArrayList.get(position).getIsAccepted()
                && !pendingArrayList.get(position).getNotReturnable()) {
            holder.ratingbar.setVisibility(View.VISIBLE);
            holder.ratingtext.setVisibility(View.GONE);
            holder.ratingbar.setRating(pendingArrayList.get(position).getRating());
        } else {
            holder.ratingbar.setVisibility(View.GONE);
            holder.ratingtext.setVisibility(View.GONE);
        }

        if (!pendingArrayList.get(position).getNotReturnable() && pendingArrayList.get(position).getIsAccepted()) {
//            if (ordertype == 2) {
            if (pendingArrayList.get(position).getReturnRequestStatus() == 1) {
                holder.retunstatus.setVisibility(View.VISIBLE);
                holder.ratingbar.setVisibility(View.GONE);
                holder.ratingtext.setVisibility(View.GONE);
            } else {
                holder.retunstatus.setVisibility(View.GONE);
            }
        } else if (pendingArrayList.get(position).getNotReturnable() && pendingArrayList.get(position).getIsAccepted()) {
            holder.retunstatus.setVisibility(View.GONE);
        } else {
            holder.retunstatus.setVisibility(View.GONE);
        }

        if (orderStatus == 16 || orderStatus == 3) {
            holder.ratingtext.setVisibility(View.INVISIBLE);
        }

        Log.d("TAG", "orderStatus: " + orderStatus);
        if (orderStatus == 11) {
            if (ordertype == 1 || ordertype == 2 || ordertype == 3) {
                if (!pendingArrayList.get(position).getNotReturnable()) {
                    if (ordertype == 2) {
                        if (pendingArrayList.get(position).getReturnDays() >= pendingArrayList.get(position).getRemainingDays()) {
                            holder.retunbtn.setVisibility(View.VISIBLE);
                            holder.retunstatus.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }

        holder.ratingtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductRatingActivity.class);
                intent.putExtra("Order", pendingArrayList);
                intent.putExtra("Orderpos", position);
                intent.putExtra("Type", ordertype);
                intent.putExtra("OrderId", pendingArrayList.get(position).getOrderId());
                intent.putExtra("Ids", pendingArrayList.get(position).getSKUId());
                intent.putExtra("BranchId", branchid);
                context.startActivity(intent);
            }
        });

//        if ArrayModel.getOrderHistoryArray[obj_SectionPath!].OrderType == 1 || ArrayModel.getOrderHistoryArray[obj_SectionPath!].OrderType == 2 || ArrayModel.getOrderHistoryArray[obj_SectionPath!].OrderType == 3 {
//            if let NotReturnable:Bool = ArrayModel.getOrderHistoryArray[obj_SectionPath!].Items[indexPath.row].NotReturnable {
//                if NotReturnable == false {
//                    if OrderType! == 2 {
//                        if ArrayModel.getOrderHistoryArray[obj_SectionPath!].Items[indexPath.row].ReturnRequestStatus! == 1 {
//                            cell.returnBtn.setTitle(" Return Pending ", for: .normal)
//                            cell.returnBtn.tag = indexPath.row
//                            cell.returnBtn.addTarget(self, action: #selector(returnProductBtn_tapped(_:)) , for: .touchUpInside)
//                            cell.returnBtn.isHidden = false
//                        }else if ArrayModel.getOrderHistoryArray[obj_SectionPath!].Items[indexPath.row].ReturnDays! >= ArrayModel.getOrderHistoryArray[obj_SectionPath!].Items[indexPath.row].RemainingDays! {
//                                cell.returnBtn.setTitle("Return", for: .normal)
//                        cell.returnBtn.tag = indexPath.row
//                        cell.returnBtn.addTarget(self, action: #selector(returnProductBtn_tapped(_:)) , for: .touchUpInside)
//                        cell.returnBtn.isHidden = false
//            }
//                    }
//                }
//            }
//        }

//        if (ordertype){
//            if (pendingArrayList.get(position).getReturnRequestStatus()!=1){
//                Log.d("TAG", "getView: "+ordertype);
//                if (pendingArrayList.get(position).getReturnDays() >= pendingArrayList.get(position).getRemainingDays()) {
//                    holder.retunbtn.setVisibility(View.VISIBLE);
//                }
//                else {
//                    holder.retunbtn.setVisibility(View.GONE);
//                }
//            }
//        }
        holder.retunbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderRetunActivity.class);
                intent.putExtra("Order", pendingArrayList);
                intent.putExtra("Orderpos", position);
                context.startActivity(intent);
            }
        });

        holder.retunstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderRetunDetailsActivity.class);
                intent.putExtra("Order", pendingArrayList);
                intent.putExtra("Orderpos", position);
                context.startActivity(intent);
            }
        });

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL + pendingArrayList.get(position).getImage())
                .into(holder.storeimage);

        holder.qty.setText("" + context.getResources().getString(R.string.qty) + " " + pendingArrayList.get(position).getQty());
        holder.price.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(position).getPrice()));
        return convertView;
    }
}
