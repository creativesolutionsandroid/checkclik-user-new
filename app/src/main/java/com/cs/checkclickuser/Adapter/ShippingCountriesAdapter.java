package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ShippingCountriesAdapter extends  RecyclerView.Adapter<ShippingCountriesAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<AdressCountryResponce.CityList> countries_list = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchList = new ArrayList<>();
    int type;
    AppCompatActivity activity;

    public ShippingCountriesAdapter(Context context, ArrayList<AdressCountryResponce.CityList> countries_list, AppCompatActivity activity) {
        this.context = context;
        this.type = type;
        this.countries_list = countries_list;
        this.branchList = branchList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.branch_list_child, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.country_name.setText("" + countries_list.get(position).getNameEn());

    }

    @Override
    public int getItemCount() {

        return countries_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView country_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            country_name = (TextView) itemView.findViewById(R.id.branch_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("country", countries_list.get(getAdapterPosition()));
                    activity.setResult(Activity.RESULT_OK, intent);
                    activity.finish();
                }
            });
        }
    }
}

