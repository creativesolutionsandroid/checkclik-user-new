package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeScreenBannersAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    ArrayList<HomePageResponse.Homebanners> bannersList;
    Context context;
    Activity activity;

    public HomeScreenBannersAdapter(Context context, ArrayList<HomePageResponse.Homebanners> bannersList, Activity activity) {
        this.context = context;
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannersList = bannersList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return (bannersList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_homescreen_banners, container, false);

        ImageView banner_image = (ImageView) itemView.findViewById(R.id.banner_image);
        TextView discount_value = (TextView) itemView.findViewById(R.id.discount_value);

        CamomileSpinner spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
        spinner.start();

        spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        banner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bannersList.get(position).getStoreNameEn() != null || bannersList.get(position).getStoreNameEn().length()>0)
                prepareGetBranchDetailsJSON(bannersList.get(position).getStoreNameEn(), bannersList.get(position).getLatitude()
                ,bannersList.get(position).getLongitude());
            }
        });

        Glide.with(context)
                .load(Constants.ADS_IMAGE_URL + bannersList.get(position).getBannerimageen())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(banner_image);

        if(bannersList.get(position).getOffertype() == 1) {
            discount_value.setText(bannersList.get(position).getPercentage() + "%\n" + context.getResources().getString(R.string.off_non_caps));
        }
        else {
            discount_value.setText(bannersList.get(position).getPercentage() + "\n" + context.getResources().getString(R.string.off_non_caps));
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    private class GetBranchDetailsApi extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchDetails> call = apiService.getBranchDetails(
                    RequestBody.create(MediaType.parse("application/json"), strings[0]));
            call.enqueue(new Callback<BranchDetails>() {
                @Override
                public void onResponse(Call<BranchDetails> call, Response<BranchDetails> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        BranchDetails branchResponse = response.body();
                        if (branchResponse.getData().getBranch() != null &&
                                branchResponse.getData().getBranch().size() > 0) {
                            Intent intent = new Intent(activity, ProductStoresActivityStep1.class);
                            intent.putExtra("stores", branchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", branchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "vendor_app_intent");
                            context.startActivity(intent);
                        }
                        else {
                            Toast.makeText(context, context.getResources().getString(R.string.store_details_not_available), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchDetails> call, Throwable t) {
                    Log.e("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private void prepareGetBranchDetailsJSON(String storeName, double lat, double longi){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("StoreName",  storeName);
            parentObj.put("Latitude",  lat);
            parentObj.put("Longitude",  longi);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj.toString());
        new GetBranchDetailsApi().execute(parentObj.toString());
    }
}
