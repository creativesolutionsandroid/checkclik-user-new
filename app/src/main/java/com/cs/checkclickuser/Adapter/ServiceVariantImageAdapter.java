package com.cs.checkclickuser.Adapter;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.List;

public class ServiceVariantImageAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    List<ServiceVariantResponse.JImages> bannersList;
    Context context;

    public ServiceVariantImageAdapter(Context context, List<ServiceVariantResponse.JImages> bannersList) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bannersList = bannersList;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public int getCount() {
        return (bannersList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_variants_image, container, false);

        ImageView banner_image = (ImageView) itemView.findViewById(R.id.image);

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + bannersList.get(position).getImage())
                .into(banner_image);

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
