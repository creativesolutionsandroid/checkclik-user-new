package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.checkclickuser.Models.Districts;
import com.cs.checkclickuser.Models.HomeCityResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class CitiesListAdapter extends RecyclerView.Adapter<CitiesListAdapter.MyViewHolder> {
    public Context context;
    public LayoutInflater inflater;
    private ArrayList<Districts> cityList;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences LanguagePrefs;
    String language;

    public CitiesListAdapter(Context context, ArrayList<Districts> cartArryLists) {
        this.context = context;
        this.cityList = cartArryLists;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_city_search, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView;
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_city_search_ar, parent, false);
            return new MyViewHolder(itemView);
        }



    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (cityList.get(position).getId() == 0) {
            if (language.equals("En")){
                holder.cityName.setText("" + cityList.get(position).getCityen());
            }else {
                holder.cityName.setText("" + cityList.get(position).getCityar());
            }

        }
        else {
            if (language.equals("En")){
                holder.cityName.setText("" + cityList.get(position).getNameen() + ", " +cityList.get(position).getCityen());
            }else {
                holder.cityName.setText("" + cityList.get(position).getNameen() + ", " +cityList.get(position).getCityar());
            }

        }
    }

    @Override
    public int getItemCount() {
        return cityList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cityName;

        public MyViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.city_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constants.CityName = cityList.get(getAdapterPosition()).getCityen();
                    Constants.CityNameAr = cityList.get(getAdapterPosition()).getCityar();

                    Constants.CityId = cityList.get(getAdapterPosition()).getCityid();
                    if (cityList.get(getAdapterPosition()).getId() != 0){
                        Constants.DistrictName = cityList.get(getAdapterPosition()).getNameen();
                        Constants.DistrictNameAr = cityList.get(getAdapterPosition()).getNamear();
                        Constants.DistrictId = cityList.get(getAdapterPosition()).getId();
                        Constants.Latitude = Double.parseDouble(cityList.get(getAdapterPosition()).getLatitude());
                        Constants.Longitude = Double.parseDouble(cityList.get(getAdapterPosition()).getLongitude());
                    }
                    else {
                        Constants.DistrictName = "";
                        Constants.DistrictId = 0;
                        Constants.Latitude = Double.parseDouble(cityList.get(getAdapterPosition()).getLatitude());
                        Constants.Longitude = Double.parseDouble(cityList.get(getAdapterPosition()).getLongitude());
                    }

                    userPrefsEditor.putString("city", Constants.CityName);
                    userPrefsEditor.putString("city_ar", Constants.CityNameAr);
                    userPrefsEditor.putInt("cityId", Constants.CityId);
                    userPrefsEditor.putString("district", Constants.DistrictName);
                    userPrefsEditor.putString("district_ar", Constants.DistrictNameAr);
                    userPrefsEditor.putInt("districtId", Constants.DistrictId);
                    userPrefsEditor.putString("lat", Double.toString(Constants.Latitude));
                    userPrefsEditor.putString("lng", Double.toString(Constants.Longitude));
                    userPrefsEditor.commit();

                    ((Activity) context).setResult(Activity.RESULT_OK);
                    ((Activity) context).finish();
                }
            });
        }
    }

}
