package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class FavServicesAdapter extends RecyclerView.Adapter< FavServicesAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private ArrayList<FavStoresResponce.ServiceList> storeArrayList = new ArrayList<>();
    int pos = 0;
    private String inputStr;
    SharedPreferences userPrefs;
    private ArrayList<HomePageResponse.Offers2> servicesOffersList;
    private ArrayList<ServiceVariantResponse.ServiceDetails> serviceProductLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> serviceVariantsLists = new ArrayList<>();

    SharedPreferences LanguagePrefs;
    String language;

    public FavServicesAdapter(Context context,  ArrayList<FavStoresResponce.ServiceList> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_items, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_items_ar, parent, false);
            return new MyViewHolder(itemView);
        }



    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,  int position) {

        holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (language.equalsIgnoreCase("En")){
            holder.itemname.setText(storeArrayList.get(position).getServiceNameEn());
            holder.discount.setText(""+storeArrayList.get(pos).getDiscountValue()+"% OFF");
        }else {
            holder.itemname.setText(storeArrayList.get(position).getServiceNameAr());
            holder.discount.setText(""+storeArrayList.get(pos).getDiscountValue()+"% إيقاف");
        }

        holder.mrpprice.setText("SAR "+storeArrayList.get(position).getPrice());
        holder.ourprice.setText("SAR "+storeArrayList.get(pos).getSellingPrice());

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL+storeArrayList.get(position).getImage())
                .into(holder.storeimage);
        Log.d(TAG, "serviesimage: "+STORE_IMAGE_URL+storeArrayList.get(position).getImage());

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "adaptersize: "+storeArrayList.size());
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView itemname, mrpprice, ourprice, discount;
        ImageView storeimage;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemname = (TextView) itemView.findViewById(R.id.title);
            mrpprice = (TextView) itemView.findViewById(R.id.discounted_price);
            ourprice = (TextView) itemView.findViewById(R.id.price);
            discount = (TextView) itemView.findViewById(R.id.discount_percent);
            storeimage = (ImageView)itemView.findViewById(R.id.image);
            storelayout =(RelativeLayout)itemView.findViewById(R.id.main_layout);


            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inputStr = prepareGetStoresJSON(getAdapterPosition());
                    new GetServiceVariantsApi().execute("" + getAdapterPosition());
                }
            });

        }
    }

    private class GetServiceVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            serviceProductLists = response.body().getData().getServiceDetails();
                            serviceVariantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", serviceProductLists);
                            intent.putExtra("variantList", serviceVariantsLists);
                            intent.putExtra("storeId", storeArrayList.get(Integer.parseInt(strings[0])).getStoreId());
                            intent.putExtra("skuid", storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                            intent.putExtra("branchId", storeArrayList.get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  storeArrayList.get(position).getServiceId());
            parentObj.put("SubCategoryId",  storeArrayList.get(position).getSubCategoryId());
            parentObj.put("BranchId",  storeArrayList.get(position).getBranchId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}
