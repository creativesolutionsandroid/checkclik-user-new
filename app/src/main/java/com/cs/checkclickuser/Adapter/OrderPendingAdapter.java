package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.CustomListView;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPendingAdapter extends RecyclerView.Adapter<OrderPendingAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    int pos = 0;
    String language;
    String itemimage, itemimage1, itemimage2;

    public OrderPendingAdapter(Context context, ArrayList<OrderPendingResponce.Data> pendingArrayList, String language) {
        this.language = language;
        this.context = context;
        this.pendingArrayList = pendingArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.oredr_pending_adapter, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.oredr_pending_adapter_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (language.equalsIgnoreCase("En")) {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.number_of_units) + " : " + pendingArrayList.get(position).getItems().size() + " " + context.getResources().getString(R.string.item));
            holder.orderno.setText("" + context.getResources().getString(R.string.order_no) + ": " + pendingArrayList.get(position).getInvoiceNo());
        } else {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.item_ar) + " " + pendingArrayList.get(position).getItems().size() + " : " +  context.getResources().getString(R.string.number_of_units_ar));
            holder.orderno.setText("" + pendingArrayList.get(position).getInvoiceNo() + " :" + context.getResources().getString(R.string.order_no_ar));
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM d, yyyy", Locale.US);

        Date order_date = null;
        String str_order_date;

        try {
            order_date = simpleDateFormat.parse(pendingArrayList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        str_order_date = simpleDateFormat1.format(order_date);

        if (language.equalsIgnoreCase("En")) {
            holder.order_date.setText("" + context.getResources().getString(R.string.order_date) + " " + str_order_date);
        } else {
            holder.order_date.setText("" + str_order_date + " " +  context.getResources().getString(R.string.order_date_ar));
        }

        String[] mimage = null;
        mimage = pendingArrayList.get(position).getMultipleimage().split(",");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        if (mimage.length == 3 || mimage.length > 3) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.VISIBLE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];
            itemimage2 = mimage[2];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image1);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage2)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image2);

        } else if (mimage.length == 2) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .into(holder.image1);

        } else if (mimage.length == 1) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.GONE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = pendingArrayList.get(position).getItems().get(0).getImage();

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);
        }

        holder.orderstatus.setImageResource(R.drawable.status_red);

        if (position == 0) {
            holder.startingLayout.setPadding(0, 20, 0, 0);
        } else {
            holder.startingLayout.setPadding(0, 70, 0, 0);
        }

        if (pendingArrayList.get(position).getOrderStatus() == 1) {
            holder.accepetedstatus.setVisibility(View.VISIBLE);
            holder.rejectlayout.setVisibility(View.GONE);
        } else {
            holder.accepetedstatus.setVisibility(View.GONE);
            if (language.equalsIgnoreCase("En")) {
                holder.numberof_orders.setText(pendingArrayList.get(position).getRejectedItemsCount() + " out of " + pendingArrayList.get(position).getItemsCount() + " order have been Rejected");
            } else {
                holder.numberof_orders.setText("" + context.getResources().getString(R.string.order_have_been_rejected_ar) + " " + pendingArrayList.get(position).getItemsCount() + " " + context.getResources().getString(R.string.out_of_ar) + " " + pendingArrayList.get(position).getRejectedItemsCount());
            }
            holder.rejectlayout.setVisibility(View.VISIBLE);
        }

        holder.yes_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getaccepetApi().execute();
            }
        });

        holder.no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new getcancelApi().execute();
            }
        });

//        Log.d(TAG, "pending list count: "+pendingArrayList.get(position).getItems());
        if (pendingArrayList.get(position).getItems() == null) {

        } else {

            PendingOrderItemsList mAdapter = new PendingOrderItemsList(context, pendingArrayList.get(position).getItems(), pendingArrayList.get(position).getOrderStatus(), language);
            holder.list_item.setAdapter(mAdapter);
        }
    }

    @Override
    public int getItemCount() {
//        Log.d(TAG, "getItemCount: "+cartlistArrayList.size());
        return pendingArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView status, orderno;
        ImageView orderstatus;
        CustomListView list_item;
        LinearLayout datelayout, buttonlayout;
        TextView accepetedstatus, order_date;
        RelativeLayout rejectlayout, layout1;
        TextView numberof_orders, no_of_items;
        ImageView image, image1, image2;
        RelativeLayout item1Layout, item2Layout, item3Layout;
        CamomileSpinner spinner, spinner1, spinner2;
        Button yes_btn;
        Button no_btn;
        LinearLayout layout;
        RelativeLayout viewlayout, startingLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            yes_btn = itemView.findViewById(R.id.yes_btn);
            no_btn = itemView.findViewById(R.id.no_btn);
            accepetedstatus = itemView.findViewById(R.id.ordestatustext);
            rejectlayout = (RelativeLayout) itemView.findViewById(R.id.rejectlayout);

            order_date = (TextView) itemView.findViewById(R.id.order_date);
            no_of_items = (TextView) itemView.findViewById(R.id.no_of_items);
            numberof_orders = (TextView) itemView.findViewById(R.id.scheduldate);
            status = (TextView) itemView.findViewById(R.id.pendingstatus);
            orderno = (TextView) itemView.findViewById(R.id.orderno);
            orderstatus = (ImageView) itemView.findViewById(R.id.greenimage);
            list_item = (CustomListView) itemView.findViewById(R.id.relativelist);
            datelayout = (LinearLayout) itemView.findViewById(R.id.datelayout);
            buttonlayout = (LinearLayout) itemView.findViewById(R.id.buttonlayout);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            layout1 = (RelativeLayout) itemView.findViewById(R.id.layout1);
            viewlayout = (RelativeLayout) itemView.findViewById(R.id.viewlayout);
            startingLayout = (RelativeLayout) itemView.findViewById(R.id.starting_layout);

            image = (ImageView) itemView.findViewById(R.id.image);
            image1 = (ImageView) itemView.findViewById(R.id.image1);
            image2 = (ImageView) itemView.findViewById(R.id.image2);
            item1Layout = (RelativeLayout) itemView.findViewById(R.id.item1_layout);
            item2Layout = (RelativeLayout) itemView.findViewById(R.id.item2_layout);
            item3Layout = (RelativeLayout) itemView.findViewById(R.id.item3_layout);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
            spinner1 = (CamomileSpinner) itemView.findViewById(R.id.spinner1);
            spinner2 = (CamomileSpinner) itemView.findViewById(R.id.spinner2);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "pending");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    Log.d("TAG", "onClick: ");
                    context.startActivity(intent);
                }
            });

            list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "pending");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    Log.d("TAG", "onClick: ");
                    context.startActivity(intent);
                }
            });

            viewlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderDetailsActivity.class);
                    intent.putExtra("Order", "pending");
                    intent.putExtra("arrylist", pendingArrayList);
                    intent.putExtra("Orderpos", getAdapterPosition());
                    Log.d("TAG", "onClick: ");
                    context.startActivity(intent);
                }
            });

        }
    }

    private String prepareJson(int pos) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", pendingArrayList.get(pos).getUserId());
            parentObj.put("OrderId", pendingArrayList.get(pos).getOrderId());
            parentObj.put("Message", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("TAG", "prepareOrderCancelJson: " + parentObj.toString());
        return parentObj.toString();
    }

    private class getcancelApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getcancl(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if (response.isSuccessful()) {
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            } else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class getaccepetApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareJson(pos);
            Constants.showLoadingDialog((Activity) context);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getaccepet(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    if (response.isSuccessful()) {
                        OrderCancelResponce requestsResponse = response.body();
                        try {
                            if (requestsResponse.getStatus()) {
                                Intent intent = new Intent("cancel_request");
                                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                            } else {
                                // status false case
                                String failureResponse = requestsResponse.getMessage();
                                Toast.makeText(context, failureResponse, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            String failureResponse = requestsResponse.getMessage();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.str_connection_error_ar), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
}
