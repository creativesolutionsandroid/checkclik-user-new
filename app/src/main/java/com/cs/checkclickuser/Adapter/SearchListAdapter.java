package com.cs.checkclickuser.Adapter;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class SearchListAdapter extends BaseAdapter {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private ArrayList<String> seachArrayList = new ArrayList<>();
    String inputStr;
    public LayoutInflater inflater;

    SharedPreferences LanguagePrefs;
    String language = "En";

    public SearchListAdapter(Context context, ArrayList<String> storeArrayList) {
        this.context = context;
        this.seachArrayList = storeArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return seachArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView productname;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        holder = new ViewHolder();

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.searchlist_adapter, null);
        } else {
            convertView = inflater.inflate(R.layout.searchlist_adapter_ar, null);
        }

        holder.productname = (TextView) convertView.findViewById(R.id.productname);

        holder.productname.setText(seachArrayList.get(position));

        return convertView;
    }
}
