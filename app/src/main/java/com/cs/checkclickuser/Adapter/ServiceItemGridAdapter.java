package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.ServiceItemResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceItemGridAdapter extends RecyclerView.Adapter<ServiceItemGridAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ArrayList<ServiceItemResponse.ServiceList> storeArrayList = new ArrayList<>();
    private AppCompatActivity activity;
    private int storeId;
    SharedPreferences userPrefs;
    private ArrayList<ServiceVariantResponse.ServiceDetails> productLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> variantsLists = new ArrayList<>();
    String inputStr;
    int type;
    SharedPreferences LanguagePrefs;
    String language;


    public ServiceItemGridAdapter(Context context, ArrayList<ServiceItemResponse.ServiceList> storesArrayList, int storeId, int type){
        this.context = context;
        this.activity = activity;
        this.storeId = storeId;
        this.type = type;
        this.storeArrayList = storesArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_grid_items, parent, false);

            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_product_grid_items, parent, false);

            return new MyViewHolder(itemView);
        }


    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.discountedPrice.setPaintFlags(holder.discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

//        if(storeArrayList.get(position).getDiscounttype() == 1) {
            holder.discount_percent.setText((int)storeArrayList.get(position).getDiscountValue()+"% " + context.getResources().getString(R.string.off_non_caps));
//        }
//        else {
//            holder.discount_percent.setText(storeArrayList.get(position).getDiscountValue()+" Off");
//        }
        if (language.equals("En")){
            holder.title.setText(storeArrayList.get(position).getServiceNameEn());
        }else {
            holder.title.setText(storeArrayList.get(position).getServiceNameAr());
        }

        holder.discountedPrice.setText("SAR "+Constants.priceFormat1.format(storeArrayList.get(position).getSellingPrice()));
        holder.price.setText("SAR "+Constants.priceFormat1.format(storeArrayList.get(position).getPrice()));

        if(storeArrayList.get(position).getSellingPrice() == storeArrayList.get(position).getPrice()){
            holder.discountLayout.setVisibility(View.INVISIBLE);
            holder.discountedPrice.setVisibility(View.INVISIBLE);
        }
        else {
            holder.discountLayout.setVisibility(View.VISIBLE);
            holder.discountedPrice.setVisibility(View.VISIBLE);
        }

        if(storeArrayList.get(position).getFavorite() ||
                (CacheData.favProductId != null && CacheData.favProductId.contains(storeArrayList.get(position).getServiceId()))) {
            holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
        }
        else {
            holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + storeArrayList.get(position).getImage())
                .into(holder.productImage);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView productImage, favIcon;
        TextView discount_percent;
        TextView title;
        TextView discountedPrice;
        TextView price;
        RelativeLayout discountLayout;
        LinearLayout mainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            productImage = (ImageView) itemView.findViewById(R.id.image);
            favIcon = (ImageView) itemView.findViewById(R.id.fav_icon);
            discount_percent = (TextView) itemView.findViewById(R.id.discount_percent);
            title = (TextView) itemView.findViewById(R.id.title);
            discountedPrice = (TextView) itemView.findViewById(R.id.discounted_price);
            price = (TextView) itemView.findViewById(R.id.price);
            discountLayout = (RelativeLayout) itemView.findViewById(R.id.discount_layout);
            mainLayout = (LinearLayout) itemView.findViewById(R.id.main_layout);

            favIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new InsertStoreFav().execute(""+getAdapterPosition());
                }
            });
            mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inputStr = prepareGetStoresJSON(getAdapterPosition());
                    new GetVariantsApi().execute("" + getAdapterPosition());
                }
            });
        }
    }

    private class GetVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getServiceDetails();
                            variantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("storeId", storeId);
                            intent.putExtra("skuid", storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                            intent.putExtra("branchId", storeArrayList.get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  storeArrayList.get(position).getServiceId());
            parentObj.put("SubCategoryId",  storeArrayList.get(position).getSubCategoryId());
            parentObj.put("BranchId",  storeArrayList.get(position).getBranchId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog((Activity)context);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                if(storeArrayList.get(Integer.parseInt(strings[0])).getFavorite()) {
                    parentObj.put("Type", 3);
                }
                else {
                    parentObj.put("Type", 2);
                }
                parentObj.put("StatusId",  "1");
                parentObj.put("Ids", storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                parentObj.put("UserId",userPrefs.getString("userId", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if(storeArrayList.get(Integer.parseInt(strings[0])).getFavorite()) {
                                storeArrayList.get(Integer.parseInt(strings[0])).setFavorite(false);
                                if (CacheData.favProductId != null && CacheData.favProductId.contains(storeArrayList.get(Integer.parseInt(strings[0])).getServiceId())) {
                                    CacheData.favProductId.remove(storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                                }
                            }
                            else {
                                storeArrayList.get(Integer.parseInt(strings[0])).setFavorite(true);
                                CacheData.favProductId.add(""+storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                            }
                            notifyDataSetChanged();
                            Constants.showOneButtonAlertDialog(stores.getMessage(), context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), (Activity)context);
                        }
                    }
                    else {
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}