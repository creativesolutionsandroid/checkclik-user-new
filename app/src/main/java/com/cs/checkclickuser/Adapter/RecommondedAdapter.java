package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class RecommondedAdapter extends RecyclerView.Adapter<RecommondedAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    public LayoutInflater inflater;
    private ArrayList<HomePageResponse.Featuredstores> featuredstores = new ArrayList<>();
    int pos = 0;

    SharedPreferences LanguagePrefs;
    String language;


    public RecommondedAdapter(Context context, ArrayList<HomePageResponse.Featuredstores> featuredstores) {
        this.context = context;
        this.featuredstores = featuredstores;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_recommended_view_all, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_recommended_view_all_ar, parent, false);
            return new MyViewHolder(itemView);
        }



    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (language.equals("En")){

            holder.title.setText(featuredstores.get(position).getStoreen());
        }else {

            holder.title.setText(featuredstores.get(position).getStorear());
        }
        holder.title.setText(featuredstores.get(position).getStoreen());

        holder.rating.setText(""+ featuredstores.get(position).getReviews());
        holder.reviewsCount.setText("("+ featuredstores.get(position).getRatings() + " " + context.getResources().getString(R.string.reviews) + ")");

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL + featuredstores.get(position).getBranchlogoimage())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return featuredstores.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title, rating, reviewsCount, visitedCount;
        ImageView image;
        RelativeLayout list_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            rating = (TextView) itemView.findViewById(R.id.rating);
            reviewsCount = (TextView) itemView.findViewById(R.id.revies_count);
            visitedCount = (TextView) itemView.findViewById(R.id.visited);

            image =(ImageView)itemView.findViewById(R.id.image);
            list_item=(RelativeLayout)itemView.findViewById(R.id.relativelist);

            list_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                    intent.putExtra("stores", featuredstores);
                    intent.putExtra("pos", getAdapterPosition());
                    intent.putExtra("class", "home");
                    context.startActivity(intent);
                }
            });
        }
    }
}
