package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.SearchProductListResponce;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchItemAdapter extends RecyclerView.Adapter<SearchItemAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    SearchProductListResponce.Data seachproductArrayList ;
    String inputStr;
    SharedPreferences userPrefs;
    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceDetails> serviceProductLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> serviceVariantsLists = new ArrayList<>();


    SharedPreferences LanguagePrefs;
    String language = "En";

    public SearchItemAdapter(Context context,  SearchProductListResponce.Data storeArrayList){
        this.context = context;
        this.activity = activity;
        this.seachproductArrayList = storeArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_search_items, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_search_items_ar, parent, false);
            return new MyViewHolder(itemView);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int pos) {
        if (language.equalsIgnoreCase("En")){
            holder.title.setText(seachproductArrayList.getResults().get(pos).getNameEn());
        }else {
            holder.title.setText(seachproductArrayList.getResults().get(pos).getNameAr());
        }

        holder.discountprice.setText("SAR "+Constants.priceFormat1.format(seachproductArrayList.getResults().get(pos).getPrice()));
        holder.mrpprice.setText("SAR "+Constants.priceFormat1.format(seachproductArrayList.getResults().get(pos).getSellingPrice()));

        if(seachproductArrayList.getResults().get(pos).getPrice() == seachproductArrayList.getResults().get(pos).getSellingPrice()) {
            holder.mrpprice.setVisibility(View.INVISIBLE);
        }
        else {
            holder.mrpprice.setVisibility(View.VISIBLE);
        }

        holder.mrpprice.setPaintFlags(holder.discountprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        Glide.with(context).load(Constants.PRODUCTS_IMAGE_URL + seachproductArrayList.getResults().get(pos).getProductMainImage()).into(holder.productimage);

    }

    @Override
    public int getItemCount() {
        return seachproductArrayList.getResults().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title,mrpprice,discountprice;
        CardView produtlistview;
        ImageView productimage;

        public MyViewHolder(View itemView) {
            super(itemView);
            produtlistview=(CardView)itemView.findViewById(R.id.layout);
            productimage=(ImageView)itemView.findViewById(R.id.productimage);
            title=(TextView)itemView.findViewById(R.id.title);
            mrpprice=(TextView)itemView.findViewById(R.id.discounted_price);
            discountprice=(TextView)itemView.findViewById(R.id.price);

            produtlistview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(seachproductArrayList.getResults().get(getAdapterPosition()).getItemType().equals("P")) {
                        inputStr = prepareProductVariantJSON(getAdapterPosition());
                        new GetProductVariantsApi().execute("" + getAdapterPosition());
                    }
                    else if (seachproductArrayList.getResults().get(getAdapterPosition()).getItemType().equals("S")){
                        inputStr = prepareGetStoresJSON(getAdapterPosition());
                        new GetServiceVariantsApi().execute("" + getAdapterPosition());
                    }
                }
            });
        }
    }

    private class GetProductVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductList();
                            variantsLists = response.body().getData().getVariantsList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ProductVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("skuid", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getProductSKUId());
                            intent.putExtra("storeid", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getStoreId());
                            intent.putExtra("branchId", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareProductVariantJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  seachproductArrayList.getResults().get(position).getProductBranchId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetServiceVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            serviceProductLists = response.body().getData().getServiceDetails();
                            serviceVariantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", serviceProductLists);
                            intent.putExtra("variantList", serviceVariantsLists);
                            intent.putExtra("skuid", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getProductSKUId());
                            intent.putExtra("storeid", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getStoreId());
                            intent.putExtra("branchId", seachproductArrayList.getResults().get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  seachproductArrayList.getResults().get(position).getProductBranchId());
            parentObj.put("SubCategoryId",  "0");
            parentObj.put("BranchId",  seachproductArrayList.getResults().get(position).getBranchId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}
