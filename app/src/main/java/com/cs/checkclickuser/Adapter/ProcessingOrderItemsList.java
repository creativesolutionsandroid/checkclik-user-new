package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.util.ArrayList;

public class ProcessingOrderItemsList extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String TAG = "TAG";
    SharedPreferences userPrefs;
    String inputStr;
    int orderStatus;
    private ArrayList<OrderPendingResponce.Items> itemsList = new ArrayList<>();
    String language;

    public ProcessingOrderItemsList(Context context, ArrayList<OrderPendingResponce.Items> itemsList, int orderStatus, String language) {
        this.context = context;
        this.language = language;
        this.itemsList = itemsList;
        this.orderStatus = orderStatus;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView imageView;
        TextView productname;
        TextView qty;
        LinearLayout layout;
        RelativeLayout rejecatedlayout, acceptedlayout;
        CamomileSpinner spinner;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.pending_adapter_list, null);
            } else {
                convertView = inflater.inflate(R.layout.pending_adapter_list_arabic, null);
            }

            holder.imageView = convertView.findViewById(R.id.storeimage);
            holder.productname = convertView.findViewById(R.id.storename);
            holder.qty = convertView.findViewById(R.id.products);
            holder.layout = convertView.findViewById(R.id.layout);
            holder.acceptedlayout = convertView.findViewById(R.id.acceptedlayout);
            holder.rejecatedlayout = convertView.findViewById(R.id.rejectlayout);
            holder.spinner = (CamomileSpinner) convertView.findViewById(R.id.spinner);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.qty.setText("" + context.getResources().getString(R.string.qty) + " : " + itemsList.get(position).getQty());
            holder.productname.setText(itemsList.get(position).getNameEn());
        } else {
            holder.qty.setText("" + itemsList.get(position).getQty() + " : " + context.getResources().getString(R.string.qty_ar));
            holder.productname.setText(itemsList.get(position).getNameAr());
        }

        if (!itemsList.get(position).getIsAccepted()) {
            holder.rejecatedlayout.setVisibility(View.VISIBLE);
        } else {
            holder.rejecatedlayout.setVisibility(View.GONE);
        }

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + itemsList.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })

                .into(holder.imageView);


        return convertView;
    }
}
