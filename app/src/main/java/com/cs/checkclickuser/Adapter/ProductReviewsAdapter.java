package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.ProductStoreReviews;
import com.cs.checkclickuser.Models.ReviewResponce;
import com.cs.checkclickuser.R;
import com.lovejjfg.shadowcircle.CircleImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;

public class ProductReviewsAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    List<ProductStoreReviews.Data> reviewsList;
    SharedPreferences LanguagePrefs;
    String language;

    public ProductReviewsAdapter(Context context, List<ProductStoreReviews.Data> reviewsList) {
        this.context = context;
        this.reviewsList = reviewsList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return reviewsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView customerName, review, reviewDate;
        RatingBar ratingBar;
        CircleImageView profilePic;

    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = LanguagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.product_reviews_list, null);
            } else {
                convertView = inflater.inflate(R.layout.product_reviews_list_ar, null);
            }


            holder.customerName = (TextView) convertView.findViewById(R.id.customer_name);
            holder.reviewDate = (TextView) convertView.findViewById(R.id.review_date);
            holder.review = (TextView) convertView.findViewById(R.id.comment);

            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.ratingbar);

            holder.profilePic = (CircleImageView) convertView.findViewById(R.id.ac_profilepic);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.customerName.setText(reviewsList.get(position).getNameen());
        holder.review.setText(reviewsList.get(position).getComments());

        holder.ratingBar.setRating(reviewsList.get(position).getRating());

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL+ reviewsList.get(position).getProfilephoto())
                .into(holder.profilePic);

        String date1 = reviewsList.get(position).getCreatedon();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd'th' MMM yyyy", Locale.US);

        try {
            Date datetime = inputFormat.parse(date1);
            date1 = outputFormat.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.reviewDate.setText(""+date1);

        return convertView;
    }
}
