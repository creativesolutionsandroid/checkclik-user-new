package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.OrderDetailsActivity;
import com.cs.checkclickuser.Activites.OrderDetailsHistoryActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.CustomListView;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    public LayoutInflater inflater;
    private ArrayList<OrderPendingResponce.Data> histoyArrayList = new ArrayList<>();
    boolean ordertype;
    String language;
    String itemimage, itemimage1, itemimage2;

    public OrderHistoryAdapter(Context context, ArrayList<OrderPendingResponce.Data> histoyArrayList, String language) {
        this.language = language;
        this.context = context;
        this.activity = activity;
        this.histoyArrayList = histoyArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_history_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_history_list_arabic, parent, false);
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        if (language.equalsIgnoreCase("En")) {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.number_of_units) + " : " + histoyArrayList.get(position).getItems().size() + " " + context.getResources().getString(R.string.item));
            holder.orderno.setText("" + context.getResources().getString(R.string.order_no) + " :" + histoyArrayList.get(position).getInvoiceNo());
        } else {
            holder.no_of_items.setText("" + context.getResources().getString(R.string.item_ar) + " " + histoyArrayList.get(position).getItems().size() + " : " +  context.getResources().getString(R.string.number_of_units_ar));
            holder.orderno.setText("" + histoyArrayList.get(position).getInvoiceNo() + " :" + context.getResources().getString(R.string.order_no_ar));
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMM d, yyyy", Locale.US);

        Date order_date = null;
        String str_order_date;

        try {
            order_date = simpleDateFormat.parse(histoyArrayList.get(position).getOrderDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        str_order_date = simpleDateFormat1.format(order_date);

        if (language.equalsIgnoreCase("En")) {
            holder.order_date.setText("" + context.getResources().getString(R.string.order_date) + " " + str_order_date);
        } else {
            holder.order_date.setText("" + str_order_date + " " +  context.getResources().getString(R.string.order_date_ar));
        }

        String[] mimage = null;
        mimage = histoyArrayList.get(position).getMultipleimage().split(",");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );


        if (mimage.length == 3 || mimage.length > 3) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.VISIBLE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];
            itemimage2 = mimage[2];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner1.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image1);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage2)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner2.setVisibility(View.GONE);
                            return false;
                        }
                    })

                    .into(holder.image2);

        } else if (mimage.length == 2) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.VISIBLE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = mimage[0];
            itemimage1 = mimage[1];

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage1)
                    .into(holder.image1);

        } else if (mimage.length == 1) {

            holder.item1Layout.setVisibility(View.VISIBLE);
            holder.item2Layout.setVisibility(View.GONE);
            holder.item3Layout.setVisibility(View.GONE);

            itemimage = histoyArrayList.get(position).getItems().get(0).getImage();

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + itemimage)
                    .into(holder.image);
        }

        if (position == 0) {
            holder.startingLayout.setPadding(0, 20, 0, 0);
        } else {
            holder.startingLayout.setPadding(0, 70, 0, 0);
        }

        if (language.equalsIgnoreCase("En")) {
            if (histoyArrayList.get(position).getOrderStatus() == 11) {
                holder.status.setText("" + context.getResources().getString(R.string.delivered_str));
            } else if (histoyArrayList.get(position).getOrderStatus() == 16) {
                holder.status.setText("" + context.getResources().getString(R.string.cancelled));
            } else if (histoyArrayList.get(position).getOrderStatus() == 3) {
                holder.status.setText("" + context.getResources().getString(R.string.rejected));
            }
        } else {
            if (histoyArrayList.get(position).getOrderStatus() == 11) {
                holder.status.setText("" + context.getResources().getString(R.string.delivered_str_ar));
            } else if (histoyArrayList.get(position).getOrderStatus() == 16) {
                holder.status.setText("" + context.getResources().getString(R.string.cancelled_ar));
            } else if (histoyArrayList.get(position).getOrderStatus() == 3) {
                holder.status.setText("" + context.getResources().getString(R.string.rejected_ar));
            }
        }

        if (histoyArrayList.get(position).getItems() == null) {

        } else {
            HistoryOrderItemsList mAdapter = new HistoryOrderItemsList(context, histoyArrayList.get(position).getItems(), histoyArrayList.get(position).getExpectingDelivery(), histoyArrayList.get(position).getOrderType(), histoyArrayList.get(position).getBranchId(), histoyArrayList.get(position).getOrderStatus(), language);
            holder.list_item.setAdapter(mAdapter);
        }

        holder.layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsHistoryActivity.class);
                intent.putExtra("arrylist", histoyArrayList);
                intent.putExtra("Orderpos", position);
                intent.putExtra("ordertype", ordertype);
                context.startActivity(intent);
            }
        });

        holder.list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(context, OrderDetailsHistoryActivity.class);
                intent.putExtra("arrylist", histoyArrayList);
                intent.putExtra("Orderpos", position);
                intent.putExtra("ordertype", ordertype);
                context.startActivity(intent);
            }
        });

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsHistoryActivity.class);
                intent.putExtra("arrylist", histoyArrayList);
                intent.putExtra("Orderpos", position);
                intent.putExtra("ordertype", ordertype);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return histoyArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView status, orderno, no_of_items, order_date;
        ImageView orderstatus;
        CustomListView list_item;
        RelativeLayout layout1, startingLayout;
        LinearLayout layout;
        ImageView image, image1, image2;
        RelativeLayout item1Layout, item2Layout, item3Layout;
        CamomileSpinner spinner, spinner1, spinner2;

        public MyViewHolder(View itemView) {
            super(itemView);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
            status = (TextView) itemView.findViewById(R.id.pendingstatus);
            orderno = (TextView) itemView.findViewById(R.id.orderno);
            no_of_items = (TextView) itemView.findViewById(R.id.no_of_items);
            orderstatus = (ImageView) itemView.findViewById(R.id.greenimage);
            list_item = (CustomListView) itemView.findViewById(R.id.relativelist);
            layout1 = (RelativeLayout) itemView.findViewById(R.id.viewlayout);
            startingLayout = (RelativeLayout) itemView.findViewById(R.id.starting_layout);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);

            image = (ImageView) itemView.findViewById(R.id.image);
            image1 = (ImageView) itemView.findViewById(R.id.image1);
            image2 = (ImageView) itemView.findViewById(R.id.image2);
            item1Layout = (RelativeLayout) itemView.findViewById(R.id.item1_layout);
            item2Layout = (RelativeLayout) itemView.findViewById(R.id.item2_layout);
            item3Layout = (RelativeLayout) itemView.findViewById(R.id.item3_layout);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);
            spinner1 = (CamomileSpinner) itemView.findViewById(R.id.spinner1);
            spinner2 = (CamomileSpinner) itemView.findViewById(R.id.spinner2);
        }
    }
}
