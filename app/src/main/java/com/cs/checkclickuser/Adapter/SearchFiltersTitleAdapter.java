package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Models.SearchProductListResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.CustomListView;

import java.util.ArrayList;
import java.util.List;

public class SearchFiltersTitleAdapter extends BaseAdapter {

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private List<SearchProductListResponce.Facet> filtersList = new ArrayList<>();
    String inputStr;
    public LayoutInflater inflater;

    public SearchFiltersTitleAdapter(Context context, List<SearchProductListResponce.Facet> storeArrayList) {
        this.context = context;
        this.filtersList = storeArrayList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return filtersList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title;
        CustomListView subTitlesList;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = inflater.inflate(R.layout.item_search_filter_title, null);

            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.subTitlesList = (CustomListView) convertView.findViewById(R.id.search_subtitles);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(filtersList.get(position).getFacetEn());

        SearchFiltersSubTitleAdapter mAdapter = new SearchFiltersSubTitleAdapter(context, filtersList.get(position).getFacetFilters());
        holder.subTitlesList.setAdapter(mAdapter);

        return convertView;
    }
}
