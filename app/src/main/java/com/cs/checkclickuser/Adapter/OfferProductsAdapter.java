package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductVariantsActivityStep5;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferProductsAdapter extends RecyclerView.Adapter<OfferProductsAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";

    int storeId;
    private AppCompatActivity activity;
    SharedPreferences userPrefs;
    private ArrayList<HomePageResponse.Offers> productOffersList;
    private ArrayList<HomePageResponse.Offers2> servicesOffersList;
    String inputStr;
    int type;
    SharedPreferences LanguagePrefs;
    String language;

    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceDetails> serviceProductLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> serviceVariantsLists = new ArrayList<>();

    public OfferProductsAdapter(Context context, ArrayList<HomePageResponse.Offers> productOffersList,
                                ArrayList<HomePageResponse.Offers2> servicesOffersList, AppCompatActivity activity) {
        this.context = context;
        this.activity = activity;
        this.productOffersList = productOffersList;
        this.servicesOffersList = servicesOffersList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dealsadapter, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dealsadapter_ar, parent, false);

            return new MyViewHolder(itemView);
        }


    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.00");
        final DecimalFormat decimalFormat = new DecimalFormat("0.00");
        final DecimalFormat decimalFormat1 = new DecimalFormat("0");

        if(position < productOffersList.size()) {
            HomePageResponse.Offers storeDetails = productOffersList.get(position);
            if (language.equals("En")){
                holder.iteamname.setText(storeDetails.getNameen());
            }else {
                holder.iteamname.setText(storeDetails.getNamear());
            }

            String discount;

            discount = decimalFormat.format(storeDetails.getDiscountvalue());

            holder.discount.setText("" + discount.replace(".00", "") + "% " + context.getResources().getString(R.string.off));

            holder.mrpprice.setText("" + "SAR " + priceFormat.format(storeDetails.getSellingprice()));
            holder.ourprice.setText("" + "SAR " + priceFormat.format(storeDetails.getPrice()));

            holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if (productOffersList.get(position).isFavorite() ||
                    (CacheData.favProductId != null && CacheData.favProductId.contains(productOffersList.get(position).getProductskuid()))) {
                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
            } else {
                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
            }

            holder.spinner.start();

            holder.spinner.recreateWithParams(
                    context,
                    DialogUtils.getColor(context, R.color.black),
                    120,
                    true
            );

//            if (storeDetails.()) {
//                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
//            } else {
//                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
//            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.STORE_IMAGE_URL + storeDetails.getLogo())
                    .into(holder.logo);

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + storeDetails.getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.store_image);

        }
        else {
            HomePageResponse.Offers2 storeDetails = servicesOffersList.get(position);
            holder.iteamname.setText(storeDetails.getServicenameen());
            String discount;

            discount = decimalFormat.format(storeDetails.getDiscountvalue());

            holder.discount.setText("" + discount.replace(".00", "") + "% " + context.getResources().getString(R.string.off));

            holder.mrpprice.setText("" + "SAR " + priceFormat.format(storeDetails.getSellingprice()));
            holder.ourprice.setText("" + "SAR " + priceFormat.format(storeDetails.getPrice()));

            holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if (servicesOffersList.get(position).isFavorite() ||
                    (CacheData.favProductId != null && CacheData.favProductId.contains(servicesOffersList.get(position).getServiceid()))) {
                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
            } else {
                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
            }

            holder.spinner.start();

            holder.spinner.recreateWithParams(
                    context,
                    DialogUtils.getColor(context, R.color.black),
                    120,
                    true
            );

//            if (storeDetails.()) {
//                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
//            } else {
//                holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
//            }

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

            Glide.with(context)
                    .load(Constants.STORE_IMAGE_URL + storeDetails.getLogo())
                    .into(holder.logo);

            Log.d(TAG, "onBindViewHolder: "+Constants.STORE_IMAGE_URL + storeDetails.getLogo());
            Log.d(TAG, "getVisibility: "+holder.logo.getVisibility());

            Glide.with(context)
                    .load(Constants.PRODUCTS_IMAGE_URL + storeDetails.getImage())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.spinner.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.store_image);
        }


        holder.favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject parentObj = new JSONObject();
                if(position < productOffersList.size()) {
                    try {
                        if(productOffersList.get(position).isFavorite()) {
                            parentObj.put("Type", 3);
                        }
                        else {
                            parentObj.put("Type", 2);
                        }
                        parentObj.put("StatusId",  "1");
                        parentObj.put("Ids", productOffersList.get(position).getProductid());
                        parentObj.put("UserId",userPrefs.getString("userId", ""));
                        new InsertStoreFav().execute(parentObj.toString(), "1", ""+position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    try {
                        int pos = position - productOffersList.size();
                        if(servicesOffersList.get(position).isFavorite()) {
                            parentObj.put("Type", 3);
                        }
                        else {
                            parentObj.put("Type", 2);
                        }
                        parentObj.put("StatusId",  "1");
                        parentObj.put("Ids", servicesOffersList.get(pos).getServiceid());
                        parentObj.put("UserId",userPrefs.getString("userId", ""));
                        new InsertStoreFav().execute(parentObj.toString(), "2", ""+pos);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (productOffersList.size() + servicesOffersList.size());
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView discount, iteamname, mrpprice, ourprice;
        ImageView store_image, favIcon, logo;
        RelativeLayout layout;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);

            favIcon = (ImageView) itemView.findViewById(R.id.fav_icon);
            discount = (TextView) itemView.findViewById(R.id.offer);
            iteamname = (TextView) itemView.findViewById(R.id.iteamname);
            mrpprice = (TextView) itemView.findViewById(R.id.mrp);
            ourprice = (TextView) itemView.findViewById(R.id.ourtprice);
            store_image = (ImageView) itemView.findViewById(R.id.iteamimage);
            logo = (ImageView) itemView.findViewById(R.id.logo);
            layout=(RelativeLayout)itemView.findViewById(R.id.layout) ;
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);

            logo.setVisibility(View.VISIBLE);

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getAdapterPosition() < productOffersList.size()) {
                        inputStr = prepareProductVariantJSON(getAdapterPosition());
                        new GetProductVariantsApi().execute("" + getAdapterPosition());
                    }
                    else {
                        inputStr = prepareGetStoresJSON(getAdapterPosition());
                        new GetServiceVariantsApi().execute("" + getAdapterPosition());
                    }
                }
            });
        }
    }

    private class GetProductVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductVariantResponse> call = apiService.getProductVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductVariantResponse>() {
                @Override
                public void onResponse(Call<ProductVariantResponse> call, Response<ProductVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getProductList();
                            variantsLists = response.body().getData().getVariantsList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ProductVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("skuid", productOffersList.get(Integer.parseInt(strings[0])).getProductskuid());
                            intent.putExtra("storeid", productOffersList.get(Integer.parseInt(strings[0])).getStoreid());
                            intent.putExtra("branchId", productOffersList.get(Integer.parseInt(strings[0])).getBranchid());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareProductVariantJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductInventoryId",  productOffersList.get(position).getInventoryid());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetServiceVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            serviceProductLists = response.body().getData().getServiceDetails();
                            serviceVariantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", serviceProductLists);
                            intent.putExtra("variantList", serviceVariantsLists);
                            intent.putExtra("storeId", servicesOffersList.get(Integer.parseInt(strings[0])).getStoreid());
                            intent.putExtra("skuid", servicesOffersList.get(Integer.parseInt(strings[0])).getServiceid());
                            intent.putExtra("branchId", servicesOffersList.get(Integer.parseInt(strings[0])).getBranchid());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  servicesOffersList.get(position).getServiceid());
            parentObj.put("SubCategoryId",  servicesOffersList.get(position).getSubcategoryid());
            parentObj.put("BranchId",  servicesOffersList.get(position).getBranchid());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog((Activity)context);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), strings[0]));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if (strings[1].equals("1")) {
                                if (productOffersList.get(Integer.parseInt(strings[2])).isFavorite()) {
                                    productOffersList.get(Integer.parseInt(strings[2])).setFavorite(false);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid())) {
                                        CacheData.favProductId.remove(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid());
                                    }
                                } else {
                                    productOffersList.get(Integer.parseInt(strings[2])).setFavorite(true);
                                    CacheData.favProductId.add(productOffersList.get(Integer.parseInt(strings[2])).getProductskuid());
                                }
                            }
                            else {
                                if (servicesOffersList.get(Integer.parseInt(strings[2])).isFavorite()) {
                                    servicesOffersList.get(Integer.parseInt(strings[2])).setFavorite(false);
                                    if (CacheData.favProductId != null && CacheData.favProductId.contains(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid())) {
                                        CacheData.favProductId.remove(servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid());
                                    }
                                } else {
                                    servicesOffersList.get(Integer.parseInt(strings[2])).setFavorite(true);
                                    CacheData.favProductId.add(""+servicesOffersList.get(Integer.parseInt(strings[2])).getServiceid());
                                }
                            }
                            notifyDataSetChanged();
                            Constants.showOneButtonAlertDialog(stores.getMessage(), context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), (Activity)context);
                        }
                    }
                    else {
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}

