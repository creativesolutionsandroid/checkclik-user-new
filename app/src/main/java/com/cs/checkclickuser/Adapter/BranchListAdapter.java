package com.cs.checkclickuser.Adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class BranchListAdapter extends RecyclerView.Adapter<BranchListAdapter.MyViewHolder> {
    public Context context;
    public LayoutInflater inflater;
    int QTY = 0;
    String TAG = "TAG";
    private ArrayList<String> cartArryLists;
    PopupWindow popupWindow;

    public BranchListAdapter(Context context, ArrayList<String> cartArryLists, PopupWindow popupWindow) {
        this.context = context;
        this.cartArryLists = cartArryLists;
        this.popupWindow = popupWindow;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        itemView = LayoutInflater.from(parent.getContext())

                .inflate(R.layout.branch_list_child, parent, false);
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.branch_name.setText("" + cartArryLists.get(position));

        if (cartArryLists.size() == (position + 1)){

            holder.view.setVisibility(View.GONE);

        } else {

            holder.view.setVisibility(View.VISIBLE);

        }

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "Arrylistsize" + cartArryLists.size());
        return cartArryLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView branch_name;
        RelativeLayout branch_layout;
        View view;
//        store_type
//                store_logo;
//        LinearLayout rating_layout;

        public MyViewHolder(View itemView) {
            super(itemView);
            branch_name = (TextView) itemView.findViewById(R.id.branch_name);
            branch_layout = (RelativeLayout) itemView.findViewById(R.id.branch_layout);
            view = (View) itemView.findViewById(R.id.view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ProductStoresActivityStep1.hidden_layout.setVisibility(View.GONE);
                    ProductStoresActivityStep1.branch_pos = getAdapterPosition();

                    if (context instanceof ProductStoresActivityStep1) {
                        ((ProductStoresActivityStep1)context).initView();
                    }

                    popupWindow.dismiss();

                }
            });
        }
    }

}
