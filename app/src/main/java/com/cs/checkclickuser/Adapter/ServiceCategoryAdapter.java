package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Fragements.ProductFragment;
import com.cs.checkclickuser.Fragements.ServiceFragment;
import com.cs.checkclickuser.Models.ProductCategoryResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class ServiceCategoryAdapter extends  RecyclerView.Adapter<ServiceCategoryAdapter.MyViewHolder>  {

    private Context context;
    private int selectedPosition = 0;
    private List<ProductCategoryResponce.MainCategory> catArrayList = new ArrayList<>();
    private AppCompatActivity activity;
    int pos = 0;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;

    SharedPreferences LanguagePrefs;
    String language;

    public ServiceCategoryAdapter(Context context, List<ProductCategoryResponce.MainCategory> catArrayList) {
        this.context = context;
        this.activity = activity;
        this.pos = pos;
        this.catArrayList = catArrayList;

        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_chaild, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.category_chaild_ar, parent, false);
            return new MyViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");
        if (language.equalsIgnoreCase("En")){
            holder.catname.setText(catArrayList.get(position).getNameEn());
        }else {
            holder.catname.setText(catArrayList.get(position).getNameAr());
        }

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Flaticon.ttf");
        String fontawesome = catArrayList.get(position).getUniCode().replace("\\" ,"\\u");
//        String fontawesome = catArrayList.get(position).getUniCode();

        holder.storeimage.setTypeface(font);

        holder.storeimage.setText(new String(Character.toChars(Integer.parseInt(fontawesome.substring(2), 16))));

        String colorCode = catArrayList.get(position).getColorCode();

        holder.storeimage.setTextColor(Color.parseColor(colorCode));

        colorCode = colorCode.replace("#", "");
        int startColor = Color.parseColor("#FF"+colorCode);
        int endtColor = Color.parseColor("#00"+colorCode);

        if (language.equalsIgnoreCase("En")){
            GradientDrawable gd = new GradientDrawable(
                    GradientDrawable.Orientation.LEFT_RIGHT,
                    new int[] {startColor,endtColor});
            gd.setCornerRadius(0f);

            holder.backgroundGradientView.setBackground(gd);

        }else {
            GradientDrawable gd = new GradientDrawable(
                    GradientDrawable.Orientation.RIGHT_LEFT,
                    new int[] {startColor,endtColor});
            gd.setCornerRadius(0f);

            holder.backgroundGradientView.setBackground(gd);

        }
            }

    @Override
    public int getItemCount() {
        return catArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView catname;
        RelativeLayout catclick;
        TextView storeimage;
        View backgroundGradientView;

//        ImageView storeimage;
        public MyViewHolder(View itemView) {
            super(itemView);
            catname = (TextView) itemView.findViewById(R.id.catname);
            catclick = (RelativeLayout)itemView.findViewById(R.id.catclick);
            storeimage = (TextView) itemView.findViewById(R.id.caticon);
            backgroundGradientView = (View) itemView.findViewById(R.id.background);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constants.serviceCategoryid = catArrayList.get(getAdapterPosition()).getId();
                    Constants.serviceCategoryname = ""+(catArrayList.get(getAdapterPosition()).getNameEn());
                    CacheData.serviceStoresArrayList = null;

                    FragmentManager fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    Fragment categoryFragment = new ServiceFragment();
                    ft.replace(R.id.fragment_layout, categoryFragment);
                    ft.addToBackStack(categoryFragment.getClass().getName());
                    ft.commit();
                }
            });
        }
    }
}
