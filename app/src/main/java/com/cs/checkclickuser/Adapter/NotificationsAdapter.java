package com.cs.checkclickuser.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.checkclickuser.Models.Notifications;
import com.cs.checkclickuser.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NotificationsAdapter extends RecyclerView.Adapter< NotificationsAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    private List<Notifications.Data> notificationsArrayList = new ArrayList<>();

    SharedPreferences LanguagePrefs;
    String language;


    public NotificationsAdapter(Context context, List<Notifications.Data> notificationsArrayList) {
        this.context = context;
        this.notificationsArrayList = notificationsArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.notification_child, parent, false);
            return new MyViewHolder(itemView);

        } else {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.notification_child_ar, parent, false);
            return new MyViewHolder(itemView);

        }

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (language.equals("En")){
            holder.summary.setText(notificationsArrayList.get(position).getMessageEn());
        }else {
            holder.summary.setText(notificationsArrayList.get(position).getMessageAr());
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy hh:mm aa",Locale.US); ;
        String dateStr = "";
        try {
            Date datetime = format.parse(notificationsArrayList.get(position).getSentDate());
            dateStr = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.date.setText(""+dateStr);
    }

    @Override
    public int getItemCount() {
        return notificationsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView summary, date, ourprice, discount;
        ImageView storeimage;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            summary = (TextView) itemView.findViewById(R.id.n_summerytext);
            date = (TextView) itemView.findViewById(R.id.date);

//            storelayout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
        }
    }
}
