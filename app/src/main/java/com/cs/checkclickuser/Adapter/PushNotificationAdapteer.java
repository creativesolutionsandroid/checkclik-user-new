package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


import com.cs.checkclickuser.Activites.PushNotificationActivity;
import com.cs.checkclickuser.Models.PushNotificationResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class PushNotificationAdapteer extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    String TAG ="TAG";
    SharedPreferences LanguagePrefs;
    String language;


    public ArrayList<PushNotificationResponce.PNConfig> pushadapterarry = new ArrayList<>();

    public PushNotificationAdapteer(Context context, ArrayList<PushNotificationResponce.PNConfig> pushList) {
        this.context = context;
        this.pushadapterarry = pushList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        Log.d(TAG, "pushcount: "+ pushadapterarry.size());
        return pushadapterarry.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView category;
        Switch catswitch;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            convertView = inflater.inflate(R.layout.pushnotification_adapter, null);
        } else {

            convertView = inflater.inflate(R.layout.pushnotification_adapter_ar, null);
        }




        holder.category=(TextView)convertView.findViewById(R.id.cat);
        holder.catswitch=(Switch) convertView.findViewById(R.id.catswitch);
        holder.category.setText(pushadapterarry.get(position).getName());

        if (pushadapterarry.get(position).getStatus()){
            holder.catswitch.setChecked(true);
        }else {
            holder.catswitch.setChecked(false);
        }

      holder.catswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
              PushNotificationActivity.pushnotificationarry.get(position).setStatus(isChecked);
          }
      });

        return convertView;
    }

}
