package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickuser.Models.FilterSubResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class FillterSubAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String selectedValue;
    private ArrayList<FilterSubResponce> subCategoriesArrayList = new ArrayList<>();

    SharedPreferences LanguagePrefs;
    String language;

    public FillterSubAdapter(Context context, ArrayList<FilterSubResponce> orderList, String selectedValue) {
        this.context = context;
        this.subCategoriesArrayList = orderList;
        this.selectedValue = selectedValue;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return subCategoriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView radioButton;
        TextView subCatName;
    }
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = LanguagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.list_sub_cat_filter, null);
            } else {
                convertView = inflater.inflate(R.layout.list_sub_cat_filter_ar, null);
            }

            holder.radioButton = (ImageView) convertView.findViewById(R.id.rb_subcat);
            holder.subCatName = (TextView) convertView.findViewById(R.id.sub_cat_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.subCatName.setText(subCategoriesArrayList.get(position).getName());

        if(selectedValue.equals(subCategoriesArrayList.get(position).getValue())) {
            holder.radioButton.setImageDrawable(context.getResources().getDrawable(R.drawable.radio_button_selected));
        }
        else {
            holder.radioButton.setImageDrawable(context.getResources().getDrawable(R.drawable.radio_button_unselected));
        }

        return convertView;
    }
}
