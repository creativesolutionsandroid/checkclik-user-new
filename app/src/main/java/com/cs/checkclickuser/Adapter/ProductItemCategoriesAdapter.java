package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Activites.ProductItemListActivityStep4;
import com.cs.checkclickuser.Activites.ServiceItemListActivityStep4;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class ProductItemCategoriesAdapter extends RecyclerView.Adapter< ProductItemCategoriesAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    private ProductstoreResponce.Data storeArrayList;
    int storePos, storeId;
    private AppCompatActivity activity;
    private int type;

    SharedPreferences LanguagePrefs;
    String language;
    private ArrayList<ProductstoreResponce.SubCategory> subCategoryArrayList = new ArrayList<>();

    public ProductItemCategoriesAdapter(Context context, ArrayList<ProductstoreResponce.SubCategory> subCategoryArrayList,
                                        ProductstoreResponce.Data storesArrayList, int storePos, int storeId, int type){
        this.context = context;
        this.activity = activity;
        this.storePos = storePos;
        this.storeId = storeId;
        this.type = type;
        this.storeArrayList = storesArrayList;
        this.subCategoryArrayList = subCategoryArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equals("En")){

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_iteam_list, parent, false);
            return new MyViewHolder(itemView);
        }else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_iteam_list_ar, parent, false);
            return new MyViewHolder(itemView);
        }

          }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (language.equals("En")){
            holder.store_name.setText(subCategoryArrayList.get(position).getNameEn());
        }else {
            holder.store_name.setText(subCategoryArrayList.get(position).getNameAr());
        }

        holder.products.setText(subCategoryArrayList.get(position).getProductCount() + " " + context.getResources().getString(R.string.products));
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL +subCategoryArrayList.get(position).getImage())
                .into(holder.store_image);
    }

    @Override
    public int getItemCount() {
        return subCategoryArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name,products;
        ImageView store_image;
        RelativeLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            store_name = (TextView) itemView.findViewById(R.id.storename);
            products=(TextView)itemView.findViewById(R.id.products);
            store_image = (ImageView) itemView.findViewById(R.id.storeimage);
            storelayout= (RelativeLayout)itemView.findViewById(R.id.store_layout);

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(type == 1) {
                        Intent intent = new Intent(context, ProductItemListActivityStep4.class);
                        intent.putExtra("stores", storeArrayList);
                        intent.putExtra("name", subCategoryArrayList.get(getAdapterPosition()).getNameEn());
                        intent.putExtra("id", subCategoryArrayList.get(getAdapterPosition()).getId());
                        intent.putExtra("storePos", storePos);
                        intent.putExtra("storeId", storeId);
                        intent.putExtra("type", type);
                        context.startActivity(intent);
                    }
                    else if(type == 2) {
                        Intent intent = new Intent(context, ServiceItemListActivityStep4.class);
                        intent.putExtra("stores", storeArrayList);
                        intent.putExtra("name", subCategoryArrayList.get(getAdapterPosition()).getNameEn());
                        intent.putExtra("id", subCategoryArrayList.get(getAdapterPosition()).getId());
                        intent.putExtra("storePos", storePos);
                        intent.putExtra("storeId", storeId);
                        intent.putExtra("type", type);
                        context.startActivity(intent);
                    }
                }
            });
        }
    }
}