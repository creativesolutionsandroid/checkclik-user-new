package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ServiceVariantsActivityStep5;
import com.cs.checkclickuser.Models.ServiceDealsResponse;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.wdullaer.materialdatetimepicker.time.RadialSelectorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServcieDealsAdapter extends RecyclerView.Adapter< ServcieDealsAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    //    private int selectedPosition = 0;

    private ArrayList<ServiceDealsResponse.Deals> storeArrayList = new ArrayList<>();
    int storeId;
    private AppCompatActivity activity;
    SharedPreferences userPrefs;
    private ArrayList<ServiceVariantResponse.ServiceDetails> productLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> variantsLists = new ArrayList<>();
    String inputStr;
    int type;

    SharedPreferences LanguagePrefs;
    String language;

    public ServcieDealsAdapter(Context context, ArrayList<ServiceDealsResponse.Deals> storesArrayList, int storeId, int type){
        this.context = context;
        this.activity = activity;
        this.storeId = storeId;
        this.type = type;
        this.storeArrayList = storesArrayList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dealsadapter, parent, false);


        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        return new MyViewHolder(itemView);
    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.00");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        ServiceDealsResponse.Deals storeDetails = storeArrayList.get(position);
        if (language.equals("En")){

            holder.iteamname.setText(storeDetails.getServiceNameEn());
        }
        else {

            holder.iteamname.setText(storeDetails.getServiceNameAr());
        }
        holder.discount.setText(""+storeDetails.getDiscountValue() + "% " + context.getResources().getString(R.string.off));
        holder.mrpprice.setText("" + "SAR  " + priceFormat.format(storeDetails.getSellingPrice()));
        holder.ourprice.setText("" + "SAR  " + priceFormat.format(storeDetails.getPrice()));

        holder.mrpprice.setPaintFlags(holder.mrpprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if(storeArrayList.get(position).getFavorite()) {
            holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.fav_selected));
        }
        else {
            holder.favIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.favwhite));
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL +storeArrayList.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                }).into(holder.store_image);
        Log.d(TAG, "onBindViewHolder "+Constants.STORE_IMAGE_URL +storeArrayList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView discount,iteamname,mrpprice,ourprice;
        ImageView store_image, favIcon;
        RelativeLayout storelayout;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);

            favIcon = (ImageView) itemView.findViewById(R.id.fav_icon);
            discount = (TextView) itemView.findViewById(R.id.offer);
            iteamname = (TextView) itemView.findViewById(R.id.iteamname);
            mrpprice = (TextView) itemView.findViewById(R.id.mrp);
            ourprice = (TextView) itemView.findViewById(R.id.ourtprice);
            store_image = (ImageView) itemView.findViewById(R.id.iteamimage);
//            storelayout=(RelativeLayout)itemView.findViewById(R.id.main_layout) ;
            spinner=(CamomileSpinner) itemView.findViewById(R.id.spinner) ;

            favIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new InsertStoreFav().execute(""+getAdapterPosition());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    inputStr = prepareGetStoresJSON(getAdapterPosition());
                    new GetVariantsApi().execute("" + getAdapterPosition());
                }
            });
        }
    }

    private class GetVariantsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(((Activity) context));
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceVariantResponse> call = apiService.getServiceVariants(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceVariantResponse>() {
                @Override
                public void onResponse(Call<ServiceVariantResponse> call, Response<ServiceVariantResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            productLists = response.body().getData().getServiceDetails();
                            variantsLists = response.body().getData().getServiceTypeList();
                            Constants.closeLoadingDialog();

                            Intent intent = new Intent(context, ServiceVariantsActivityStep5.class);
                            intent.putExtra("productList", productLists);
                            intent.putExtra("variantList", variantsLists);
                            intent.putExtra("storeId", storeId);
                            intent.putExtra("skuid", storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                            intent.putExtra("branchId", storeArrayList.get(Integer.parseInt(strings[0])).getBranchId());
                            context.startActivity(intent);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), ((Activity) context));
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServiceVariantResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  storeArrayList.get(position).getServiceId());
            parentObj.put("SubCategoryId",  storeArrayList.get(position).getSubCategoryId());
            parentObj.put("BranchId",  storeArrayList.get(position).getBranchId());
            parentObj.put("UserId",  userPrefs.getString("userId",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog((Activity)context);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                if(storeArrayList.get(Integer.parseInt(strings[0])).getFavorite()) {
                    parentObj.put("Type", 3);
                }
                else {
                    parentObj.put("Type", 2);
                }
                parentObj.put("StatusId",  "1");
                parentObj.put("Ids", storeArrayList.get(Integer.parseInt(strings[0])).getServiceId());
                parentObj.put("UserId",userPrefs.getString("userId", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if(storeArrayList.get(Integer.parseInt(strings[0])).getFavorite()) {
                                storeArrayList.get(Integer.parseInt(strings[0])).setFavorite(false);
                            }
                            else {
                                storeArrayList.get(Integer.parseInt(strings[0])).setFavorite(true);
                            }
                            notifyDataSetChanged();
                            Constants.showOneButtonAlertDialog(stores.getMessage(), context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), (Activity)context);
                        }
                    }
                    else {
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}

