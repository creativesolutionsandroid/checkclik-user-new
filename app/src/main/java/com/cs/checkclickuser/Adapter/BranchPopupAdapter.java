package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cs.checkclickuser.Activites.FillterActivity;
import com.cs.checkclickuser.Models.ProductStoreReviews;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class BranchPopupAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    private ArrayList<ProductlistResponce.JOtherBranch> mainCategoriesArrayList = new ArrayList<>();


    public BranchPopupAdapter(Context context, ArrayList<ProductlistResponce.JOtherBranch> orderList) {
        this.context = context;
        this.mainCategoriesArrayList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mainCategoriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView branchname;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.branchpopup_adapter, null);
            holder.branchname = (TextView) convertView.findViewById(R.id.branchname);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.branchname.setText(mainCategoriesArrayList.get(position).getBranchEn());

        return convertView;
    }

}