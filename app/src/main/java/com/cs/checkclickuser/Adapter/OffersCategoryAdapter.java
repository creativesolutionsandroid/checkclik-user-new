package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.OfferProductsActivity;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

public class OffersCategoryAdapter extends RecyclerView.Adapter<OffersCategoryAdapter.MyViewHolder> {
    private Context context;
    public static final String TAG = "TAG";
    public LayoutInflater inflater;
    private ArrayList<HomePageResponse.Subcategorylist> subcategorylist = new ArrayList<>();
    int pos = 0;
    ArrayList<HomePageResponse.Offers> offers = new ArrayList<>();
    ArrayList<HomePageResponse.Offers2> offers2 = new ArrayList<>();


    SharedPreferences LanguagePrefs;
    String language;

    public OffersCategoryAdapter(Context context, ArrayList<HomePageResponse.Subcategorylist> subcategorylist,
                                 ArrayList<HomePageResponse.Offers> offers,
                                 ArrayList<HomePageResponse.Offers2> offers2) {
        this.context = context;
        this.offers = offers;
        this.offers2 = offers2;
        this.subcategorylist = subcategorylist;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_offers_view_all, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_offers_view_all_ar, parent, false);
            return new MyViewHolder(itemView);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (language.equals("En")){
            holder.offerName.setText(subcategorylist.get(position).getNameEn());
        }else {
            holder.offerName.setText(subcategorylist.get(position).getNameAr());
        }

        String fontawesome = subcategorylist.get(position).getUniCode().replace("\\" ,"\\u");
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Flaticon.ttf");

        holder.offerImage.setTypeface(font);

        holder.offerImage.setText(new String(Character.toChars(Integer.parseInt(fontawesome.substring(2), 16))));
        holder.offerImage.setTextColor(Color.parseColor(subcategorylist.get(position).getColorCode()));
    }

    @Override
    public int getItemCount() {
        return subcategorylist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView offerName;
        TextView offerImage;
        LinearLayout list_item;

        public MyViewHolder(View itemView) {
            super(itemView);
            offerName = (TextView) itemView.findViewById(R.id.offer_name);
            offerImage = (TextView) itemView.findViewById(R.id.offer_image);
            list_item= (LinearLayout) itemView.findViewById(R.id.layout);

            list_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OfferProductsActivity.class);
                    intent.putExtra("id", subcategorylist.get(getAdapterPosition()).getId());
                    if (language.equalsIgnoreCase("En")) {
                        intent.putExtra("name", subcategorylist.get(getAdapterPosition()).getNameEn());
                    }
                    else {
                        intent.putExtra("name", subcategorylist.get(getAdapterPosition()).getNameAr());
                    }
                    intent.putExtra("offers", offers);
                    intent.putExtra("offers2", offers2);
                    context.startActivity(intent);
                }
            });
        }
    }
}
