package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.checkclickuser.Fragements.ProductCategoryFragment.TAG;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

/**
 * Created by PULi on 18-4-2019.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchList = new ArrayList<>();
    int type;
    String language;

    public ProductAdapter(Context context, ArrayList<ProductlistResponce.Stores> productArrayList, int type, ArrayList<ProductlistResponce.JOtherBranch> branchList, String language) {
        this.context = context;
        this.type = type;
        this.productArrayList = productArrayList;
        this.branchList = branchList;
        this.language = language;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (language.equalsIgnoreCase("En")) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_list, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_list_arabic, parent, false);
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        DecimalFormat decimalFormat1 = new DecimalFormat("0");

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        final DecimalFormat priceFormat = new DecimalFormat("##,##,##0.0");
        if (language.equalsIgnoreCase("En")) {
            holder.mainstore.setText(productArrayList.get(position).getStoreEn());
            holder.distance.setText(priceFormat.format(productArrayList.get(position).getDistance()) + " " + context.getResources().getString(R.string.km));
        } else {
            holder.mainstore.setText(productArrayList.get(position).getStoreAr());
            holder.distance.setText(context.getResources().getString(R.string.km_ar) + " " + priceFormat.format(productArrayList.get(position).getDistance()));
        }
        holder.storerating.setText(decimalFormat.format(productArrayList.get(position).getReviews()));
//        holder.reviews.setText("("+productArrayList.get(pos).getReviews()+" Reviews)");
//        holder.deliverytime.setText(productArrayList.get(position).getDeliveryTime());
        holder.minoder.setText("SAR " + Constants.priceFormat2.format(productArrayList.get(position).getMinimumOrderValue()));

        if (productArrayList.get(position).getIsDelivery()) {
            if (productArrayList.get(position).getIsDeliveryCompany() == 0) {
                if (language.equals("En")){
                    holder.deliverytime.setText(context.getResources().getString(R.string.unavailable));
                } else {
                    holder.deliverytime.setText(context.getResources().getString(R.string.unavailable_ar));
                }
            }
            else {
                holder.deliverytime.setText(productArrayList.get(position).getDeliveryTime());
            }

        } else if (!productArrayList.get(position).getIsDelivery()) {
            if (language.equals("En")){
                holder.deliverytime.setText(context.getResources().getString(R.string.unavailable));
            }else {
                holder.deliverytime.setText(context.getResources().getString(R.string.unavailable_ar));
            }
        }

        holder.storestatus.setText(productArrayList.get(position).getBranchStatus());
        if (productArrayList.get(position).getBranchStatus().equalsIgnoreCase("open")) {
            holder.storestatus.setTextColor(Color.parseColor("#7CD322"));
            holder.statusimage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_green2x));
        } else {
            holder.storestatus.setTextColor(Color.parseColor("#E74C3C"));
            holder.statusimage.setImageDrawable(context.getResources().getDrawable(R.drawable.status_red));
        }

        if (productArrayList.get(position).getVendorType() == 1) {
            holder.gropicon.setImageDrawable(context.getResources().getDrawable(R.drawable.groupusersicon2x));
        } else {
            holder.gropicon.setImageDrawable(context.getResources().getDrawable(R.drawable.singleicon2x));
        }

        if (productArrayList.get(position).getReviewsCount() == 0) {
            if (language.equalsIgnoreCase("En")) {
                holder.reviews.setText("(" + context.getResources().getString(R.string.no_reviews) + ")");
            } else {
                holder.reviews.setText("(" + context.getResources().getString(R.string.no_reviews_ar) + ")");
            }
        } else if (productArrayList.get(position).getReviewsCount() == 1) {
            if (language.equalsIgnoreCase("En")) {
                holder.reviews.setText("(" + decimalFormat1.format(productArrayList.get(position).getReviewsCount()) + " " + context.getResources().getString(R.string.review) + ")");
            } else {
                holder.reviews.setText("(" + context.getResources().getString(R.string.review_ar) + " " +  decimalFormat1.format(productArrayList.get(position).getReviewsCount()) + ")");
            }
        } else {
            if (language.equalsIgnoreCase("En")) {
                holder.reviews.setText("(" + decimalFormat1.format(productArrayList.get(position).getReviewsCount()) + " " + context.getResources().getString(R.string.reviews) + ")");
            } else {
                holder.reviews.setText("(" + context.getResources().getString(R.string.reviews_ar) + " " + decimalFormat1.format(productArrayList.get(position).getReviewsCount()) + ")");
            }
        }

        if (productArrayList.get(position).getIsCashAllowed().equals("true") && (productArrayList.get(position).getIsCreditCardAllowed().equals("true"))) {
            holder.paymentcard.setVisibility(View.VISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        } else if (productArrayList.get(position).getIsCreditCardAllowed().equals("false")) {
            holder.paymentcard.setVisibility(View.INVISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        } else if (productArrayList.get(position).getIsCashAllowed().equals("false")) {
            holder.paymentcash.setVisibility(View.INVISIBLE);
            holder.paymentcard.setVisibility(View.VISIBLE);
        }

        Glide.with(context)
                .load(STORE_IMAGE_URL + productArrayList.get(position).getLogo())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.storeimage);

    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: " + productArrayList.size());
        return productArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, storerating, reviews, storestatus, distance, minoder, deliverytime;
        ImageView storeimage, paymentcard, paymentcash, statusimage, gropicon;
        LinearLayout storelayout;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.mainstore);
            storerating = (TextView) itemView.findViewById(R.id.revies_count);
            distance = (TextView) itemView.findViewById(R.id.distance);
            reviews = (TextView) itemView.findViewById(R.id.revies);
            storestatus = (TextView) itemView.findViewById(R.id.storestatus);
            minoder = (TextView) itemView.findViewById(R.id.minmodr);
            deliverytime = (TextView) itemView.findViewById(R.id.delivery_time);
            storeimage = (ImageView) itemView.findViewById(R.id.storeimage);
            storelayout = (LinearLayout) itemView.findViewById(R.id.storelayout);
            paymentcard = (ImageView) itemView.findViewById(R.id.payment_card);
            paymentcash = (ImageView) itemView.findViewById(R.id.payment_cash);
            statusimage = (ImageView) itemView.findViewById(R.id.storestatusimage);
            gropicon = (ImageView) itemView.findViewById(R.id.gropicon);
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);

            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                    intent.putExtra("stores", productArrayList);
                    intent.putExtra("pos", getAdapterPosition());
                    intent.putExtra("type", type);
                    if (type == 1) {
                        intent.putExtra("class", "product");
                    } else if (type == 2) {
                        intent.putExtra("class", "service");
                    }
                    context.startActivity(intent);
                }
            });
        }
    }
}
