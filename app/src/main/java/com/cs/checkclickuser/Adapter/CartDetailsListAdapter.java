package com.cs.checkclickuser.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.CartActivity;
import com.cs.checkclickuser.Activites.OrderRetunActivity;
import com.cs.checkclickuser.Listeners.UpdateCartListener;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.Models.UpdateCartResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;

public class CartDetailsListAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    int QTY = 0;
    String TAG = "TAG";
    SharedPreferences userPrefs;
    String inputStr;
    private UpdateCartListener updateCartListener;
    private List<CartResponce.CartList> cartArryLists;

    SharedPreferences LanguagePrefs;
    String language;

    public CartDetailsListAdapter(Context context, List<CartResponce.CartList> cartArryLists, UpdateCartListener listener) {
        this.context = context;
        this.updateCartListener = listener;
        this.cartArryLists = cartArryLists;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return cartArryLists.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
            language = LanguagePrefs.getString("language", "En");

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.cart_adapter_list, null);
            } else {
                convertView = inflater.inflate(R.layout.cart_adapter_list_ar, null);
            }



            holder.storename = (TextView) convertView.findViewById(R.id.storename);
            holder.iteamcount = (TextView) convertView.findViewById(R.id.iteamcount);
            holder.qty = (TextView) convertView.findViewById(R.id.products);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.qtycount = (TextView) convertView.findViewById(R.id.qtycount);
            holder.retunbtn = (TextView) convertView.findViewById(R.id.retun);
            holder.storeimage = (ImageView) convertView.findViewById(R.id.storeimage);
            holder.decrease = (ImageView) convertView.findViewById(R.id.decrease);
            holder.increase = (ImageView) convertView.findViewById(R.id.increase);
            holder.cartAddLayout = (RelativeLayout) convertView.findViewById(R.id.qty_layout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        int icgridViewIconId = context.getResources().getIdentifier("plus_" + Constants.appColor, "drawable", context.getPackageName());
        holder.increase.setImageDrawable(context.getResources().getDrawable(icgridViewIconId));

        int iclistViewIconId = context.getResources().getIdentifier("minus_" + Constants.appColor, "drawable", context.getPackageName());
        holder.decrease.setImageDrawable(context.getResources().getDrawable(iclistViewIconId));

        holder.price.setText("SAR " + Constants.priceFormat1.format(cartArryLists.get(position).getPrice()));
        holder.storename.setText(cartArryLists.get(position).getProductNameEn());
        holder.qtycount.setText("" + cartArryLists.get(position).getCartQuantity());

        if (cartArryLists.get(position).getIsActive()) {
            if (language.contains("En")){
                holder.iteamcount.setText("" + cartArryLists.get(position).getCartQuantity() + " " + context.getResources().getString(R.string.pics));
            }else {
                holder.iteamcount.setText("" + cartArryLists.get(position).getCartQuantity() + " " + context.getResources().getString(R.string.pics_ar));
            }

            holder.iteamcount.setTextColor(context.getResources().getColor(R.color.black));
            if (cartArryLists.get(position).getServiceType() == 2) {
                holder.cartAddLayout.setVisibility(View.GONE);
            } else {
                holder.cartAddLayout.setVisibility(View.VISIBLE);
            }
        } else {
            if (language.equals("En")){
                holder.iteamcount.setText("" + context.getResources().getString(R.string.not_available));
            }else {
                 holder.iteamcount.setText("" + context.getResources().getString(R.string.not_available_ar));
            }

            holder.iteamcount.setTextColor(context.getResources().getColor(R.color.close_red));
            holder.cartAddLayout.setVisibility(View.GONE);
        }

        holder.decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = cartArryLists.get(position).getCartQuantity();
//                if(qty > cartArryLists.get(position).getMinCartQty()) {
                qty = qty - cartArryLists.get(position).getMinCartQty();
//                qty = qty - 1;
                inputStr = prepareGetStoresJSON(qty, cartArryLists.get(position).getCartId());
                new updateCartApi().execute();
//                }
//                else {
//                    Toast.makeText(context, "Minimum quantity required: "+cartArryLists.get(position).getMinCartQty(), Toast.LENGTH_SHORT).show();
//                }
            }
        });

        holder.increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = cartArryLists.get(position).getCartQuantity();
                qty = qty + cartArryLists.get(position).getMinCartQty();
//                qty = qty + 1;
                if (qty < cartArryLists.get(position).getAvailableQuantity() &&
                        qty < cartArryLists.get(position).getMaxCartQty()) {
                    inputStr = prepareGetStoresJSON(qty, cartArryLists.get(position).getCartId());
                    new updateCartApi().execute();
                } else {
                    if (language.equals("En")){
                        Toast.makeText(context, context.getResources().getString(R.string.cant_increase_the_qunatity), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(context, context.getResources().getString(R.string.cant_increase_the_qunatity_ar), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        Glide.with(context)
                .load(PRODUCTS_IMAGE_URL + cartArryLists.get(position).getImage())
                .into(holder.storeimage);
        return convertView;
    }

    private String prepareGetStoresJSON(int qty, int cartId) {
        JSONObject parentObj = new JSONObject();
        try {
            if (qty > 0) {
                parentObj.put("FlagId", 1);
                parentObj.put("StockQuantity", qty);
            } else {
                parentObj.put("FlagId", 2);
            }
            parentObj.put("CartId", cartId);
            parentObj.put("UserId", userPrefs.getString("userId", ""));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    public static class ViewHolder {
        TextView storename, iteamcount, qty, price, retunbtn, qtycount;
        ImageView storeimage, decrease, increase;
        RelativeLayout cartAddLayout;
    }

    private class updateCartApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog((Activity) context);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UpdateCartResponse> call = apiService.updateCart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UpdateCartResponse>() {
                @Override
                public void onResponse(Call<UpdateCartResponse> call, Response<UpdateCartResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            updateCartListener.onCartUpdated();
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (language.equalsIgnoreCase("En")){
                                Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok), (Activity) context);
                            }else {
                                Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                        context.getResources().getString(R.string.ok_ar), (Activity) context);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<UpdateCartResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
}
