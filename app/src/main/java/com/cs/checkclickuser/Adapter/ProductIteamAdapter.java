package com.cs.checkclickuser.Adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.text.DecimalFormat;

public class ProductIteamAdapter extends RecyclerView.Adapter< ProductIteamAdapter.MyViewHolder> {

    private Context context;
    public static final String TAG = "TAG";
    //    private int selectedPosition = 0;
    ProductstoreResponce.Data storeArrayList;
    private AppCompatActivity activity;

    SharedPreferences LanguagePrefs;
    String language;

    public ProductIteamAdapter(Context context,ProductstoreResponce.Data storesArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storesArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_iteam_list, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_iteam_list_ar, parent, false);
            return new MyViewHolder(itemView);
        }


    }

    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        ProductstoreResponce.Data storeDetails = storeArrayList;
        if (language.equals("En")){
            holder.store_name.setText(storeDetails.getSubCategory().get(position).getNameEn());
        }else {
            holder.store_name.setText(storeDetails.getSubCategory().get(position).getNameAr());
        }

        holder.products.setText(""+storeDetails.getSubCategory().get(position).getProductCount());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .load(Constants.STORE_IMAGE_URL+storeArrayList.getSubCategory().get(position).getImage())
                .into(holder.store_image);
    }

    @Override
    public int getItemCount() {
        return storeArrayList.getSubCategory().size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView store_name,products;
        ImageView store_image;
        LinearLayout storelayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            store_name = (TextView) itemView.findViewById(R.id.storename);
            products=(TextView)itemView.findViewById(R.id.products);
            store_image = (ImageView) itemView.findViewById(R.id.storeimage);
            storelayout= (LinearLayout)itemView.findViewById(R.id.layout);



        }
    }
}