package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductRatingActivity;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class HistoryOrderItemsList extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    String TAG = "TAG";
    int branchid, ordertype, orderstatus;
    SharedPreferences userPrefs;
    String inputStr, expectingdate;
    private ArrayList<OrderPendingResponce.Items> itemsList = new ArrayList<>();
    String language;

    public HistoryOrderItemsList(Context context, ArrayList<OrderPendingResponce.Items> itemsList, String expectingdate, int branchid, int ordertype, int orderstatus, String language) {
        this.context = context;
        this.language = language;
        this.itemsList = itemsList;
        this.expectingdate = expectingdate;
        this.branchid = branchid;
        this.ordertype = ordertype;
        this.orderstatus = orderstatus;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return itemsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView imageView;
        TextView productname;
        TextView exprtingdate, ratingtext, return_pending_text;
        LinearLayout layout;
        RelativeLayout rejecatedlayout, acceptedlayout;
        RatingBar ratingbar;
        CamomileSpinner spinner;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            if (language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.history_adapter_list, null);
            } else {
                convertView = inflater.inflate(R.layout.history_adapter_list_arabic, null);
            }

            holder.imageView = convertView.findViewById(R.id.storeimage);
            holder.productname = convertView.findViewById(R.id.storename);
            holder.exprtingdate = convertView.findViewById(R.id.exprtingdate);
            holder.rejecatedlayout = convertView.findViewById(R.id.rejectlayout);
            holder.layout = convertView.findViewById(R.id.layout);
            holder.ratingtext = convertView.findViewById(R.id.productrate);
            holder.return_pending_text = convertView.findViewById(R.id.return_pending_text);
            holder.ratingbar = convertView.findViewById(R.id.ratingbar);
            holder.spinner = (CamomileSpinner) convertView.findViewById(R.id.spinner);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (language.equalsIgnoreCase("En")) {
            holder.productname.setText(itemsList.get(position).getNameEn());
        } else {
            holder.productname.setText(itemsList.get(position).getNameAr());
        }

        if (itemsList.get(position).getRating() == 0 && itemsList.get(position).getIsAccepted()) {
            holder.ratingtext.setVisibility(View.VISIBLE);
            holder.ratingbar.setVisibility(View.GONE);
        } else if (itemsList.get(position).getRating() > 0 && itemsList.get(position).getIsAccepted()
                && !itemsList.get(position).getNotReturnable()) {
            holder.ratingbar.setVisibility(View.VISIBLE);
            holder.ratingtext.setVisibility(View.GONE);
            holder.ratingbar.setRating(itemsList.get(position).getRating());
        }
        else {
            holder.ratingbar.setVisibility(View.GONE);
            holder.ratingtext.setVisibility(View.GONE);
        }

        if (!itemsList.get(position).getNotReturnable() && itemsList.get(position).getIsAccepted()) {
//            if (ordertype == 2) {
            if (itemsList.get(position).getReturnRequestStatus() == 1) {
                holder.return_pending_text.setVisibility(View.VISIBLE);
                holder.ratingbar.setVisibility(View.GONE);
                holder.ratingtext.setVisibility(View.GONE);
            }
            else {
                holder.return_pending_text.setVisibility(View.GONE);
            }
        }
        else if (itemsList.get(position).getNotReturnable() && itemsList.get(position).getIsAccepted()) {
            holder.return_pending_text.setVisibility(View.GONE);
        }
        else {
            holder.return_pending_text.setVisibility(View.GONE);
        }

        if (!itemsList.get(position).getIsAccepted()) {
            holder.rejecatedlayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.rejecatedlayout.setVisibility(View.GONE);
        }

        if (orderstatus == 16 || orderstatus == 3) {
            holder.ratingtext.setVisibility(View.INVISIBLE);
        }

        String date1 = expectingdate;
        Log.d(TAG, "expettingdae: " + expectingdate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        date1.replace(date1, "yyyy-MM-dd");
        try {
            Date datetime = format.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.exprtingdate.setText("" + date1);

        holder.ratingtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductRatingActivity.class);
                intent.putExtra("Order", itemsList);
                intent.putExtra("Orderpos", position);
                intent.putExtra("Type", ordertype);
                intent.putExtra("OrderId", itemsList.get(position).getOrderId());
                intent.putExtra("Ids", itemsList.get(position).getSKUId());
                intent.putExtra("BranchId", branchid);
                context.startActivity(intent);
            }
        });

        holder.spinner.start();
        holder.spinner.recreateWithParams(
                context,
                DialogUtils.getColor(context, R.color.black),
                120,
                true
        );

        Glide.with(context)
                .load(Constants.PRODUCTS_IMAGE_URL + itemsList.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })

                .into(holder.imageView);


        return convertView;
    }
}
