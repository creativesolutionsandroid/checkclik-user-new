package com.cs.checkclickuser.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.R;
import com.gmail.samehadar.iosdialog.CamomileSpinner;

import java.text.DecimalFormat;
import java.util.ArrayList;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;


public class FavStoreAdapter extends RecyclerView.Adapter< FavStoreAdapter.MyViewHolder>{

    private Context context;
    public static final String TAG = "TAG";
    private AppCompatActivity activity;
    private ArrayList<FavStoresResponce.StoreList> storeArrayList = new ArrayList<>();
    int pos = 0;

    SharedPreferences LanguagePrefs;
    String language;

    public FavStoreAdapter(Context context,  ArrayList<FavStoresResponce.StoreList> storeArrayList){
        this.context = context;
        this.activity = activity;
        this.storeArrayList = storeArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LanguagePrefs = context.getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_list, parent, false);
            return new MyViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_list_ar, parent, false);
            return new MyViewHolder(itemView);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder,  int position) {


        final DecimalFormat priceFormat = new DecimalFormat("##,##,###.##");

        holder.storerating.setText(""+storeArrayList.get(position).getReviews());
        holder.distance.setText(priceFormat.format(storeArrayList.get(pos).getDistance()) + " " + context.getResources().getString(R.string.km));
        if (language.equalsIgnoreCase("En")){
            holder.mainstore.setText(storeArrayList.get(position).getStoreEn());
            holder.reviews.setText("("+storeArrayList.get(pos).getReviewsCount() + " " + context.getResources().getString(R.string.reviews) + ")");
        }else {
            holder.mainstore.setText(storeArrayList.get(position).getStoreAr());
            holder.reviews.setText("("+storeArrayList.get(pos).getReviewsCount() + " " + context.getResources().getString(R.string.reviews_ar) + ")");
        }

        holder.deliverytime.setText(storeArrayList.get(position).getDeliveryTime());
        holder.minoder.setText(""+storeArrayList.get(position).getMinimumOrderValue()+" SAR");
        holder.storestatus.setText(storeArrayList.get(position).getBranchStatus());

        if (storeArrayList.get(position).getIsCashAllowed().equals("true")&&(storeArrayList.get(position).getIsCreditCardAllowed().equals("true"))) {
            holder.paymentcard.setVisibility(View.VISIBLE);
            holder.paymentcash.setVisibility(View.VISIBLE);
        }

        else if (storeArrayList.get(position).getIsCreditCardAllowed().equals("false")){
            holder.paymentcard.setVisibility(View.INVISIBLE);

        }
        else if (storeArrayList.get(position).getIsCashAllowed().equals("false")){
            holder.paymentcash.setVisibility(View.INVISIBLE);

        }
        if (storeArrayList.get(position).getBranchStatus().equalsIgnoreCase("open")){
            holder.statusimage.setColorFilter(Color.GREEN);
            holder.storestatus.setTextColor(Color.GREEN);
        }
        else {
            holder.statusimage.setColorFilter(Color.RED);
            holder.storestatus.setTextColor(Color.RED);
        }

        Glide.with(context)
                .load(STORE_IMAGE_URL + storeArrayList.get(position).getLogo())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.spinner.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.storeimage);



    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mainstore, storerating, reviews, storestatus, distance,minoder,deliverytime;
        ImageView storeimage,paymentcard,paymentcash,statusimage;
        LinearLayout storelayout;
        CamomileSpinner spinner;

        public MyViewHolder(View itemView) {
            super(itemView);
            mainstore = (TextView) itemView.findViewById(R.id.mainstore);
            storerating = (TextView) itemView.findViewById(R.id.revies_count);
            distance = (TextView) itemView.findViewById(R.id.distance);
            reviews = (TextView) itemView.findViewById(R.id.revies);
            storestatus = (TextView)itemView.findViewById(R.id.storestatus);
            minoder = (TextView)itemView.findViewById(R.id.minmodr);
            deliverytime = (TextView)itemView.findViewById(R.id.delivery_time);
            storeimage = (ImageView)itemView.findViewById(R.id.storeimage);
            storelayout =(LinearLayout)itemView.findViewById(R.id.storelayout);
            paymentcard=(ImageView)itemView.findViewById(R.id.payment_card);
            paymentcash=(ImageView)itemView.findViewById(R.id.payment_cash);
            statusimage=(ImageView)itemView.findViewById(R.id.storestatusimage) ;
            spinner = (CamomileSpinner) itemView.findViewById(R.id.spinner);


            storelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                    intent.putExtra("stores", storeArrayList);
                    intent.putExtra("class", "fav");
                    intent.putExtra("pos",getAdapterPosition());
                    context.startActivity(intent);
                }
            });
        }
    }
}
