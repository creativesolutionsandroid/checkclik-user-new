package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.ServiceVariantImageAdapter;
import com.cs.checkclickuser.Models.AddToCartResponse;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.ServiceVariantResponse;
import com.cs.checkclickuser.Models.TimeSlots;
import com.cs.checkclickuser.Models.TimingsResponse;
import com.cs.checkclickuser.Models.UrlEncryptResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BranchColor;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ServiceVariantsActivityStep5 extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    private ImageView backBtn, cart, fav, notification, favIcon, shareIcon, logo;
    private ViewPager imagesViewPager;
    private TextView itemName, imagesCount;
    private LinearLayout variant1Layout, variant2Layout, variant3Layout;
    private LinearLayout popupVariant1Layout, popupVariant2Layout, popupVariant3Layout;
    private TextView variant1Title, variant2Title, variant3Title;
    private TextView popupVariant1Title, popupVariant2Title, popupVariant3Title;
    private TextView variant1, variant2, variant3;
    private TextView homeVariant, inStockVariant;
    private TextView discountedPrice, price, itemQty, stockStatus;
    private TextView selectDate, selectTime;
    private Button popupDoneBtn, addToCartBtn;
    private String TAG = "TAG";
    SharedPreferences userPrefs;
    private RelativeLayout variantBottomView;
    private ServiceVariantImageAdapter mAdapter;
    private int variant1Pos = 0,  variant2Pos = 0;
    private int spPos = 0,  timingsPos = 0;
    private Spinner spSpinner, timingsSpinner;
    private TextView spProviderName;
    private ArrayAdapter<ServiceVariantResponse.JServiceProvidersJson> spAdapter;
    private ArrayAdapter<TimeSlots> timingsAdapter;
    private String selectedDate = "", selectedTime = "";

    private ArrayList<ServiceVariantResponse.ServiceDetails> productLists = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.ServiceTypeList> variantsLists = new ArrayList<>();
    private ArrayList<TimingsResponse.DisableDays> disableDaysList = new ArrayList<>();
    private ArrayList<TimingsResponse.TimingSlots> timingSlotsList = new ArrayList<>();
    private ArrayList<TimeSlots> timings = new ArrayList<>();
    private ArrayList<ServiceVariantResponse.JServiceProvidersJson> sp = new ArrayList<>();

    DatePickerDialog datePickerDialog ;
    int Year, Month, Day;
    Calendar calendar ;

    UrlEncryptResponse.Data encryptData;
    File file12;
    private AlertDialog customDialog;
    private int cartFlag = 1;
    private ImageView cartStatusIcon, notifictionStatusIcon;

    SharedPreferences LanguagePrefs;
    String language = "En";

    private static final int STORAGE_REQUEST = 2;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.service_variants);
        } else {
            setContentView(R.layout.service_variants_ar);
        }

        productLists = (ArrayList<ServiceVariantResponse.ServiceDetails>) getIntent().getSerializableExtra("productList");
        variantsLists = (ArrayList<ServiceVariantResponse.ServiceTypeList>) getIntent().getSerializableExtra("variantList");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        fav = (ImageView) findViewById(R.id.fav);
        shareIcon = (ImageView) findViewById(R.id.share_product);
        logo = (ImageView) findViewById(R.id.logo);
        notification = (ImageView) findViewById(R.id.noti);

        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        imagesViewPager = (ViewPager) findViewById(R.id.images_viewpager);

        popupVariant1Layout = (LinearLayout) findViewById(R.id.popup_varinat1_layout);
        popupVariant2Layout = (LinearLayout) findViewById(R.id.popup_varinat2_layout);
        popupVariant3Layout = (LinearLayout) findViewById(R.id.popup_varinat3_layout);

        variant1Layout = (LinearLayout) findViewById(R.id.variant1_layout);
        variant2Layout = (LinearLayout) findViewById(R.id.variant2_layout);
        variant3Layout = (LinearLayout) findViewById(R.id.variant3_layout);
        variantBottomView = (RelativeLayout) findViewById(R.id.variants_bottom_view);

        itemName = (TextView) findViewById(R.id.item_name);
        imagesCount = (TextView) findViewById(R.id.images_count);
        discountedPrice = (TextView) findViewById(R.id.discounted_price);
        price = (TextView) findViewById(R.id.price);
        itemQty = (TextView) findViewById(R.id.item_qty);
        stockStatus = (TextView) findViewById(R.id.stock_status);
        spProviderName = (TextView) findViewById(R.id.sp);

        variant1 = (TextView) findViewById(R.id.variant1);
        variant2 = (TextView) findViewById(R.id.variant2);
        variant3 = (TextView) findViewById(R.id.variant3);
        variant1Title = (TextView) findViewById(R.id.variant_title1);
        variant2Title = (TextView) findViewById(R.id.variant_title2);
        variant3Title = (TextView) findViewById(R.id.variant_title3);
        popupVariant1Title = (TextView) findViewById(R.id.popuop_variant1_title);
        popupVariant2Title = (TextView) findViewById(R.id.popuop_variant2_title);
        popupVariant3Title = (TextView) findViewById(R.id.popuop_variant3_title);

        homeVariant = (TextView) findViewById(R.id.home_variant);
        inStockVariant = (TextView) findViewById(R.id.instore_variant);

        selectDate = (TextView) findViewById(R.id.date);
        selectTime = (TextView) findViewById(R.id.time);

        popupDoneBtn = (Button) findViewById(R.id.bt_done);
        addToCartBtn = (Button) findViewById(R.id.bt_continue);

        spSpinner = (Spinner) findViewById(R.id.sp_spinner);
        timingsSpinner = (Spinner) findViewById(R.id.timings_spinner);

        cart = (ImageView) findViewById(R.id.cart);

        setVariants();

        BranchColor branchColor = new BranchColor();
//        Constants.appColor = branchColor.BranchColor(productLists.get(0).get());

        variant1Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        variant2Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        variant3Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        addToCartBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ServiceVariantsActivityStep5.this);
                } else {
                    Intent intent = new Intent(ServiceVariantsActivityStep5.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetBranchDetailsApi().execute();
            }
        });

        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    new GetEncrytptURLApi().execute();
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ServiceVariantsActivityStep5.this, CartActivity.class);
                startActivity(intent);
            }
        });

        variant1Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        variant2Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        variant3Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        popupDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                variant1.setText(variantsLists.get(variant1Pos).getServiceType());
                initData();
                variantBottomView.setVisibility(View.GONE);
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedDate.equals("") || selectedTime.equals("")) {
                    if (language.equals("En")){
                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_date),
                            getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                            ServiceVariantsActivityStep5.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_date_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                ServiceVariantsActivityStep5.this);
                    }

                }
                else {
                    new AddToCart().execute();
                }
            }
        });

        homeVariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                variant1Pos = 0;
                homeVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_selected));
                inStockVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_unselected));

                final ArrayList<ServiceVariantResponse.JServiceProvidersJson> sp = new ArrayList<>();
                sp.addAll(variantsLists.get(variant1Pos).getJServiceProvidersJson());
                spSpinner.setAdapter(spAdapter);
                spAdapter.notifyDataSetChanged();

                new getAvailableDates().execute();
            }
        });

        inStockVariant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                variant1Pos = 1;
                inStockVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_selected));
                homeVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_unselected));

                final ArrayList<ServiceVariantResponse.JServiceProvidersJson> sp = new ArrayList<>();
                sp.addAll(variantsLists.get(variant1Pos).getJServiceProvidersJson());
                spSpinner.setAdapter(spAdapter);
                spAdapter.notifyDataSetChanged();

                new getAvailableDates().execute();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendarData();
            }
        });

        sp.addAll(variantsLists.get(variant1Pos).getJServiceProvidersJson());
        spAdapter = new ArrayAdapter<ServiceVariantResponse.JServiceProvidersJson>(this, R.layout.list_spinner, sp) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(sp.get(position).getServiceProviderName());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        spSpinner.setAdapter(spAdapter);
        spSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                variant2Pos = i;
                spProviderName.setText(sp.get(i).getServiceProviderName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        initData();
        new getAvailableDates().execute();

        calendar = Calendar.getInstance();

        Year = calendar.get(Calendar.YEAR) ;
        Month = calendar.get(Calendar.MONTH);
        Day = calendar.get(Calendar.DAY_OF_MONTH);

        discountedPrice.setPaintFlags(discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_fav_icon, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);

        favIcon = (ImageView) bubbleLayout.findViewById(R.id.fav_icon);
        ImageView infoIcon = (ImageView) bubbleLayout.findViewById(R.id.info_icon);

        if (productLists.get(0).getFavorite() ||
                (CacheData.favProductId != null && CacheData.favProductId.contains(productLists.get(0).getServiceId()))) {
            favIcon.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
        } else {
            favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favblack));
        }

        favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: favIcon");
                new InsertStoreFav().execute();
            }
        });

        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceVariantsActivityStep5.this, ProductInfoActivity.class);
                intent.putExtra("Arraylist",productLists);
                intent.putExtra("itempos",0);
                intent.putExtra("StoreId",getIntent().getIntExtra("storeId", 0));
                intent.putExtra("BranchId",getIntent().getIntExtra("branchId", 0));
                intent.putExtra("type", IdConstants.SERVICE);
//                startActivity(intent);
            }
        });

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationInWindow(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, (location[0] - 16), v.getHeight() + location[1]);
            }
        });
    }

    private void initData(){
        mAdapter = new ServiceVariantImageAdapter(ServiceVariantsActivityStep5.this, productLists.get(0).getJImages());
        imagesViewPager.setAdapter(mAdapter);

        if (productLists.get(0).getCartItemCount() != 0 ||
                (Constants.cartCount != 0)) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (productLists.get(0).getNotificationCount() != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }

        if (language.equals("En")){
            itemName.setText(productLists.get(0).getServiceNameEn());
        }else {
            itemName.setText(productLists.get(0).getServiceNameAr());
        }

        imagesCount.setText(productLists.get(0).getJImages().size() + " " + getResources().getString(R.string.pics));
        discountedPrice.setText("SAR "+Constants.priceFormat1.format(productLists.get(0).getSellingPrice()));
        price.setText("SAR "+Constants.priceFormat1.format(productLists.get(0).getPrice()));


        Glide.with(this)
                .load(Constants.STORE_IMAGE_URL + productLists.get(0).getLogoCopy())
                .into(logo);

//        if(productLists.get(0).get() > 0) {
//            stockStatus.setText(getString(R.string.available));
//        }
//        else {
//            stockStatus.setText(getString(R.string.unavailable));
//        }

//        if(productLists.get(0).getCa() > 1) {
//            itemQty.setText(""+productLists.get(0).getCartquantity());
//        }

        if(productLists.get(0).getPrice() == productLists.get(0).getSellingPrice()){
            discountedPrice.setVisibility(View.GONE);
        }

        variant2.setText(variantsLists.get(variant1Pos).getJServiceProvidersJson().get(variant2Pos).getServiceProviderName());
    }

    private class AddToCart extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAddToCartJSON();
            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AddToCartResponse> call = apiService.addToCart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus() && response.body().getStatusmessage().equals("Success")) {
                           Constants.closeLoadingDialog();
                           Constants.cartCount = 1;
                            showSuccessDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (failureResponse.equals("0")) {
                                showClearCartDialog();
                            }
                            else {
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ServiceVariantsActivityStep5.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ServiceVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                             Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }



    private String prepareAddToCartJSON(){
        String dateStr = null;
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd,yyyy", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            Date formattedDate = sdf2.parse(selectedDate);
            dateStr = sdf1.format(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ServiceId",  productLists.get(0).getServiceId());
            parentObj.put("StoreId",  getIntent().getIntExtra("storeId", 0));
            parentObj.put("OrderType",  2);
            parentObj.put("Flag", cartFlag);
            parentObj.put("BranchId",  getIntent().getIntExtra("branchId", 0));
            parentObj.put("UserId",  userPrefs.getString("userId",""));
            parentObj.put("StockQuantity",  "1");
            parentObj.put("ServiceTypeId",  variantsLists.get(variant1Pos).getServiceTypeId());
            parentObj.put("ServiceProviderId", sp.get(variant2Pos).getServiceProviderId());
            parentObj.put("RequestDate",  dateStr+" 00:00:00.000");
            parentObj.put("TimeSlotId",  timings.get(timingsPos).getTimeSlotId());
            cartFlag = 1;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareAddToCartJSON: "+parentObj.toString());
        return parentObj.toString();
    }

    private void setVariants(){
        if(variantsLists.size() == 1) {
            inStockVariant.setVisibility(View.GONE);
            homeVariant.setText(variantsLists.get(0).getServiceType());
            variant1.setText(variantsLists.get(0).getServiceType());
            homeVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_selected));
        }
        else if(variantsLists.size() == 2) {
            variant1.setText(variantsLists.get(0).getServiceType());
            homeVariant.setText(variantsLists.get(0).getServiceType());
            inStockVariant.setText(variantsLists.get(1).getServiceType());
            homeVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_selected));
            inStockVariant.setBackground(getResources().getDrawable(R.drawable.shape_variant_unselected));
        }
    }

    private class getAvailableDates extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetDatesJSON();
            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<TimingsResponse> call = apiService.getTimingsOfSP(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<TimingsResponse>() {
                @Override
                public void onResponse(Call<TimingsResponse> call, Response<TimingsResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            disableDaysList = response.body().getData().getDisableDays();
                            timingSlotsList = response.body().getData().getTimingSlots();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ServiceVariantsActivityStep5.this);
                        }
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<TimingsResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ServiceVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetDatesJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            if (variantsLists.get(variant1Pos).getServiceType().equalsIgnoreCase("home")) {
                parentObj.put("TimingType", "H");
            }
            else {
                parentObj.put("TimingType", "S");
            }
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareGetDatesJSON: "+parentObj.toString());
        return parentObj.toString();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int Year, int Month, int Day) {
        String date = "";
        Date formttedDate = null;
        if(Day <= 9 && Month < 9) {
            date = "0"+Day + "/0" + (Month + 1) + "/" + Year;
        }
        else if(Day <= 9) {
            date = "0"+Day + "/" + (Month + 1) + "/" + Year;
        }
        else if(Month < 9) {
            date = Day + "/0" + (Month + 1) + "/" + Year;
        }
        else {
            date = Day + "/" + (Month + 1) + "/" + Year;
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMM dd,yyyy", Locale.US);

        try {
            formttedDate = sdf1.parse(date);
            date = sdf2.format(formttedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        selectedDate = date;
        selectDate.setText(date);
        setTimingsSpinner(formttedDate, date);
    }

    private void setTimingsSpinner(Date date, final String dateStr){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        timings.clear();
        for (int i = 0; i < timingSlotsList.size(); i++) {
            if(timingSlotsList.get(i).getWeekdayId() == calendar.get(Calendar.DAY_OF_WEEK)) {
                TimeSlots timeSlots = new TimeSlots();
                timeSlots.setTimeSlot(timingSlotsList.get(i).getTimeSlot());
                timeSlots.setTimeSlotId(""+timingSlotsList.get(i).getTimeSlotId());
                timings.add(timeSlots);
            }
        }

        timingsAdapter = new ArrayAdapter<TimeSlots>(this, R.layout.list_spinner, timings) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(timings.get(position).getTimeSlot());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        timingsSpinner.setAdapter(timingsAdapter);
        timingsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                timingsPos = i;
                selectTime.setText(timings.get(i).getTimeSlot());
                variant3.setText(dateStr + "\n"+timings.get(i).getTimeSlot());
                selectedTime = timings.get(i).getTimeSlot();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void showCalendarData(){

        datePickerDialog = DatePickerDialog.newInstance(ServiceVariantsActivityStep5.this, Year, Month, Day);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setTitle("" + getResources().getString(R.string.select_date_title));

        // Setting Min Date to today date
        Calendar min_date_c = Calendar.getInstance();
        datePickerDialog.setMinDate(min_date_c);

        // Setting Max Date to next 2 years
        Calendar max_date_c = Calendar.getInstance();
        max_date_c.add(Calendar.MONTH, 1);
        datePickerDialog.setMaxDate(max_date_c);

        for (int i = 0; i < disableDaysList.size(); i++) {
            Calendar min = Calendar.getInstance();
            Calendar max = Calendar.getInstance();
            max.add(Calendar.MONTH, 1);
            max.add(Calendar.DATE, 1);
            Calendar loopdate = null;

            for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
                int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek == disableDaysList.get(i).getWeekdayId()) {
                    Calendar[] disabledDays = new Calendar[1];
                    disabledDays[0] = loopdate;
                    datePickerDialog.setDisabledDays(disabledDays);
                }
            }
        }

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });

        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                if(productLists.get(0).getFavorite()) {
                    parentObj.put("Type", 3);
                }
                else {
                    parentObj.put("Type", 2);
                }
                parentObj.put("StatusId",  "1");
                parentObj.put("Ids", productLists.get(0).getServiceId());
                parentObj.put("UserId",userPrefs.getString("userId", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if(productLists.get(0).getFavorite()) {
                                productLists.get(0).setFavorite(false);
                                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favblack));
                                if (CacheData.favProductId != null && CacheData.favProductId.contains(productLists.get(0).getServiceId())) {
                                    CacheData.favProductId.remove(productLists.get(0).getServiceId());
                                }
                            }
                            else {
                                productLists.get(0).setFavorite(true);
                                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                CacheData.favProductId.add(""+productLists.get(0).getServiceId());}
                            if (language.equalsIgnoreCase("En")){
                            Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ServiceVariantsActivityStep5.this);
                            }else {
                                Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok_ar), ServiceVariantsActivityStep5.this);
                            }

                        }
                    }
                    else {
                        Toast.makeText(ServiceVariantsActivityStep5.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ServiceVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ServiceVariantsActivityStep5.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:
                new GetEncrytptURLApi().execute();
                break;
        }
    }

    private class GetEncrytptURLApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareURLEncryptJSON();
            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UrlEncryptResponse> call = apiService.UrlEncrypt(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UrlEncryptResponse>() {
                @Override
                public void onResponse(Call<UrlEncryptResponse> call, Response<UrlEncryptResponse> response) {
                    Log.d(TAG, "vonResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getMessage().equals("Success")) {
                            encryptData = response.body().getData();
                            if (canAccessStorage() && productLists.get(0).getImages() != null &&
                                    productLists.get(0).getImages().length() > 0) {
                                new DownloadImage().execute();
                            }
                            else {
                                prepareEncryptURL();
                            }
//                            Constants.closeLoadingDialog();
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ServiceVariantsActivityStep5.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UrlEncryptResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ServiceVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareURLEncryptJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductNameEn", productLists.get(0).getServiceNameEn());
            parentObj.put("BranchId", getIntent().getIntExtra("branchId", 0));
            parentObj.put("Id", productLists.get(0).getServiceId());
            parentObj.put("BranchSubCategoryId", productLists.get(0).getSubCategoryId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private void prepareEncryptURL(){
        Constants.closeLoadingDialog();
        String encyptedURL = Constants.UNIVERSAL_LINKS_URL +
                encryptData.getStoreName() + "/" + productLists.get(0).getMainCatEn() + "/" +
                productLists.get(0).getSubCatEn() + "/" + encryptData.getStoreType() + "/" + encryptData.getEncryId();

        String productName = productLists.get(0).getServiceNameEn();
        String productDesc = " " + getResources().getString(R.string.at_SAR) + " " + Constants.priceFormat1.format(productLists.get(0).getPrice());
        encyptedURL = encyptedURL.replace(" ", "-");

        String shareBody = productName + productDesc + " \n\n "+encyptedURL;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        if (file12 != null) {
            File outputFile = new File(Environment.getExternalStorageDirectory()+"/CheckClik/"+file12.getName());
            Uri photoURI = FileProvider.getUriForFile(ServiceVariantsActivityStep5.this,
                    getApplicationContext().getPackageName() + ".my.package.name.provider", outputFile);
            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            sharingIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        }
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, productName);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share using.."));
    }

    private class DownloadImage extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "CheckClik");
                folder.mkdir();
                file12 = new File(folder, "CheckClik_"+System.currentTimeMillis()+".png");
                try {
                    file12.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                FileOutputStream f = new FileOutputStream(file12);

                URL u = new URL(Constants.PRODUCTS_IMAGE_URL+productLists.get(0).getJImages().get(0).getImage());
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
//                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d(TAG, "doInBackground: "+buffer.length);
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            Constants.closeLoadingDialog();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (file12.exists()) {
                prepareEncryptURL();
            }
        }
    }

    private void showSuccessDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        if (language.equals("En")){
            int layout = R.layout.cart_success_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
        }else {
            int layout = R.layout.cart_success_dialog_ar;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // close this activity
                customDialog.dismiss();
                finish();
            }
        }, 2000);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.65;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showClearCartDialog(){

        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ServiceVariantsActivityStep5.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equals("En")){
            title.setText("" + getResources().getString(R.string.replace_cart_products));
            yes.setText("" + getResources().getString(R.string.yes));
            no.setText("" + getResources().getString(R.string.no));
            desc.setText("" + getResources().getString(R.string.your_cart_contains_products));

        }else {
            title.setText("" + getResources().getString(R.string.replace_cart_products_ar));
            yes.setText("" + getResources().getString(R.string.yes_ar));
            no.setText("" + getResources().getString(R.string.no));
            desc.setText("" + getResources().getString(R.string.your_cart_contains_products));

        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartFlag = 0;
                new AddToCart().execute();
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }

    private class GetBranchDetailsApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetBranchDetailsJSON();
            Constants.showLoadingDialog(ServiceVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchDetails> call = apiService.getBranchDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchDetails>() {
                @Override
                public void onResponse(Call<BranchDetails> call, Response<BranchDetails> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        BranchDetails branchResponse = response.body();
                        if (branchResponse.getData().getBranch() != null &&
                                branchResponse.getData().getBranch().size() > 0) {
                            Intent intent = new Intent(ServiceVariantsActivityStep5.this, ProductStoresActivityStep1.class);
                            intent.putExtra("stores", branchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", branchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "vendor_app_intent");
                            startActivity(intent);
                        }
                        else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(ServiceVariantsActivityStep5.this, getResources().getString(R.string.store_details_not_available), Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ServiceVariantsActivityStep5.this, getResources().getString(R.string.store_details_not_available_ar), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchDetails> call, Throwable t) {
                    Log.e("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ServiceVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ServiceVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetBranchDetailsJSON(){
        int branchId = 0;
        try {
            JSONArray relatedProductsArray = new JSONArray(productLists.get(0).getRelatedService());
            JSONObject relatedProductsObj = relatedProductsArray.getJSONObject(0);
            branchId = relatedProductsObj.getInt("BranchId");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId",  branchId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj.toString());
        return parentObj.toString();
    }
}
