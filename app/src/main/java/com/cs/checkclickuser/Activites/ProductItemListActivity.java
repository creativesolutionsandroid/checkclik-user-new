package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.cs.checkclickuser.Adapter.ProductIteamAdapter;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductItemListActivity extends AppCompatActivity {

    ProductstoreResponce.Data storeArrayList;

    int storePos;
    ImageView back_btn;
    public ProductIteamAdapter miteamlistadaper;
    TextView branchname;
    String TAG = "TAG";
    RecyclerView storeslist;

    SharedPreferences LanguagePrefs;
    String language;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_iteam_activity);
        } else {
            setContentView(R.layout.product_iteam_activity_ar);
        }

        back_btn=(ImageView)findViewById(R.id.back_btn);
        branchname=(TextView)findViewById(R.id.branchname);
        storeslist=(RecyclerView)findViewById(R.id.list_item);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        storeArrayList = (ProductstoreResponce.Data) getIntent().getSerializableExtra("stores1");
        storePos= getIntent().getIntExtra("pos1",0);
        if (language.equals("En")){
            branchname.setText(storeArrayList.getSubCategory().get(storePos).getNameEn());
        }else {
            branchname.setText(storeArrayList.getSubCategory().get(storePos).getNameAr());
        }

        miteamlistadaper = new ProductIteamAdapter(ProductItemListActivity.this, storeArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivity.this);
        storeslist.setLayoutManager(new GridLayoutManager(ProductItemListActivity.this, 1));
        storeslist.setAdapter(miteamlistadaper);

    }
}

