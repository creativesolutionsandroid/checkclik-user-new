package com.cs.checkclickuser.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.ShippingCountriesAdapter;
import com.cs.checkclickuser.Adapter.ShippingCountriesExpandbleAdapter;
import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShippingCountriesActivity extends AppCompatActivity {

    ExpandableListView countries_list;
    ImageView back_btn;
    ShippingCountriesExpandbleAdapter mAdapter;

    SharedPreferences LanguagePrefs;
    String language = "En";


    ArrayList<AdressCountryResponce.CityList> list_of_cities = new ArrayList<>();
    ArrayList<AdressCountryResponce.CountryList> list_of_countries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_shipping_countries);
        } else {
            setContentView(R.layout.activity_shipping_countries_ar);
        }

        countries_list = (ExpandableListView) findViewById(R.id.countries_list);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setListener();

        new getcountylistApi().execute();
    }

    private class getcountylistApi extends AsyncTask<String, String, String> {

        String inputStr;
        String TAG = "TAG";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(ShippingCountriesActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ShippingCountriesActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AdressCountryResponce> call = apiService.getcounrylist();
            call.enqueue(new Callback<AdressCountryResponce>() {
                @Override
                public void onResponse(Call<AdressCountryResponce> call, Response<AdressCountryResponce> response) {
                    if (response.isSuccessful()) {
                        list_of_countries = response.body().getData().getCountryList();
                        list_of_cities = response.body().getData().getCityList();
                        filterCities();

//                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ShippingCountriesActivity.this);
//                        countries_list.setLayoutManager(mLayoutManager);
//
//                        mAdapter = new ShippingCountriesAdapter(ShippingCountriesActivity.this, list_of_cities, ShippingCountriesActivity.this);
//                        countries_list.setAdapter(mAdapter);

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ShippingCountriesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ShippingCountriesActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<AdressCountryResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ShippingCountriesActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ShippingCountriesActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ShippingCountriesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ShippingCountriesActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void filterCities() {
        // Array list for header
        ArrayList<String> countries = new ArrayList<String>();

        // Hash map for both header and child
        HashMap<String, List<String>> hashMap = new HashMap<String, List<String>>();

        for (int i = 0; i < list_of_countries.size(); i++) {
            if (language.equals("En")){
                countries.add(list_of_countries.get(i).getNameEn());

                // Array list for child items
                List<String> cities = new ArrayList<String>();
                for (int j = 0; j < list_of_cities.size(); j++) {
                    if (list_of_cities.get(j).getCountryId() == list_of_countries.get(i).getId()) {
                        cities.add(list_of_cities.get(j).getNameEn());
                    }
                }
                Log.d("TAG", "filterCities: "+cities);
                hashMap.put(countries.get(i), cities);
            }else {
                countries.add(list_of_countries.get(i).getNameAr());

                // Array list for child items
                List<String> cities = new ArrayList<String>();
                for (int j = 0; j < list_of_cities.size(); j++) {
                    if (list_of_cities.get(j).getCountryId() == list_of_countries.get(i).getId()) {
                        cities.add(list_of_cities.get(j).getNameAr());
                    }
                }
                Log.d("TAG", "filterCities: "+cities);
                hashMap.put(countries.get(i), cities);
            }

        }

        mAdapter = new ShippingCountriesExpandbleAdapter(ShippingCountriesActivity.this, countries, hashMap);
        countries_list.setAdapter(mAdapter);
    }

    // Setting different listeners to expandablelistview
    void setListener() {

        // This listener will show toast on child click
        countries_list.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView listview, View view,
                                        int groupPos, int childPos, long id) {
                Intent intent = new Intent();
                Log.d("TAG", "onChildClick: "+childPos);
//                for (int j = 0; j < list_of_cities.size(); j++) {
//                    if (list_of_cities.get(j).getCountryId() == list_of_cities.get(childPos).getCountryId()) {
                if (language.equals("En")){
                    intent.putExtra("city", list_of_cities.get(childPos).getNameEn());
                }else {
                    intent.putExtra("city", list_of_cities.get(childPos).getNameAr());
                }
                Log.d("TAG", "city: "+list_of_cities.get(childPos).getNameAr());

                        intent.putExtra("cityId", list_of_cities.get(childPos).getCountryId());

//                    }
//                }
                setResult(Activity.RESULT_OK, intent);
                finish();
                return false;
            }
        });
    }

}
