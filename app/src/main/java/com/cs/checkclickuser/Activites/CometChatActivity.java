package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

//import com.cometchat.pro.core.AppSettings;
import com.cs.checkclickuser.Utils.AppConfig;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

//import com.cometchat.pro.core.CometChat;
//import com.cometchat.pro.exceptions.CometChatException;
//import com.cometchat.pro.models.User;
import com.cs.checkclickuser.Adapter.ViewPagerAdapter;
import com.cs.checkclickuser.Contracts.CometChatActivityContract;
import com.cs.checkclickuser.Contracts.StringContract;
//import com.cs.checkclickuser.Fragment.GroupListFragment;
import com.cs.checkclickuser.Fragements.GroupListFragment;
import com.cs.checkclickuser.Presenters.CometChatActivityPresenter;
import com.cs.checkclickuser.R;
//import com.cs.checkclickuser.Utils.FabIconAnimator;
//import com.cs.checkclickuser.Utils.Utils.ScrollHelper;

import com.cs.checkclickuser.Utils.Constants;

import java.util.HashMap;

public class CometChatActivity extends AppCompatActivity implements CometChatActivityContract.CometChatActivityView {

    private ViewPager mViewPager; //view pager
    private TabLayout tabs;
    private Context context;
    private CoordinatorLayout mainContent;
    private CometChatActivityContract.CometChatActivityPresenter cometChatActivityPresenter;
    private ViewPagerAdapter adapter;
    private static final String TAG = "CometChatActivity";
    public static HashMap<String ,Integer> countMap;
    private MenuItem searchItem;
    private SearchView searchView;
    private int pageNumber=0;
//    View rootView;
    SharedPreferences userPrefs;
    ImageView menu_btn;
    private String appID = "APP_ID";


    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_comet_chat);
        } else {
            setContentView(R.layout.activity_comet_chat);

        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        context = CometChatActivity.this;
//        cometChatActivityPresenter = new CometChatActivityPresenter();
//        cometChatActivityPresenter.attach(this);
//        initViewComponents();

//        new Thread(() -> cometChatActivityPresenter.getBlockedUser(getContext())).start();

        tabs = (TabLayout) findViewById(R.id.tabs);

        mViewPager = (ViewPager) findViewById(R.id.container);
        menu_btn = (ImageView) findViewById(R.id.menu_btn);

        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mViewPager.setOffscreenPageLimit(1);

        Constants.showLoadingDialog(CometChatActivity.this);

//        AppSettings appSettings = new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(AppConfig.AppDetails.REGION).build();
//
//        CometChat.init(CometChatActivity.this, StringContract.AppDetails.APP_ID, appSettings, new CometChat.CallbackListener<String>() {
//            @Override
//            public void onSuccess(String successMessage) {
//                Log.d(TAG, "Initialization completed successfully");
//            }
//            @Override
//            public void onError(CometChatException e) {
//                Log.d(TAG, "Initialization failed with exception: " + e.getMessage());
//            }
//        });

        // checking cometchat login user
//        if(CometChat.getLoggedInUser() == null) {
//            if(!userPrefs.getString("chatUserId", "").equals("")) {
//                CometChat.login(userPrefs.getString("chatUserId", ""), StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
//                    @Override
//                    public void onSuccess(User user) {
//                        setViewPager();
//                        Log.i(TAG, "viewpager1: ");
//                    }
//
//                    @Override
//                    public void onError(CometChatException e) {
//                        e.printStackTrace();
//                    }
//
//                });
//            }
//            else {
//                Toast.makeText(CometChatActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
//            }
//        }
//        else {
//            setViewPager();
//
//            Log.i(TAG, "viewpager: ");
//        }

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adapter.notifyDataSetChanged();
                pageNumber=i;
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        cometChatActivityPresenter.addCallEventListener(CometChatActivity.this, TAG);
        Log.d(TAG, "onResume: ");
        cometChatActivityPresenter.addMessageListener(CometChatActivity.this,TAG);
    }


    @Override
    public void onPause() {
        super.onPause();
        cometChatActivityPresenter.removeMessageListener(TAG);
        cometChatActivityPresenter.removeCallEventListener(TAG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        new GroupListFragment().onActivityResult(requestCode, resultCode, data);
//        mViewPager.setCurrentItem(2);
//    }

    private void setViewPager() {

        Constants.closeLoadingDialog();
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(new ContactsFragment(), getString(R.string.contacts));
//        adapter.addFragment(new GroupListFragment(), getString(R.string.group));
        mViewPager.setAdapter(adapter);
        tabs.setupWithViewPager(mViewPager);

    }

}
