package com.cs.checkclickuser.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickuser.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangeLanguage extends AppCompatActivity {

    Button arabic, english, done;
    TextView back_btn;

    SharedPreferences LanguagePrefs;
    String language;
    SharedPreferences.Editor languagePrefsEditor;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = LanguagePrefs.edit();
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.change_language);
        } else {
            setContentView(R.layout.change_language_ar);
        }

        arabic = (Button) findViewById(R.id.Arabic);
        english = (Button) findViewById(R.id.English);
        done = (Button) findViewById(R.id.done);
        back_btn = (TextView) findViewById(R.id.back_btn);

        if (language.equalsIgnoreCase("En")) {
            english.setBackground(getDrawable(R.drawable.shape_select_langueage));
        } else {
            arabic.setBackground(getDrawable(R.drawable.shape_select_langueage));
        }

        arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arabic.setBackground(getDrawable(R.drawable.shape_select_langueage));
                english.setBackground(getDrawable(R.drawable.shape_langueage));

            }
        });
        english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arabic.setBackground(getDrawable(R.drawable.shape_langueage));
                english.setBackground(getDrawable(R.drawable.shape_select_langueage));

            }
        });


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = LanguagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();

                    Intent intent = new Intent(ChangeLanguage.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = LanguagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(ChangeLanguage.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
//
//                String DeviceLangauge = "Device Langauge", UserId = "User Id", AppLanguage = "App Language";
//
////                Crashlytics.setString(DeviceLangauge, Locale.getDefault().getDisplayLanguage()/* string value */);
////                Crashlytics.setString(AppLanguage, language/* string value */);
////                Crashlytics.setString("Device Token", regId);
////
////                if (!userId.equals("0")) {
////                    Crashlytics.setString(UserId, userId/* string value */);
////                    Crashlytics.setString("Name", userPrefs.getString("name", "-")/* string value */);
////                    Crashlytics.setString("Mobile", userPrefs.getString("mobile", "-")/* string value */);
////                }
//
//            }
//        });


            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
