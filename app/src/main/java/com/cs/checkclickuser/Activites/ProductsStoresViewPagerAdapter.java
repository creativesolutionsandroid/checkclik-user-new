package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.PagerAdapter;

import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Activites.ProductStoresActivityStep1;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.UrlEncryptResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ProductsStoresViewPagerAdapter extends PagerAdapter {

    LayoutInflater mLayoutInflater;
    Context context;
    String TAG = "TAG";
    SharedPreferences userPrefs;
    private ArrayList<ProductlistResponce.Stores> storeList = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchesList = new ArrayList<>();
    String inputStr;
    LinearLayout discountLayout;
    int type;
    Activity activity;

    private static final int STORAGE_REQUEST = 2;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public ProductsStoresViewPagerAdapter(Context context, ArrayList<ProductlistResponce.Stores> storeList, int type, ArrayList<ProductlistResponce.JOtherBranch> branchesList, Activity activity) {
        this.context = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.storeList = storeList;
        this.type = type;
        this.activity = activity;
        this.branchesList = branchesList;
        userPrefs = context.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        return (branchesList.size());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((CardView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.list_map_view_pager, container, false);

        ImageView share = (ImageView) itemView.findViewById(R.id.share_btn);
        ImageView storeImage = (ImageView) itemView.findViewById(R.id.image);
        ImageView cashImage = (ImageView) itemView.findViewById(R.id.cash);
        ImageView cardImage = (ImageView) itemView.findViewById(R.id.card);
        ImageView storeStatus = (ImageView) itemView.findViewById(R.id.store_status);
        TextView address = (TextView) itemView.findViewById(R.id.address);
        TextView storeName = (TextView) itemView.findViewById(R.id.store_name);
        TextView ratingCount = (TextView) itemView.findViewById(R.id.ratings_count);
        TextView avgAmount = (TextView) itemView.findViewById(R.id.avg_amount);
        TextView avgTime = (TextView) itemView.findViewById(R.id.avg_time);
        TextView distance = (TextView) itemView.findViewById(R.id.distance);

        RatingBar ratingBar = (RatingBar) itemView.findViewById(R.id.ratingbar);
        RelativeLayout main_layout = (RelativeLayout) itemView.findViewById(R.id.main_layout);

        storeName.setText(branchesList.get(position).getBranchEn());
        ratingCount.setText("("+branchesList.get(position).getReviewsCount()+")");
        avgAmount.setText(branchesList.get(position).getMinimumOrderValue()+" SAR");
        avgTime.setText(branchesList.get(position).getDeliveryTime());
        address.setText(branchesList.get(position).getAddress());
        ratingBar.setRating(branchesList.get(position).getReviews());

        final DecimalFormat distanceFormat = new DecimalFormat("##,##,###.#");
        distance.setText(distanceFormat.format(branchesList.get(position).getDistance()) + " " + context.getResources().getString(R.string.km));

        if(branchesList.get(position).getBranchStatus().equalsIgnoreCase("open")) {
            storeStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.status_green2x));
        }
        else {
            storeStatus.setImageDrawable(context.getResources().getDrawable(R.drawable.status_red));
        }

        if (branchesList.get(position).getIsCashAllowed()&&(branchesList.get(position).getIsCreditCardAllowed())) {
            cardImage.setVisibility(View.VISIBLE);
            cashImage.setVisibility(View.VISIBLE);
        }
        else if (!branchesList.get(position).getIsCreditCardAllowed()){
            cardImage.setVisibility(View.GONE);
        }
        else if (!branchesList.get(position).getIsCashAllowed()){
            cashImage.setVisibility(View.GONE);
        }

        Glide.with(context)
                .load(STORE_IMAGE_URL +branchesList.get(position).getLogo())
                .into(storeImage);

        main_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProductStoresActivityStep1.class);
                intent.putExtra("stores", storeList);
                intent.putExtra("pos",position);
                intent.putExtra("type",type);
                if(type == 1) {
                    intent.putExtra("class", "product");
                }
                else if(type == 2) {
                    intent.putExtra("class", "service");
                }
                context.startActivity(intent);
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    activity.requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    new GetEncrytptURLApi().execute(""+branchesList.get(position).getBranchId(),
                            branchesList.get(position).getStoreEn(), branchesList.get(position).getBranchEn(), branchesList.get(position).getAddress());
                }
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public float getPageWidth(int position) {
        float nbPages = 1.05f; // You could display partial pages using a float value
        return (1 / nbPages);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((CardView) object);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context, perm));
    }

    private class GetEncrytptURLApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(activity);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("Type", IdConstants.URL_SHARE_TYPE_BRANCH);
                parentObj.put("BranchId", strings[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            inputStr = parentObj.toString();

            Call<UrlEncryptResponse> call = apiService.UrlEncrypt(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UrlEncryptResponse>() {
                @Override
                public void onResponse(Call<UrlEncryptResponse> call, Response<UrlEncryptResponse> response) {
                    Log.d(TAG, "vonResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getMessage().equals("Success")) {
                            prepareEncryptURL(response.body().getData(), strings[1], strings[2], strings[3]);
                            Constants.closeLoadingDialog();
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, context.getResources().getString(R.string.app_name),
                                    context.getResources().getString(R.string.ok), activity);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UrlEncryptResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(context);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(context, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private void prepareEncryptURL(UrlEncryptResponse.Data encryptData,
                                   String storeName, String branchName, String branchAddress){
        String encyptedURL = Constants.UNIVERSAL_LINKS_URL +
                encryptData.getStoreName() + "/" + IdConstants.URL_SHARE_TYPE_BRANCH + "/" + encryptData.getEncryId();
        encyptedURL = encyptedURL.replace(" ", "-");

        String shareBody = storeName + "(" + branchName + "), " + branchAddress +" \n\n "+encyptedURL;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, storeName);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        context.startActivity(Intent.createChooser(sharingIntent, "Share using.."));
    }

}
