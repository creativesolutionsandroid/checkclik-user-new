package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.cs.checkclickuser.Adapter.ProductsStoresViewPagerAdapter;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private ArrayList<ProductlistResponce.Stores> storesArrayList = new ArrayList<>();
//    private ArrayList<ProductlistResponce.Stores> storesArrayListCopy = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchList = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchListCopy = new ArrayList<>();
    private int type;
    private ViewPager mViewPager;
    private ProductsStoresViewPagerAdapter mAdapter;
    private GoogleMap mMap;
    Marker prevMarker;
    private static boolean flag = true;
    public ArrayList<Marker> mMarkerList = new ArrayList<>();
    private String prevRating, prevDistance;
    private ImageView backBtn;
    private String TAG = "TAG";
    private TextView sortingBy;
    private int sortId = 3;
    private RadioButton ratingRb, distanceRb, noneRb;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_map);
        } else {
            setContentView(R.layout.activity_map_ar );
        }



        storesArrayList = (ArrayList<ProductlistResponce.Stores>) getIntent().getSerializableExtra("array");
        type = getIntent().getIntExtra("type", 1);

        for (int i = 0; i < storesArrayList.size(); i++) {
            branchList.addAll(storesArrayList.get(i).getJOtherBranch());
        }
        branchListCopy = (ArrayList<ProductlistResponce.JOtherBranch>) branchList.clone();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        backBtn = (ImageView) findViewById(R.id.back_btn);
        sortingBy = (TextView) findViewById(R.id.sorting_by);

        mViewPager = (ViewPager) findViewById(R.id.stores_pager);
        mViewPager.setPadding(16, 0, 16, 0);
        mViewPager.setClipToPadding(false);
        mViewPager.setPageMargin(8);

        mAdapter = new ProductsStoresViewPagerAdapter(MapActivity.this, storesArrayList, type, branchList, MapActivity.this);
        mViewPager.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {

                if (flag) {
                    flag = false;
                    final ProductlistResponce.JOtherBranch temp = branchList.get(position);
                    LatLng newLatLng = new LatLng(Double.parseDouble(temp.getLatitude()), Double.parseDouble(temp.getLongitude()));
                    // map.animateCenterZoom(newLatLng, 15);
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLng, 14));
                    Marker marker = mMarkerList.get(position);

                    if (prevMarker != null) {
                        //Set prevMarker back to default color
                        IconGenerator iconFactory = new IconGenerator(MapActivity.this);
                        iconFactory.setRotation(0);
                        iconFactory.setBackground(null);
                        View view = View.inflate(MapActivity.this, R.layout.map_marker_text, null);
                        TextView distance, rating;

                        distance = (TextView) view.findViewById(R.id.distance);
                        rating = (TextView) view.findViewById(R.id.rating);
                        final DecimalFormat distanceFormat = new DecimalFormat("##,##,##0.0");
                        rating.setText(distanceFormat.format(Float.parseFloat(prevRating)));
                        distance.setText(distanceFormat.format(Float.parseFloat(prevDistance)) + "" + getResources().getString(R.string.km));
                        FrameLayout layout = (FrameLayout) view.findViewById(R.id.layout);

                        layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(MapActivity.this, ProductStoresActivityStep1.class);
                                intent.putExtra("stores", storesArrayList);
                                intent.putExtra("pos",position);
                                intent.putExtra("type",type);
                                if(type == 1) {
                                    intent.putExtra("class", "product");
                                }
                                else if(type == 2) {
                                    intent.putExtra("class", "service");
                                }
                                startActivity(intent);

                            }
                        });

                        iconFactory.setContentView(view);
                        prevRating = Float.toString(branchList.get(position).getReviews());
                        prevDistance = Double.toString((int)branchList.get(position).getDistance());
                        prevMarker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(Float.toString(temp.getReviews()))));
                    }

                    //leave Marker default color if re-click current Marker
                    if (!marker.equals(prevMarker)) {

                        IconGenerator iconFactory = new IconGenerator(MapActivity.this);
                        iconFactory.setRotation(0);
                        iconFactory.setBackground(null);
                        View view = View.inflate(MapActivity.this, R.layout.map_marker_text_active, null);
                        TextView distance, rating;

                        distance = (TextView) view.findViewById(R.id.distance);
                        rating = (TextView) view.findViewById(R.id.rating);
                        final DecimalFormat distanceFormat = new DecimalFormat("##,##,##0.0");
                        rating.setText(distanceFormat.format(branchList.get(position).getReviews()));
                        distance.setText(distanceFormat.format(branchList.get(position).getDistance()) + "" + getResources().getString(R.string.km));
                        FrameLayout layout = (FrameLayout) view.findViewById(R.id.layout);

                        layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(MapActivity.this, ProductStoresActivityStep1.class);
                                intent.putExtra("stores", storesArrayList);
                                intent.putExtra("pos",position);
                                intent.putExtra("type",type);
                                if(type == 1) {
                                    intent.putExtra("class", "product");
                                }
                                else if(type == 2) {
                                    intent.putExtra("class", "service");
                                }
                                startActivity(intent);

                            }
                        });

                        iconFactory.setContentView(view);
                        //
                        marker.setIcon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon()));
                        prevMarker = marker;
                        prevRating = Float.toString(branchList.get(position).getReviews());
                        prevDistance = Double.toString((int)branchList.get(position).getDistance());

                    }
                    prevMarker = marker;
                    prevRating = Float.toString(branchList.get(position).getReviews());
                    prevDistance = Double.toString((int)branchList.get(position).getDistance());
                    flag = true;
                } else {
                    Log.i("", "" + mMarkerList);
                    Log.i("", "" + position);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if(branchList.size() > 0) {
            mViewPager.setCurrentItem(0);
        }

        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_map_sortby, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);

        ratingRb = (RadioButton) bubbleLayout.findViewById(R.id.rb_rating);
        distanceRb = (RadioButton) bubbleLayout.findViewById(R.id.rb_distance);
        noneRb = (RadioButton) bubbleLayout.findViewById(R.id.rb_none);

        sortingBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int[] location = new int[2];
                view.getLocationInWindow(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow.showAtLocation(view, Gravity.NO_GRAVITY, (location[0] + 32), view.getHeight() + location[1]);
            }
        });

        ratingRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sortId = 1;
                    sortStores();
                }
            }
        });

        distanceRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sortId = 2;
                    sortStores();
                }
            }
        });

        noneRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    sortId = 3;
                    sortStores();
                }
            }
        });
    }

    private void sortStores(){
        if(sortId == 1) {
            Collections.sort(branchList, ProductlistResponce.sortByRating);
            Collections.reverse(branchList);
        }
        else if(sortId == 2) {
            Collections.sort(branchList, ProductlistResponce.sortByDistance);
        }
        else {
            branchList.clear();
            branchList = branchListCopy;
        }

        prevMarker = null;

        mAdapter = new ProductsStoresViewPagerAdapter(MapActivity.this, storesArrayList, type, branchList, MapActivity.this);
        mViewPager.setAdapter(mAdapter);

        mAdapter.notifyDataSetChanged();
        setMarkers();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            mMap.setMyLocationEnabled(true);
        }
        setMarkers();
    }

    private void setMarkers(){
        mMap.clear();
        mMarkerList.clear();
        for (int j = 0; j < branchList.size(); j++) {
            LatLng newLatLngTemp = new LatLng(Double.parseDouble(branchList.get(j).getLatitude()), Double.parseDouble(branchList.get(j).getLongitude()));

            MarkerOptions options = new MarkerOptions();
            IconGenerator iconFactory = new IconGenerator(MapActivity.this);
            iconFactory.setRotation(0);
            iconFactory.setBackground(null);

            View view = View.inflate(MapActivity.this, R.layout.map_marker_text, null);
            TextView distance, rating;

            distance = (TextView) view.findViewById(R.id.distance);
            rating = (TextView) view.findViewById(R.id.rating);
            final DecimalFormat distanceFormat = new DecimalFormat("##,##,##0.0");
            rating.setText(distanceFormat.format(branchList.get(j).getReviews()));

            distance.setText(distanceFormat.format(branchList.get(j).getDistance()) + "" + getResources().getString(R.string.km));

            FrameLayout layout = (FrameLayout) view.findViewById(R.id.layout);

            final int finalJ = j;
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MapActivity.this, ProductStoresActivityStep1.class);
                    intent.putExtra("stores", storesArrayList);
                    intent.putExtra("pos", finalJ);
                    intent.putExtra("type",type);
                    if(type == 1) {
                        intent.putExtra("class", "product");
                    }
                    else if(type == 2) {
                        intent.putExtra("class", "service");
                    }
                    startActivity(intent);

                }
            });

            iconFactory.setContentView(view);

            options.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(Float.toString(branchList.get(j).getReviews()))));
            options.anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
            options.position(newLatLngTemp);
            options.snippet(String.valueOf(j));

            Marker mapMarker = mMap.addMarker(options);
            mMarkerList.add(mapMarker);
//            mDealMap.put(storesArrayList.get(j).getRatings(), storesArrayList.get(j));

        }

        if (branchList.size() > 0) {
            LatLng latlngOne = new LatLng(Double.parseDouble(branchList.get(0).getLatitude()), Double.parseDouble(branchList.get(0).getLongitude()));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlngOne, 14));
        }
        mViewPager.postDelayed(new Runnable() {

            @Override
            public void run() {
                mViewPager.setCurrentItem(0);
            }
        }, 100);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "getId: "+marker.getId());
        Log.d(TAG, "getTitle: "+marker.getTitle());
        Log.d(TAG, "getPosition: "+marker.getPosition());
        Log.d(TAG, "getZIndex: "+marker.getZIndex());
//        Intent intent = new Intent(MapActivity.this, ProductStoresActivityStep1.class);
//        intent.putExtra("stores", storesArrayList);
//        intent.putExtra("pos",marker.getId());
//        intent.putExtra("type",type);
//        if(type == 1) {
//            intent.putExtra("class", "product");
//        }
//        else if(type == 2) {
//            intent.putExtra("class", "service");
//        }
//        startActivity(intent);
        return false;
    }
}
