package com.cs.checkclickuser.Activites;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;

import com.cs.checkclickuser.Firebase.MyFirebaseMessagingService;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.cs.checkclickuser.Fragements.HomeFragment;
import com.cs.checkclickuser.Fragements.OrdersFragment;
import com.cs.checkclickuser.Fragements.ProductCategoryFragment;
import com.cs.checkclickuser.Fragements.ProfileFragment;
import com.cs.checkclickuser.Fragements.ServiceCategoryFragment;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    SharedPreferences userPrefs;
    public static int currentSelectedTab = 3; // 1 = product, 2 = service, 3 = home, 4 = orders, 5 = profile
    public static BottomNavigationView navigation;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_main);
        } else {
            setContentView(R.layout.activity_main_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        // Deep link functionality from vendor app
        Bundle args = new Bundle();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    args.putString("id", sharedText);
                } else {
                    args.putString("id", "");
                }
            }
        } else {
            args.putString("id", "");
        }

        try {
            Log.d("TAG", "onCreate main activity: "+getIntent().getStringExtra("url"));
            if (!getIntent().getStringExtra("url").equals("")) {
                args.putString("url", getIntent().getStringExtra("url"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        HomeFragment homeScreenFragment = new HomeFragment();
        homeScreenFragment.setArguments(args);
        fragmentTransaction.add(R.id.fragment_layout, homeScreenFragment);
        fragmentTransaction.commit();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(navigation);
        navigation.setItemIconTintList(null);
        navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);

        if (language.equalsIgnoreCase("Ar")) {
            TextView homeTitle = (TextView) navigation.findViewById(R.id.navigation_home).findViewById(R.id.largeLabel);
            TextView profileTitle = (TextView) navigation.findViewById(R.id.navigation_profile).findViewById(R.id.largeLabel);
            homeTitle.setTextSize(9);
            profileTitle.setTextSize(9);
        }

        showNotification();
    }

    private void showNotification(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", "2");
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        Notification noti = new NotificationCompat.Builder(MainActivity.this, "1")
                .setContentTitle("title")
                .setContentText("remoteMessage.getNotification().getBody()")
                .setSmallIcon(R.drawable.notification_icon)
                .setColor(Color.parseColor("#000000"))
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("remoteMessage.getNotification().getBody()"))
                .setContentIntent(resultPendingIntent).build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify((int) System.currentTimeMillis(), noti);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        // 1 = product, 2 = service, 3 = home, 4 = orders, 5 = profile
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_product:
//                    if (currentSelectedTab != 1) {
                    int currentapiVersion1 = Build.VERSION.SDK_INT;
                    if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                        | View.SYSTEM_UI_FLAG_VISIBLE);
                    }

                    currentSelectedTab = 1;
                    Fragment product_fragment = new ProductCategoryFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, product_fragment).commit();
                    return true;
//                    }
//                    else {
//                        return false;
//                    }
                case R.id.navigation_service:
//                    if (currentSelectedTab != 2) {
                    int currentapiVersion2 = Build.VERSION.SDK_INT;
                    if (currentapiVersion2 >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().getDecorView().setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                        | View.SYSTEM_UI_FLAG_VISIBLE);
                    }

                    currentSelectedTab = 2;
                    Fragment serviceFragment = new ServiceCategoryFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, serviceFragment).commit();
                    return true;
//                    }
//                    else {
//                        return false;
//                    }
                case R.id.navigation_home:
                    if (currentSelectedTab != 3) {
                        Fragment homeFragment = new HomeFragment();
                        Bundle args = new Bundle();
                        args.putString("id", "");
                        homeFragment.setArguments(args);
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, homeFragment).commit();
                        currentSelectedTab = 3;
                        return true;
                    } else {
                        return false;
                    }
                case R.id.navigation_order:
//                    if (currentSelectedTab != 4) {

                    if (userPrefs.getString("userId", "").equals("")) {
                        if (language.equalsIgnoreCase("En")) {
                            Constants.showTwoButtonAlertDialog(MainActivity.this);
                        } else {
                            Constants.showTwoButtonAlertDialog_arabic(MainActivity.this);
                        }

                        navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
                        return false;
                    } else {
                        int currentapiVersion4 = Build.VERSION.SDK_INT;
                        if (currentapiVersion4 >= Build.VERSION_CODES.LOLLIPOP) {
                            getWindow().getDecorView().setSystemUiVisibility(
                                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                                            | View.SYSTEM_UI_FLAG_VISIBLE);
                        }

                        Fragment ordersFragment = new OrdersFragment();
                        fragmentManager.beginTransaction().replace(R.id.fragment_layout, ordersFragment).commit();
                        currentSelectedTab = 4;
                        return true;
                    }
//                    }
//                    else {
//                        return false;
//                    }

                case R.id.navigation_profile:

                    Fragment profirfrgment = new ProfileFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_layout, profirfrgment).commit();
                    currentSelectedTab = 5;
                    return true;
            }
            return false;
        }
    };

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setPadding(0, 15, 0, 0);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    @Override
    public void onBackPressed() {
        if (currentSelectedTab == 3) {
            super.onBackPressed();
        }
        else {
            navigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
            currentSelectedTab = 3;
            Fragment homeFragment = new HomeFragment();
            Bundle args = new Bundle();
            args.putString("id", "");
            homeFragment.setArguments(args);
            fragmentManager.beginTransaction().replace(R.id.fragment_layout,homeFragment).commit();
        }
    }
}
