package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.cs.checkclickuser.Adapter.ProductDealsAdapter;
import com.cs.checkclickuser.Adapter.ProductShopAdapter;
import com.cs.checkclickuser.Adapter.ServcieDealsAdapter;
import com.cs.checkclickuser.Models.DealsResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.Models.ServiceDealsResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class ProductMaincatActivityStep2 extends AppCompatActivity {
    public ProductShopAdapter mshopadapter;
    public ProductDealsAdapter mdealsadapter;
    public ServcieDealsAdapter mServiceDealsAdapter;
    ImageView stroeback, back_btn, cart, favourite, notification;
    CircleImageView storeimage;
    RecyclerView productstorelist;
    CardView dealslist;
    TextView shop, deals;
    int storePos;
    String TAG = "TAG";
    ProductstoreResponce.Data storeArrayList;
    SharedPreferences userPrefs;
    View shopLine, dealsLine;
    int type = 1;
    boolean isFavUpdated = false;

    SharedPreferences LanguagePrefs;
    String language;

    private ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();
    private ArrayList<ProductlistResponce.JOtherBranch> branchArrayList = new ArrayList<>();
    public static ArrayList<DealsResponce.Deals> storeDealsList = new ArrayList<>();
    public static ArrayList<ServiceDealsResponse.Deals> serviceDealsList = new ArrayList<>();
    private ImageView cartStatusIcon, notifictionStatusIcon;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_main_cat);
        } else {
            setContentView(R.layout.product_main_cat_ar);
        }




        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        int currentapiVersion1 = Build.VERSION.SDK_INT;
        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        if (ProductStoresActivityStep1.branch_pos == -1) {
            productArrayList = (ArrayList<ProductlistResponce.Stores>) getIntent().getSerializableExtra("stores");
        } else {
            branchArrayList = (ArrayList<ProductlistResponce.JOtherBranch>) getIntent().getSerializableExtra("stores");
        }
        storePos = getIntent().getIntExtra("pos", 0);
        type = getIntent().getIntExtra("type", 1);

        stroeback = (ImageView) findViewById(R.id.storebackimage);
        storeimage = (CircleImageView) findViewById(R.id.ac_storepic);
        shop = (TextView) findViewById(R.id.shop);
        deals = (TextView) findViewById(R.id.deals);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        productstorelist = (RecyclerView) findViewById(R.id.product_listview);
        cart = (ImageView) findViewById(R.id.cart);
        favourite = (ImageView) findViewById(R.id.fav);
        notification = (ImageView) findViewById(R.id.noti);

        shopLine = (View) findViewById(R.id.shop_line);
        dealsLine = (View) findViewById(R.id.deals_line);

//        BranchColor branchColor = new BranchColor();
//        Constants.appColor = branchColor.BranchColor(productArrayList.get(storePos).getBranchColor());
        shop.setTextColor(Color.parseColor("#" + Constants.appColor));
        deals.setTextColor(Color.parseColor("#" + Constants.appColor));
        shopLine.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
        dealsLine.setBackgroundColor(Color.parseColor("#" + Constants.appColor));

        if (productArrayList.size() != 0) {
            if (productArrayList.get(storePos).getFavoriteId()) {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
            } else {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
            }

            Constants.cartCount = productArrayList.get(storePos).getCartItemCount();
            Constants.notificationCount = productArrayList.get(storePos).getNotificationCount();
            if (productArrayList.get(storePos).getCartItemCount() != 0) {
                cartStatusIcon.setVisibility(View.VISIBLE);
            }
            if (productArrayList.get(storePos).getNotificationCount() != 0) {
                notifictionStatusIcon.setVisibility(View.VISIBLE);
            }
        } else {
            if (branchArrayList.get(storePos).getFavorite()) {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
            } else {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
            }

            Constants.cartCount = branchArrayList.get(storePos).getCartItemCount();
            Constants.notificationCount = branchArrayList.get(storePos).getNotificationCount();
            if (branchArrayList.get(storePos).getCartItemCount() != 0) {
                cartStatusIcon.setVisibility(View.VISIBLE);
            }
            if (branchArrayList.get(storePos).getNotificationCount() != 0) {
                notifictionStatusIcon.setVisibility(View.VISIBLE);
            }
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductMaincatActivityStep2.this);
                } else {
                    Intent intent = new Intent(ProductMaincatActivityStep2.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {

                    Constants.showTwoButtonAlertDialog(ProductMaincatActivityStep2.this);

                } else {

                    new InsertStoreFav().execute();

                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {

                    Constants.showTwoButtonAlertDialog(ProductMaincatActivityStep2.this);

                } else {

                    Intent intent = new Intent(ProductMaincatActivityStep2.this, CartActivity.class);
                    startActivity(intent);

                }

            }
        });

        deals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopLine.setVisibility(View.INVISIBLE);
                dealsLine.setVisibility(View.VISIBLE);
                productstorelist.setVisibility(View.VISIBLE);
//                deals.setTextColor(Color.parseColor("#"+Constants.appColor));
//                shop.setTextColor(getResources().getColor(R.color.black));
                if (type == 1) {
                    if (CacheData.storeDealsList != null) {
                        setProductDealsFromCache();
                    } else {
                        new GetProductDealsApi().execute();
                    }
                } else {
                    if (CacheData.serviceDealsList != null) {
                        setServiceDealsFromCache();
                    } else {
                        new GetServiceDealsApi().execute();
                    }
                }
            }
        });

        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopLine.setVisibility(View.VISIBLE);
                dealsLine.setVisibility(View.INVISIBLE);
                productstorelist.setVisibility(View.VISIBLE);
//                deals.setTextColor(getResources().getColor(R.color.black));
//                shop.setTextColor(Color.parseColor("#"+Constants.appColor));
                if (CacheData.storeArrayList != null) {
                    setStoreListFromCache();
                } else {
                    new GetstoreApi().execute();
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CacheData.storeDealsList != null)
                    CacheData.storeDealsList = null;

                if (CacheData.serviceDealsList != null)
                    CacheData.serviceDealsList = null;

                if (CacheData.storeArrayList != null)
                    CacheData.storeArrayList = null;

                if (isFavUpdated) {
                    Intent intent = new Intent();
                    if (productArrayList.size() != 0) {
                        intent.putExtra("fav", productArrayList.get(storePos).getFavoriteId());

                    } else {
                        intent.putExtra("fav", productArrayList.get(storePos).getFavoriteId());
                    }
                    setResult(RESULT_OK, intent);
                }
                finish();
            }
        });

        CamomileSpinner spinner = (CamomileSpinner) findViewById(R.id.spinner);
        spinner.start();

        spinner.recreateWithParams(
                this,
                DialogUtils.getColor(this, R.color.black),
                120,
                true
        );

        if (productArrayList.size() != 0) {
            Glide.with(ProductMaincatActivityStep2.this)
                    .load(Constants.STORE_IMAGE_URL + productArrayList.get(storePos).getLogo())
                    .into(storeimage);
            Glide.with(ProductMaincatActivityStep2.this)
                    .load(STORE_IMAGE_URL + productArrayList.get(storePos).getBackground())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(stroeback);
//            Log.d(TAG, "imagesize: " + STORE_IMAGE_URL + productArrayList.get(storePos).getBackgroundImage());
        } else {
            Glide.with(ProductMaincatActivityStep2.this)
                    .load(Constants.STORE_IMAGE_URL + branchArrayList.get(storePos).getLogo())
                    .into(storeimage);
            Glide.with(ProductMaincatActivityStep2.this)
                    .load(STORE_IMAGE_URL + branchArrayList.get(storePos).getBackground())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(stroeback);
//            Log.d(TAG, "imagesize: " + STORE_IMAGE_URL + branchArrayList.get(storePos).getBackgroundImage());
        }

        if (CacheData.storeArrayList != null) {
            setStoreListFromCache();
        } else {
            new GetstoreApi().execute();
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            if (productArrayList.size() != 0) {
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
            } else {
                parentObj.put("BranchId", branchArrayList.get(storePos).getBranchId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private String prepareGetDealsJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            if (productArrayList.size() != 0) {
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
                parentObj.put("StoreId", productArrayList.get(storePos).getStoreId());
            } else {
                parentObj.put("BranchId", branchArrayList.get(storePos).getBranchId());
                parentObj.put("StoreId", branchArrayList.get(storePos).getStoreId());
            }
            parentObj.put("Sort", 1);
            parentObj.put("PageNumber", 1);
            parentObj.put("PageSize", 1000);
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private String prepareFavJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            if (productArrayList.size() != 0) {
                if (productArrayList.get(storePos).getFavoriteId()) {
                    parentObj.put("Type", IdConstants.TYPE_DELETE_FAV);
                } else {
                    parentObj.put("Type", IdConstants.TYPE_INSERT_FAV);
                }
            } else {
                if (branchArrayList.get(storePos).getFavorite()) {
                    parentObj.put("Type", IdConstants.TYPE_DELETE_FAV);
                } else {
                    parentObj.put("Type", IdConstants.TYPE_INSERT_FAV);
                }
            }
            parentObj.put("StatusId", IdConstants.STATUS_ID_STORE);
            if (productArrayList.size() != 0) {
                parentObj.put("Ids", productArrayList.get(storePos).getBranchId());
            } else {
                parentObj.put("Ids", branchArrayList.get(storePos).getBranchId());
            }
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareGetStoresJSON: " + parentObj.toString());
        return parentObj.toString();
    }

    private void setStoreListFromCache() {
        storeArrayList = CacheData.storeArrayList;
        if (productArrayList.size() != 0) {
            if (storeArrayList != null) {
                mshopadapter = new ProductShopAdapter(ProductMaincatActivityStep2.this, storeArrayList, productArrayList.get(storePos).getStoreId(), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
                productstorelist.setAdapter(mshopadapter);
            } else {
                productstorelist.setVisibility(View.GONE);
            }
        } else {
            if (storeArrayList != null) {
                mshopadapter = new ProductShopAdapter(ProductMaincatActivityStep2.this, storeArrayList, branchArrayList.get(storePos).getStoreId(), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
                productstorelist.setAdapter(mshopadapter);
            } else {
                productstorelist.setVisibility(View.GONE);
            }
        }
    }

    private void setProductDealsFromCache() {
        storeDealsList = CacheData.storeDealsList;
        if (productArrayList.size() != 0) {
            if (storeDealsList.size() > 0) {
                mdealsadapter = new ProductDealsAdapter(ProductMaincatActivityStep2.this, storeDealsList, productArrayList.get(storePos).getStoreId(), type, ProductMaincatActivityStep2.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                productstorelist.setAdapter(mdealsadapter);
                productstorelist.setVisibility(View.VISIBLE);
            } else {
                productstorelist.setVisibility(View.GONE);
            }
        } else {

            if (storeDealsList.size() > 0) {
                mdealsadapter = new ProductDealsAdapter(ProductMaincatActivityStep2.this, storeDealsList, branchArrayList.get(storePos).getStoreId(), type, ProductMaincatActivityStep2.this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                productstorelist.setAdapter(mdealsadapter);
                productstorelist.setVisibility(View.VISIBLE);
            } else {
                productstorelist.setVisibility(View.GONE);
            }

        }
    }

    private void setServiceDealsFromCache() {
        serviceDealsList = CacheData.serviceDealsList;
        if (productArrayList.size() != 0) {
            if (serviceDealsList.size() > 0) {
                mServiceDealsAdapter = new ServcieDealsAdapter(ProductMaincatActivityStep2.this, serviceDealsList, productArrayList.get(storePos).getStoreId(), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                productstorelist.setAdapter(mServiceDealsAdapter);
                productstorelist.setVisibility(View.VISIBLE);
            } else {
                productstorelist.setVisibility(View.GONE);
            }
        } else {
            if (serviceDealsList.size() > 0) {
                mServiceDealsAdapter = new ServcieDealsAdapter(ProductMaincatActivityStep2.this, serviceDealsList, branchArrayList.get(storePos).getStoreId(), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                productstorelist.setAdapter(mServiceDealsAdapter);
                productstorelist.setVisibility(View.VISIBLE);
            } else {
                productstorelist.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (CacheData.storeDealsList != null)
            CacheData.storeDealsList = null;

        if (CacheData.serviceDealsList != null)
            CacheData.serviceDealsList = null;

        if (CacheData.storeArrayList != null)
            CacheData.storeArrayList = null;

//        if (isFavUpdated) {
            Intent intent = new Intent();
            if (productArrayList.size() != 0) {
                intent.putExtra("fav", productArrayList.get(storePos).getFavoriteId());

            } else {
                intent.putExtra("fav", branchArrayList.get(storePos).getFavorite());
            }
            setResult(RESULT_OK, intent);
//        }
        super.onBackPressed();
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductMaincatActivityStep2.this);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductstoreResponce> call = apiService.getstorelist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductstoreResponce>() {
                @Override
                public void onResponse(Call<ProductstoreResponce> call, Response<ProductstoreResponce> response) {
                    Log.d(TAG, "product servies responce: " + response);

                    if (response.isSuccessful()) {
                        ProductstoreResponce stores = response.body();

                        if (stores.getStatus()) {
                            storeArrayList = stores.getData();
                            CacheData.storeArrayList = storeArrayList;
                        }
                        Log.d(TAG, "arry list size " + storeArrayList);
                    }

                    if (productArrayList.size() != 0) {
                        if (storeArrayList != null) {
                            mshopadapter = new ProductShopAdapter(ProductMaincatActivityStep2.this, storeArrayList, productArrayList.get(storePos).getStoreId(), type);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
                            productstorelist.setAdapter(mshopadapter);

                            Log.d(TAG, "array: " + storeArrayList);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }
                    } else {
                        if (storeArrayList != null) {
                            mshopadapter = new ProductShopAdapter(ProductMaincatActivityStep2.this, storeArrayList, branchArrayList.get(storePos).getStoreId(), type);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
                            productstorelist.setAdapter(mshopadapter);

                            Log.d(TAG, "array: " + storeArrayList);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductstoreResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private class GetProductDealsApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetDealsJSON();
            Constants.showLoadingDialog(ProductMaincatActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<DealsResponce> call = apiService.dealsresponce(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<DealsResponce>() {
                @Override
                public void onResponse(Call<DealsResponce> call, Response<DealsResponce> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        DealsResponce stores = response.body();

                        if (stores.getStatus()) {
                            storeDealsList = stores.getData().getDeals();
                            CacheData.storeDealsList = storeDealsList;
                        }
                        Log.d(TAG, "onResponse " + storeDealsList);
                    }

                    if (productArrayList.size() != 0) {
                        if (storeDealsList.size() > 0) {
                            mdealsadapter = new ProductDealsAdapter(ProductMaincatActivityStep2.this, storeDealsList, productArrayList.get(storePos).getStoreId(), type, ProductMaincatActivityStep2.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                            productstorelist.setAdapter(mdealsadapter);
                            productstorelist.setVisibility(View.VISIBLE);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }
                    } else {

                        if (storeDealsList.size() > 0) {
                            mdealsadapter = new ProductDealsAdapter(ProductMaincatActivityStep2.this, storeDealsList, branchArrayList.get(storePos).getStoreId(), type, ProductMaincatActivityStep2.this);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                            productstorelist.setAdapter(mdealsadapter);
                            productstorelist.setVisibility(View.VISIBLE);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<DealsResponce> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private class GetServiceDealsApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetDealsJSON();
            Constants.showLoadingDialog(ProductMaincatActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ServiceDealsResponse> call = apiService.serviceDealsResponce(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ServiceDealsResponse>() {
                @Override
                public void onResponse(Call<ServiceDealsResponse> call, Response<ServiceDealsResponse> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        ServiceDealsResponse stores = response.body();

                        if (stores.getStatus()) {
                            serviceDealsList = stores.getData().getDeals();
                            CacheData.serviceDealsList = serviceDealsList;
                        }
                    }

                    if (productArrayList.size() != 0) {
                        if (serviceDealsList.size() > 0) {
                            mServiceDealsAdapter = new ServcieDealsAdapter(ProductMaincatActivityStep2.this, serviceDealsList, productArrayList.get(storePos).getStoreId(), type);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                            productstorelist.setAdapter(mServiceDealsAdapter);
                            productstorelist.setVisibility(View.VISIBLE);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }
                    } else {
                        if (serviceDealsList.size() > 0) {
                            mServiceDealsAdapter = new ServcieDealsAdapter(ProductMaincatActivityStep2.this, serviceDealsList, branchArrayList.get(storePos).getStoreId(), type);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductMaincatActivityStep2.this);
                            productstorelist.setLayoutManager(new GridLayoutManager(ProductMaincatActivityStep2.this, 2));
//                        productstorelist.setLayoutManager(mLayoutManager);
                            productstorelist.setAdapter(mServiceDealsAdapter);
                            productstorelist.setVisibility(View.VISIBLE);
                        } else {
                            productstorelist.setVisibility(View.GONE);
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ServiceDealsResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareFavJSON();
            Constants.showLoadingDialog(ProductMaincatActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            isFavUpdated = true;
                            if (productArrayList.size() != 0) {
                                if (productArrayList.get(storePos).getFavoriteId()) {
                                    productArrayList.get(storePos).setFavoriteId(false);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                                } else {
                                    productArrayList.get(storePos).setFavoriteId(true);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                }
                            } else {
                                if (branchArrayList.get(storePos).getFavorite()) {
                                    branchArrayList.get(storePos).setFavorite(false);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                                } else {
                                    branchArrayList.get(storePos).setFavorite(true);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                }
                            }

                            Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductMaincatActivityStep2.this);
                        }
                    } else {
                        Toast.makeText(ProductMaincatActivityStep2.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductMaincatActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductMaincatActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (Constants.notificationCount != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }
    }
}
