package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.ProductVariantImageAdapter;
import com.cs.checkclickuser.Adapter.Variants1Adapter;
import com.cs.checkclickuser.Adapter.Variants2Adapter;
import com.cs.checkclickuser.Adapter.Variants3Adapter;
import com.cs.checkclickuser.Models.AddToCartResponse;
import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.Models.UrlEncryptResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BranchColor;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.CustomGridView;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductVariantsActivityStep5 extends AppCompatActivity {

    private ImageView backBtn, plusBtn, minusBtn, cart, fav, notification,  favIcon, shareIcon, logo;
    private ViewPager imagesViewPager;
    private TextView itemName, imagesCount;
    private LinearLayout variant1Layout, variant2Layout, variant3Layout;
    private LinearLayout popupVariant1Layout, popupVariant2Layout, popupVariant3Layout;
    private TextView variant1Title, variant2Title, variant3Title;
    private TextView popupVariant1Title, popupVariant2Title, popupVariant3Title;
    private TextView variant1, variant2, variant3;
    private TextView discountedPrice, price, itemQty, stockStatus;
    private Button popupDoneBtn, addToCartBtn;
    private String TAG = "TAG";
    SharedPreferences userPrefs;
    private int productPos;
    public static String variant1Pos, variant2Pos, variant3Pos;
    private LinearLayout variantBottomView;
    private ProductVariantImageAdapter mAdapter;

    private CustomGridView mVariant1GridView;
    private CustomGridView mVariant2GridView;
    private CustomGridView mVariant3GridView;

    private Variants1Adapter mVariant1Adapter;
    private Variants2Adapter mVariant2Adapter;
    private Variants3Adapter mVariant3Adapter;

    private ArrayList<ProductVariantResponse.ProductList> productLists = new ArrayList<>();
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();

    private MaterialRatingBar mRatingBar;
    TextView reviews;

    LinearLayout ShippingCountriesLayout;
    TextView mCountryName;

    private int cartFlag = 1;
    private static int GET_SHIPPING_DETAILS = 1;
    private AdressCountryResponce.CityList selectedCity = new AdressCountryResponce.CityList();
    UrlEncryptResponse.Data encryptData;
    File file12;

    private AlertDialog customDialog;
    private ImageView cartStatusIcon, notifictionStatusIcon;

    private static final int STORAGE_REQUEST = 2;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_variants);

        } else {
            setContentView(R.layout.product_variants_ar);

        }

        productLists = (ArrayList<ProductVariantResponse.ProductList>) getIntent().getSerializableExtra("productList");
        variantsLists = (ArrayList<ProductVariantResponse.VariantsList>) getIntent().getSerializableExtra("variantList");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        plusBtn = (ImageView) findViewById(R.id.qty_plus);
        logo = (ImageView) findViewById(R.id.logo);
        minusBtn = (ImageView) findViewById(R.id.qty_minus);
        shareIcon = (ImageView) findViewById(R.id.share_product);
        fav = (ImageView) findViewById(R.id.fav);
        notification = (ImageView) findViewById(R.id.noti);
        mRatingBar = (MaterialRatingBar) findViewById(R.id.ratingbar);

        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        imagesViewPager = (ViewPager) findViewById(R.id.images_viewpager);

        popupVariant1Layout = (LinearLayout) findViewById(R.id.popup_varinat1_layout);
        popupVariant2Layout = (LinearLayout) findViewById(R.id.popup_varinat2_layout);
        popupVariant3Layout = (LinearLayout) findViewById(R.id.popup_varinat3_layout);

        variant1Layout = (LinearLayout) findViewById(R.id.variant1_layout);
        variant2Layout = (LinearLayout) findViewById(R.id.variant2_layout);
        variant3Layout = (LinearLayout) findViewById(R.id.variant3_layout);
        variantBottomView = (LinearLayout) findViewById(R.id.variants_bottom_view);

        itemName = (TextView) findViewById(R.id.item_name);
        imagesCount = (TextView) findViewById(R.id.images_count);
        discountedPrice = (TextView) findViewById(R.id.discounted_price);
        price = (TextView) findViewById(R.id.price);
        itemQty = (TextView) findViewById(R.id.item_qty);
        stockStatus = (TextView) findViewById(R.id.stock_status);

        variant1 = (TextView) findViewById(R.id.variant1);
        variant2 = (TextView) findViewById(R.id.variant2);
        variant3 = (TextView) findViewById(R.id.variant3);
        variant1Title = (TextView) findViewById(R.id.variant_title1);
        variant2Title = (TextView) findViewById(R.id.variant_title2);
        variant3Title = (TextView) findViewById(R.id.variant_title3);
        popupVariant1Title = (TextView) findViewById(R.id.popuop_variant1_title);
        popupVariant2Title = (TextView) findViewById(R.id.popuop_variant2_title);
        popupVariant3Title = (TextView) findViewById(R.id.popuop_variant3_title);

        popupDoneBtn = (Button) findViewById(R.id.bt_done);
        addToCartBtn = (Button) findViewById(R.id.bt_continue);

        mVariant1GridView = (CustomGridView) findViewById(R.id.popup_variant1_gridview);
        mVariant2GridView = (CustomGridView) findViewById(R.id.popup_variant2_gridview);
        mVariant3GridView = (CustomGridView) findViewById(R.id.popup_variant3_gridview);

        cart = (ImageView) findViewById(R.id.cart);

        reviews = (TextView) findViewById(R.id.reviews);
        mCountryName = (TextView) findViewById(R.id.country_name);

        ShippingCountriesLayout = (LinearLayout) findViewById(R.id.shipping_countries_layout);

        getProductPos();

        BranchColor branchColor = new BranchColor();
        Constants.appColor = branchColor.BranchColor(productLists.get(productPos).getBranchColor());

        variant1Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        variant2Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        variant3Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        popupVariant1Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        popupVariant2Title.setTextColor(Color.parseColor("#"+Constants.appColor));
        popupVariant3Title.setTextColor(Color.parseColor("#"+Constants.appColor));

        int icgridViewIconId = getResources().getIdentifier("plus_"+Constants.appColor, "drawable", getPackageName());
        plusBtn.setImageDrawable(getResources().getDrawable(icgridViewIconId));

        int iclistViewIconId = getResources().getIdentifier("minus_"+Constants.appColor, "drawable", getPackageName());
        minusBtn.setImageDrawable(getResources().getDrawable(iclistViewIconId));
        addToCartBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        popupDoneBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetBranchDetailsApi().execute();
            }
        });

        shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    new GetEncrytptURLApi().execute();
                }
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductVariantsActivityStep5.this);
                } else {
                    Intent intent = new Intent(ProductVariantsActivityStep5.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userPrefs.getString("userId", "").equals("")) {

                    Constants.showTwoButtonAlertDialog(ProductVariantsActivityStep5.this);

                } else {

                    Intent intent = new Intent(ProductVariantsActivityStep5.this, CartActivity.class);
                    startActivity(intent);

                }
            }
        });

        ShippingCountriesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductVariantsActivityStep5.this, ShippingCountriesActivity.class);
                startActivityForResult(intent, GET_SHIPPING_DETAILS);
            }
        });

        variant1Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        variant2Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        variant3Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                variantBottomView.setVisibility(View.VISIBLE);
            }
        });

        popupDoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchProductPosUsingVariantsPos();
                variantBottomView.setVisibility(View.GONE);
            }
        });

        itemQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int qty = Integer.parseInt(editable.toString());
                if (qty > productLists.get(productPos).getMaxCartQty() ||
                        qty > productLists.get(productPos).getAvailableQuantity()) {
                    plusBtn.setAlpha(0.3f);
                    plusBtn.setEnabled(false);
                }
                else  {
                    plusBtn.setAlpha(1.0f);
                    plusBtn.setEnabled(true);
                }

                if (qty < productLists.get(productPos).getMinCartQty()) {
                    minusBtn.setAlpha(0.3f);
                    minusBtn.setEnabled(false);
                }
                else {
                    minusBtn.setAlpha(1.0f);
                    minusBtn.setEnabled(true);
                }
            }
        });

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(itemQty.getText().toString());
                qty = qty + productLists.get(productPos).getMinCartQty();
//                qty = qty + 1;
                if (qty <= productLists.get(productPos).getMaxCartQty() &&
                        qty <= productLists.get(productPos).getAvailableQuantity()) {
                    itemQty.setText("" + qty);
                    if (language.equals("En")){
                    stockStatus.setText(getString(R.string.str_in_stock));
                    }else {
                        stockStatus.setText(getString(R.string.str_in_stock_ar));
                    }

                    stockStatus.setTextColor(Color.parseColor("#006633"));
                    addToCartBtn.setEnabled(true);
                    addToCartBtn.setAlpha(1.0f);
                }
                else {
                    itemQty.setText("" + qty);
                    if (language.equals("En")){
                        stockStatus.setText(getString(R.string.str_out_of_stock));
                    }else {
                        stockStatus.setText(getString(R.string.str_out_of_stock_ar));
                    }

                    stockStatus.setTextColor(getResources().getColor(R.color.red));
                    addToCartBtn.setEnabled(false);
                    addToCartBtn.setAlpha(0.5f);
                }
            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(itemQty.getText().toString());
                if (qty != productLists.get(productPos).getMinCartQty()) {
                    qty = qty - productLists.get(productPos).getMinCartQty();
//                    qty = qty - 1;
                    itemQty.setText("" + qty);

                    if (qty <= productLists.get(productPos).getAvailableQuantity()) {
                        if (language.equals("En")){
                            stockStatus.setText(getString(R.string.str_in_stock));
                        }else {
                            stockStatus.setText(getString(R.string.str_in_stock_ar));
                        }

                        stockStatus.setTextColor(Color.parseColor("#006633"));
                        addToCartBtn.setEnabled(true);
                        addToCartBtn.setAlpha(1.0f);
                    }
                    else {
                        if (language.equals("En")){
                            stockStatus.setText(getString(R.string.str_out_of_stock));
                        }else {
                            stockStatus.setText(getString(R.string.str_out_of_stock_ar));
                        }

                        stockStatus.setTextColor(getResources().getColor(R.color.red));
                        addToCartBtn.setEnabled(false);
                        addToCartBtn.setAlpha(0.5f);
                    }
                }
            }
        });

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userPrefs.getString("userId", "").equals("")) {

                    if (language.equals("En")){
                        Constants.showTwoButtonAlertDialog(ProductVariantsActivityStep5.this);
                    }else {
                        Constants.showTwoButtonAlertDialog_arabic(ProductVariantsActivityStep5.this);
                    }


                } else {

                    int qty = Integer.parseInt(itemQty.getText().toString());
                    Log.d(TAG, "onClick: " + productLists.get(productPos).getMinCartQty());
                    if (qty >= productLists.get(productPos).getMinCartQty()) {
                        new AddToCart().execute();
                    } else {
                        if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_items),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                        }
                        else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_items_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                        }

                    }

                }
            }
        });

        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_fav_icon, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);

        favIcon = (ImageView) bubbleLayout.findViewById(R.id.fav_icon);
        ImageView infoIcon = (ImageView) bubbleLayout.findViewById(R.id.info_icon);

        if (productLists.get(productPos).getFavorite() ||
                (CacheData.favProductId != null && CacheData.favProductId.contains(productLists.get(productPos).getProductSkuId()))) {
            favIcon.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
        } else {
            favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favblack));
        }

        favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: favIcon");
                new InsertStoreFav().execute();
            }
        });

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] location = new int[2];
                v.getLocationInWindow(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, (location[0]), v.getHeight() + location[1]);
            }
        });

        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductVariantsActivityStep5.this, ProductInfoActivity.class);
                intent.putExtra("Arraylist",productLists);
                intent.putExtra("itempos",productPos);
                intent.putExtra("StoreId",getIntent().getIntExtra("storeId", 0));
                intent.putExtra("BranchId",getIntent().getIntExtra("branchId", 0));
                intent.putExtra("type", IdConstants.PRODUCT);
                startActivity(intent);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        discountedPrice.setPaintFlags(discountedPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    private void initData() {
        mAdapter = new ProductVariantImageAdapter(ProductVariantsActivityStep5.this, productLists.get(productPos).getJImages());
        imagesViewPager.setAdapter(mAdapter);

        Glide.with(this)
                .load(Constants.STORE_IMAGE_URL + productLists.get(0).getLogoCopy())
                .into(logo);

        if (productLists.get(0).getCartItemCount() != 0 ||
                (Constants.cartCount != 0)) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (productLists.get(0).getNotificationCount() != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }

        if(language.equals("En")){
            itemName.setText(productLists.get(productPos).getProductNameEn());
        }
        else {
            itemName.setText(productLists.get(productPos).getProductNameAr());
        }

        imagesCount.setText(productLists.get(productPos).getJImages().size() + " " + getResources().getString(R.string.images));
        discountedPrice.setText("SAR " + Constants.priceFormat1.format(productLists.get(productPos).getSellingPrice()));
        price.setText("SAR " + Constants.priceFormat1.format(productLists.get(productPos).getPrice()));

        mRatingBar.setRating(productLists.get(productPos).getAvgUserReviews());
        if (productLists.get(productPos).getUserReviews() == 0) {
           if (language.equals("En")){
            reviews.setText("(" + getResources().getString(R.string.no_reviews) +")");
           }else {
               reviews.setText("(" + getResources().getString(R.string.no_reviews_ar) +")");
           }

        } else {
            if (language.equals("En")){
            reviews.setText("(" + productLists.get(productPos).getUserReviews() + " " + getResources().getString(R.string.reviews) + ")");
            }else {
                 reviews.setText("(" + productLists.get(productPos).getUserReviews() + " " + getResources().getString(R.string.reviews_ar) + ")");
            }

        }

        if (productLists.get(productPos).getMinCartQty() <=
                productLists.get(productPos).getAvailableQuantity()) {
           if (language.equals("En")){
            stockStatus.setText(getString(R.string.str_in_stock));
           }else {
                stockStatus.setText(getString(R.string.str_in_stock_ar));
           }

            stockStatus.setTextColor(Color.parseColor("#006633"));
            addToCartBtn.setEnabled(true);
            addToCartBtn.setAlpha(1.0f);
        } else {
              if (language.equals("En")){
                  stockStatus.setText(getString(R.string.str_out_of_stock));
           }else {
                  stockStatus.setText(getString(R.string.str_out_of_stock_ar));
           }

            stockStatus.setTextColor(getResources().getColor(R.color.red));
            addToCartBtn.setEnabled(false);
            addToCartBtn.setAlpha(0.5f);
        }

        if (productLists.get(productPos).getCartQuantity() != 0) {
            itemQty.setText("" + productLists.get(productPos).getCartQuantity());
        }
        else {
            itemQty.setText("" + productLists.get(productPos).getMinCartQty());
        }

        if (productLists.get(productPos).getPrice() == productLists.get(productPos).getSellingPrice()) {
            discountedPrice.setVisibility(View.GONE);
        }

        if (variantsLists.size() == 1) {
//            Variants available = 1
            variant2Layout.setVisibility(View.INVISIBLE);
            variant3Layout.setVisibility(View.GONE);
            variant1Title.setText(variantsLists.get(0).getVariantNameEn());
            variant1.setText(productLists.get(productPos).getJProductVariant().get(0).getValueEn());
        } else if (variantsLists.size() == 2) {
//            Variants available = 2
            variant3Layout.setVisibility(View.GONE);
            if (language.equals("En")){
                variant1Title.setText(variantsLists.get(0).getVariantNameEn());
                variant1.setText(productLists.get(productPos).getJProductVariant().get(0).getValueEn());
                variant2Title.setText(variantsLists.get(1).getVariantNameEn());
                variant2.setText(productLists.get(productPos).getJProductVariant().get(1).getValueEn());

            }else {
                variant1Title.setText(variantsLists.get(0).getVariantNameAr());
                variant1.setText(productLists.get(productPos).getJProductVariant().get(0).getValueAr());
                variant2Title.setText(variantsLists.get(1).getVariantNameAr());
                variant2.setText(productLists.get(productPos).getJProductVariant().get(1).getValueAr());

            }

            for (int j = 0; j < variantsLists.get(1).getJvariants().size(); j++) {
                if (variantsLists.get(1).getJvariants().get(j).getValueEn()
                        .equals(productLists.get(productPos).getJProductVariant().get(1).getValueEn())) {
                    variant2Pos = variantsLists.get(1).getJvariants().get(j).getValueEn();
                }
            }
        } else if (variantsLists.size() == 3) {
//            Variants available = 3
            if (language.equals("En")){
                variant1Title.setText(variantsLists.get(0).getVariantNameEn());
                variant1.setText(productLists.get(productPos).getJProductVariant().get(0).getValueEn());
                variant2Title.setText(variantsLists.get(1).getVariantNameEn());
                variant2.setText(productLists.get(productPos).getJProductVariant().get(1).getValueEn());
                variant3Title.setText(variantsLists.get(2).getVariantNameEn());
                variant3.setText(productLists.get(productPos).getJProductVariant().get(2).getValueEn());
            }else {
                variant1Title.setText(variantsLists.get(0).getVariantNameAr());
                variant1.setText(productLists.get(productPos).getJProductVariant().get(0).getValueAr());
                variant2Title.setText(variantsLists.get(1).getVariantNameAr());
                variant2.setText(productLists.get(productPos).getJProductVariant().get(1).getValueAr());
                variant3Title.setText(variantsLists.get(2).getVariantNameAr());
                variant3.setText(productLists.get(productPos).getJProductVariant().get(2).getValueAr());
            }


            for (int j = 0; j < variantsLists.get(1).getJvariants().size(); j++) {
                if (variantsLists.get(1).getJvariants().get(j).getValueEn()
                        .equals(productLists.get(productPos).getJProductVariant().get(1).getValueEn())) {
                    variant2Pos = variantsLists.get(1).getJvariants().get(j).getValueEn();
                }
            }

            for (int j = 0; j < variantsLists.get(2).getJvariants().size(); j++) {
                if (variantsLists.get(2).getJvariants().get(j).getValueEn()
                        .equals(productLists.get(productPos).getJProductVariant().get(2).getValueEn())) {
                    variant3Pos = variantsLists.get(2).getJvariants().get(j).getValueEn();
                }
            }
        }

        for (int j = 0; j < variantsLists.get(0).getJvariants().size(); j++) {
            if (variantsLists.get(0).getJvariants().get(j).getValueEn()
                    .equals(productLists.get(productPos).getJProductVariant().get(0).getValueEn())) {
                variant1Pos = variantsLists.get(0).getJvariants().get(j).getValueEn();
            }
        }

        if (favIcon != null) {
            if (productLists.get(productPos).getFavorite() ||
                    (CacheData.favProductId != null && CacheData.favProductId.contains(productLists.get(productPos).getProductSkuId()))) {
                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
            } else {
                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favblack));
            }
        }

        setVariantsLists();
    }

    private void setVariantsLists() {
        if (variantsLists.size() == 1) {
            if (language.equals("En")){
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameEn());
                mVariant1Adapter = new Variants1Adapter(ProductVariantsActivityStep5.this, variantsLists.get(0).getJvariants());
                mVariant1GridView.setAdapter(mVariant1Adapter);
            }else {
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameAr());
                mVariant1Adapter = new Variants1Adapter(ProductVariantsActivityStep5.this, variantsLists.get(0).getJvariants());
                mVariant1GridView.setAdapter(mVariant1Adapter);
            }


            popupVariant2Layout.setVisibility(View.GONE);
            popupVariant3Layout.setVisibility(View.GONE);
        } else if (variantsLists.size() == 2) {
            if (language.equals("En")){
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameEn());
                popupVariant2Title.setText(variantsLists.get(1).getVariantNameEn());
            }else {
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameAr());
                popupVariant2Title.setText(variantsLists.get(1).getVariantNameAr());
            }

            mVariant1Adapter = new Variants1Adapter(ProductVariantsActivityStep5.this, variantsLists.get(0).getJvariants());
            mVariant1GridView.setAdapter(mVariant1Adapter);


            mVariant2Adapter = new Variants2Adapter(ProductVariantsActivityStep5.this, variantsLists.get(1).getJvariants());
            mVariant2GridView.setAdapter(mVariant2Adapter);

            popupVariant3Layout.setVisibility(View.GONE);
        } else if (variantsLists.size() == 3) {
            if (language.equals("En")){
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameEn());
                popupVariant2Title.setText(variantsLists.get(1).getVariantNameEn());
                popupVariant3Title.setText(variantsLists.get(2).getVariantNameEn());
            }else {
                popupVariant1Title.setText(variantsLists.get(0).getVariantNameAr());
                popupVariant2Title.setText(variantsLists.get(1).getVariantNameAr());
                popupVariant3Title.setText(variantsLists.get(2).getVariantNameAr());
            }

            mVariant1Adapter = new Variants1Adapter(ProductVariantsActivityStep5.this, variantsLists.get(0).getJvariants());
            mVariant1GridView.setAdapter(mVariant1Adapter);


            mVariant2Adapter = new Variants2Adapter(ProductVariantsActivityStep5.this, variantsLists.get(1).getJvariants());
            mVariant2GridView.setAdapter(mVariant2Adapter);


            mVariant3Adapter = new Variants3Adapter(ProductVariantsActivityStep5.this, variantsLists.get(2).getJvariants());
            mVariant3GridView.setAdapter(mVariant3Adapter);
        }
    }

    private void fetchProductPosUsingVariantsPos() {
        boolean variantsMatched = false;
        if (variantsLists.size() == 1) {
            for (int i = 0; i < productLists.size(); i++) {
                if (productLists.get(i).getJProductVariant().get(0).getValueEn().equals(variant1Pos)) {
                    productPos = i;
                    variantsMatched = true;
                    break;
                }
            }
            variant1.setText(variant1Pos);
        } else if (variantsLists.size() == 2) {
            for (int i = 0; i < productLists.size(); i++) {
                if (productLists.get(i).getJProductVariant().get(0).getValueEn().equals(variant1Pos) &&
                        productLists.get(i).getJProductVariant().get(1).getValueEn().equals(variant2Pos)) {
                    productPos = i;
                    variantsMatched = true;
                    break;
                }
            }
            variant1.setText(variant1Pos);
            variant2.setText(variant2Pos);
        } else if (variantsLists.size() == 3) {
            for (int i = 0; i < productLists.size(); i++) {
                if (productLists.get(i).getJProductVariant().get(0).getValueEn().equals(variant1Pos) &&
                        productLists.get(i).getJProductVariant().get(1).getValueEn().equals(variant2Pos) &&
                        productLists.get(i).getJProductVariant().get(2).getValueEn().equals(variant3Pos)) {
                    productPos = i;
                    variantsMatched = true;
                    break;
                }
            }
            variant1.setText(variant1Pos);
            variant2.setText(variant2Pos);
            variant3.setText(variant3Pos);
        }
        if (variantsMatched) {
            initData();
        }
        else {
            discountedPrice.setText("SAR 0.00");
            price.setText("SAR 0.00");

            mRatingBar.setRating(0);
            if (language.equals("En")){
            reviews.setText("(" + getResources().getString(R.string.no_reviews) + ")");
            stockStatus.setText(getString(R.string.str_out_of_stock));
            }else {
                reviews.setText("(" + getResources().getString(R.string.no_reviews_ar) + ")");
                stockStatus.setText(getString(R.string.str_out_of_stock_ar));
            }


            stockStatus.setTextColor(getResources().getColor(R.color.red));
            addToCartBtn.setEnabled(false);
            addToCartBtn.setAlpha(0.5f);
            discountedPrice.setVisibility(View.GONE);
        }
    }

    private class AddToCart extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAddToCartJSON();
            Constants.showLoadingDialog(ProductVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AddToCartResponse> call = apiService.addToCart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    Log.d(TAG, "vonResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getStatusmessage().equals("Success")) {
//                            finish();
                            Constants.closeLoadingDialog();
                            showSuccessDialog();
                            Constants.cartCount = 1;
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (failureResponse.equals("0")) {
                                showClearCartDialog();
                            }
                            else {
                                if (language.equals("En")){
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                                }else {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), ProductVariantsActivityStep5.this);
                                }

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equals("En")){
                        Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareAddToCartJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("StockQuantity", itemQty.getText().toString());
            parentObj.put("ProductInventoryId", productLists.get(productPos).getProductInventoryId());
            parentObj.put("StoreId", getIntent().getIntExtra("storeId", 0));
            parentObj.put("OrderType", 1);
            parentObj.put("flagId", cartFlag);
            parentObj.put("BranchId", getIntent().getIntExtra("branchId", 0));
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            cartFlag = 1;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private void getProductPos() {
        for (int i = 0; i < productLists.size(); i++) {
            if (productLists.get(i).getProductSkuId().equals(getIntent().getStringExtra("skuid"))) {
                productPos = i;
                break;
            }
        }
        initData();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(ProductVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            JSONObject parentObj = new JSONObject();
            try {
                if (productLists.get(productPos).getFavorite()) {
                    parentObj.put("Type", 3);
                } else {
                    parentObj.put("Type", 2);
                }
                parentObj.put("StatusId", "1");
                parentObj.put("Ids", productLists.get(productPos).getProductSkuId());
                parentObj.put("UserId", userPrefs.getString("userId", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if (productLists.get(productPos).getFavorite()) {
                                productLists.get(productPos).setFavorite(false);
                                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.favblack));
                                if (CacheData.favProductId != null && CacheData.favProductId.contains(productLists.get(productPos).getProductSkuId())) {
                                    CacheData.favProductId.remove(productLists.get(productPos).getProductSkuId());
                                }
                            } else {
                                productLists.get(productPos).setFavorite(true);
                                favIcon.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                CacheData.favProductId.add(productLists.get(productPos).getProductSkuId());

                            }

                            Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                        }
                    } else {
                        if (language.equals("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, response.message(), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, response.message(), Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_SHIPPING_DETAILS && resultCode == RESULT_OK) {
//            selectedCity = (AdressCountryResponce.CityList) data.getSerializableExtra("country");
            mCountryName.setText(data.getStringExtra("city"));
        }
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProductVariantsActivityStep5.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:
                    new GetEncrytptURLApi().execute();
                break;
        }
    }

    private class GetEncrytptURLApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareURLEncryptJSON();
            Constants.showLoadingDialog(ProductVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UrlEncryptResponse> call = apiService.UrlEncrypt(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UrlEncryptResponse>() {
                @Override
                public void onResponse(Call<UrlEncryptResponse> call, Response<UrlEncryptResponse> response) {
                    Log.d(TAG, "vonResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getMessage().equals("Success")) {
                            encryptData = response.body().getData();
                            if (canAccessStorage() && productLists.get(productPos).getImages() != null &&
                                    productLists.get(productPos).getImages().length() > 0) {
                                new DownloadImage().execute();
                            }
                            else {
                                prepareEncryptURL();
                            }
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (language.equalsIgnoreCase("En")){
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                            }else {
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ProductVariantsActivityStep5.this);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<UrlEncryptResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareURLEncryptJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("ProductNameEn", productLists.get(productPos).getProductNameEn());
            parentObj.put("BranchId", getIntent().getIntExtra("branchId", 0));
            parentObj.put("Id", productLists.get(productPos).getProductInventoryId());
            parentObj.put("BranchSubCategoryId", productLists.get(productPos).getSubCatId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private void prepareEncryptURL(){
        Constants.closeLoadingDialog();
        String encyptedURL = Constants.UNIVERSAL_LINKS_URL +
                encryptData.getStoreName() + "/" + productLists.get(productPos).getMainCatEn() + "/" +
                productLists.get(productPos).getSubCatEn() + "/" + encryptData.getStoreType() + "/" + encryptData.getEncryId();

        String productName = productLists.get(productPos).getProductNameEn();
        String variants = "";

        if (variantsLists.size() == 1) {
//            Variants available = 1
            variants = variant1Pos;
        } else if (variantsLists.size() == 2) {
//            Variants available = 2
            variants = variant1Pos + "," + variant2Pos;
        } else if (variantsLists.size() == 3) {
//            Variants available = 3
            variants = variant1Pos + "," + variant2Pos + "," + variant3Pos;
        }

        encyptedURL = encyptedURL.replace(" ", "-");
        String shareBody = productName + "(" + variants + ") "+ getResources().getString(R.string.at_SAR) +" " + Constants.priceFormat1.format(productLists.get(productPos).getPrice())+" \n\n "+encyptedURL;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        if (file12 != null) {
            File outputFile = new File(Environment.getExternalStorageDirectory()+"/CheckClik/"+file12.getName());
            Uri photoURI = FileProvider.getUriForFile(ProductVariantsActivityStep5.this,
                    getApplicationContext().getPackageName() + ".my.package.name.provider", outputFile);
            sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            sharingIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        }
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, productName);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share using.."));
    }

    private class DownloadImage extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "CheckClik");
                folder.mkdir();
                file12 = new File(folder, "CheckClik_"+System.currentTimeMillis()+".png");
                try {
                    file12.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                FileOutputStream f = new FileOutputStream(file12);

                URL u = new URL(Constants.PRODUCTS_IMAGE_URL+productLists.get(productPos).getJImages().get(0).getImage());
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
//                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d(TAG, "doInBackground: "+buffer.length);
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Constants.closeLoadingDialog();

            if (file12.exists()) {
                prepareEncryptURL();
            }
        }
    }

    private void showSuccessDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        if (language.equals("En")){
            int layout = R.layout.cart_success_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

        }else {
            int layout = R.layout.cart_success_dialog_ar;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // close this activity
                customDialog.dismiss();
                finish();
            }
        }, 2000);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.65;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showClearCartDialog(){

        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductVariantsActivityStep5.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equals("En")){
        title.setText("" + getResources().getString(R.string.replace_cart_products));
        yes.setText("" + getResources().getString(R.string.yes));
        no.setText("" + getResources().getString(R.string.no));
        desc.setText("" + getResources().getString(R.string.your_cart_contains_products));
        }else {
            title.setText("" + getResources().getString(R.string.replace_cart_products_ar));
        yes.setText("" + getResources().getString(R.string.yes_ar));
        no.setText("" + getResources().getString(R.string.no_ar));
        desc.setText("" + getResources().getString(R.string.your_cart_contains_products_ar));

        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartFlag = 0;
                new AddToCart().execute();
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }

    private class GetBranchDetailsApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetBranchDetailsJSON();
            Constants.showLoadingDialog(ProductVariantsActivityStep5.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchDetails> call = apiService.getBranchDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchDetails>() {
                @Override
                public void onResponse(Call<BranchDetails> call, Response<BranchDetails> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        BranchDetails branchResponse = response.body();
                        if (branchResponse.getData().getBranch() != null &&
                                branchResponse.getData().getBranch().size() > 0) {
                            Intent intent = new Intent(ProductVariantsActivityStep5.this, ProductStoresActivityStep1.class);
                            intent.putExtra("stores", branchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", branchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "vendor_app_intent");
                            startActivity(intent);
                        }
                        else {
                            if (language.equals("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, getResources().getString(R.string.store_details_not_available), Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductVariantsActivityStep5.this, getResources().getString(R.string.store_details_not_available_ar), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchDetails> call, Throwable t) {
                    Log.e("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductVariantsActivityStep5.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductVariantsActivityStep5.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetBranchDetailsJSON(){
        int branchId = 0;
        try {
            JSONArray relatedProductsArray = new JSONArray(productLists.get(productPos).getRelatedProducts());
            JSONObject relatedProductsObj = relatedProductsArray.getJSONObject(0);
            branchId = relatedProductsObj.getInt("BranchId");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId",  branchId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj.toString());
        return parentObj.toString();
    }
}
