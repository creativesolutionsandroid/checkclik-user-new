package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.checkclickuser.Adapter.OfferProductsAdapter;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;

public class OfferProductsActivity extends AppCompatActivity {

    ImageView back_btn;
    String TAG = "TAG";
    String  userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    TextView categoryName;
    RecyclerView listView;
    EditText productSearch;

    private ArrayList<HomePageResponse.Offers> offers = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers> offersFiltered = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers2> offers2 = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers2> offers2Filtered = new ArrayList<>();
    private int id;
    private OfferProductsAdapter mAdapter;
    private boolean itemPresentInOffers;
    private ImageView cartStatusIcon, notifictionStatusIcon;


    SharedPreferences LanguagePrefs;
    String language;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_offers_product);
        } else {
            setContentView(R.layout.activity_offers_product_ar);
        }


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        id = getIntent().getIntExtra("id", 0);
        offers = (ArrayList<HomePageResponse.Offers>) getIntent().getSerializableExtra("offers");
        offers2 = (ArrayList<HomePageResponse.Offers2>) getIntent().getSerializableExtra("offers2");

        categoryName = (TextView) findViewById(R.id.product_category);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        listView = (RecyclerView) findViewById(R.id.product_listview);
        productSearch = (EditText) findViewById(R.id.product_search);

        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (Constants.notificationCount != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }

        categoryName.setText(getIntent().getStringExtra("name"));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        productSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length() > 0) {
                    String searchText = editable.toString();
                    if(itemPresentInOffers) {
                        filterOffers(searchText);
                    }
                    else {
                        filterOffers2(searchText);
                    }
                }
                else {
                    setAdapter();
                }
            }
        });

        productSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        productSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(OfferProductsActivity.this);

                    return true;
                }
                return false;
            }
        });

        fetchProducts();

    }

    private void fetchProducts(){
        for (int i = 0; i < offers.size(); i++) {
            boolean offerMatched = false;
            for (int j = 0; j < offers.get(i).getjCategories().size(); j++) {
                if (id == offers.get(i).getjCategories().get(j).getCategoryId()) {
                    offerMatched = true;
                    break;
                }
            }

            if (!offerMatched) {
                // remove item if id not matches
                offers.remove(i);
                i = i - 1;
            }
        }

        for (int i = 0; i < offers2.size(); i++) {
            boolean offerMatched = false;
            for (int j = 0; j < offers2.get(i).getjCategories().size(); j++) {
                if (id == offers2.get(i).getjCategories().get(j).getCategoryId()) {
                    offerMatched = true;
                    break;
                }
            }

            if (!offerMatched) {
                // remove item if id not matches
                offers2.remove(i);
                i = i - 1;
            }
        }

        if(offers.size() == 0) {
            itemPresentInOffers = false;
        }
        else {
            itemPresentInOffers = true;
        }

        setAdapter();
    }

    private void setAdapter(){
        offersFiltered = (ArrayList<HomePageResponse.Offers>) offers.clone();
        offers2Filtered = (ArrayList<HomePageResponse.Offers2>) offers2.clone();

        mAdapter = new OfferProductsAdapter(OfferProductsActivity.this, offersFiltered, offers2Filtered, OfferProductsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OfferProductsActivity.this);
        listView.setLayoutManager(new GridLayoutManager(OfferProductsActivity.this, 2));
        listView.setAdapter(mAdapter);
    }

    private void filterOffers(String searchText){
        offersFiltered.clear();

        for (int i = 0; i < offers.size(); i++) {
            if(offers.get(i).getNameen().toLowerCase().contains(searchText.toLowerCase())
                    || offers.get(i).getNamear().contains(searchText)) {
                HomePageResponse.Offers offerList = new HomePageResponse.Offers();
                offerList.setBannerimagear(offers.get(i).getBannerimagear());
                offerList.setBannerimageen(offers.get(i).getBannerimageen());
                offerList.setBranchcategoryid(offers.get(i).getBranchcategoryid());
                offerList.setBranchid(offers.get(i).getBranchid());
                offerList.setBranchnamear(offers.get(i).getBranchnamear());
                offerList.setBranchnameen(offers.get(i).getBranchnameen());
                offerList.setBranchsubcategoryid(offers.get(i).getBranchsubcategoryid());
                offerList.setCondition(offers.get(i).getCondition());
                offerList.setCountryid(offers.get(i).getCountryid());
                offerList.setDiscount(offers.get(i).getDiscount());
                offerList.setDiscounttype(offers.get(i).getDiscounttype());
                offerList.setDiscountvalue(offers.get(i).getDiscountvalue());
                offerList.setDistance(offers.get(i).getDistance());
                offerList.setEnddate(offers.get(i).getEnddate());
                offerList.setId(offers.get(i).getId());
                offerList.setImage(offers.get(i).getImage());
                offerList.setInventoryid(offers.get(i).getInventoryid());
                offerList.setIsvatapplicable(offers.get(i).getIsvatapplicable());
                offerList.setJproductvariant(offers.get(i).getJproductvariant());
                offerList.setNamear(offers.get(i).getNamear());
                offerList.setNameen(offers.get(i).getNameen());
                offerList.setOffertype(offers.get(i).getOffertype());
                offerList.setPercentage(offers.get(i).getPercentage());
                offerList.setPrice(offers.get(i).getPrice());
                offerList.setPercentage(offers.get(i).getPercentage());
                offerList.setOffertype(offers.get(i).getOffertype());
                offerList.setProductid(offers.get(i).getProductid());
                offerList.setProductskuid(offers.get(i).getProductskuid());
                offerList.setProductvariant(offers.get(i).getProductvariant());
                offerList.setRnk(offers.get(i).getRnk());
                offerList.setSellingprice(offers.get(i).getSellingprice());
                offerList.setStartdate(offers.get(i).getStartdate());
                offerList.setStockquantity(offers.get(i).getStockquantity());
                offerList.setStoreid(offers.get(i).getStoreid());
                offerList.setSubcategoryimage(offers.get(i).getSubcategoryimage());
                offerList.setSubcategorynamear(offers.get(i).getSubcategorynamear());
                offerList.setSubcategorynameen(offers.get(i).getSubcategorynameen());
                offerList.setUpcbarcode(offers.get(i).getUpcbarcode());
                offerList.setLogo(offers.get(i).getLogo());
                offerList.setMainCategoryNameEn(offers.get(i).getMainCategoryNameEn());
                offerList.setMainCategoryNameAr(offers.get(i).getMainCategoryNameAr());
                offerList.setMainCategoryImage(offers.get(i).getMainCategoryImage());
                offerList.setBranchMainCategoryId(offers.get(i).getBranchMainCategoryId());
                offersFiltered.add(offerList);
            }
        }

        mAdapter = new OfferProductsAdapter(OfferProductsActivity.this, offersFiltered, offers2Filtered, OfferProductsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OfferProductsActivity.this);
        listView.setLayoutManager(new GridLayoutManager(OfferProductsActivity.this, 2));
        listView.setAdapter(mAdapter);
    }

    private void filterOffers2(String searchText){
        offers2Filtered.clear();;

        for (int i = 0; i < offers2.size(); i++) {
            if(offers2.get(i).getServicenameen().toLowerCase().contains(searchText.toLowerCase())
                    || offers2.get(i).getServicenamear().contains(searchText)) {
                HomePageResponse.Offers2 offers2List = new HomePageResponse.Offers2();
                offers2List.setBranchid(offers2.get(i).getBranchid());
                offers2List.setBranchsubcategoryid(offers2.get(i).getBranchsubcategoryid());
                offers2List.setCategoryid(offers2.get(i).getCategoryid());
                offers2List.setCountingnamear(offers2.get(i).getCountingnamear());
                offers2List.setCountingnameen(offers2.get(i).getCountingnameen());
                offers2List.setDiscount(offers2.get(i).getDiscount());
                offers2List.setDiscounttype(offers2.get(i).getDiscounttype());
                offers2List.setDiscountvalue(offers2.get(i).getDiscountvalue());
                offers2List.setDistance(offers2.get(i).getDistance());
                offers2List.setEnddate(offers2.get(i).getEnddate());
                offers2List.setId(offers2.get(i).getId());
                offers2List.setImage(offers2.get(i).getImage());
                offers2List.setIsvatapplicable(offers2.get(i).getIsvatapplicable());
                offers2List.setJproductvariant(offers2.get(i).getJproductvariant());
                offers2List.setPrice(offers2.get(i).getPrice());
                offers2List.setProductvariant(offers2.get(i).getProductvariant());
                offers2List.setRnk(offers2.get(i).getRnk());
                offers2List.setSellingprice(offers2.get(i).getSellingprice());
                offers2List.setServiceid(offers2.get(i).getServiceid());
                offers2List.setServicenamear(offers2.get(i).getServicenamear());
                offers2List.setServicenameen(offers2.get(i).getServicenameen());
                offers2List.setStoreid(offers2.get(i).getStoreid());
                offers2List.setStartdate(offers2.get(i).getStartdate());
                offers2List.setSubcategoryid(offers2.get(i).getSubcategoryid());
                offers2List.setSubcategoryimage(offers2.get(i).getSubcategoryimage());
                offers2List.setSubcategorynamear(offers2.get(i).getSubcategorynamear());
                offers2List.setSubcategorynameen(offers2.get(i).getSubcategorynameen());
                offers2List.setLogo(offers.get(i).getLogo());
                offers2List.setMainCategoryNameEn(offers.get(i).getMainCategoryNameEn());
                offers2List.setMainCategoryNameAr(offers.get(i).getMainCategoryNameAr());
                offers2List.setMainCategoryImage(offers.get(i).getMainCategoryImage());
                offers2List.setBranchMainCategoryId(offers.get(i).getBranchMainCategoryId());
                offers2Filtered.add(offers2List);
            }
        }
        mAdapter = new OfferProductsAdapter(OfferProductsActivity.this, offersFiltered, offers2Filtered, OfferProductsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OfferProductsActivity.this);
        listView.setLayoutManager(new GridLayoutManager(OfferProductsActivity.this, 2));
        listView.setAdapter(mAdapter);
    }
}
