package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cs.checkclickuser.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StartScrrenActivity extends AppCompatActivity {
    TextView language_tv,skip,signin;
    Button signup;
    FragmentManager fragmentManager = getSupportFragmentManager();
    private static int LOGIN_STATUS = 1;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEdit;
    SharedPreferences LanguagePrefs;

    String language;
    SharedPreferences.Editor languagePrefsEditor;




    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEdit = userPrefs.edit();
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor = LanguagePrefs.edit();
        language = LanguagePrefs.getString("language", "En");
        if (language.equals("En")) {
            setContentView(R.layout.startscrren_activity);
        } else {
            setContentView(R.layout.startscrren_activity_ar);
        }



        signup = (Button) findViewById(R.id.sign_btn);
        language_tv =(TextView) findViewById(R.id.tv_en);
        skip =(TextView) findViewById(R.id.tv_skip);
        signin =(TextView) findViewById(R.id.tv_signin);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartScrrenActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartScrrenActivity.this, SignInActivity.class);
                intent.putExtra("reset", true);
                startActivityForResult(intent, LOGIN_STATUS);
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userPrefsEdit.putBoolean("skip", true);
                userPrefsEdit.commit();
                Intent i = new Intent(StartScrrenActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
        language_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (language.equalsIgnoreCase("En")) {
                    languagePrefsEditor.putString("language", "Ar");
                    languagePrefsEditor.commit();
                    language = LanguagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(StartScrrenActivity.this, StartScrrenActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    languagePrefsEditor.putString("language", "En");
                    languagePrefsEditor.commit();
                    language = LanguagePrefs.getString("language", "En");
//                    new ChangeLanguageApi().execute();
                    Intent intent = new Intent(StartScrrenActivity.this, StartScrrenActivity.class);
                    startActivity(intent);
                    finish();
                }            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == LOGIN_STATUS && resultCode == RESULT_OK){
            Intent intent = new Intent(StartScrrenActivity.this, MainActivity.class);
            startActivity(intent);

        }
//        else if(requestCode == 2 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, InboxFragment.class);
//            Fragment inboxfrahmet = new InboxFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, inboxfrahmet).commit();
//        }
//        else if(requestCode == 3 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, NotificationFragment.class);
//            Fragment notificationfrahmet = new NotificationFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, notificationfrahmet).commit();
//        }
//        else if(requestCode == 4 && resultCode == RESULT_OK){
////            Intent intent = new Intent(MainActivity.this, MyProfileFragment.class);
//            Fragment myProfileFragment = new MyProfileFragment();
//            fragmentManager.beginTransaction().replace(R.id.fragment_layout, myProfileFragment).commit();
//        }
    }

}
