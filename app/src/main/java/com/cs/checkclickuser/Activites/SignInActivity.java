package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.Signinresponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignInActivity extends AppCompatActivity {
    ImageView backbtn;
    EditText inputMobile, inputPassword;
    TextView forgot;
    Button signbtn;
    String strMobile, strPassword;
    private static final String TAG = "TAG";
    private static int SIGNUP_REQUEST = 1;
    private static int FORGOT_PASSWORD_REQUEST = 2;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    boolean reset = false;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.signinactivity);
        } else {
            setContentView(R.layout.signinactivity_ar);
        }

        backbtn = (ImageView) findViewById(R.id.back_btn);
        inputMobile = (EditText) findViewById(R.id.signin_input_phone);
        inputPassword = (EditText) findViewById(R.id.signin_input_password);
        forgot = (TextView) findViewById(R.id.forgot);
        signbtn = (Button) findViewById(R.id.bt_continue);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        try {
            reset = getIntent().getBooleanExtra("reset", false);
        } catch (Exception e) {
            reset = false;
            e.printStackTrace();
        }

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        signbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        userLoginApi();
                    } else {
                        if (language.equalsIgnoreCase("En")){

                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        inputMobile.addTextChangedListener(new TextWatcher() {
            String previousText = "";
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                previousText = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String text = charSequence.toString();
                if (text.matches("[0-9]+") && !text.contains("+966") && text.length() >= 1 && text.length() <= 9) {
                    if (text.startsWith("5")) {
                        inputMobile.removeTextChangedListener(this);
                        inputMobile.setText("+966 " + text);
                        inputMobile.setSelection(inputMobile.length());
                        inputMobile.addTextChangedListener(this);
                    }
//                    else {
//                        inputMobile.removeTextChangedListener(this);
//                        inputMobile.setText("");
////                    inputMobile.setSelection(inputMobile.length());
//                        inputMobile.addTextChangedListener(this);
//                    }
                } else {
                    String textCopy = text;
                    textCopy = textCopy.replace("+966 ", "");
                    if (textCopy.matches("[0-9]+") && textCopy.length() <= 9) {

                    } else {
                        if (text.contains("+966")) {
                            inputMobile.removeTextChangedListener(this);
                            text = text.replace("+966 ", "");
                            inputMobile.setText(text);
                            inputMobile.setSelection(inputMobile.length());
                            inputMobile.addTextChangedListener(this);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        } else if (requestCode == FORGOT_PASSWORD_REQUEST && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    private boolean validations() {
        strMobile = inputMobile.getText().toString();
        strPassword = inputPassword.getText().toString();
        strMobile = strMobile.replace("+966 ", "");
        if (strMobile.matches("[0-9]+")) {
            strMobile = "+966" + strMobile;
        }

        if (strMobile.length() == 0) {

          if (language.equals("En")){

              inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));
          }
          else {

              inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
          }
            Constants.requestEditTextFocus(inputMobile, SignInActivity.this);
            return false;
        }
//
//        }
        else if (strPassword.length() == 0) {
            if (language.equals("En")){

                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }else {

                inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }

//
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        } else if (strPassword.length() < 4 || strPassword.length() > 20) {
            if (language.equals("En")){

                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            }else {

                inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }

//
            Constants.requestEditTextFocus(inputPassword, SignInActivity.this);
            return false;
        }
        return true;
    }

    private void userLoginApi() {
        String inputStr = prepareLoginJson();
        Constants.showLoadingDialog(SignInActivity.this);
        final String networkStatus = NetworkUtil.getConnectivityStatusString(SignInActivity.this);

        APIInterface apiService = ApiClient.getClient().create(APIInterface.class);
        Call<Signinresponce> call = apiService.userLogin(
                RequestBody.create(MediaType.parse("application/json"), inputStr));
        call.enqueue(new Callback<Signinresponce>() {

            public void onResponse(Call<Signinresponce> call, Response<Signinresponce> response) {
                if (response.isSuccessful()) {
                    Signinresponce registrationResponse = response.body();
                    try {
                        if (registrationResponse.getStatus()) {
                            //status true case
                            String groupChatId = "";
                            String userId = registrationResponse.getData().getId();
                            if (registrationResponse.getData().getJGroupChatId().size() > 0) {
                                Gson gson = new Gson();
                                groupChatId = gson.toJson(registrationResponse.getData().getJGroupChatId());
                            }

                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("FirstName", registrationResponse.getData().getFirstName());
                            userPrefsEditor.putString("LastName", registrationResponse.getData().getLastName());
                            userPrefsEditor.putString("EmailId", registrationResponse.getData().getEmailId());
                            userPrefsEditor.putString("MobileNo", registrationResponse.getData().getMobileNo());
                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilePhoto());
                            userPrefsEditor.putString("chatUserId", registrationResponse.getData().getUserChatId());
                            userPrefsEditor.putString("groupChatUserId", groupChatId);
                            userPrefsEditor.commit();
                            setResult(RESULT_OK);

                            if (reset) {
                                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                finish();
                            }

                        } else {
                            //status false case
                            String failureResponse = registrationResponse.getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                    getResources().getString(R.string.ok), SignInActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody());
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }
                }

                Constants.closeLoadingDialog();

            }

            public void onFailure(Call<Signinresponce> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(SignInActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(SignInActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                    }

                }

                Constants.closeLoadingDialog();
            }
        });
    }

    private String prepareLoginJson() {
        JSONObject parentObj = new JSONObject();
        String appVersion = "";
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        try {
            parentObj.put("LoginName", strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("Language", "en");
            parentObj.put("DeviceType", "Android");
            parentObj.put("DeviceVersion", "andr v"+appVersion);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: " + parentObj.toString());
        return parentObj.toString();
    }


}
