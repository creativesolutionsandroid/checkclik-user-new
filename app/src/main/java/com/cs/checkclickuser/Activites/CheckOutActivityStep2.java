package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.CheckoutSchedule;
import com.cs.checkclickuser.Models.GetOrderDetails;
import com.cs.checkclickuser.Models.InsertOrderResponse;
import com.cs.checkclickuser.Models.PaymentMethods;
import com.cs.checkclickuser.Models.ShipmentBasicResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckOutActivityStep2 extends AppCompatActivity {

    private TextView totalprice, deliverycharges, Vat, subtotal, shipment_cod, shipping_title, shipment_cod_title, deliverychtext, discount, nettotal;
    private TextView shippingDate, netAmount, vatTitle;
    private ImageView backBtn, step1Icon, step2Icon, step3Icon;
    View line10, line11, line12, line13;
    private ArrayList<CartResponce.CartList> cartArryLists;
    private CheckoutSchedule checkoutData;
    private RelativeLayout orderDetailsLayout, paymentMethodLayout, creditCardLayout, debitCardLayout;
    private LinearLayout priceBreakUpLayout;
    private Boolean isPriceBreakUpVisible = false;
    private RadioButton cashOnRadioRb, creditCardRb, debitCardRb, payNowRb, payLaterRb;
    private String PaymentMethod = "";
    private String PaymentType = "";
    private Button placeOrderBtn;
    private ImageView payNowInfo, payLaterInfo;
    private BubbleLayout payNowLayout, payLaterLayout;
    SharedPreferences userPrefs;
    String userId;
    private String TAG = "TAG";
    private List<GetOrderDetails.Data> orderData = new ArrayList<>();
    private List<PaymentMethods.PaymentData> paymentMethods = new ArrayList<>();
    private String selectedDate = "";
    private float deliveryAmount = 0, CODAmount = 0;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_checkout_step2);
        } else {
            setContentView(R.layout.activity_checkout_step2_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        cartArryLists = (ArrayList<CartResponce.CartList>) getIntent().getSerializableExtra("cart");
        checkoutData = (CheckoutSchedule) getIntent().getSerializableExtra("checkout");

        totalprice = (TextView) findViewById(R.id.totalprice);
        deliverycharges = (TextView) findViewById(R.id.deliverycharges);
        Vat = (TextView) findViewById(R.id.Vat);
        vatTitle = (TextView) findViewById(R.id.Vattext);
        subtotal = (TextView) findViewById(R.id.subtotal);
        shipment_cod = (TextView) findViewById(R.id.shipment_cod);
        shipping_title = (TextView) findViewById(R.id.shipping_title);
        shipment_cod_title = (TextView) findViewById(R.id.shipment_cod_title);
        deliverychtext = (TextView) findViewById(R.id.deliverychtext);
        discount = (TextView) findViewById(R.id.discount);
        nettotal = (TextView) findViewById(R.id.nettotal);
        shippingDate = (TextView) findViewById(R.id.shipping_time);
        netAmount = (TextView) findViewById(R.id.net_amount);
        placeOrderBtn = (Button) findViewById(R.id.place_order_btn);

        cashOnRadioRb = (RadioButton) findViewById(R.id.payment_cash);
        creditCardRb = (RadioButton) findViewById(R.id.payment_credit_card);
        debitCardRb = (RadioButton) findViewById(R.id.payment_debit_card);
        payNowRb = (RadioButton) findViewById(R.id.paynow_rb);
        payLaterRb = (RadioButton) findViewById(R.id.paylater_rb);

        payNowInfo = (ImageView) findViewById(R.id.paynow_info);
        payLaterInfo = (ImageView) findViewById(R.id.paylater_info);

        step1Icon = (ImageView) findViewById(R.id.step1_icon);
        step2Icon = (ImageView) findViewById(R.id.step2_icon);
        step3Icon = (ImageView) findViewById(R.id.step3_icon);
        line10 = (View) findViewById(R.id.line10);
        line11 = (View) findViewById(R.id.line11);
        line12 = (View) findViewById(R.id.line12);
        line13 = (View) findViewById(R.id.line13);

        payNowLayout = (BubbleLayout) findViewById(R.id.bubble_pay_now);
        payLaterLayout = (BubbleLayout) findViewById(R.id.bubble_pay_later);

        backBtn = (ImageView) findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        orderDetailsLayout = (RelativeLayout) findViewById(R.id.order_details_layout);
        paymentMethodLayout = (RelativeLayout) findViewById(R.id.payment_method_layout);
        creditCardLayout = (RelativeLayout) findViewById(R.id.credit_cards_layout);
        debitCardLayout = (RelativeLayout) findViewById(R.id.debit_card_layout);
        priceBreakUpLayout = (LinearLayout) findViewById(R.id.price_breakup_layout);

        int icback_btnId = getResources().getIdentifier("ic_back_" + Constants.appColor, "drawable", getPackageName());
        backBtn.setImageDrawable(getResources().getDrawable(icback_btnId));

        int icstep1IconId = getResources().getIdentifier("check_done_" + Constants.appColor, "drawable", getPackageName());
        step1Icon.setImageDrawable(getResources().getDrawable(icstep1IconId));

        int icstep2IconId = getResources().getIdentifier("check_done_" + Constants.appColor, "drawable", getPackageName());
        step2Icon.setImageDrawable(getResources().getDrawable(icstep2IconId));

        int icstep3IconId = getResources().getIdentifier("check_three_" + Constants.appColor, "drawable", getPackageName());
        step3Icon.setImageDrawable(getResources().getDrawable(icstep3IconId));

        line10.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
        line11.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
        line12.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
        line13.setBackgroundColor(Color.parseColor("#" + Constants.appColor));
        placeOrderBtn.setBackgroundColor(Color.parseColor("#" + Constants.appColor));

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{
                        Color.parseColor("#" + Constants.appColor), //disabled
                        Color.parseColor("#" + Constants.appColor) //enabled
                }
        );

        cashOnRadioRb.setButtonTintList(colorStateList);
        creditCardRb.setButtonTintList(colorStateList);
        debitCardRb.setButtonTintList(colorStateList);
        payNowRb.setButtonTintList(colorStateList);
        payLaterRb.setButtonTintList(colorStateList);

        selectedDate = getIntent().getStringExtra("date");

        if (cartArryLists.get(0).getOrderType() == IdConstants.PRODUCT) {
            if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                shippingDate.setText(generateShippingDate());
            } else {
                shippingDate.setText(selectedDate + " " +
                        getIntent().getStringExtra("slot"));
            }
        } else {
            Date formttedDate = null;
            String outputDate = null;
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);

            try {
                formttedDate = input.parse(cartArryLists.get(0).getDate());
                outputDate = output.format(formttedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            shippingDate.setText(outputDate);
        }

        if (cartArryLists.get(0).getOrderType() == IdConstants.PRODUCT) {
            if (getIntent().getIntExtra("deliveryType", 0) == 3) {
                if (language.equals("En")) {
                    shipping_title.setText("" + getResources().getString(R.string.shipment));
                    deliverychtext.setText("" + getResources().getString(R.string.shipment_charges));
                    cashOnRadioRb.setText("" + getResources().getString(R.string.cash_on_delivery) + "(" + Constants.priceFormat1.format(cartArryLists.get(0).getShimentCODFee()) + " SAR)");
                    shipment_cod.setText("SAR " + Constants.priceFormat1.format(cartArryLists.get(0).getShimentCODFee()));
                } else {
                    shipping_title.setText("" + getResources().getString(R.string.shipment_ar));
                    deliverychtext.setText("" + getResources().getString(R.string.shipment_charges_ar));
                    cashOnRadioRb.setText("" + getResources().getString(R.string.cash_on_delivery_ar) + "(" + Constants.priceFormat1.format(cartArryLists.get(0).getShimentCODFee()) + " SAR)");
                    shipment_cod.setText("SAR " + Constants.priceFormat1.format(cartArryLists.get(0).getShimentCODFee()));
                }
            } else if (getIntent().getIntExtra("deliveryType", 0) == 2) {
                if (language.equals("En")) {
                    shipping_title.setText("" + getResources().getString(R.string.delivery));
                    deliverychtext.setText("" + getResources().getString(R.string.delivery_charges));
                } else {
                    shipping_title.setText("" + getResources().getString(R.string.delivery_ar));
                    deliverychtext.setText("" + getResources().getString(R.string.delivery_charges_ar));
                }

            } else {
                if (language.equals("En")) {
                    shipping_title.setText("" + getResources().getString(R.string.pick_up));
                } else {
                    shipping_title.setText("" + getResources().getString(R.string.pick_up_ar));
                }

                deliverychtext.setVisibility(View.GONE);
                deliverycharges.setVisibility(View.GONE);
                deliveryAmount = 0;
            }
        } else {
            if (getIntent().getIntExtra("deliveryType", 0) == 1) {
                if (language.equals("En")) {
                    shipping_title.setText("" + getResources().getString(R.string.home_service));
                    deliverychtext.setText("" + getResources().getString(R.string.transportation_charges));

                } else {
                    shipping_title.setText("" + getResources().getString(R.string.home_service_ar));
                    deliverychtext.setText("" + getResources().getString(R.string.transportation_charges_ar));
                }

            } else {
                if (language.equals("En")) {
                    shipping_title.setText("" + getResources().getString(R.string.store_service));
                } else {
                    shipping_title.setText("" + getResources().getString(R.string.store_service_ar));
                }

                deliverychtext.setVisibility(View.GONE);
                deliverycharges.setVisibility(View.GONE);
                deliveryAmount = 0;
            }
        }

        setPriceDetails();

        orderDetailsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isPriceBreakUpVisible) {
                    isPriceBreakUpVisible = true;
                    priceBreakUpLayout.setVisibility(View.VISIBLE);
                } else {
                    isPriceBreakUpVisible = false;
                    priceBreakUpLayout.setVisibility(View.GONE);
                }
            }
        });

        cashOnRadioRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PaymentMethod = "COD";
                    PaymentType = "COD";
                    paymentMethodLayout.setVisibility(View.GONE);
                    cashOnRadioRb.setChecked(true);
                    creditCardRb.setChecked(false);
                    debitCardRb.setChecked(false);
                    CODAmount = cartArryLists.get(0).getShimentCODFee();

                    if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                        shipment_cod.setVisibility(View.VISIBLE);
                        shipment_cod_title.setVisibility(View.VISIBLE);
                        setPriceDetails();
                    }
                }
            }
        });

        creditCardRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PaymentMethod = "CC";
                    paymentMethodLayout.setVisibility(View.VISIBLE);
                    cashOnRadioRb.setChecked(false);
                    creditCardRb.setChecked(true);
                    debitCardRb.setChecked(false);
                    if (payNowRb.isChecked()) {
                        PaymentType = "PayNow";
                    }
                    else {
                        PaymentType = "PayLater";
                    }
                    CODAmount = 0;

                    if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                        shipment_cod.setVisibility(View.GONE);
                        shipment_cod_title.setVisibility(View.GONE);
                        setPriceDetails();
                    }
                }
            }
        });

        debitCardRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    PaymentMethod = "MADA";
                    debitCardRb.setChecked(true);
                    creditCardRb.setChecked(false);
                    cashOnRadioRb.setChecked(false);
                    paymentMethodLayout.setVisibility(View.GONE);
//                    if (PaymentType.equals("COD")) {
                    PaymentType = "Instant";
//                    }
                    CODAmount = 0;

                    if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                        shipment_cod.setVisibility(View.GONE);
                        shipment_cod_title.setVisibility(View.GONE);
                        setPriceDetails();
                    }
                }
            }
        });

        payNowRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    payLaterRb.setChecked(false);
                    PaymentMethod = "PayNow";
                }
            }
        });

        payLaterRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    payNowRb.setChecked(false);
                    PaymentMethod = "PayLater";
                }
            }
        });

        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PaymentMethod.equals("")) {
                    if (language.equals("En")) {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_payment_method),
                                getString(R.string.app_name), getString(R.string.ok), CheckOutActivityStep2.this);
                    } else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_payment_method_ar),
                                getString(R.string.app_name), getString(R.string.ok_ar), CheckOutActivityStep2.this);
                    }

                } else {
                    new InsertOrder().execute();
                }
            }
        });

        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_popup, null);
        final BubbleLayout bubbleLayout1 = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_popup_pay_later, null);
        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
        final PopupWindow popupWindow1 = BubblePopupHelper.create(this, bubbleLayout1);

        payNowInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                payNowLayout.setVisibility(View.VISIBLE);
//                payLaterLayout.setVisibility(View.GONE);
                int[] location = new int[2];
                v.getLocationInWindow(location);
                bubbleLayout.setArrowDirection(ArrowDirection.TOP);
                popupWindow.showAtLocation(v, Gravity.NO_GRAVITY, location[0], v.getHeight() + location[1]);
            }
        });

        payLaterInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                payNowLayout.setVisibility(View.GONE);
//                payLaterLayout.setVisibility(View.VISIBLE);
                int[] location = new int[2];
                v.getLocationOnScreen(location);
                bubbleLayout1.setArrowDirection(ArrowDirection.TOP_CENTER);
                popupWindow1.showAtLocation(v, Gravity.NO_GRAVITY, location[0], v.getHeight() + location[1]);
            }
        });

        new GetPaymentMethods().execute();

    }

    private void setPriceDetails() {
        double totalAmount = 0, vatAmount = 0, discountAmount = 0, subTotaalAmount = 0, nettotalAmount = 0;
        totalAmount = cartArryLists.get(0).getSubTotal();
        vatAmount = cartArryLists.get(0).getTotalVatAmount();
        discountAmount = cartArryLists.get(0).getDiscountAmount();

        if (cartArryLists.get(0).getOrderType() == IdConstants.PRODUCT) {
            if (getIntent().getIntExtra("deliveryType", 0) == 3) {
                deliveryAmount = cartArryLists.get(0).getShippingDeliveryFee();
            } else if (getIntent().getIntExtra("deliveryType", 0) == 2) {
                deliveryAmount = cartArryLists.get(0).getDelDeliveryFee();
            }
        } else {
            if (getIntent().getIntExtra("deliveryType", 0) == 1) {
                deliveryAmount = cartArryLists.get(0).getDelDeliveryFee();
            }
        }

        subTotaalAmount = totalAmount - discountAmount + deliveryAmount + CODAmount;
        vatAmount = subTotaalAmount * (cartArryLists.get(0).getVatPercentage() / 100);
        nettotalAmount = subTotaalAmount + vatAmount;

        totalprice.setText("SAR " + Constants.priceFormat1.format(totalAmount));
        discount.setText("SAR " + Constants.priceFormat1.format(discountAmount));
        subtotal.setText("SAR " + Constants.priceFormat1.format(subTotaalAmount));
        vatTitle.setText("" + getResources().getString(R.string.tax_vat) + "(" + (int)cartArryLists.get(0).getVatPercentage() + "%)");
        Vat.setText("SAR " + Constants.priceFormat1.format(vatAmount));
        deliverycharges.setText("SAR " + Constants.priceFormat1.format(deliveryAmount));
        shipment_cod.setText("SAR " + Constants.priceFormat1.format(CODAmount));
        nettotal.setText("SAR " + Constants.priceFormat1.format(nettotalAmount));
        netAmount.setText("SAR " + Constants.priceFormat1.format(nettotalAmount));

        if (nettotalAmount > cartArryLists.get(0).getMaxCODAmount()) {
            cashOnRadioRb.setVisibility(View.GONE);
            PaymentMethod = "";
            PaymentType = "";
            cashOnRadioRb.setChecked(false);
            shipment_cod.setVisibility(View.GONE);
            shipment_cod_title.setVisibility(View.GONE);
        }
        else {
            cashOnRadioRb.setVisibility(View.VISIBLE);
        }
    }

    private class InsertOrder extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareOrderJson();
            Constants.showLoadingDialog(CheckOutActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<InsertOrderResponse> call = apiService.insertOrder(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<InsertOrderResponse>() {
                @Override
                public void onResponse(Call<InsertOrderResponse> call, Response<InsertOrderResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            if (PaymentType.equalsIgnoreCase("COD")) {
                                if (cartArryLists.get(0).getOrderType() == 1) {
                                    if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                                        String orderId = response.body().getData().getOrderId();
                                        String invoiceNo = response.body().getData().getInvoiceNo();
                                        new GetOrder().execute(orderId, invoiceNo);
                                    } else {
                                        showSuccessDialog(response.body().getData().getInvoiceNo());
                                    }
                                } else {
                                    showSuccessDialog(response.body().getData().getInvoiceNo());
                                }
                            } else {
                                String orderId = "";
                                String invoiceNo = "";
                                if (cartArryLists.get(0).getOrderType() == 1) {
                                    if (getIntent().getIntExtra("deliveryType", 0) == IdConstants.PRODUCT_SHIPPING) {
                                        orderId = response.body().getData().getOrderId();
                                        invoiceNo = response.body().getData().getInvoiceNo();
                                    }
                                }
                                Intent intent = new Intent(CheckOutActivityStep2.this, CheckoutActivityStep3.class);
                                intent.putExtra("orderId", orderId);
                                intent.putExtra("invoiceNo", invoiceNo);
                                intent.putExtra("paymentMode", PaymentMethod);
                                intent.putExtra("amount", (cartArryLists.get(0).getTotalItemPrice() + cartArryLists.get(0).getTotalVatAmount()));
                                startActivity(intent);
                            }
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            if (language.equals("En")) {
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), CheckOutActivityStep2.this);
                            } else {
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), CheckOutActivityStep2.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<InsertOrderResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equals("En")) {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareOrderJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userId);
            parentObj.put("Comments", getIntent().getStringExtra("comments"));
            parentObj.put("AddressId", getIntent().getIntExtra("addressId", 0));
            parentObj.put("PaymentMode", PaymentMethod);
            parentObj.put("PaymentType", PaymentType);
            if (!selectedDate.equals("")) {
                parentObj.put("OrderType", getIntent().getIntExtra("deliveryType", 0));
                parentObj.put("ExpectingDelivery", formatedDateForOrder());
                parentObj.put("TimeSlotId", getIntent().getStringExtra("slotId"));
            }
            parentObj.put("PaymentStatus", false);
            parentObj.put("DeviceToken", SplashScreen.regId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class GetOrder extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(CheckOutActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);
            String inputStr = prepareGetOrderJson(strings[0], strings[1]);

            Call<GetOrderDetails> call = apiService.NewGetOrderDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetOrderDetails>() {
                @Override
                public void onResponse(Call<GetOrderDetails> call, Response<GetOrderDetails> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            orderData = response.body().getData();
                            if (orderData.get(0).getOrderStatus() != 2) {
                                showSuccessDialog(strings[1]);
                            } else {
                                new UpdateShipment().execute(strings[1]);
                            }
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CheckOutActivityStep2.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetOrderDetails> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equals("En")) {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetOrderJson(String orderId, String invoiceId) {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Type", IdConstants.PRODUCT);
            parentObj.put("StatusId", 1);
            parentObj.put("OrderId", orderId);
            parentObj.put("InvoiceNo", invoiceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class UpdateShipment extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateShipmentJson();
            Constants.showLoadingDialog(CheckOutActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ShipmentBasicResponse> call = apiService.AddShipment(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ShipmentBasicResponse>() {
                @Override
                public void onResponse(Call<ShipmentBasicResponse> call, Response<ShipmentBasicResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            showSuccessDialog(strings[0]);
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server),
                                    getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CheckOutActivityStep2.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ShipmentBasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equals("En")) {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareUpdateShipmentJson() {
        int qty = 0;
        float weight = 0;
        String itemDescription = "";

        for (int i = 0; i < orderData.get(0).getItems().size(); i++) {
            qty = qty + orderData.get(0).getItems().get(i).getQty();
            weight = weight + (orderData.get(0).getItems().get(i).getWeight() *
                    orderData.get(0).getItems().get(i).getQty());

            String itemVariantsDescription = "";
            for (int j = 0; j < orderData.get(0).getItems().get(i).getVariants().size(); j++) {
                if (j == 0) {
                    itemVariantsDescription = orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                } else {
                    itemVariantsDescription = itemVariantsDescription + "/" + orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                }
            }
            if (i == 0) {
                itemDescription = orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), " + getResources().getString(R.string.qty) + ":" +
                        orderData.get(0).getItems().get(i).getQty();
            } else {
                itemDescription = itemDescription + ", " + orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), " + getResources().getString(R.string.qty) + ":" +
                        orderData.get(0).getItems().get(i).getQty();
            }
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("passKey", "Testing0");
            parentObj.put("refNo", orderData.get(0).getInvoiceNo());
            parentObj.put("sentDate", getOrderDate(orderData.get(0).getOrderDate()));
            parentObj.put("idNo", orderData.get(0).getInvoiceNo());
            parentObj.put("cName", orderData.get(0).getUserDetails().get(0).getFirstName() + " " +
                    orderData.get(0).getUserDetails().get(0).getLastName());
            parentObj.put("Cntry", orderData.get(0).getUserDetails().get(0).getCountryName());
            parentObj.put("cCity", orderData.get(0).getUserDetails().get(0).getCityName());
            parentObj.put("cZip", orderData.get(0).getUserDetails().get(0).getZipCode());
            parentObj.put("cPOBox", "");
            parentObj.put("cMobile", orderData.get(0).getUserDetails().get(0).getMobileNo());
            parentObj.put("cTel1", orderData.get(0).getUserDetails().get(0).getMobile2());
            parentObj.put("cTel2", "");
            parentObj.put("cAddr1", orderData.get(0).getUserDetails().get(0).getAddress1());
            parentObj.put("cAddr2", orderData.get(0).getUserDetails().get(0).getAddress2());
            parentObj.put("shipType", "DLV");
            parentObj.put("PCs", qty);
            parentObj.put("cEmail", orderData.get(0).getUserDetails().get(0).getEmailId());
            parentObj.put("codAmt", orderData.get(0).getGrandTotal());
            parentObj.put("weight", weight);
            parentObj.put("itemDesc", itemDescription);
            parentObj.put("prefDelvDate", getOrderDate(orderData.get(0).getExpectingDelivery()));
            parentObj.put("carrValue", "");
            parentObj.put("carrCurr", "");
            parentObj.put("custCurr", "");
            parentObj.put("custVal", "");
            parentObj.put("insrCurr", "");
            parentObj.put("insrAmt", "");
            parentObj.put("sName", "");
            parentObj.put("sContact", "");
            parentObj.put("sAddr1", "");
            parentObj.put("sAddr2", "");
            parentObj.put("sCity", "");
            parentObj.put("sPhone", "");
            parentObj.put("sCntry", "");
            parentObj.put("gpsPoints", "");
            parentObj.put("awbNo", "");
            parentObj.put("reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private String getOrderDate(String date) {
        String orderDate = null;

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.US);

        try {
            Date formattedDate = sdf1.parse(date);
            orderDate = sdf2.format(formattedDate);
            Log.d(TAG, "getOrderDate: " + orderDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderDate;
    }

    private void showSuccessDialog(String orderId) {

        Intent intent = new Intent(CheckOutActivityStep2.this, OrderCompletedActivity.class);
        intent.putExtra("orderId", orderId);
        startActivity(intent);
        finish();

//        AlertDialog customDialog = null;
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckOutActivityStep2.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.alert_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(false);
//
//        TextView title = (TextView) dialogView.findViewById(R.id.title);
//        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//        View line = (View) dialogView.findViewById(R.id.vert_line);
//
//        no.setVisibility(View.GONE);
//        line.setVisibility(View.GONE);
//
//        if (language.equals("En")){
//            title.setText(getResources().getString(R.string.app_name));
//            yes.setText(getResources().getString(R.string.ok));
//            desc.setText(getResources().getString(R.string.order_placed_successfully));
//
//        }else {
//            title.setText(getResources().getString(R.string.app_name));
//            yes.setText(getResources().getString(R.string.ok_ar));
//            desc.setText(getResources().getString(R.string.order_placed_successfully_ar));
//
//        }
//
//        customDialog = dialogBuilder.create();
//        customDialog.show();
//
//        final AlertDialog finalCustomDialog = customDialog;
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finalCustomDialog.dismiss();
//                Intent intent = new Intent(CheckOutActivityStep2.this, OrderCompletedActivity.class);
//                intent.putExtra("orderId", getIntent().getStringExtra("orderId"));
//                startActivity(intent);
//                finish();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = customDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }

    private String generateShippingDate() {
        Calendar todayDate = Calendar.getInstance();
        if (todayDate.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            // add 2 days
            todayDate.add(Calendar.DATE, 2);
        } else {
            // add 1 day
            todayDate.add(Calendar.DATE, 1);
        }

        String outputDate = null;
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        try {
//            formttedDate = input.parse(cartArryLists.get(0).getDate());
            outputDate = output.format(todayDate.getTime());
            selectedDate = input.format(todayDate.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outputDate;
    }

    private String formatedDateForOrder() {
        if (selectedDate.length() == 10) {
            // only dates with dd/MM/yyyy format date will be changed
            Date formttedDate = null;
            String outputDate = null;
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
            SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

            try {
                formttedDate = output.parse(selectedDate);
                selectedDate = input.format(formttedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return selectedDate;
    }

    private class GetPaymentMethods extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetPaymentMethodsJson();
            Constants.showLoadingDialog(CheckOutActivityStep2.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<PaymentMethods> call = apiService.NewGetOrderBranchPayment(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PaymentMethods>() {
                @Override
                public void onResponse(Call<PaymentMethods> call, Response<PaymentMethods> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            paymentMethods = response.body().getData().getPaymentData();
                            if (paymentMethods.size() > 0) {
                                updatePaymentUI();
                            }
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            if (language.equals("En")) {
                                String failureResponse = response.body().getMessageEn();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), CheckOutActivityStep2.this);
                            } else {
                                String failureResponse = response.body().getMessageAr();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), CheckOutActivityStep2.this);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<PaymentMethods> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckOutActivityStep2.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equals("En")) {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CheckOutActivityStep2.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetPaymentMethodsJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("CreatedBy", userId);
            parentObj.put("CityId", cartArryLists.get(0).getCityId());
            parentObj.put("OrderType", cartArryLists.get(0).getOrderType());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private void updatePaymentUI() {
        // cash payment
        if (!paymentMethods.get(0).getIsCashAllowed()) {
            cashOnRadioRb.setVisibility(View.GONE);
        }

        // International card
        if (!paymentMethods.get(0).getIsCreditCardAllowed()) {
            creditCardLayout.setVisibility(View.GONE);
        }

        // card paymnet types
        if (!paymentMethods.get(0).getIsCcPayNow()) {
            payNowRb.setVisibility(View.GONE);
            payNowInfo.setVisibility(View.GONE);
        }
        if (!paymentMethods.get(0).getIsCcPayLater()) {
            payLaterRb.setVisibility(View.GONE);
            payLaterInfo.setVisibility(View.GONE);
        }

        // Mada card
        if (!paymentMethods.get(0).getIsMadaCardAllowed()) {
            debitCardLayout.setVisibility(View.GONE);
        }

        if (!paymentMethods.get(0).getIsCashAllowed() && paymentMethods.get(0).getIsCreditCardAllowed()) {
            creditCardRb.setChecked(true);
        } else if (!paymentMethods.get(0).getIsCashAllowed() && !paymentMethods.get(0).getIsCreditCardAllowed() &&
                paymentMethods.get(0).getIsMadaCardAllowed()) {
            debitCardRb.setChecked(true);
        }
    }
}
