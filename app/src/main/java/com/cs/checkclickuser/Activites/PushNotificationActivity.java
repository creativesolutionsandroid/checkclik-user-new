package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.PushNotificationAdapteer;
import com.cs.checkclickuser.Models.PushNotificationResponce;
import com.cs.checkclickuser.Models.PushNotificationsUpdateResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PushNotificationActivity extends AppCompatActivity {

    String TAG = "TAG";
    Button done;
    SharedPreferences userPrefs;
    ImageView back_btn;
    ListView pushlist;
    SharedPreferences.Editor userPrefEditor;
    String strUserid;
    Boolean st1,st2,st3,st4,st5;
    PushNotificationAdapteer mPushAdapter;

    SharedPreferences LanguagePrefs;
    String language;

    int pos;
    public static ArrayList<PushNotificationResponce.PNConfig> pushnotificationarry = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.pushnotification);
        } else {
            setContentView(R.layout.pushnotification_ar);
        }

        userPrefs = PushNotificationActivity.this.getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        strUserid = userPrefs.getString("userId", null);

        new Pushnotificationapi().execute();

        done =(Button)findViewById(R.id.done);
        back_btn=(ImageView) findViewById(R.id.back_btn);
        pushlist = (ListView) findViewById(R.id.pushlist);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new preparepushsetapi().execute();
            }
        });
    }
    private class Pushnotificationapi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparepushnotifactionJson();
            Constants.showLoadingDialog(PushNotificationActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PushNotificationResponce> call = apiService.getpnotifaction(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PushNotificationResponce>() {
                @Override
                public void onResponse(Call<PushNotificationResponce> call, Response<PushNotificationResponce> response) {
                    Log.d(TAG, "pushResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            pushnotificationarry = response.body().getData().getPNConfig();
                            Log.d(TAG, "cpushArryLists: " + pushnotificationarry.size());

                            mPushAdapter = new PushNotificationAdapteer(PushNotificationActivity.this, pushnotificationarry);
                            pushlist.setAdapter(mPushAdapter);
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), PushNotificationActivity.this);
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<PushNotificationResponce> call, Throwable t) {
                    Log.d(TAG, "pushFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(PushNotificationActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparepushnotifactionJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id", strUserid);
            parentObj.put("FlagId", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "preparepushnotifactionJson: " + parentObj);
        return parentObj.toString();
    }

    private class preparepushsetapi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparepushsetJson();
            Constants.showLoadingDialog(PushNotificationActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<PushNotificationsUpdateResponce> call = apiService.getpushnotifactionupdate(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<PushNotificationsUpdateResponce>() {
                @Override
                public void onResponse(Call<PushNotificationsUpdateResponce> call, Response<PushNotificationsUpdateResponce> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        PushNotificationsUpdateResponce PushNotificationsUpdateResponce = response.body();
                        try {
                            if (PushNotificationsUpdateResponce.getStatus()) {
//                                finish();
                                if (language.equalsIgnoreCase("En")){
                                    Toast.makeText(PushNotificationActivity.this, getResources().getString(R.string.updated_successfully), Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(PushNotificationActivity.this, getResources().getString(R.string.updated_successfully_ar), Toast.LENGTH_SHORT).show();
                                }


                            } else {

                                String failureResponse = PushNotificationsUpdateResponce.getMessage();
                                if (language.equalsIgnoreCase("En")){
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), PushNotificationActivity.this);
                                }else {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok_ar), PushNotificationActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<PushNotificationsUpdateResponce> call, Throwable t) {
                    Log.d("TAG", "on failur: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(PushNotificationActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(PushNotificationActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(PushNotificationActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;

        }

        private String preparepushsetJson() {
            JSONObject parentObj = new JSONObject();
            JSONArray jPNConfigarry = new JSONArray();
            try {
                parentObj.put("Id", strUserid);

                for (int i = 0; i < pushnotificationarry.size(); i++) {

                    if (pushnotificationarry.get(i).getPNTypeId()==1){

                        JSONObject orderObj = new JSONObject();
                        orderObj.put("Id", 6);
                        orderObj.put("PNTypeId", 1);
                        orderObj.put("Name", "Order");
                        orderObj.put("Status", pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(orderObj);
                        Log.d(TAG, "orderObj: "+orderObj);
                    }

                    else if(pushnotificationarry.get(i).getPNTypeId()==2){
                        JSONObject customerObj = new JSONObject();
                        customerObj.put("Id", 7);
                        customerObj.put("PNTypeId", 2);
                        customerObj.put("Name", "Customer Review");
                        customerObj.put("Status",  pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(customerObj);
                        Log.d(TAG, "customerObj: "+customerObj);

                    }
                    else  if (pushnotificationarry.get(i).getPNTypeId()==3){

                        JSONObject SubscriptionObj = new JSONObject();

                        SubscriptionObj.put("Id", 8);
                        SubscriptionObj.put("PNTypeId", 3);
                        SubscriptionObj.put("Name", "Subscription");
                        SubscriptionObj.put("Status",  pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(SubscriptionObj);
                        Log.d(TAG, "SubscriptionObj: "+SubscriptionObj);
                    }

                    else if (pushnotificationarry.get(i).getPNTypeId()==4) {
                        JSONObject ChattingObj = new JSONObject();

                        ChattingObj.put("Id", 9);
                        ChattingObj.put("PNTypeId", 4);
                        ChattingObj.put("Name", "Chatting");
                        ChattingObj.put("Status",  pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(ChattingObj);
                        Log.d(TAG, "ChattingObj: "+ChattingObj);

                    }
                    else if (pushnotificationarry.get(i).getPNTypeId()==5) {

                        JSONObject AlertObj = new JSONObject();

                        AlertObj.put("Id", 10);
                        AlertObj.put("PNTypeId", 5);
                        AlertObj.put("Name", "Alert");
                        AlertObj.put("Status",  pushnotificationarry.get(pos).getStatus());
                        jPNConfigarry.put(AlertObj);
                        Log.d(TAG, "AlertObj: "+AlertObj);

                    }

                }

                parentObj.put("jPNConfig", jPNConfigarry);
                parentObj.put("FlagId", 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.i("TAG", "pushnotifactionJson: " + parentObj.toString());
            return parentObj.toString();
        }
    }
}