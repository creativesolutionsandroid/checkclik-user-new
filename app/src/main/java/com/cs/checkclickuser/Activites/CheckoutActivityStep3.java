package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.GetOrderDetails;
import com.cs.checkclickuser.Models.MyCards;
import com.cs.checkclickuser.Models.ShipmentBasicResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.FourDigitCardFormatWatcher;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.cs.checkclickuser.hyperpay.CheckoutIdRequestAsyncTask;
import com.cs.checkclickuser.hyperpay.CheckoutIdRequestListener;
import com.cs.checkclickuser.hyperpay.PaymentStatusRequestAsyncTask;
import com.cs.checkclickuser.hyperpay.PaymentStatusRequestListener;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.exception.PaymentException;
import com.oppwa.mobile.connect.payment.BrandsValidation;
import com.oppwa.mobile.connect.payment.CheckoutInfo;
import com.oppwa.mobile.connect.payment.ImagesRequest;
import com.oppwa.mobile.connect.payment.PaymentParams;
import com.oppwa.mobile.connect.payment.card.CardPaymentParams;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.ITransactionListener;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;
import com.oppwa.mobile.connect.service.ConnectService;
import com.oppwa.mobile.connect.service.IProviderBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckoutActivityStep3 extends AppCompatActivity implements ITransactionListener, CheckoutIdRequestListener, PaymentStatusRequestListener{

    private EditText cardNumberEt, cardNameEt, expiryMonthEt, expiryYearEt, cvvEt;
    private Button paybtn;
    private String strName, strNumber, strMonth, strYear, strCVV;
    private AlertDialog loadingDialog;
    private ImageView backBtn, step1Icon, step2Icon, step3Icon;
    private String checkoutId;

    private String TAG = "TAG";
    private List<GetOrderDetails.Data> orderData = new ArrayList<>();

    SharedPreferences userPrefs;
    String userId;

    SharedPreferences LanguagePrefs;
    String language;

    String addressLine = "";
    String locality = "";
    String postalCode = "";
    String countryName = "";
    String stateName = "";

    private static final String STATE_RESOURCE_PATH = "STATE_RESOURCE_PATH";

    Transaction transaction = null;
    protected String resourcePath;
    String merchantId = "";
    private boolean isPaymentStatusRequested = false;
    private MyCards myRegistrationCardDetails = new MyCards();

    private IProviderBinder providerBinder;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            /* we have a connection to the service */
            providerBinder = (IProviderBinder) service;
            providerBinder.addTransactionListener(CheckoutActivityStep3.this);

            try {
//                providerBinder.initializeProvider(Connect.ProviderMode.TEST);
                providerBinder.initializeProvider(Connect.ProviderMode.TEST);
            } catch (PaymentException ee) {
                Constants.showOneButtonAlertDialog(ee.getMessage(), getResources().getString(R.string.app_name),
                        getResources().getString(R.string.ok), CheckoutActivityStep3.this);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            providerBinder = null;
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            resourcePath = savedInstanceState.getString(STATE_RESOURCE_PATH);
        }

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equals("En")){
            setContentView(R.layout.activity_checkout_step3);
        }else {
            setContentView(R.layout.activity_checkout_step3_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        cardNameEt = (EditText) findViewById(R.id.name_et);
        cardNumberEt = (EditText) findViewById(R.id.number_et);
        expiryMonthEt = (EditText) findViewById(R.id.expiry_month_et);
        expiryYearEt = (EditText) findViewById(R.id.expiry_year_et);
        cvvEt = (EditText) findViewById(R.id.cvv_et);
        paybtn = (Button) findViewById(R.id.pay_btn);

        step1Icon = (ImageView) findViewById(R.id.step1_icon);
        step2Icon = (ImageView) findViewById(R.id.step2_icon);
        step3Icon = (ImageView) findViewById(R.id.step3_icon);

        int icstep1IconId = getResources().getIdentifier("check_done_"+Constants.appColor, "drawable", getPackageName());
        step1Icon.setImageDrawable(getResources().getDrawable(icstep1IconId));

        int icstep2IconId = getResources().getIdentifier("check_done_"+Constants.appColor, "drawable", getPackageName());
        step2Icon.setImageDrawable(getResources().getDrawable(icstep2IconId));

        int icstep3IconId = getResources().getIdentifier("check_done_"+Constants.appColor, "drawable", getPackageName());
        step3Icon.setImageDrawable(getResources().getDrawable(icstep3IconId));

        paybtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        paybtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    if (!CardPaymentParams.isNumberValid(strNumber)) {
                        /* display error that card number is invalid */
                        if (language.equals("En")){
                        cardNumberEt.setError("" + getResources().getString(R.string.invalid_card_number));
                        }else {
                            cardNumberEt.setError("" + getResources().getString(R.string.invalid_card_number_ar));
                        }

                    }
                    else {
                        generateMerchantId();
                    }
                }
            }
        });

        cardNumberEt.addTextChangedListener(new FourDigitCardFormatWatcher());

        expiryYearEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 4) {
                    int year = Integer.parseInt(editable.toString());
                    if(year < Calendar.getInstance().get(Calendar.YEAR)) {
//                        expiryYearEt.removeTextChangedListener(this);
//                        expiryYearEt.setText(""+Calendar.getInstance().get(Calendar.YEAR));
//                        expiryYearEt.addTextChangedListener(this);
                        if (language.equals("En")){
                        expiryYearEt.setError("" + getResources().getString(R.string.invalid_year));
                        }else {
                            expiryYearEt.setError("" + getResources().getString(R.string.invalid_year_ar));
                        }

                    }
                    else {
                        if (cvvEt.getText().toString().length() == 0)
                        cvvEt.requestFocus();
                    }
                }
            }
        });

        expiryMonthEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 2) {
                    int month = Integer.parseInt(editable.toString());
                    if(month > 12) {
//                        expiryMonthEt.removeTextChangedListener(this);
//                        expiryMonthEt.setText("12");
//                        expiryMonthEt.addTextChangedListener(this);
                        if (language.equals("En")){
                        expiryMonthEt.setError("" + getResources().getString(R.string.invalid_month));
                        }else {
                            expiryMonthEt.setError("" + getResources().getString(R.string.invalid_month_ar));
                        }

                    }
                    else {
                        if (expiryYearEt.getText().toString().length() == 0)
                            expiryYearEt.requestFocus();
                    }
                }
            }
        });
    }

    private boolean validate(){
        boolean valid = true;
        long cardNum = 0;
        int cvvNum = 0;
        strName = cardNameEt.getText().toString();
        strNumber = cardNumberEt.getText().toString();
        strMonth = expiryMonthEt.getText().toString();
        strYear = expiryYearEt.getText().toString();
        strCVV = cvvEt.getText().toString();

        if(strNumber.length() > 0)
            cardNum = Long.parseLong(strNumber.replace(" ", ""));

        if(strCVV.length() > 0)
            cvvNum = Integer.parseInt(strCVV.replace(" ", ""));

        if(strName.length() == 0) {
            if (language.equals("En")){
                cardNameEt.setError("" + getResources().getString(R.string.please_enter_card_holder_name));
            }else {
                cardNameEt.setError("" + getResources().getString(R.string.please_enter_card_holder_name_ar));
            }

            if(valid)
                valid = false;
        }
        if(strNumber.length() < 15 || cardNum == 0) {
            if (language.equals("En")){
                cardNumberEt.setError("" + getResources().getString(R.string.invalid_card_number));
            }else {
                cardNumberEt.setError("" + getResources().getString(R.string.invalid_card_number_ar));
            }

            if(valid)
                valid = false;
        }
        if(strYear.length() == 0) {
            if (language.equals("En")){

                expiryYearEt.setError("" + getResources().getString(R.string.please_enter_expiry_year));
            }else {

                expiryYearEt.setError("" + getResources().getString(R.string.please_enter_expiry_year_ar));
            }

            if(valid)
                valid = false;
        }
        if(strMonth.length() == 0) {
            if (language.equals("En")){

                expiryMonthEt.setError("" + getResources().getString(R.string.please_enter_expiry_month));
            }else {

                expiryMonthEt.setError("" + getResources().getString(R.string.please_enter_expiry_month_ar));
            }

            if(valid)
                valid = false;
        }
        if(strCVV.length() == 0 || cvvNum == 0) {
            if (language.equals("En")){
                cvvEt.setError("" + getResources().getString(R.string.invalid_cvv));
            }else {
                cvvEt.setError("" + getResources().getString(R.string.invalid_cvv_ar));
            }

            if(valid)
                valid = false;
        }
        return valid;
    }

    protected boolean hasCallbackScheme(Intent intent) {
        String scheme = intent.getScheme();
        return Constants.SHOPPER_RESULT_URL.equals(scheme+"://result");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent(this, ConnectService.class);

        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unbindService(serviceConnection);
        stopService(new Intent(this, ConnectService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_RESOURCE_PATH, resourcePath);
    }

    protected void requestCheckoutId() {
        showProgressDialog(R.string.progress_message_checkout_id);

        String isMadaCard = "false";
        String isVisaMaster = "false";

        Log.d(TAG, "paymentMode: "+getIntent().getStringExtra("paymentMode"));
        if (getIntent().getStringExtra("paymentMode").equals("CC")) {
            isVisaMaster = "true";
        }
        else if (getIntent().getStringExtra("paymentMode").equals("DC")){
            isMadaCard = "true";
        }

        String address = getAddress();

        new CheckoutIdRequestAsyncTask(this)
                .execute(Constants.priceFormat1.format(getIntent().getDoubleExtra("amount", 0)), Constants.SHOPPER_RESULT_URL, "false",
                        merchantId, userPrefs.getString("EmailId", ""), userId, userPrefs.getString("MobileNo", "")
                        , userPrefs.getString("FirstName", ""), "false", isVisaMaster, isMadaCard, addressLine, locality, stateName, countryName, postalCode);
    }

    public String getAddress() {
        List<Address> addresses = getGeocoderAddress(this);
        if (addresses != null && addresses.size() > 0) {
            Address address = addresses.get(0);
            addressLine = address.getSubLocality();
            locality = address.getLocality();
            postalCode = address.getPostalCode();
            countryName = address.getCountryName();
            stateName = address.getSubAdminArea();
            return addressLine;
        }
        else {
            return "";
        }
    }

    /**
     * Get list of address by latitude and longitude
     *
     * @return null or List<Address>
     */
    public List<Address> getGeocoderAddress(Context context) {
//        if (location != null) {
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(Constants.Latitude,
                    Constants.Longitude, 1);
            return addresses;
        } catch (IOException e) {
            e.printStackTrace();
        }
//        }
        return null;
    }

    public void generateMerchantId(){
        SimpleDateFormat dateFormater = new SimpleDateFormat("ddMMyyyy", Locale.US);
        SimpleDateFormat timeFormater = new SimpleDateFormat("HHmm", Locale.US);

        Calendar calendar = Calendar.getInstance();
        String timeStr = timeFormater.format(calendar.getTimeInMillis());
        String dateStr = dateFormater.format(calendar.getTimeInMillis());

        merchantId = userId + dateStr + timeStr;

        try {
            requestCheckoutId();
        } catch (Exception e) {
            Log.e("connect", "error creating the payment page", e);
        }
    }

    @Override
    public void onCheckoutIdReceived(String checkoutId) {
        hideProgressDialog();
        Log.d("TAG", "onCheckoutIdReceived: "+checkoutId);

        if (checkoutId == null) {
            if (language.equals("En")){
            showAlertDialog(R.string.error_message);
            }else {
                showAlertDialog(R.string.error_message_ar);
            }

        }

        if (checkoutId != null) {
            this.checkoutId = checkoutId;

            isPaymentStatusRequested = false;
            openCheckoutUI(checkoutId);
        }
    }

    private void openCheckoutUI(String checkoutId) {
        Log.d(TAG, "openCheckoutUI: ");
        if (providerBinder != null) {
            try {
                providerBinder.requestCheckoutInfo(checkoutId);
                if (language.equals("En")){
                showProgressDialog(R.string.progress_message_checkout_info);
                }else {
                     showProgressDialog(R.string.progress_message_please_wait_ar);
                }

            } catch (PaymentException e) {
                showAlertDialog(e.getMessage());
            }
        }
    }

    @Override
    public void onErrorOccurred() {
        hideProgressDialog();
        if (language.equals("En")){
        showAlertDialog(R.string.error_message);
        }else {
            showAlertDialog(R.string.error_message_ar);
        }

    }

    @Override
    public void onPaymentStatusReceived(MyCards paymentStatus) {
        hideProgressDialog();
        myRegistrationCardDetails = paymentStatus;
        if ("OK".equals(paymentStatus.getData().get(0).getCustomPayment())) {
            if (!getIntent().getStringExtra("orderId").equals("")) {
                new GetOrder().execute(getIntent().getStringExtra("orderId"), getIntent().getStringExtra("invoiceNo"));
            }
            else {
                showSuccessDialog();
            }
//            return;
        }
        else{
            if (language.equals("En")){
             showAlertDialog(R.string.message_unsuccessful_payment);
            }else {
                showAlertDialog(R.string.message_unsuccessful_payment_ar);
            }

        }
    }

    protected void requestPaymentStatus(String resourcePath) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (language.equals("En")){
                 showProgressDialog(R.string.progress_message_payment_status);
                }else {
                    showProgressDialog(R.string.progress_message_payment_status_ar);
                }
            }
        });

        String isMadaCard = "false";
        String isVisaMaster = "false";

        if (getIntent().getStringExtra("paymentMode").equals("CC")) {
            isVisaMaster = "true";
        }
        else if (getIntent().getStringExtra("paymentMode").equals("DC")){
            isMadaCard = "true";
        }

        Log.d("TAG", "requestPaymentStatus: "+resourcePath);
        if(!isPaymentStatusRequested) {
            new PaymentStatusRequestAsyncTask(this).execute(userId, resourcePath, "false", isVisaMaster, isMadaCard);
            isPaymentStatusRequested = true;
        }
    }

    @Override
    public void brandsValidationRequestSucceeded(BrandsValidation brandsValidation) {

    }

    @Override
    public void brandsValidationRequestFailed(PaymentError paymentError) {

    }

    @Override
    public void imagesRequestSucceeded(ImagesRequest imagesRequest) {

    }

    @Override
    public void imagesRequestFailed() {

    }

    @Override
    public void paymentConfigRequestSucceeded(final CheckoutInfo checkoutInfo) {
        hideProgressDialog();

        Log.d(TAG, "paymentConfigRequestSucceeded: ");

        if (checkoutInfo == null) {
            if (language.equals("En")){
            showErrorDialog(getString(R.string.error_message));
            }else {
                showErrorDialog(getString(R.string.error_message_ar));
            }


            return;
        }

        /* Get the resource path from checkout info to request the payment status later. */
        resourcePath = checkoutInfo.getResourcePath();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pay(checkoutId);
            }
        });
    }

    private void pay(String checkoutId) {
        Log.d(TAG, "pay: ");
        try {
            PaymentParams paymentParams = createPaymentParams(checkoutId);
            paymentParams.setShopperResultUrl(Constants.SHOPPER_RESULT_URL);
            Transaction transaction = new Transaction(paymentParams);

            providerBinder.submitTransaction(transaction);
            if (language.equals("En")){
            showProgressDialog(R.string.progress_message_processing_payment);
            }else {
                showProgressDialog(R.string.progress_message_processing_payment_ar);
            }

        } catch (PaymentException e) {
            showErrorDialog(e.getError().getErrorMessage());
        }
    }

    private PaymentParams createPaymentParams(String checkoutId) throws PaymentException {

        Log.d(TAG, "createPaymentParams: ");
        return new CardPaymentParams(
                checkoutId,
                Constants.Config.CARD_BRAND,
                strNumber,
                strName,
                strMonth,
                strYear,
                strCVV
        );
    }

    @Override
    public void paymentConfigRequestFailed(PaymentError paymentError) {
        Log.d(TAG, "paymentConfigRequestFailed: ");
        hideProgressDialog();
        showErrorDialog(paymentError.getErrorMessage());
    }

    @Override
    public void transactionCompleted(Transaction transaction) {
        hideProgressDialog();

        if (transaction == null) {
            Log.d(TAG, "transactionCompleted nul: ");
            if (language.equals("En")){
            showErrorDialog(getString(R.string.error_message));
            }else {
                showErrorDialog(getString(R.string.error_message_ar));
            }


            return;
        }

        if (transaction.getTransactionType() == TransactionType.SYNC) {
            /* check the status of synchronous transaction */
            Log.d(TAG, "transactionCompleted requestPaymentStatus: "+resourcePath);
            requestPaymentStatus(resourcePath);

        } else {
            Log.d(TAG, "transactionCompleted ACTION_VIEW: ");
            /* wait for the callback in the onNewIntent() */
            if (language.equals("En")){
            showProgressDialog(R.string.progress_message_please_wait);
            }else {
                showProgressDialog(R.string.progress_message_please_wait_ar);
            }

            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(transaction.getRedirectUrl())));
        }
    }

    @Override
    public void transactionFailed(Transaction transaction, PaymentError paymentError) {
        Log.d(TAG, "transactionFailed: ");
        hideProgressDialog();
        showErrorDialog(paymentError.getErrorMessage());
    }

    private void showErrorDialog(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showAlertDialog(message);
            }
        });
    }

    protected void showProgressDialog(int messageId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivityStep3.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.loading_dialog1;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        CamomileSpinner spinner1 = (CamomileSpinner) dialogView.findViewById(R.id.spinner1);
        spinner1.start();

        spinner1.recreateWithParams(
                this,
                DialogUtils.getColor(this, R.color.black),
                120,
                true
        );

//        TextView loadingText = (TextView) dialogView.findViewById(R.id.loading_text);
//        loadingText.setText(getString(messageId));

        loadingDialog = dialogBuilder.create();
        loadingDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loadingDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.45;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    protected void hideProgressDialog() {
        if (loadingDialog == null) {
            return;
        }

        loadingDialog.dismiss();
    }

    protected void showAlertDialog(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .setCancelable(false)
                .show();
    }

    protected void showAlertDialog(int messageId) {
        showAlertDialog(getString(messageId));
    }



    private class GetOrder extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(CheckoutActivityStep3.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);
            String inputStr = prepareGetOrderJson(strings[0], strings[1]);

            Call<GetOrderDetails> call = apiService.NewGetOrderDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<GetOrderDetails>() {
                @Override
                public void onResponse(Call<GetOrderDetails> call, Response<GetOrderDetails> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            orderData = response.body().getData();
                            if(orderData.get(0).getOrderStatus() != 2) {
                                showSuccessDialog();
                            }
                            else {
                                new UpdateShipment().execute();
                            }
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            if (language.equals("En")){
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), CheckoutActivityStep3.this);
                            }else {
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), CheckoutActivityStep3.this);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<GetOrderDetails> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivityStep3.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equals("En")){
                            Toast.makeText(CheckoutActivityStep3.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivityStep3.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equals("En")){
                            Toast.makeText(CheckoutActivityStep3.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivityStep3.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetOrderJson(String orderId, String invoiceId){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Type", 1);
            parentObj.put("StatusId", 1);
            parentObj.put("OrderId", orderId);
            parentObj.put("InvoiceNo", invoiceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class UpdateShipment extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateShipmentJson();
            Constants.showLoadingDialog(CheckoutActivityStep3.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ShipmentBasicResponse> call = apiService.AddShipment(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ShipmentBasicResponse>() {
                @Override
                public void onResponse(Call<ShipmentBasicResponse> call, Response<ShipmentBasicResponse> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            showSuccessDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            if (language.equals("En")){
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server),
                                        getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), CheckoutActivityStep3.this);
                            }else {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.cannot_reach_server_ar),
                                        getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), CheckoutActivityStep3.this);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<ShipmentBasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivityStep3.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equals("En")){
                            Toast.makeText(CheckoutActivityStep3.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivityStep3.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equals("En")){
                            Toast.makeText(CheckoutActivityStep3.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivityStep3.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareUpdateShipmentJson(){
        int qty = 0;
        float weight = 0;
        String itemDescription = "";

        for (int i = 0; i < orderData.get(0).getItems().size(); i++) {
            qty = qty + orderData.get(0).getItems().get(i).getQty();
            weight = weight + (orderData.get(0).getItems().get(i).getWeight() *
                    orderData.get(0).getItems().get(i).getQty());

            String itemVariantsDescription = "";
            for (int j = 0; j < orderData.get(0).getItems().get(i).getVariants().size(); j++) {
                if (j == 0){
                    itemVariantsDescription = orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                }
                else {
                    itemVariantsDescription = itemVariantsDescription + "/" +orderData.get(0).getItems().get(i).getVariants().get(j).getOptionValueEn();
                }
            }
            if (i == 0){
                itemDescription = orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), " + getResources().getString(R.string.qty) + ":" +
                        orderData.get(0).getItems().get(i).getQty();
            }
            else {
                itemDescription = itemDescription + ", " + orderData.get(0).getItems().get(i).getNameEn() + "(" + itemVariantsDescription + "), " + getResources().getString(R.string.qty) + ":" +
                        orderData.get(0).getItems().get(i).getQty();
            }
        }

        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("passKey", "Testing0");
            parentObj.put("refNo", orderData.get(0).getInvoiceNo());
            parentObj.put("sentDate", getOrderDate(orderData.get(0).getOrderDate()));
            parentObj.put("idNo", orderData.get(0).getInvoiceNo());
            parentObj.put("cName", orderData.get(0).getUserDetails().get(0).getFirstName() + " " +
                    orderData.get(0).getUserDetails().get(0).getLastName());
            parentObj.put("Cntry", orderData.get(0).getUserDetails().get(0).getCountryName());
            parentObj.put("cCity", orderData.get(0).getUserDetails().get(0).getCityName());
            parentObj.put("cZip", orderData.get(0).getUserDetails().get(0).getZipCode());
            parentObj.put("cPOBox", "");
            parentObj.put("cMobile", orderData.get(0).getUserDetails().get(0).getMobileNo());
            parentObj.put("cTel1", orderData.get(0).getUserDetails().get(0).getMobile2());
            parentObj.put("cTel2", "");
            parentObj.put("cAddr1", orderData.get(0).getUserDetails().get(0).getAddress1());
            parentObj.put("cAddr2", orderData.get(0).getUserDetails().get(0).getAddress2());
            parentObj.put("shipType", "DLV");
            parentObj.put("PCs", qty);
            parentObj.put("cEmail", orderData.get(0).getUserDetails().get(0).getEmailId());
            parentObj.put("codAmt", orderData.get(0).getGrandTotal());
            parentObj.put("weight", weight);
            parentObj.put("itemDesc", itemDescription);
            parentObj.put("prefDelvDate", getOrderDate(orderData.get(0).getExpectingDelivery()));
            parentObj.put("carrValue", "");
            parentObj.put("carrCurr", "");
            parentObj.put("custCurr", "");
            parentObj.put("custVal", "");
            parentObj.put("insrCurr", "");
            parentObj.put("insrAmt", "");
            parentObj.put("sName", "");
            parentObj.put("sContact", "");
            parentObj.put("sAddr1", "");
            parentObj.put("sAddr2", "");
            parentObj.put("sCity", "");
            parentObj.put("sPhone", "");
            parentObj.put("sCntry", "");
            parentObj.put("gpsPoints", "");
            parentObj.put("awbNo", "");
            parentObj.put("reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private String getOrderDate(String date){
        String orderDate = null;

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a", Locale.US);

        try {
            Date formattedDate = sdf1.parse(date);
            orderDate = sdf2.format(formattedDate);
            Log.d(TAG, "getOrderDate: "+orderDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orderDate;
    }

    private void showSuccessDialog(){

        Intent intent = new Intent(CheckoutActivityStep3.this, OrderCompletedActivity.class);
        intent.putExtra("orderId", getIntent().getStringExtra("invoiceNo"));
        startActivity(intent);
        finish();

//        AlertDialog customDialog = null;
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(CheckoutActivityStep3.this);
//        // ...Irrelevant code for customizing the buttons and title
//        LayoutInflater inflater = getLayoutInflater();
//        int layout = R.layout.alert_dialog;
//        View dialogView = inflater.inflate(layout, null);
//        dialogBuilder.setView(dialogView);
//        dialogBuilder.setCancelable(false);
//
//        TextView title = (TextView) dialogView.findViewById(R.id.title);
//        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//        View line = (View) dialogView.findViewById(R.id.vert_line);
//
//        no.setVisibility(View.GONE);
//        line.setVisibility(View.GONE);
//
//        if (language.equals("En")){
//            title.setText(getResources().getString(R.string.app_name));
//            yes.setText(getResources().getString(R.string.ok));
//            desc.setText(getResources().getString(R.string.order_placed_successfully));
//        }else {
//            title.setText(getResources().getString(R.string.app_name));
//            yes.setText(getResources().getString(R.string.ok_ar));
//            desc.setText(getResources().getString(R.string.order_placed_successfully_ar));
//
//        }
//
//        customDialog = dialogBuilder.create();
//        customDialog.show();
//
//        final AlertDialog finalCustomDialog = customDialog;
//        yes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finalCustomDialog.dismiss();
//                Intent intent = new Intent(CheckoutActivityStep3.this, OrderCompletedActivity.class);
//                intent.putExtra("orderId", getIntent().getStringExtra("invoiceNo"));
//                startActivity(intent);
//                finish();
//            }
//        });
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        Window window = customDialog.getWindow();
//        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        lp.copyFrom(window.getAttributes());
//        //This makes the dialog take up the full width
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int screenWidth = size.x;
//
//        double d = screenWidth*0.85;
//        lp.width = (int) d;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        window.setAttributes(lp);
    }
}
