package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Dialogs.ResetPasswordDialog;
import com.cs.checkclickuser.Models.EditMobileNoResponse;
import com.cs.checkclickuser.Models.ForgetPasswordOTPResponse;
import com.cs.checkclickuser.Models.Signupresponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VerifyOtpDialog extends AppCompatActivity implements View.OnClickListener {

    private String otpEntered = "", strFirstname, strLastname, strEmail, strPassword, strMobile;

    private TextView description;
    public static boolean isResetSuccessful = false;
    private Button btnNext;
    ImageView back_btn;
    EditText inputotp;
    TextView textMobileNumber, buttonResend;
    private String screen = "", userId;
    private static String TAG = "TAG";
    CountDownTimer countDownTimer;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_verify_otp);
        } else {
            setContentView(R.layout.activity_verify_otp_ar);
        }



        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//        OtpReader.bind((OTPListener) this, "cs-test");
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        description = (TextView) findViewById(R.id.verify_desc);
        btnNext = (Button) findViewById(R.id.next_otp);
        buttonResend = (TextView) findViewById(R.id.resend);
        inputotp = (EditText) findViewById(R.id.input_phone);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        strFirstname = getIntent().getStringExtra("FirstName");
        strLastname = getIntent().getStringExtra("LastName");
        strEmail = getIntent().getStringExtra("EmailId");
        strMobile = getIntent().getStringExtra("MobileNo");
        strPassword = getIntent().getStringExtra("Password");
        screen = getIntent().getStringExtra("screen");
//        textMobileNumber.setText(Constants.Country_Code + strMobile);

        if (language.equalsIgnoreCase("En")){
            description.setText(getResources().getString(R.string.verify_desc) + "+966" +
                    getIntent().getStringExtra("MobileNo").replace("+966", "") + " " + getResources().getString(R.string.verify_desc1));

        }else {
            description.setText(getResources().getString(R.string.verify_desc_ar) + "+966" +
                    getIntent().getStringExtra("MobileNo").replace("+966", "") + " " + getResources().getString(R.string.verify_desc1_ar));

        }


        buttonResend.setEnabled(false);
        buttonResend.setAlpha(0.5f);

        setTimerForResend();
        btnNext.setOnClickListener(this);
        buttonResend.setOnClickListener(this);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void setTimerForResend() {
        countDownTimer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String timeRemaining = String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1));
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend) + " in " + timeRemaining);
//                }
//                else {
//                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar) + " in " + timeRemaining);
//                }

//                }
            }

            public void onFinish() {
//                if(getDialog()!=null) {
                if (language.equalsIgnoreCase("En")){
                buttonResend.setText(getResources().getString(R.string.otp_msg_resend));
                }
                else {
                    buttonResend.setText(getResources().getString(R.string.otp_msg_resend_ar));
                }

                buttonResend.setEnabled(true);
                buttonResend.setAlpha(1.0f);
//                }
            }

        }.start();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_otp:
                otpEntered = inputotp.getText().toString();
                if (otpEntered.length() != 4) {

                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                                getResources().getString(R.string.alert_invalid_otp), getResources().getString(R.string.ok), VerifyOtpDialog.this);

                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.otp_alert1),
                                getResources().getString(R.string.alert_invalid_otp_ar), getResources().getString(R.string.ok), VerifyOtpDialog.this);

                    }

                } else {
                    if (screen.equals("register")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new userRegistrationApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else if (screen.equals("forgot")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new ForgetPassVerfiyApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else if (screen.equals("EditProfile")) {
                        String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            new UpdateMobileNoApi().execute();
                        } else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }
                }
                break;

            case R.id.resend:
                String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new verifyMobileApi().execute();
                } else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }

                }
                break;
        }
    }

    private String prepareUpdateMobileJson() {
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("Id", userId);
            mobileObj.put("MobileNo", "+966" + strMobile.replace("+966", ""));
            mobileObj.put("OTPCode", otpEntered);
            mobileObj.put("FlagId", "1");

            Log.d(TAG, "prepareForgetPassVerifyJson: " + mobileObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareForgetPassVerifyJson() {
        JSONObject mobileObj = new JSONObject();
        try {
            mobileObj.put("OTPCode", otpEntered);
            mobileObj.put("MobileNo", strMobile);

            Log.d(TAG, "prepareForgetPassVerifyJson: " + mobileObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareVerifyMobileJson() {
        JSONObject mobileObj = new JSONObject();
        try {
            if (screen.equals("register")) {
                mobileObj.put("MobileNo", "+966" + strMobile);
                mobileObj.put("FlagId", 1);
            } else if (screen.equals("forgot")) {
                mobileObj.put("MobileNo", strMobile);
                mobileObj.put("FlagId", 2);
            } else if (screen.equals("EditProfile")) {
                mobileObj.put("MobileNo", "+966" + strMobile.replace("+966", ""));
                mobileObj.put("FlagId", 1);
            }

            Log.d(TAG, "prepareVerifyMobileJson: " + mobileObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mobileObj.toString();
    }

    private String prepareSignUpJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("FirstName", strFirstname);
            parentObj.put("LastName", strLastname);
            parentObj.put("EmailId", strEmail);
            parentObj.put("MobileNo", "+966" + strMobile);
            parentObj.put("Password", strPassword);
            parentObj.put("OTPCode", otpEntered);
            parentObj.put("Language", "En");
            parentObj.put("DeviceToken", SplashScreen.regId);
            parentObj.put("DeviceVersion", Constants.getDeviceType(VerifyOtpDialog.this));
            parentObj.put("DeviceType", "android");

            Log.d(TAG, "prepareSignUpJson: " + parentObj.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parentObj.toString();
    }

    private class userRegistrationApi extends AsyncTask<String, String, String> {
        String inputStr;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSignUpJson();
            Constants.showLoadingDialog(VerifyOtpDialog.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Signupresponse> call = apiService.userRegistration(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Signupresponse>() {
                @Override
                public void onResponse(Call<Signupresponse> call, Response<Signupresponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        Signupresponse registrationResponse = response.body();
                        if (registrationResponse.getStatus()) {
//                          status true case
                            String userId = registrationResponse.getData().getId();
                            userPrefsEditor.putString("userId", userId);
                            userPrefsEditor.putString("FirstName", registrationResponse.getData().getFirstName());
                            userPrefsEditor.putString("LastName", registrationResponse.getData().getLastName());
                            userPrefsEditor.putString("EmailId", registrationResponse.getData().getEmailId());
                            userPrefsEditor.putString("MobileNo", registrationResponse.getData().getMobileNo());
//                            userPrefsEditor.putString("Password", registrationResponse.getData().getPhone());
//                            userPrefsEditor.putString("pic", registrationResponse.getData().getProfilephoto());
                            userPrefsEditor.commit();
                            Intent intent = new Intent(VerifyOtpDialog.this, MainActivity.class);
//                                intent.putExtra("type", "1");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
//                          status false case
                            String failureResponse = registrationResponse.getMessage();
                            if (language.equalsIgnoreCase("En")){
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            }else {
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            }

                        }
                    } else {
                        if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<Signupresponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(VerifyOtpDialog.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.i(TAG, "otp: " + VerifyMobileResponse.getData().getOTP());
                            } else {
                                if (language.equals("En")){
                                    String failureResponse = VerifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VerifyOtpDialog.this);
                                }else {
                                    String failureResponse = VerifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if(language.equals("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equals("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                    buttonResend.setEnabled(false);
                    buttonResend.setAlpha(0.5f);
                    setTimerForResend();
                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if(language.equals("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equals("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }


    private class ForgetPassVerfiyApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareForgetPassVerifyJson();
            Constants.showLoadingDialog(VerifyOtpDialog.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ForgetPasswordOTPResponse> call = apiService.getforgetpass(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ForgetPasswordOTPResponse>() {
                @Override
                public void onResponse(Call<ForgetPasswordOTPResponse> call, Response<ForgetPasswordOTPResponse> response) {
                    if (response.isSuccessful()) {
                        ForgetPasswordOTPResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.i(TAG, "otp: " + VerifyMobileResponse.getData().getOTP());

                                Intent intent = new Intent(VerifyOtpDialog.this, ResetPasswordDialog.class);
                                intent.putExtra("mobile", strMobile);
                                intent.putExtra("otp", otpEntered);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            } else {
                                if (language.equals("En")) {
                                    String failureResponse = VerifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), VerifyOtpDialog.this);
                                }else {
                                    String failureResponse = VerifyMobileResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok_ar), VerifyOtpDialog.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equals("En")) {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equals("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ForgetPasswordOTPResponse> call, Throwable t) {

                    Log.i(TAG, "onFailure: " + t);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equals("En")) {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class UpdateMobileNoApi extends AsyncTask<String, String, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareUpdateMobileJson();
            Constants.showLoadingDialog(VerifyOtpDialog.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOtpDialog.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<EditMobileNoResponse> call = apiService.getupdateMobileNo(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<EditMobileNoResponse>() {
                @Override
                public void onResponse(Call<EditMobileNoResponse> call, Response<EditMobileNoResponse> response) {
                    if (response.isSuccessful()) {
                        EditMobileNoResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.i(TAG, "otp: " + VerifyMobileResponse.getData().getMobileNo());

                                userPrefsEditor.putString("MobileNo", "+966 " + strMobile.replace("+966", ""));
                                userPrefsEditor.commit();
                                Intent i = new Intent(VerifyOtpDialog.this, MyAccountActivity.class);
                                startActivity(i);
                                finish();

                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), VerifyOtpDialog.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<EditMobileNoResponse> call, Throwable t) {

                    Log.i(TAG, "onFailure: " + t);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(VerifyOtpDialog.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void displayResetPasswordDiaolg() {


    }

}
