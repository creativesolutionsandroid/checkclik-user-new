package com.cs.checkclickuser.Activites;//package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Models.AddAddressResponse;
import com.cs.checkclickuser.Models.AdressCountryResponce;
import com.cs.checkclickuser.Models.CityListResponce;
import com.cs.checkclickuser.Models.EditAddressResponse;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditiAddressActivity extends AppCompatActivity {

    EditText fullname, streetname, apartment, zip, phonenumber;
    Spinner countrylist, citylist, selettype;
    TextView countryview, cityview, selettypeview, title;
    Button addadress;
    String TAG = "TAG";
    ImageView back_btn;
    private TextInputLayout inputLayoutfullname, inputLayoutstreetname, inputLayoutapartment, inputLayoutzip, inputLayoutphonenumber;
    private String strfullname, strstreetname, strapartment, strzip, strphonenumber;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;

    SharedPreferences LanguagePrefs;
    String language;

    private ManageAdressResponce.Data addressArrayList = new ManageAdressResponce.Data();

    ArrayList<String> countryArray = new ArrayList<>();
    ArrayList<String> cityArray = new ArrayList<>();
    ArrayList<String> typeArray = new ArrayList<>();

    private ArrayAdapter<String> countryAdapter, cityAdapter, addresstypedapter;
    int countryId = 0, citylId = 0, Address_type = 0;
    int countySelected = 0, citySelected = 0;
    boolean isFirstTime = true;

    RelativeLayout make_layout, city_layout, select_layout;

    AdressCountryResponce.Data data;
    private List<CityListResponce> filtercitylist = new ArrayList<>();
//    private ArrayList<AdressCountryResponce.Data> data = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.edit_address_activity);
        } else {
            setContentView(R.layout.edit_address_activity_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        fullname = (EditText) findViewById(R.id.fullname);
        streetname = (EditText) findViewById(R.id.streetname);
        apartment = (EditText) findViewById(R.id.apartmentname);
        zip = (EditText) findViewById(R.id.zip);
        phonenumber = (EditText) findViewById(R.id.phone);

        countryview = (TextView) findViewById(R.id.maketxt);
        cityview = (TextView) findViewById(R.id.citytxt);
        selettypeview = (TextView) findViewById(R.id.adresstype);
        title = (TextView) findViewById(R.id.title);

        make_layout = (RelativeLayout) findViewById(R.id.make_layout);
        city_layout = (RelativeLayout) findViewById(R.id.city_layout);
        select_layout = (RelativeLayout) findViewById(R.id.select_layout);

        inputLayoutfullname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutstreetname = (TextInputLayout) findViewById(R.id.layout_street);
        inputLayoutapartment = (TextInputLayout) findViewById(R.id.layout_apartmentname);
        inputLayoutzip = (TextInputLayout) findViewById(R.id.layout_zip);
        inputLayoutphonenumber = (TextInputLayout) findViewById(R.id.layout_phone);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        countrylist = (Spinner) findViewById(R.id.countrylist);
        citylist = (Spinner) findViewById(R.id.citylist);
        selettype = (Spinner) findViewById(R.id.adresstypelist);
        addadress = (Button) findViewById(R.id.addadress);

        try {
            addressArrayList = (ManageAdressResponce.Data) getIntent().getSerializableExtra("addressarry");
        } catch (Exception e) {
            e.printStackTrace();
        }

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fullname.setText(addressArrayList.getFullName());
        streetname.setText(addressArrayList.getAddress1());
        apartment.setText(addressArrayList.getAddress2());
        zip.setText(addressArrayList.getZipcode());
        phonenumber.setText(addressArrayList.getPhoneNumber().replace("+966", ""));

        addadress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(EditiAddressActivity.this);

                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new EditAddressApi().execute();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        make_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        city_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (countryId == 0) {
                    if (language.equalsIgnoreCase("En")) {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_country), getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EditiAddressActivity.this);
                    } else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_country_ar), getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar), EditiAddressActivity.this);
                    }
                }
            }
        });

//        select_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                setAddressSpinner();
//
//            }
//        });
        setAddressSpinner();

        selettype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selettypeview.setText(typeArray.get(i));
                if (selettypeview.getText().toString().equals("Home")
                        || selettypeview.getText().toString().equals("الصفحة الرئيسية") ) {
                    Address_type = 1;
                } else if (selettypeview.getText().toString().equals("Office")
                        || selettypeview.getText().toString().equals("مكتب. مقر. مركز")) {
                    Address_type = 2;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        citylist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!isFirstTime) {
                    cityview.setText(cityArray.get(i));
                    citylId = data.getCityList().get(i).getId();
                }
                else {
                    isFirstTime = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        countrylist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!isFirstTime) {
                    countryview.setText(countryArray.get(i));
//                if (i > 0) {
                    countryId = data.getCountryList().get(i).getId();

                    for (int j = 0; j < data.getCityList().size(); j++) {
                        filtercitylist.clear();
                        if (data.getCountryList().get(countySelected).getId() == data.getCityList().get(j).getCountryId()) {
                            CityListResponce cityArry = new CityListResponce();
                            cityArry.setCountryEn(data.getCityList().get(i).getCountryEn());
                            cityArry.setCountryAr(data.getCityList().get(i).getCountryAr());
                            cityArry.setCountryId(data.getCityList().get(i).getCountryId());
                            cityArry.setId(data.getCityList().get(i).getId());
                            cityArry.setLatitude(data.getCityList().get(i).getLatitude());
                            cityArry.setLongitude(data.getCityList().get(i).getLongitude());
                            cityArry.setCountryEn(data.getCityList().get(i).getCountryEn());
                            cityArry.setCountryAr(data.getCityList().get(i).getCountryAr());
                            filtercitylist.add(cityArry);
                        }
                    }

                    countySelected = i;

                    if (cityAdapter != null) {
                        cityArray.clear();
//                    cityArray.add("-- Select City --");
                        for (int k = 0; k < data.getCityList().size(); k++) {

                            if (data.getCountryList().get(countySelected).getId() == data.getCityList().get(k).getCountryId()) {
                                cityArray.add(data.getCityList().get(k).getNameEn());
                            }
                        }
                        if (cityArray != null && cityArray.size() > 0) {
                            citylist.setAdapter(cityAdapter);
                            citylId = data.getCityList().get(0).getId();
                        } else {
                            citylist.setAdapter(null);
                            cityview.setText("City");
                            citylId = 0;
                        }
                        cityAdapter.notifyDataSetChanged();
//                    Log.i(TAG, "setCitySpinner: " + cityArray);
                    }
                }
//                }
//                else {
//                    countryId = 0;
//                    citylist.setAdapter(null);
//                }

//                    typeArray.clear();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        if (address_boolean) {
//            title.setText("" + getResources().getString(R.string.edit_address));
//            addadress.setText("" + getResources().getString(R.string.edit_address));
////            String networkStatus = NetworkUtil.getConnectivityStatusString(AddAdressActivity.this);
////            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
////                new EditAddressApi().execute();
////            } else {
////                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
////            }
//        }
//        else {
        String networkStatus = NetworkUtil.getConnectivityStatusString(EditiAddressActivity.this);
        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
            new getcountylistApi().execute();
        } else {
            if (language.equalsIgnoreCase("En")){
                Toast.makeText(getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
            }

        }
//        }

        fullname.addTextChangedListener(new TextWatcher(fullname));
        streetname.addTextChangedListener(new TextWatcher(streetname));
        apartment.addTextChangedListener(new TextWatcher(apartment));
        zip.addTextChangedListener(new TextWatcher(zip));
        phonenumber.addTextChangedListener(new TextWatcher(phonenumber));

    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.fullname:
                    clearErrors();
                    break;
                case R.id.streetname:
                    clearErrors();
                    break;
                case R.id.apartmentname:
                    clearErrors();
                    break;
                case R.id.zip:
                    clearErrors();
                    break;
                case R.id.phone:
                    clearErrors();
                    phonenumber.setCursorVisible(true);
                    String enteredMobile = editable.toString();
                    if (!enteredMobile.contains(Constants.Country_Code)) {
                        if (enteredMobile.length() > Constants.Country_Code.length()) {
                            enteredMobile = enteredMobile.substring((Constants.Country_Code.length() - 1));
                            phonenumber.setText(Constants.Country_Code + enteredMobile);
                        } else {
                            phonenumber.setText(Constants.Country_Code);
                        }
                        phonenumber.setSelection(phonenumber.length());
                    }
                    break;
            }
        }
    }

    private void clearErrors() {
        inputLayoutfullname.setErrorEnabled(false);
        inputLayoutstreetname.setErrorEnabled(false);
        inputLayoutapartment.setErrorEnabled(false);
        inputLayoutzip.setErrorEnabled(false);
        inputLayoutphonenumber.setErrorEnabled(false);
    }

    private boolean validation() {
        strfullname = fullname.getText().toString();
        strstreetname = streetname.getText().toString();
        strapartment = apartment.getText().toString();
        strzip = zip.getText().toString();
        strphonenumber = phonenumber.getText().toString();
        strphonenumber = strphonenumber.replace("+966 ", "");

        if (strfullname.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutfullname.setError(getResources().getString(R.string.error_fullname_number));
            } else {
                inputLayoutfullname.setError(getResources().getString(R.string.error_fullname_number_ar));
            }

            Constants.requestEditTextFocus(fullname, EditiAddressActivity.this);
            return false;
        } else if (strstreetname.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutstreetname.setError(getResources().getString(R.string.error_streetname));
            } else {
                inputLayoutstreetname.setError(getResources().getString(R.string.error_streetname_ar));
            }

            Constants.requestEditTextFocus(streetname, EditiAddressActivity.this);
            return false;
        } else if (strapartment.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutapartment.setError(getResources().getString(R.string.error_apartment));
            } else {
                inputLayoutapartment.setError(getResources().getString(R.string.error_apartment_ar));
            }

            Constants.requestEditTextFocus(apartment, EditiAddressActivity.this);
            return false;
        } else if (strzip.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutzip.setError(getResources().getString(R.string.error_zip));
            } else {
                inputLayoutzip.setError(getResources().getString(R.string.error_zip_ar));
            }

            Constants.requestEditTextFocus(zip, EditiAddressActivity.this);
            return false;
        } else if (strzip.length() < 6) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutzip.setError(getResources().getString(R.string.error_Invalid_zip));
            } else {
                inputLayoutzip.setError(getResources().getString(R.string.error_Invalid_zip_ar));
            }

            Constants.requestEditTextFocus(zip, EditiAddressActivity.this);
            return false;
        } else if (strphonenumber.length() == 0) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutphonenumber.setError(getResources().getString(R.string.error_phonenumber));
            } else {
                inputLayoutphonenumber.setError(getResources().getString(R.string.error_phonenumber_ar));
            }

            Constants.requestEditTextFocus(phonenumber, EditiAddressActivity.this);
            return false;
        } else if (strphonenumber.length() != 9) {
            if (language.equalsIgnoreCase("En")) {
                inputLayoutphonenumber.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            } else {
                inputLayoutphonenumber.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }

            Constants.requestEditTextFocus(phonenumber, EditiAddressActivity.this);
            return false;
        } else if (countryId == 0) {
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_country), getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EditiAddressActivity.this);
            } else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_country_ar), getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar), EditiAddressActivity.this);
            }

            return false;
        } else if (citylId == 0) {
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_city), getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EditiAddressActivity.this);
            } else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_city_ar), getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar), EditiAddressActivity.this);
            }

            return false;
        } else if (Address_type == 0) {
            if (language.equalsIgnoreCase("En")) {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_address_type), getResources().getString(R.string.app_name), getResources().getString(R.string.ok), EditiAddressActivity.this);
            } else {
                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_address_type_ar), getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar), EditiAddressActivity.this);
            }

            return false;
        }
        return true;
    }

    private class getcountylistApi extends AsyncTask<String, String, String> {

        String inputStr;
        String TAG = "TAG";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(EditiAddressActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(EditiAddressActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AdressCountryResponce> call = apiService.getcounrylist();
            call.enqueue(new Callback<AdressCountryResponce>() {
                @Override
                public void onResponse(Call<AdressCountryResponce> call, Response<AdressCountryResponce> response) {
                    if (response.isSuccessful()) {
                        data = response.body().getData();
                        setCountrySpinner();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<AdressCountryResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(EditiAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private void setCountrySpinner() {
        countryArray.clear();
//        countryArray.add("-- Select Country --");
        if (data != null && data.getCountryList() != null && data.getCountryList().size() > 0) {
            for (int i = 0; i < data.getCountryList().size(); i++) {

                if (language.equalsIgnoreCase("En")) {
                    countryArray.add(data.getCountryList().get(i).getNameEn());
                } else {
                    countryArray.add(data.getCountryList().get(i).getNameAr());
                }

                if (isFirstTime && addressArrayList.getCountryId() == data.getCountryList().get(i).getId()) {
                    countySelected = i;
                    countryId = data.getCountryList().get(i).getId();
                }
            }

            countryAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, countryArray) {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);

                    ((TextView) v).setTextSize(1);
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                    return v;
                }
            };

        }

        countrylist.setAdapter(countryAdapter);
        countrylist.setSelection(countySelected);
        setCitySpinner();

    }

    private void setCitySpinner() {
//        countryArray.add("-- Select Country --");

        cityArray.clear();
//        cityArray.add("-- Select City --");
        for (int i = 0; i < data.getCityList().size(); i++) {

            if (data.getCountryList().get(countySelected).getId() == data.getCityList().get(i).getCountryId()) {

                if (language.equalsIgnoreCase("En")) {
                    cityArray.add(data.getCityList().get(i).getNameEn());
                } else {
                    cityArray.add(data.getCityList().get(i).getNameAr());
                }
                if (isFirstTime && addressArrayList.getCityId() == data.getCityList().get(i).getId()) {
                    citySelected = i;
                    citylId = data.getCityList().get(i).getId();
                }
            }
        }

        cityAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, cityArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }
        };

        citylist.setAdapter(cityAdapter);
        citylist.setSelection(citySelected);

        setSpinners();
    }

    private void setSpinners(){
        countrylist.setSelection(countySelected);
        citylist.setSelection(citySelected);
        countryview.setText(countryArray.get(countySelected));
        cityview.setText(cityArray.get(citySelected));

        if (addressArrayList.getAddressType() == 1) {
            Address_type = 1;
        } else {
            Address_type = 2;
            selettype.setSelection(1);
        }
    }

    private void setAddressSpinner() {
//        countryArray.add("-- Select Country --");

        typeArray.clear();
//        typeArray.add("-- Select Address Type --");
        if (language.equalsIgnoreCase("En")) {
            typeArray.add("Home");
            typeArray.add("Office");
        } else {
            typeArray.add("الصفحة الرئيسية");
            typeArray.add("مكتب. مقر. مركز");
        }

        addresstypedapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_spinner, typeArray) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }
        };

        selettype.setAdapter(addresstypedapter);
    }

    private class EditAddressApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetAddressJSON();
            Constants.showLoadingDialog(EditiAddressActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<EditAddressResponse> call = apiService.getEditAddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<EditAddressResponse>() {
                @Override
                public void onResponse(Call<EditAddressResponse> call, Response<EditAddressResponse> response) {
                    Log.d("TAG", "onResponse: " + response);
                    if (response.isSuccessful()) {
                        finish();
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<EditAddressResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditiAddressActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(EditiAddressActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditiAddressActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EditiAddressActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }


                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }


    private String prepareGetAddressJSON() {
        JSONObject parentObj = new JSONObject();
//        if (address_boolean) {

        if (selettypeview.getText().toString().equals("Home")
                || selettypeview.getText().toString().equals("الصفحة الرئيسية") ) {
            Address_type = 1;
        } else if (selettypeview.getText().toString().equals("Office")
                || selettypeview.getText().toString().equals("مكتب. مقر. مركز")) {
            Address_type = 2;
        }

        try {
            parentObj.put("Id", addressArrayList.getId());
            parentObj.put("UserId", userId);
            parentObj.put("FullName", strfullname);
            parentObj.put("Address1", strstreetname);
            parentObj.put("Address2", strapartment);
            parentObj.put("CountryId", countryId);
            parentObj.put("CityId", citylId);
            parentObj.put("Zipcode", strzip);
            parentObj.put("PhoneNumber", strphonenumber.replace("+966 ", ""));
            parentObj.put("AddressType", Address_type);
            parentObj.put("FlagId", 2);

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        }

        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

}
