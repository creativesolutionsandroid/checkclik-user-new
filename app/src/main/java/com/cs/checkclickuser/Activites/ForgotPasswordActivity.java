package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.cs.checkclickuser.Models.VerifyMobileResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {

    ImageView back_btn;
    EditText inputMobile;
    Button send;
    String strMobile;
    private TextInputLayout inputLayoutMobile;
    public static boolean isOTPSuccessful = false;
    public static boolean isResetSuccessful = false;
    private String serverOtp;
    public static final String TAG = "TAG";

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.forgotpassword_activity);
        } else {
            setContentView(R.layout.forgotpassword_activity_ar);
        }



        back_btn=(ImageView)findViewById(R.id.back_btn);
        inputMobile =(EditText)findViewById(R.id.forgot_input_phone);
        send=(Button)findViewById(R.id.bt_continue);
        inputLayoutMobile=(TextInputLayout)findViewById(R.id.layout_forgot_input_phone);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new verifyMobileApi().execute();
                    }
                    else{
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                        }
                }
            }

        });

        inputMobile.addTextChangedListener(new TextWatcher() {
            String previousText = "";
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                previousText = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String text = charSequence.toString();
                if (text.matches("[0-9]+") && !text.contains("+966") && text.length() >= 1 && text.length() <= 9) {
                    if (text.length() >= 1 && text.startsWith("5")) {
                        inputMobile.removeTextChangedListener(this);
                        inputMobile.setText("+966 " + text);
                        inputMobile.setSelection(inputMobile.length());
                        inputMobile.addTextChangedListener(this);
                    }
//                    else {
//                        inputMobile.removeTextChangedListener(this);
//                        inputMobile.setText("");
////                    inputMobile.setSelection(inputMobile.length());
//                        inputMobile.addTextChangedListener(this);
//                    }
                } else {
                    String textCopy = text;
                    textCopy = textCopy.replace("+966 ", "");
                    if (textCopy.matches("[0-9]+") && textCopy.length() <= 9) {

                    } else {
                        if (text.contains("+966")) {
                            inputMobile.removeTextChangedListener(this);
                            text = text.replace("+966 ", "");
                            inputMobile.setText(text);
                            inputMobile.setSelection(inputMobile.length());
                            inputMobile.addTextChangedListener(this);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void clearErrors(){
        inputLayoutMobile.setErrorEnabled(false);
    }

    private class MyTextWatcher implements TextWatcher {
        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.layout_forgot_input_phone:
                    clearErrors();
                    break;
            }
        }
    }

    private boolean validations(){
        strMobile = inputMobile.getText().toString();
        strMobile = strMobile.replace("+966 ","");
        if(strMobile.matches("[0-9]+")) {
            strMobile = "+966"+strMobile;
        }

        if (strMobile.length() == 0) {
            if (language.equals("En")){
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile));

            }else {
                inputMobile.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }


            Constants.requestEditTextFocus(inputMobile, ForgotPasswordActivity.this);
            return false;
        }
        return true;
    }

//    private void displayResetPasswordDiaolg(){
//        Bundle args = new Bundle();
//        args.putString("mobile", strMobile);
//
//        isResetSuccessful = false;
//
//        final ResetPasswordDialog newFragment = ResetPasswordDialog.newInstance();
//        newFragment.setCancelable(false);
//        newFragment.setArguments(args);
//        newFragment.show(getSupportFragmentManager(), "forgot");
//
//        getSupportFragmentManager().executePendingTransactions();
//        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                //do whatever you want when dialog is dismissed
//                if (newFragment != null) {
//                    newFragment.dismiss();
//                }
//
//                if(isResetSuccessful){
//                    setResult(RESULT_OK);
//                    finish();
//                }
//            }
//        });
//    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(ForgotPasswordActivity.this);

        }
        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyMobileResponse.getData().getOTP());
                                showVerifyDialog();
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ForgotPasswordActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }



                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ForgotPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareVerifyMobileJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("MobileNo", strMobile);
            parentObj.put("FlagId", "2");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }
    private void showVerifyDialog() {
        Intent intent = new Intent(ForgotPasswordActivity.this, VerifyOtpDialog.class);
        intent.putExtra("screen","forgot");
        intent.putExtra("MobileNo", strMobile);
        startActivity(intent);
    }
}
