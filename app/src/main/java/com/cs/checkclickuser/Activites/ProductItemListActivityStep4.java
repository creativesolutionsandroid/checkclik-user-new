package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.ProductItemGridAdapter;
import com.cs.checkclickuser.Adapter.ProductItemListAdapter;
import com.cs.checkclickuser.Adapter.SearchListAdapter;
import com.cs.checkclickuser.Models.ProductItemResponse;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.Models.SearchListResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BarCodeScanner;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;

public class ProductItemListActivityStep4 extends AppCompatActivity {

    private ProductstoreResponce.Data storeArrayList;
    private ArrayList<ProductItemResponse.ProductList> productLists = new ArrayList<>();
    int subCatPos;
    ImageView back_btn, gridViewIcon, listViewIcon, cart, search, notification;
    TextView branchname, categoryName;
    private static String TAG = "TAG";
    private int pageNumber = 1;
    AlertDialog customDialog;
    ProductItemGridAdapter mGridAdapter;
    ProductItemListAdapter mListAdapter;
    RecyclerView productsList;
    boolean gridSelected = true;
    SharedPreferences userPrefs;
    int type;

    private RelativeLayout searchLayout;
    private EditText productSearch;
    private boolean isSearchVisible = false;
    private String searchText = "";
    private ArrayList<String> searchlistarray = new ArrayList<>();
    SearchListAdapter mSearchlistAdapter;
    ListView searchlistview;
    LinearLayout itemsLayout;
    String searchTextLocal = "";
    private ImageView cartStatusIcon, notifictionStatusIcon;

    private ImageView scanner;
    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };


    SharedPreferences LanguagePrefs;
    String language = "En";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_product_items);
        } else {
            setContentView(R.layout.activity_product_items_ar);

        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        gridViewIcon = (ImageView) findViewById(R.id.gridview_icon);
        listViewIcon = (ImageView) findViewById(R.id.listview_icon);
        branchname = (TextView) findViewById(R.id.store_name);
        categoryName = (TextView) findViewById(R.id.category_name);
        productsList = (RecyclerView) findViewById(R.id.product_listview);
        cart = (ImageView) findViewById(R.id.cart);

        search = (ImageView) findViewById(R.id.search);
        notification = (ImageView) findViewById(R.id.product_noti);
        searchLayout = (RelativeLayout) findViewById(R.id.search_layout);
        itemsLayout = (LinearLayout) findViewById(R.id.items_layout);
        productSearch = (EditText) findViewById(R.id.product_search);
        searchlistview = (ListView)findViewById(R.id.searchlist);

        scanner=(ImageView) findViewById(R.id.cameraicon);
        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        int icback_btnId = getResources().getIdentifier("ic_back_"+Constants.appColor, "drawable", getPackageName());
        back_btn.setImageDrawable(getResources().getDrawable(icback_btnId));

        int icnotificationId = getResources().getIdentifier("ic_noti_"+Constants.appColor, "drawable", getPackageName());
        notification.setImageDrawable(getResources().getDrawable(icnotificationId));

        int iccartId = getResources().getIdentifier("ic_cart_"+Constants.appColor, "drawable", getPackageName());
        cart.setImageDrawable(getResources().getDrawable(iccartId));

        int icsearchId = getResources().getIdentifier("search_"+Constants.appColor, "drawable", getPackageName());
        search.setImageDrawable(getResources().getDrawable(icsearchId));

        int icgridViewIconId = getResources().getIdentifier("gridview_icon_"+Constants.appColor, "drawable", getPackageName());
        gridViewIcon.setImageDrawable(getResources().getDrawable(icgridViewIconId));

        int iclistViewIconId = getResources().getIdentifier("listviewicon_"+Constants.appColor, "drawable", getPackageName());
        listViewIcon.setImageDrawable(getResources().getDrawable(iclistViewIconId));

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductItemListActivityStep4.this);
                } else {
                    Intent intent = new Intent(ProductItemListActivityStep4.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(ProductItemListActivityStep4.this);
                    }
                } else {
                    BarCodeScanner.showScannerDialog(ProductItemListActivityStep4.this);
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userPrefs.getString("userId", "").equals("")) {

                    Constants.showTwoButtonAlertDialog(ProductItemListActivityStep4.this);

                } else {

                    Intent intent = new Intent(ProductItemListActivityStep4.this, CartActivity.class);
                    startActivity(intent);

                }
            }
        });

        storeArrayList = (ProductstoreResponce.Data) getIntent().getSerializableExtra("stores");
        subCatPos = getIntent().getIntExtra("pos", 0);
        type = getIntent().getIntExtra("type", 1);

        if (language.equals("En")){
            branchname.setText(storeArrayList.getMainCategory().get(getIntent().getIntExtra("storePos", 0)).getNameEn());
        }else {
            branchname.setText(storeArrayList.getMainCategory().get(getIntent().getIntExtra("storePos", 0)).getNameAr());
        }
        categoryName.setText(getIntent().getStringExtra("name"));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        gridViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridSelected = true;
                mGridAdapter = new ProductItemGridAdapter(ProductItemListActivityStep4.this, productLists, getIntent().getIntExtra("storeId", 0), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivityStep4.this);
                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivityStep4.this, 2));
                productsList.setAdapter(mGridAdapter);
            }
        });

        listViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gridSelected = false;
                mListAdapter = new ProductItemListAdapter(ProductItemListActivityStep4.this, productLists, getIntent().getIntExtra("storeId", 0), type);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivityStep4.this);
                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivityStep4.this, 1));
                productsList.setAdapter(mListAdapter);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSearchVisible) {
                    searchLayout.setVisibility(View.VISIBLE);
                    isSearchVisible = true;
                    productSearch.requestFocus();
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                    productSearch.setText(searchTextLocal);
                    itemsLayout.setVisibility(View.GONE);
                    searchlistview.setVisibility(View.VISIBLE);
                }
                else {
                    searchLayout.setVisibility(View.GONE);
                    isSearchVisible = false;
                    itemsLayout.setVisibility(View.VISIBLE);
                    searchlistview.setVisibility(View.GONE);
                    searchText = "";
                    searchTextLocal = "";
                    new GetstoreApi().execute();

                    hideKeyboard(ProductItemListActivityStep4.this);
                }
            }
        });

        productSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                itemsLayout.setVisibility(View.GONE);
                searchlistview.setVisibility(View.VISIBLE);
                if(editable.toString().length() > 0) {
                    searchTextLocal = editable.toString();
                }
                else {
                    searchTextLocal = "";
                }
                new SearchApi().execute(searchTextLocal);
            }
        });

        productSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        productSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(ProductItemListActivityStep4.this);
                    return true;
                }
                return false;
            }
        });

        searchlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                searchText = searchlistarray.get(i);
                searchTextLocal = searchlistarray.get(i);
                productSearch.setText(searchTextLocal);
                productSearch.setSelection(productSearch.length());
                hideKeyboard(ProductItemListActivityStep4.this);
                new GetstoreApi().execute();
            }
        });
        new GetstoreApi().execute();
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductItemListActivityStep4.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductItemResponse> call = apiService.getProductList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductItemResponse>() {
                @Override
                public void onResponse(Call<ProductItemResponse> call, Response<ProductItemResponse> response) {
                    Log.d(TAG, "onResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            productLists = response.body().getData().getProductlist();

                            itemsLayout.setVisibility(View.VISIBLE);
                            searchlistview.setVisibility(View.GONE);

                            if (response.body().getData().getTotalrecords().get(0).getTotalrecords() > 0) {
                                mGridAdapter = new ProductItemGridAdapter(ProductItemListActivityStep4.this, productLists, getIntent().getIntExtra("storeId", 0), type);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemListActivityStep4.this);
                                productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivityStep4.this, 2));
                                productsList.setAdapter(mGridAdapter);
                            }
                            Constants.closeLoadingDialog();
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductItemListActivityStep4.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ProductItemResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductItemListActivityStep4.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductItemListActivityStep4.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductItemListActivityStep4.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductItemListActivityStep4.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductItemListActivityStep4.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("PageNumber", pageNumber);
            parentObj.put("PageSize", 100);
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            parentObj.put("BranchSubCategoryId", getIntent().getIntExtra("id", 0));
            parentObj.put("SortId", 1);
            parentObj.put("SearchText",  searchText);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class SearchApi extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SearchListResponce> call = apiService.getsearcresult(params[0],language);
            call.enqueue(new Callback<SearchListResponce>() {
                @Override
                public void onResponse(Call<SearchListResponce> call, Response<SearchListResponce> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {
                        SearchListResponce products = response.body();
                        if (products.getStatus()) {
                            searchlistarray = products.getData();

                            mSearchlistAdapter = new SearchListAdapter(ProductItemListActivityStep4.this, searchlistarray);
                            searchlistview.setAdapter(mSearchlistAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SearchListResponce> call, Throwable t) {

                }
            });
            return null;
        }
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProductItemListActivityStep4.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(ProductItemListActivityStep4.this);
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                    }else

                    Toast.makeText(this, getResources().getString(R.string.camera_permission_denied_Ar), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (gridSelected) {
            mGridAdapter = new ProductItemGridAdapter(ProductItemListActivityStep4.this, productLists, getIntent().getIntExtra("storeId", 0), type);
            productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivityStep4.this, 2));
            productsList.setAdapter(mGridAdapter);
        } else {
            mListAdapter = new ProductItemListAdapter(ProductItemListActivityStep4.this, productLists, getIntent().getIntExtra("storeId", 0), type);
            productsList.setLayoutManager(new GridLayoutManager(ProductItemListActivityStep4.this, 1));
            productsList.setAdapter(mListAdapter);
        }

        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (Constants.notificationCount != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }
    }
}

