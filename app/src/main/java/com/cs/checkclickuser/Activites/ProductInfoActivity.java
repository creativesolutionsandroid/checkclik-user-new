package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.AddToCartResponse;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductInfoActivity extends AppCompatActivity {

    ImageView backbtn,cart,notification;
    Button addtocart;
    TextView description ,conditions,iteamnaem,iteamcat,iteamprice;
    RelativeLayout descriptionLayout, conditionLayout;
    int postion;
    ArrayList<ProductVariantResponse.ProductList>productLists = new ArrayList<>();
    SharedPreferences userPrefs;
    private AlertDialog customDialog;
    private int cartFlag = 1;

    SharedPreferences LanguagePrefs;
    String language;

    private boolean isDescriptionVisible, isConditionVisible;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_info_activity);
        } else {
            setContentView(R.layout.product_info_activity_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        descriptionLayout=(RelativeLayout) findViewById(R.id.description_layout);
        conditionLayout=(RelativeLayout) findViewById(R.id.condition_layout);

        description=(TextView)findViewById(R.id.dsc);
        conditions=(TextView)findViewById(R.id.condition);
        iteamnaem=(TextView)findViewById(R.id.iteamname);
        iteamcat=(TextView)findViewById(R.id.iteamcat);
        iteamprice=(TextView)findViewById(R.id.iteamprice);
        addtocart=(Button)findViewById(R.id.addtocart);
        notification=(ImageView) findViewById(R.id.product_noti);
        cart=(ImageView) findViewById(R.id.product_cart);
        backbtn=(ImageView) findViewById(R.id.back_btn);

        int icback_btnId = getResources().getIdentifier("ic_back_"+Constants.appColor, "drawable", getPackageName());
        backbtn.setImageDrawable(getResources().getDrawable(icback_btnId));

        int icnotificationId = getResources().getIdentifier("ic_noti_"+Constants.appColor, "drawable", getPackageName());
        notification.setImageDrawable(getResources().getDrawable(icnotificationId));

        int iccartId = getResources().getIdentifier("ic_cart_"+Constants.appColor, "drawable", getPackageName());
        cart.setImageDrawable(getResources().getDrawable(iccartId));
        addtocart.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        productLists = (ArrayList<ProductVariantResponse.ProductList>) getIntent().getSerializableExtra("Arraylist");
        getIntent().getIntExtra("iteampos",postion);
        Log.d("TAG", "arraylistsize: "+productLists.size());

        description.setText(productLists.get(postion).getDescriptionEn1());
        if (productLists.get(postion).getCondition()==1){
            conditions.setText("" + getResources().getString(R.string.new_str));
        }
        else {
            conditions.setText("" + getResources().getString(R.string.used));
        }

        if(language.equals("En")){
            iteamnaem.setText(productLists.get(postion).getProductNameEn());
        }else {
            iteamnaem.setText(productLists.get(postion).getProductNameAr());
        }

        iteamprice.setText("SAR  "+Constants.priceFormat1.format(productLists.get(postion).getPrice()));
        iteamcat.setText(productLists.get(postion).getSubCatEn());

        descriptionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isDescriptionVisible) {
                    isDescriptionVisible = false;
                    description.setVisibility(View.GONE);
                }
                else {
                    isDescriptionVisible = true;
                    description.setVisibility(View.VISIBLE);
                }
            }
        });

        conditionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConditionVisible) {
                    isConditionVisible = false;
                    conditions.setVisibility(View.GONE);
                }
                else {
                    isConditionVisible = true;
                    conditions.setVisibility(View.VISIBLE);
                }
            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addtocart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AddToCart().execute();
            }
        });

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductInfoActivity.this);
                } else {
                    Intent intent = new Intent(ProductInfoActivity.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductInfoActivity.this);
                } else {
                    Intent intent = new Intent(ProductInfoActivity.this, CartActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private class AddToCart extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAddToCartJSON();
            Constants.showLoadingDialog(ProductInfoActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<AddToCartResponse> call = apiService.addToCart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<AddToCartResponse>() {
                @Override
                public void onResponse(Call<AddToCartResponse> call, Response<AddToCartResponse> response) {
                    Log.d("TAG", "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus() && response.body().getStatusmessage().equals("Success")) {
                            Constants.closeLoadingDialog();
                            showSuccessDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (failureResponse.equals("0")) {
                                showClearCartDialog();
                            }
                            else {
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), ProductInfoActivity.this);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductInfoActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ProductInfoActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ProductInfoActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareAddToCartJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("StockQuantity",  productLists.get(postion).getAllowedQty());
            parentObj.put("ProductInventoryId",  productLists.get(postion).getProductInventoryId());
            parentObj.put("StoreId",  getIntent().getIntExtra("storeId", 0));
            parentObj.put("OrderType",  getIntent().getIntExtra("type", 0));
            parentObj.put("BranchId",  getIntent().getIntExtra("branchId", 0));
            parentObj.put("Flag", cartFlag);
            parentObj.put("UserId",  userPrefs.getString("userId",""));
            cartFlag = 1;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareAddToCartJSON: "+parentObj);
        return parentObj.toString();
    }

    private void showSuccessDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.cart_success_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // close this activity
                customDialog.dismiss();
                finish();
            }
        }, 2000);

        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the progressDialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.65;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }

    public void showClearCartDialog(){

        AlertDialog customDialog = null;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductInfoActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        int layout = R.layout.alert_dialog;
        View dialogView = inflater.inflate(layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        TextView title = (TextView) dialogView.findViewById(R.id.title);
        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

        if (language.equalsIgnoreCase("En")){
            title.setText("" + getResources().getString(R.string.replace_cart_products));
            yes.setText("" + getResources().getString(R.string.yes));
            no.setText("" + getResources().getString(R.string.no));
            desc.setText("" + getResources().getString(R.string.your_cart_contains_products));

        }else {

            title.setText("" + getResources().getString(R.string.replace_cart_products_ar));
            yes.setText("" + getResources().getString(R.string.yes_ar));
            no.setText("" + getResources().getString(R.string.no_ar));
            desc.setText("" + getResources().getString(R.string.your_cart_contains_products_ar));

        }

        customDialog = dialogBuilder.create();
        customDialog.show();

        final AlertDialog finalCustomDialog = customDialog;
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalCustomDialog.dismiss();
            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartFlag = 0;
                new AddToCart().execute();
                finalCustomDialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

    }
}
