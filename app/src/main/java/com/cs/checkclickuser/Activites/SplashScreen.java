package com.cs.checkclickuser.Activites;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Firebase.Config;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences userPrefs;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    BroadcastReceiver mRegistrationBroadcastReceiver;
    public static String regId = "";
    private String decryptUrl = "";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        Constants.CityName = userPrefs.getString("city", "");
        Constants.CityNameAr = userPrefs.getString("city_ar", "");
        Constants.CityId = userPrefs.getInt("cityId", 0);
        Constants.DistrictName = userPrefs.getString("district", "");
        Constants.DistrictNameAr = userPrefs.getString("district_ar", "");
        Constants.DistrictId = userPrefs.getInt("districtId", 0);
//        Constants.Latitude = Double.parseDouble(userPrefs.getString("lat", "0.0"));
//        Constants.Longitude = Double.parseDouble(userPrefs.getString("lng", "0.0"));

        // App links(Universal links)
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            decryptUrl = appLinkData.toString();
            Log.d("TAG", "onCreate: "+decryptUrl);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    regId = pref.getString("regId", null);

                    Log.i("TAG", "Firebase reg id: " + regId);
                }
            }
        };

        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        regId = task.getResult().getToken();
                        Log.i("TAG", "reg id: " + regId);
                    }
                });

        if (decryptUrl.equals("")) {
            new Handler().postDelayed(new Runnable() {
                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    if (userPrefs.getBoolean("skip", false) || !userPrefs.getString("userId", "").equals("")) {
                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(SplashScreen.this, StartScrrenActivity.class);
                        startActivity(i);
                    }

                    // close this activity
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
        else {
            Intent i = new Intent(SplashScreen.this, MainActivity.class);
            i.putExtra("url", decryptUrl);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }
    }
}












