package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.app.AlertDialog;

import com.cs.checkclickuser.R;

import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingActivity extends AppCompatActivity {

    TextView logout,myaccunt,manageadress,pushnotification,contact,shareapp,tremsconditions,changelanguage;
    ImageView back_Btn;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    AlertDialog customDialog;
    String fistname;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.setting_activity);
        } else {
            setContentView(R.layout.setting_activity_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        logout =(TextView)findViewById(R.id.logout);
        myaccunt=(TextView)findViewById(R.id.myaccount);
        back_Btn=(ImageView)findViewById(R.id.back_btn);
        manageadress=(TextView)findViewById(R.id.manageaccout_text);
        changelanguage=(TextView)findViewById(R.id.chngelanguage) ;
        tremsconditions=(TextView)findViewById(R.id.treams);
        pushnotification=(TextView)findViewById(R.id.pushnotifaction);
        contact=(TextView)findViewById(R.id.concat);
        shareapp=(TextView)findViewById(R.id.shareapp);

        fistname=userPrefs.getString("name" ,null);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showtwoButtonsAlertDialog();
            }
        });
        myaccunt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, MyAccountActivity.class);
                startActivity(i);
            }
        });

        manageadress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, ManageAdreesActivity.class);
                i.putExtra("screen", "settings");
                startActivity(i);
            }
        });
        changelanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, ChangeLanguage.class);
                startActivity(i);
            }
        });

        tremsconditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fbIntent = new Intent(SettingActivity.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Terms and Conditions");
                fbIntent.putExtra("url", "http://checkclik.csadms.com/home/TermsConditions");
                startActivity(fbIntent);
            }
        });

        pushnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SettingActivity.this, PushNotificationActivity.class);
                startActivity(i);
            }
        });


        back_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("plain/text");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                    i.putExtra(Intent.EXTRA_SUBJECT, "CheckClik app feedback");
                    i.putExtra(Intent.EXTRA_TITLE, "CheckClik app feedback");
                    final PackageManager pm = getPackageManager();
                    final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
                    String className = null;
                    for (final ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                            className = info.activityInfo.name;

                            if (className != null && !className.isEmpty()) {
                                break;
                            }
                        }
                    }
                    i.setClassName("com.google.android.gm", className);
                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SettingActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SettingActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(SettingActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(SettingActivity.this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        shareapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "Check out CheckClik App!! \n Download the app from https://play.google.com/store/apps/details?id=com.cs.checkclikuser";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "CheckClik App");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using.."));
            }
        });

    }
    public void showtwoButtonsAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SettingActivity.this);
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        if (language.equals("En")){
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
            final TextView title = (TextView) dialogView.findViewById(R.id.title);
            final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

            if (language.equals("En")){
                desc.setText("" + getResources().getString(R.string.are_you_sure_to_logout));
            }else {
                desc.setText("" + getResources().getString(R.string.are_you_sure_to_logout_ar));
            }

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userPrefsEditor.clear();
                    userPrefsEditor.commit();
                    customDialog.dismiss();


                    Intent i = new Intent(SettingActivity.this, StartScrrenActivity.class);
                    i.putExtra("type", "1");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();

                }
            });

        }else {
            int layout = R.layout.alert_dialog_ar;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
            final TextView title = (TextView) dialogView.findViewById(R.id.title);
            final TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            final TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            final TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);

            desc.setText("" + getResources().getString(R.string.are_you_sure_to_logout_ar));



            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    userPrefsEditor.clear();
                    userPrefsEditor.commit();
                    customDialog.dismiss();


                    Intent i = new Intent(SettingActivity.this, StartScrrenActivity.class);
                    i.putExtra("type", "1");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();

                }
            });


        }




//            title.setText("CkeckClik");
//            yes.setText(getResources().getString(R.string.yes));
//            no.setText(getResources().getString(R.string.no));
//            desc.setText("Do you want to logout?");
        


        customDialog = dialogBuilder.create();
        customDialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = customDialog.getWindow();
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;

        double d = screenWidth * 0.85;
        lp.width = (int) d;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
    }
}
