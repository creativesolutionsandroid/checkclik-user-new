package com.cs.checkclickuser.Activites;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.cs.checkclickuser.Adapter.ManageAdressAdapter;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ManageAdreesActivity extends AppCompatActivity {

    ImageView back_btn;
    RelativeLayout addnew;
    String TAG = "TAG";
    RecyclerView adresslistview;
    private ArrayList<ManageAdressResponce.Data> addressArrayList = new ArrayList<>();
    private ManageAdressAdapter mAddressAdapter;
    private String screen = "";
    private boolean clickable;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    AlertDialog customDialog;
    boolean servicecall = false;
    SharedPreferences LanguagePrefs;
    String language;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.managae_adress_activity);
        } else {
            setContentView(R.layout.managae_adress_activity_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        back_btn = (ImageView) findViewById(R.id.back_btn);
        addnew = (RelativeLayout) findViewById(R.id.addnew);
        adresslistview = (RecyclerView) findViewById(R.id.adresslist);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        screen = getIntent().getStringExtra("screen");

        if (screen.equals("checkout")) {
            addnew.setVisibility(View.INVISIBLE);
            clickable = true;
        }

        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageAdreesActivity.this, AddAdressActivity.class);
                startActivity(intent);
            }
        });

        new GetstoreApi().execute();
        servicecall = true;
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ManageAdreesActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.loading_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            lp.copyFrom(window.getAttributes());
            //This makes the progressDialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth * 0.45;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ManageAdressResponce> call = apiService.getaddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ManageAdressResponce>() {
                @Override
                public void onResponse(Call<ManageAdressResponce> call, Response<ManageAdressResponce> response) {
                    Log.d(TAG, "product servies responce: " + response);


                    if (response.isSuccessful()) {
                        ManageAdressResponce stores = response.body();

                        if (stores.getStatus()) {
                            addressArrayList = stores.getData();

                        }
                        Log.d(TAG, "arry list size 1" + addressArrayList.size());
                    }

                    if (addressArrayList != null) {
                        mAddressAdapter = new ManageAdressAdapter(ManageAdreesActivity.this,
                                getIntent().getIntExtra("id", 0), addressArrayList, clickable, ManageAdreesActivity.this);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ManageAdreesActivity.this);
                        adresslistview.setLayoutManager(new GridLayoutManager(ManageAdreesActivity.this, 1));
                        adresslistview.setAdapter(mAddressAdapter);
                    }

                    if (customDialog != null) {
                        customDialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<ManageAdressResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ManageAdreesActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equals("En")){
                            Toast.makeText(ManageAdreesActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ManageAdreesActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        if (language.equals("En")){
                            Toast.makeText(ManageAdreesActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ManageAdreesActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (customDialog != null) {
                        customDialog.dismiss();
                    }

                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FlagId", 1);
            parentObj.put("UserId", userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!servicecall) {
            new GetstoreApi().execute();
        } else {
            servicecall = false;
        }

    }

}
