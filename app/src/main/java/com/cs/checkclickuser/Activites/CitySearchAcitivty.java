package com.cs.checkclickuser.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.CitiesListAdapter;
import com.cs.checkclickuser.Adapter.FavStoreAdapter;
import com.cs.checkclickuser.Fragements.HomeFragment;
import com.cs.checkclickuser.Models.Districts;
import com.cs.checkclickuser.Models.HomeCityResponse;
import com.cs.checkclickuser.Models.LocationResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.GPSTracker;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;

public class CitySearchAcitivty extends AppCompatActivity {

    private EditText etSearch;
    private RecyclerView cityListView;
    private CitiesListAdapter mAdapter;
    private String searchTerm = "";
    private ArrayList<HomeCityResponse.DistrictList> districts = new ArrayList<>();
    private ArrayList<Districts> districtsFiltered = new ArrayList<>();
    private ImageView back_btn, currentLocation;
    GPSTracker gps;
    Double currentLatitude = 0.0, currentLongitude = 0.0;

    SharedPreferences LanguagePrefs;
    String language;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        super.onCreate(savedInstanceState);

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_city_search_acitivty);
        } else {
            setContentView(R.layout.activity_city_search_acitivty_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        gps = new GPSTracker(CitySearchAcitivty.this);

        etSearch = (EditText) findViewById(R.id.search);
        cityListView = (RecyclerView) findViewById(R.id.list_item);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        currentLocation = (ImageView) findViewById(R.id.current_location);

        int icback_btnId = getResources().getIdentifier("ic_back_" + Constants.appColor, "drawable", getPackageName());
        back_btn.setImageDrawable(getResources().getDrawable(icback_btnId));

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.showLoadingDialog(CitySearchAcitivty.this);
                getGPSCoordinates();
            }
        });

        new GetCityListApi().execute();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchTerm = editable.toString();
                filterAreas();
            }
        });

        etSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(CitySearchAcitivty.this);
                    return true;
                }
                return false;
            }
        });
    }

    public void getGPSCoordinates() {

        if(gps != null){
            if (gps.canGetLocation()) {
                currentLatitude = gps.getLatitude();
                currentLongitude = gps.getLongitude();
                Constants.Latitude = currentLatitude;
                Constants.Longitude = currentLongitude;
                new GetLocationApi().execute();
            } else {
                gps.showSettingsAlert();
                Constants.closeLoadingDialog();
            }
        }
    }

    private String prepareCityListJSON() {
        JSONObject parentObj = new JSONObject();
        Log.d("TAG", "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private class GetCityListApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareCityListJSON();
            Constants.showLoadingDialog(CitySearchAcitivty.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<HomeCityResponse> call = apiService.getCityList(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<HomeCityResponse>() {
                @Override
                public void onResponse(Call<HomeCityResponse> call, Response<HomeCityResponse> response) {
                    Constants.closeLoadingDialog();
                    if (response.isSuccessful()) {
                        prepareList(response.body().getData());
                    }
                }

                @Override
                public void onFailure(Call<HomeCityResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CitySearchAcitivty.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(CitySearchAcitivty.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CitySearchAcitivty.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(CitySearchAcitivty.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(CitySearchAcitivty.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
            return "";
        }
    }

    private void prepareList(HomeCityResponse.Data data) {
        for (int i = 0; i < data.getCitylist().size(); i++) {
            HomeCityResponse.DistrictList districts = new HomeCityResponse.DistrictList();
            districts.setCityar(data.getCitylist().get(i).getNamear());
            districts.setCityen(data.getCitylist().get(i).getNameen());
            districts.setCityid(data.getCitylist().get(i).getId());
            districts.setLatitude(data.getCitylist().get(i).getLatitude());
            districts.setLongitude(data.getCitylist().get(i).getLongitude());
            districts.setNamear("");
            districts.setNameen("");
            districts.setId(0);
            this.districts.add(districts);
        }
        districts.addAll(data.getDistrictlist());
        filterAreas();
    }

    private void filterAreas() {
        districtsFiltered.clear();
        if (searchTerm.equals("")) {
            for (int i = 0; i < districts.size(); i++) {
                Districts district = new Districts();
                district.setCityar(districts.get(i).getCityar());
                district.setCityen(districts.get(i).getCityen());
                district.setCityid(districts.get(i).getCityid());
                district.setLatitude(districts.get(i).getLatitude());
                district.setLongitude(districts.get(i).getLongitude());
                district.setNamear(districts.get(i).getNamear());
                district.setNameen(districts.get(i).getNameen());
                district.setId(districts.get(i).getId());
                districtsFiltered.add(district);
            }
        } else {
            for (int i = 0; i < districts.size(); i++) {
                if (districts.get(i).getCityen().toLowerCase().contains(searchTerm.toLowerCase()) ||
                        districts.get(i).getCityar().toLowerCase().contains(searchTerm.toLowerCase()) ||
                        districts.get(i).getNamear().toLowerCase().contains(searchTerm.toLowerCase()) ||
                        districts.get(i).getNameen().toLowerCase().contains(searchTerm.toLowerCase())) {
                    Districts district = new Districts();
                    district.setCityar(districts.get(i).getCityar());
                    district.setCityen(districts.get(i).getCityen());
                    district.setCityid(districts.get(i).getCityid());
                    district.setLatitude(districts.get(i).getLatitude());
                    district.setLongitude(districts.get(i).getLongitude());
                    district.setNamear(districts.get(i).getNamear());
                    district.setNameen(districts.get(i).getNameen());
                    district.setId(districts.get(i).getId());
                    districtsFiltered.add(district);
                }
            }
        }

        mAdapter = new CitiesListAdapter(CitySearchAcitivty.this, districtsFiltered);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CitySearchAcitivty.this);
        cityListView.setLayoutManager(new GridLayoutManager(CitySearchAcitivty.this, 1));
        cityListView.setAdapter(mAdapter);
    }

    private class GetLocationApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Constants.showLoadingDialog(getActivity());
            inputStr = prepareGetLocationJSON();
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<LocationResponse> call = apiService.getLocationDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<LocationResponse>() {
                @Override
                public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                    Log.i("TAG", "product servies responce " + response);
                    Constants.closeLoadingDialog();

                    if (response.isSuccessful()) {
                        Constants.CityName = response.body().getData().getSelectedcity().get(0).getNameen();
                        Constants.CityNameAr = response.body().getData().getSelectedcity().get(0).getNamear();
                        Constants.CityId = response.body().getData().getSelectedcity().get(0).getId();
                        Constants.DistrictName = response.body().getData().getSelecteddistrict().get(0).getNameen();
                        Constants.DistrictNameAr = response.body().getData().getSelecteddistrict().get(0).getNamear();
                        Constants.DistrictId = response.body().getData().getSelecteddistrict().get(0).getId();

                        userPrefsEditor.putString("city", Constants.CityName);
                        userPrefsEditor.putString("city_ar", Constants.CityNameAr);
                        userPrefsEditor.putInt("cityId", Constants.CityId);
                        userPrefsEditor.putString("district", Constants.DistrictName);
                        userPrefsEditor.putString("district_ar", Constants.DistrictNameAr);
                        userPrefsEditor.putInt("districtId", Constants.DistrictId);
                        userPrefsEditor.putString("lat", "");
                        userPrefsEditor.putString("lng", "");
                        userPrefsEditor.commit();

                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<LocationResponse> call, Throwable t) {
                    Log.i("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CitySearchAcitivty.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(CitySearchAcitivty.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CitySearchAcitivty.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return "";
        }
    }
    private String prepareGetLocationJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Latitude", currentLatitude);
            parentObj.put("Longitude", currentLongitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }
}