package com.cs.checkclickuser.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Dialogs.CheckOutSelectAddress;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.Models.CheckoutSchedule;
import com.cs.checkclickuser.Models.CheckoutSlots;
import com.cs.checkclickuser.Models.TimeSlots;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckoutActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private RadioButton deliveryRadioBtn, shippingRadioBtn, pickupRadioBtn;
    private RelativeLayout dateLayout, timeLayout;
    private ImageView addressSelect, currentLocation;
    private TextView addressTextView, dateTextView, timeTextView, typeTextView, serviceTitle, addressTitle;
    private Button nextBtn;
    private View line1, line2;
    private LinearLayout serviceLayout, whenLayout;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ImageView backBtn, step1Icon, step2Icon, step3Icon;
    View line10, line11, line12, line13;
    private static int GET_ADDRESS = 1;
    private ArrayList<CartResponce.CartList> cartArryLists;
    private CheckoutSchedule checkoutData;
    private ArrayList<CheckoutSlots.TimeSlotList> checkoutSlots = new ArrayList<>();
    private String TAG = "TAG";
    private DatePickerDialog datePickerDialog ;
    private int Year, Month, Day;
    private int deliveryType = IdConstants.PRODUCT_DELIVERY;
    private String selectedDate = "", slot = "";
    private ArrayList<TimeSlots> timings = new ArrayList<>();
    private Spinner timesSpinner;
    private ArrayAdapter<TimeSlots> timingsAdapter;
    private String slotId = "";
    private int addressId = 0;

    private static int GET_SHIPPING_DETAILS = 2;
    private String shippingCity = "";
    private int shippingCityId = 0;
    CheckOutSelectAddress newFragment;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_checkout);
        } else {
            setContentView(R.layout.activity_checkout_ar);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        cartArryLists = (ArrayList<CartResponce.CartList>) getIntent().getSerializableExtra("cart");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        timesSpinner = (Spinner) findViewById(R.id.time_spinner);
        deliveryRadioBtn = (RadioButton) findViewById(R.id.delivery);
        shippingRadioBtn = (RadioButton) findViewById(R.id.shipping);
        pickupRadioBtn = (RadioButton) findViewById(R.id.pickup);

        addressTextView = (TextView) findViewById(R.id.address_textview);
        dateTextView = (TextView) findViewById(R.id.date);
        timeTextView = (TextView) findViewById(R.id.time);
        typeTextView = (TextView) findViewById(R.id.when_text);
        serviceTitle = (TextView) findViewById(R.id.service_title);
        addressTitle = (TextView) findViewById(R.id.address_title);

        backBtn = (ImageView) findViewById(R.id.back_btn);
        addressSelect = (ImageView) findViewById(R.id.address_edit);
        currentLocation = (ImageView) findViewById(R.id.current_location);
        step1Icon = (ImageView) findViewById(R.id.step1_icon);
        step2Icon = (ImageView) findViewById(R.id.step2_icon);
        step3Icon = (ImageView) findViewById(R.id.step3_icon);

        dateLayout = (RelativeLayout) findViewById(R.id.date_layout);
        timeLayout = (RelativeLayout) findViewById(R.id.time_layout);

        whenLayout = (LinearLayout) findViewById(R.id.when_layout);
        serviceLayout = (LinearLayout) findViewById(R.id.service_layout);

        nextBtn = (Button) findViewById(R.id.next_btn);

        line1 = (View) findViewById(R.id.line1);
        line2 = (View) findViewById(R.id.line2);
        line10 = (View) findViewById(R.id.line10);
        line11 = (View) findViewById(R.id.line11);
        line12 = (View) findViewById(R.id.line12);
        line13 = (View) findViewById(R.id.line13);

        new GetScheduleApi().execute();

        int icback_btnId = getResources().getIdentifier("ic_back_"+Constants.appColor, "drawable", getPackageName());
        backBtn.setImageDrawable(getResources().getDrawable(icback_btnId));

        int icstep1IconId = getResources().getIdentifier("check_done_"+Constants.appColor, "drawable", getPackageName());
        step1Icon.setImageDrawable(getResources().getDrawable(icstep1IconId));

        int icstep2IconId = getResources().getIdentifier("check_two_"+Constants.appColor, "drawable", getPackageName());
        step2Icon.setImageDrawable(getResources().getDrawable(icstep2IconId));

        int icstep3IconId = getResources().getIdentifier("check_three_"+Constants.appColor, "drawable", getPackageName());
        step3Icon.setImageDrawable(getResources().getDrawable(icstep3IconId));

        int iccurrentLocationId = getResources().getIdentifier("current_loction_"+Constants.appColor, "drawable", getPackageName());
        currentLocation.setImageDrawable(getResources().getDrawable(iccurrentLocationId));

        int icaddressSelectId = getResources().getIdentifier("selectaddress_"+Constants.appColor, "drawable", getPackageName());
        addressSelect.setImageDrawable(getResources().getDrawable(icaddressSelectId));

        line10.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        line11.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        line12.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        line13.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        nextBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        serviceTitle.setTextColor(Color.parseColor("#"+Constants.appColor));
        typeTextView.setTextColor(Color.parseColor("#"+Constants.appColor));
        addressTitle.setTextColor(Color.parseColor("#"+Constants.appColor));

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {
                        Color.parseColor("#"+Constants.appColor), //disabled
                        Color.parseColor("#"+Constants.appColor) //enabled
                }
        );

        deliveryRadioBtn.setButtonTintList(colorStateList);
        pickupRadioBtn.setButtonTintList(colorStateList);
        shippingRadioBtn.setButtonTintList(colorStateList);

        newFragment = CheckOutSelectAddress.newInstance();

        if (cartArryLists.get(0).getOrderType() == 2) {
            line1.setVisibility(View.GONE);
            line2.setVisibility(View.GONE);
            whenLayout.setVisibility(View.GONE);
            serviceLayout.setVisibility(View.GONE);
            if (cartArryLists.get(0).getServiceType() == 2) {
                currentLocation.setVisibility(View.INVISIBLE);
                addressSelect.setVisibility(View.VISIBLE);
            }
        }

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        deliveryRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    deliveryType = IdConstants.PRODUCT_DELIVERY;
                    if (language.equalsIgnoreCase("En")){
                        typeTextView.setText(getResources().getString(R.string.when));
                        dateTextView.setText(getResources().getString(R.string.date));
                        timeTextView.setText(getResources().getString(R.string.time));
                    }else {
                        typeTextView.setText(getResources().getString(R.string.when_ar));
                        dateTextView.setText(getResources().getString(R.string.date_ar));
                        timeTextView.setText(getResources().getString(R.string.time_ar));
                    }

                    dateLayout.setVisibility(View.VISIBLE);
                    currentLocation.setVisibility(View.INVISIBLE);
                    addressSelect.setVisibility(View.VISIBLE);
                    timesSpinner.setVisibility(View.VISIBLE);
                    selectedDate = "";
                    slotId = "";
                    addressId = 0;
                    addressTextView.setText("");
                }
            }
        });

        shippingRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    deliveryType = IdConstants.PRODUCT_SHIPPING;
                    if (language.equalsIgnoreCase("En")){

                          typeTextView.setText(getResources().getString(R.string.shipping_to));
                          timeTextView.setText(getResources().getString(R.string.city));
                    }else {
                        typeTextView.setText(getResources().getString(R.string.shipping_to_ar));
                          timeTextView.setText(getResources().getString(R.string.city));
                    }

                    dateLayout.setVisibility(View.GONE);
                    currentLocation.setVisibility(View.INVISIBLE);
                    addressSelect.setVisibility(View.VISIBLE);
                    timesSpinner.setVisibility(View.INVISIBLE);
                    selectedDate = "";
                    slotId = "";
                    addressId = 0;
                    addressTextView.setText("");
                }
            }
        });

        pickupRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b) {
                    deliveryType = IdConstants.PRODUCT_PICKUP;
                    if (language.equals("En")){
                    typeTextView.setText(getResources().getString(R.string.when));
                        dateTextView.setText(getResources().getString(R.string.date));
                        timeTextView.setText(getResources().getString(R.string.time));
                    }else {
                        dateTextView.setText(getResources().getString(R.string.date_ar));
                        timeTextView.setText(getResources().getString(R.string.time_ar));
                        typeTextView.setText(getResources().getString(R.string.when_ar));
                    }

                    dateLayout.setVisibility(View.VISIBLE);
                    currentLocation.setVisibility(View.INVISIBLE);
                    addressSelect.setVisibility(View.INVISIBLE);
                    timesSpinner.setVisibility(View.VISIBLE);
                    selectedDate = "";
                    slotId = "";
                    addressId = 0;
                    addressTextView.setText(checkoutData.getAddress());
                }
            }
        });

        timeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (deliveryType == IdConstants.PRODUCT_SHIPPING) {
                    Intent intent = new Intent(CheckoutActivity.this, ShippingCountriesActivity.class);
                    startActivityForResult(intent, GET_SHIPPING_DETAILS);
                }
            }
        });

        addressSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(deliveryType == IdConstants.PRODUCT_SHIPPING) {
                    if (!shippingCity.equals("")) {
                        showAddressDialog(shippingCityId, shippingCity);
                    } else {
                        if (language.equals("En")){
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_shipping_city),
                                    getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                    CheckoutActivity.this);
                        }else {
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_shipping_city_ar),
                                    getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                    CheckoutActivity.this);
                        }

                    }
                }
                else {
                    showAddressDialog(-1, "");
                }
            }
        });

        dateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCalendar();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    Log.d(TAG, "slot: "+slot);
                    Log.d(TAG, "slotId: "+slotId);
                    Intent intent = new Intent(CheckoutActivity.this, CheckOutActivityStep2.class);
                    intent.putExtra("cart", cartArryLists);
                    intent.putExtra("checkout", checkoutData);
                    intent.putExtra("date", selectedDate);
                    intent.putExtra("slotId", slotId);
                    intent.putExtra("slot", slot);
                    intent.putExtra("addressId", addressId);
                    intent.putExtra("deliveryType", deliveryType);
                    intent.putExtra("comments", getIntent().getStringExtra("comments"));
                    startActivity(intent);
                }
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("addressUpdate"));
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String name = intent.getStringExtra("name");
            String address = intent.getStringExtra("address");
            String mobile = intent.getStringExtra("mobile");
            String type = intent.getStringExtra("type");
            addressId = intent.getIntExtra("id", 0);

            if (language.equalsIgnoreCase("En")){
            addressTextView.setText(name + "(" + type + "),\n"+address+".\n"+ getResources().getString(R.string.phone_number) + ":" + mobile);
            }else {
                addressTextView.setText(name + "(" + type + "),\n"+address+".\n"+ getResources().getString(R.string.phone_number_ar) + ":" + mobile);
            }


            if (newFragment != null) {
                newFragment.dismiss();
            }
        }
    };

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GET_ADDRESS && resultCode == RESULT_OK) {
            String name = data.getStringExtra("name");
            String address = data.getStringExtra("address");
            String mobile = data.getStringExtra("mobile");
            String type = data.getStringExtra("type");
            addressId = data.getIntExtra("id", 0);

            if (language.equalsIgnoreCase("En")){
            addressTextView.setText(name + "(" + type + "),\n"+address+".\n" + getResources().getString(R.string.phone_number) + ":" + mobile);
            }else {
                addressTextView.setText(name + "(" + type + "),\n"+address+".\n" + getResources().getString(R.string.phone_number_ar) + ":" + mobile);
            }

        }
        else if (requestCode == GET_SHIPPING_DETAILS && resultCode == RESULT_OK) {
            shippingCity = data.getStringExtra("city");
            shippingCityId = data.getIntExtra("cityId", 0);
            timeTextView.setText(shippingCity);
        }
    }

    private void updateOrderTypes(){
        if (cartArryLists.get(0).getOrderType() == 1) {
            if (checkoutData.getShippingProvider()) {
                deliveryRadioBtn.setVisibility(View.GONE);
                shippingRadioBtn.setVisibility(View.VISIBLE);
                shippingRadioBtn.setChecked(true);
            } else if (!checkoutData.getShippingProvider() && checkoutData.getIsDelivery()) {
                deliveryRadioBtn.setVisibility(View.VISIBLE);
                shippingRadioBtn.setVisibility(View.GONE);
            }

            if (!checkoutData.getShippingProvider() && !checkoutData.getIsDelivery()) {
                deliveryRadioBtn.setVisibility(View.GONE);
                shippingRadioBtn.setVisibility(View.GONE);
                pickupRadioBtn.setChecked(true);
            }

            if (checkoutData.getInStoreService()) {
                pickupRadioBtn.setVisibility(View.VISIBLE);
            } else {
                pickupRadioBtn.setVisibility(View.GONE);
            }
        }
        else {
            if (cartArryLists.get(0).getServiceType() == 1) {
                addressSelect.setVisibility(View.INVISIBLE);
                currentLocation.setVisibility(View.INVISIBLE);
                addressTextView.setText(checkoutData.getAddress());
            }
        }
    }

    private class GetScheduleApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(CheckoutActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CheckoutSchedule> call = apiService.getCheckoutSchedule(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CheckoutSchedule>() {
                @Override
                public void onResponse(Call<CheckoutSchedule> call, Response<CheckoutSchedule> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            checkoutData = response.body();
                            if (checkoutData != null) {
                                updateOrderTypes();
                            }
                            Constants.closeLoadingDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CheckoutActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CheckoutSchedule> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                      if (language.equals("En")){
                        Toast.makeText(CheckoutActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                      }else {
                          Toast.makeText(CheckoutActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                      }

                    } else {
                        if (language.equals("En")){
                        Toast.makeText(CheckoutActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId",  userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private void showCalendar(){
        datePickerDialog = DatePickerDialog.newInstance(CheckoutActivity.this, Year, Month, Day);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
//        datePickerDialog.setTitle("");

        // Setting Min Date to today date
        Calendar min_date_c = Calendar.getInstance();
        datePickerDialog.setMinDate(min_date_c);

        // Setting Max Date to next 2 years
        Calendar max_date_c = Calendar.getInstance();
        max_date_c.add(Calendar.MONTH, 1);
        datePickerDialog.setMaxDate(max_date_c);

        if(deliveryType == IdConstants.PRODUCT_DELIVERY) {
            if (checkoutData.getData().getDeliveryHomeWeekdays().size() != 7) {
                for (int i = 0; i < checkoutData.getData().getDeliveryHomeWeekdays().size(); i++) {
                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();
                    max.add(Calendar.MONTH, 1);
                    max.add(Calendar.DATE, 1);
                    Calendar loopdate = null;

                    for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == checkoutData.getData().getDeliveryHomeWeekdays().get(i).getWeekdayId()) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            datePickerDialog.setDisabledDays(disabledDays);
                        }
                    }
                }
                datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
            else {
                if (language.equals("En")){
                Toast.makeText(this, getResources().getString(R.string.not_available), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, getResources().getString(R.string.not_available_ar), Toast.LENGTH_SHORT).show();
                }

            }
        }
        else if(deliveryType == IdConstants.PRODUCT_PICKUP) {
            if (checkoutData.getData().getPickupInServiceWeekdays().size() != 7) {
                for (int i = 0; i < checkoutData.getData().getPickupInServiceWeekdays().size(); i++) {
                    Calendar min = Calendar.getInstance();
                    Calendar max = Calendar.getInstance();
                    max.add(Calendar.MONTH, 1);
                    max.add(Calendar.DATE, 1);
                    Calendar loopdate = null;

                    for (loopdate = min; min.before(max); min.add(Calendar.DATE, 1), loopdate = min) {
                        int dayOfWeek = loopdate.get(Calendar.DAY_OF_WEEK);
                        if (dayOfWeek == checkoutData.getData().getPickupInServiceWeekdays().get(i).getWeekdayId()) {
                            Calendar[] disabledDays = new Calendar[1];
                            disabledDays[0] = loopdate;
                            datePickerDialog.setDisabledDays(disabledDays);
                        }
                    }
                }
                datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
            else {
                if (language.equals("En")){
                 Toast.makeText(this, getResources().getString(R.string.not_available), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, getResources().getString(R.string.not_available_ar), Toast.LENGTH_SHORT).show();
                }

            }
        }

        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialogInterface) {

            }
        });
    }

    @Override
    public void onDateSet(DatePickerDialog view, int Year, int Month, int Day) {
        String date = "";
        String dateForService = "";
        if(Day <= 9 && Month < 9) {
            date = "0"+Day + "/0" + (Month + 1) + "/" + Year;
            dateForService = "0"+(Month + 1) + "/0" + Day + "/" + Year;
        }
        else if(Day <= 9) {
            date = "0"+Day + "/" + (Month + 1) + "/" + Year;
            dateForService = (Month + 1) +"/0"+Day  + "/" + Year;
        }
        else if(Month < 9) {
            date = Day + "/0" + (Month + 1) + "/" + Year;
            dateForService = "0" + (Month + 1) + "/" + Day + "/" + Year;
        }
        else {
            date = Day + "/" + (Month + 1) + "/" + Year;
            dateForService = "" + (Month + 1) + "/" + Day + "/" + Year;
        }

        Date formttedDate = null;
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        try {
            formttedDate = sdf1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(formttedDate);

        selectedDate = date;
        dateTextView.setText(date);
        timesSpinner.setAdapter(null);
        timeTextView.setText(getResources().getString(R.string.time));
        slotId = "";
        String inputStr = prepareGetTimingsJSON(dateForService, calendar.get(Calendar.DAY_OF_WEEK));
        new GetTimeSlotsApi().execute(date, inputStr);
    }

    private class GetTimeSlotsApi extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            checkoutSlots.clear();
            Constants.showLoadingDialog(CheckoutActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CheckoutSlots> call = apiService.getCheckoutSlots(
                    RequestBody.create(MediaType.parse("application/json"), strings[1]));
            call.enqueue(new Callback<CheckoutSlots>() {
                @Override
                public void onResponse(Call<CheckoutSlots> call, Response<CheckoutSlots> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            checkoutSlots = response.body().getData().getTimeSlotList();
                            setTimingsSpinner(strings[0]);
                            Constants.closeLoadingDialog();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            if (language.equals("En")){
                            Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_another_date), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CheckoutActivity.this);
                            }else {
                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.please_select_another_date_ar), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok_ar), CheckoutActivity.this);
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<CheckoutSlots> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CheckoutActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(CheckoutActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        if (language.equals("En")){
                        Toast.makeText(CheckoutActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CheckoutActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }
    private String prepareGetTimingsJSON(String dateStr, int weekDayId){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("WeekDayId",  weekDayId);
            parentObj.put("UserId",  userId);
            parentObj.put("Date",  dateStr);
            parentObj.put("OrderType",  1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private void setTimingsSpinner(String date){
        Date formttedDate = null;
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        try {
            formttedDate = sdf1.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(formttedDate);
        timings.clear();
        for (int i = 0; i < checkoutSlots.size(); i++) {
            if(checkoutSlots.get(i).getWeekDayId() == calendar.get(Calendar.DAY_OF_WEEK)) {
                TimeSlots timeSlots = new TimeSlots();
                timeSlots.setTimeSlot(checkoutSlots.get(i).getTimeSlot());
                timeSlots.setTimeSlotId(""+checkoutSlots.get(i).getSlotId());
                timings.add(timeSlots);
            }
        }

        timingsAdapter = new ArrayAdapter<TimeSlots>(this, R.layout.list_spinner, timings) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(1);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.white));

                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.white);
                ((TextView) v).setText(timings.get(position).getTimeSlot());
                ((TextView) v).setTextSize(15);
                ((TextView) v).setGravity(Gravity.LEFT);
                ((TextView) v).setTextColor(getResources().getColorStateList(R.color.black));

                return v;
            }
        };

        timesSpinner.setAdapter(timingsAdapter);
        timesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                slotId = timings.get(i).getTimeSlotId();
                timeTextView.setText(timings.get(i).getTimeSlot());
                slot = timings.get(i).getTimeSlot();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private boolean validate(){
        if(checkoutData.getOrderType() == 1) {
            if (deliveryType == IdConstants.PRODUCT_DELIVERY) {
                if (selectedDate.equals("")) {
                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_date),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_date_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                CheckoutActivity.this);
                    }
                    return false;
                } else if (slotId.equals("")) {
                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                CheckoutActivity.this);
                    }

                    return false;
                } else if (addressId == 0) {
                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                CheckoutActivity.this);
                    }

                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_address),
                            getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                            CheckoutActivity.this);
                    return false;
                }
            } else if (deliveryType == IdConstants.PRODUCT_PICKUP) {
                if (slotId.equals("")) {
                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok_ar),
                                CheckoutActivity.this);
                    }

                    Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_time),
                            getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                            CheckoutActivity.this);
                    return false;
                }
            } else if (deliveryType == IdConstants.PRODUCT_SHIPPING) {
                if (addressId == 0) {
                    if (language.equals("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_address),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_address_ar),
                                getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                                CheckoutActivity.this);
                    }


                    return false;
                }
                else {
                    Calendar today_calendar = Calendar.getInstance();
                    if (today_calendar.get(Calendar.DAY_OF_WEEK) == 5) {
                        today_calendar.add(Calendar.DATE, 2);
                    }
                    else {
                        today_calendar.add(Calendar.DATE, 1);
                    }

                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

                    try {
                        selectedDate = sdf1.format(today_calendar.getTime());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    slotId = "0";
                }
            }
        }
        else if(checkoutData.getOrderType() == 2 && cartArryLists.get(0).getServiceType() == 2 && addressId == 0) {
            Constants.showOneButtonAlertDialog(getResources().getString(R.string.select_address),
                    getResources().getString(R.string.app_name), getResources().getString(R.string.ok),
                    CheckoutActivity.this);
            return false;
        }

        return true;
    }

    private void showAddressDialog(int countryId, String countryNameSelected){

        Bundle args = new Bundle();
        args.putInt("id", countryId);
        args.putString("country", countryNameSelected);
        newFragment.setCancelable(true);
        newFragment.setArguments(args);
        newFragment.show(getSupportFragmentManager(), "address");

        getSupportFragmentManager().executePendingTransactions();
        newFragment.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                //do whatever you want when dialog is dismissed
                if (newFragment != null) {
                    newFragment.dismiss();
                }
            }
        });
    }
}
