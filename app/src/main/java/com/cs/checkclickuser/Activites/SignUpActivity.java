package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cs.checkclickuser.Models.VerifyEmailResponse;
import com.cs.checkclickuser.Models.VerifyMobileResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private int currentStage = 1;
    ImageView backbtn;
    private static final String TAG = "TAG";
    private RelativeLayout nameLayout, emailLayout, phoneLayout;
    private EditText inputFirstname, inputLastname, inputEmail, inputPassword, inputConfirm, inputPhone;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname,inputLayoutEmail, inputLayoutPassword, inputLayoutConfirm;
    private Button btnPhoneNext, btnEmailNext, btnContinue;
    private Context context;
    private String strFirstname, strLastname, strEmail, strPassword, strMobile;
    private TextView termsNconditions;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.signupactivity);
        } else {
            setContentView(R.layout.signupactivity_ar);
        }

        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputConfirm = (EditText) findViewById(R.id.input_confirm_password);
        inputPhone = (EditText) findViewById(R.id.input_phone);
        inputFirstname = (EditText) findViewById(R.id.input_name);
        inputLastname = (EditText) findViewById(R.id.input_lastname);
        termsNconditions = (TextView) findViewById(R.id.signuptext);

        inputLayoutfirstname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutlastname = (TextInputLayout) findViewById(R.id.layout_lastname);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        inputLayoutPassword = (TextInputLayout) findViewById(R.id.layout_password);
        inputLayoutConfirm = (TextInputLayout) findViewById(R.id.layout_confirm_password);

        nameLayout = (RelativeLayout) findViewById(R.id.name_layout);
        emailLayout = (RelativeLayout) findViewById(R.id.email_layout);
        phoneLayout = (RelativeLayout) findViewById(R.id.phone_layout);

        btnPhoneNext = (Button) findViewById(R.id.next_name);
        btnEmailNext = (Button) findViewById(R.id.next_email);
        btnContinue = (Button) findViewById(R.id.next_phone);
        backbtn = (ImageView) findViewById(R.id.back_btn);

        btnPhoneNext.setOnClickListener(this);
        btnEmailNext.setOnClickListener(this);
        btnEmailNext.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        btnContinue.setOnClickListener(this);

        inputFirstname.addTextChangedListener(new TextWatcher(inputFirstname));
        inputLastname.addTextChangedListener(new TextWatcher(inputLastname));
        inputEmail.addTextChangedListener(new TextWatcher(inputEmail));
        inputPassword.addTextChangedListener(new TextWatcher(inputPassword));
        inputConfirm.addTextChangedListener(new TextWatcher(inputConfirm));
        inputPhone.addTextChangedListener(new TextWatcher(inputPhone));

        String desc ="<p>By tapping Sign Up, you agree to <br />The <span style=\"color: #4A90E2;\"><a style=\"color: #4A90E2;\" href=\"http://checkclik.csadms.com/home/TermsConditions\">Terms of Service</a></span> and <span style=\"color: #4A90E2;\"><a style=\"color: #4A90E2;\" href=\"http://checkclik.csadms.com/home/TermsConditions\">Privacy Policy</a></span></p>";
        String desc_ar ="<p>بالنقر فوق تسجيل ، فإنك توافق على<br />ال\n <span style=\"color: #4A90E2;\"><a style=\"color: #4A90E2;\" href=\"http://checkclik.csadms.com/home/TermsConditions\">شروط الخدمة</a></span> و <span style=\"color: #4A90E2;\"><a style=\"color: #4A90E2;\" href=\"http://checkclik.csadms.com/home/TermsConditions\">سياسة الخصوصية</a></span></p>";


        if (language.equalsIgnoreCase("En")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                termsNconditions.setText(Html.fromHtml(desc, Html.FROM_HTML_MODE_COMPACT));
            } else {
                termsNconditions.setText(Html.fromHtml(desc));
            }
        }else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                termsNconditions.setText(Html.fromHtml(desc_ar, Html.FROM_HTML_MODE_COMPACT));
            } else {
                termsNconditions.setText(Html.fromHtml(desc_ar));
            }

        }
                termsNconditions.setMovementMethod(LinkMovementMethod.getInstance());

//        termsNconditions.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent fbIntent = new Intent(SignUpActivity.this, WebViewActivity.class);
//                fbIntent.putExtra("title", "Terms and Conditions");
//                fbIntent.putExtra("url", "http://checkclik.csadms.com/home/TermsConditions");
//                startActivity(fbIntent);
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_btn:
                if(currentStage == 1){
                    finish();
                }
                else if(currentStage == 2){
                    currentStage = 1;
//                    ((TextView)findViewById(R.id.signup_title)).setVisibility(View.VISIBLE);
                    emailLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    nameLayout.setVisibility(View.VISIBLE);
                    nameLayout.startAnimation(slideUp);
                }
                else if(currentStage == 3){
                    currentStage = 2;
                    phoneLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
                    emailLayout.setVisibility(View.VISIBLE);
                    emailLayout.startAnimation(slideUp);
                }
                break;

            case R.id.next_name:
                if (nameValidations()) {
                    currentStage = 2;
//                    ((TextView) findViewById(R.id.signup_title)).setVisibility(View.GONE);
                    nameLayout.setVisibility(View.GONE);
                    Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
                    emailLayout.setVisibility(View.VISIBLE);
                    emailLayout.startAnimation(slideUp);
                }
                break;

            case R.id.next_email:
                if(emailValidations()){
                    currentStage = 3;
//                    emailLayout.setVisibility(View.GONE);
//                    Animation slideUp1 = AnimationUtils.loadAnimation(this, R.anim.enter_from_right);
//                    phoneLayout.setVisibility(View.VISIBLE);
//                    phoneLayout.startAnimation(slideUp1);
                    new verifyEmailApi().execute();
                }
                break;

            case R.id.next_phone:
             if(mobileValidations()){
//
                   new verifyMobileApi().execute();

               }
               break;
        }
    }
    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_email:
                    clearErrors();
                    break;
                case R.id.input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")){
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        }else {
                            inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        if (language.equalsIgnoreCase("En")){
                            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_invalid_password));
                        }else {
                            inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
                        }
                    }
                    break;

                case R.id.input_phone:
                    clearErrors();
                    if(!editable.toString().startsWith("5")){
                        if (language.equalsIgnoreCase("En")){
                            inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile_five));
                        }else {
                            inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile_five_ar));
                        }

                        inputPhone.removeTextChangedListener(this);
                        inputPhone.setText("");
                        inputPhone.addTextChangedListener(this);
                    }
                    break;
            }
        }
    }

    private void clearErrors() {
        inputLayoutEmail.setErrorEnabled(false);
        inputLayoutPassword.setErrorEnabled(false);
        inputLayoutConfirm.setErrorEnabled(false);
    }

    private boolean mobileValidations(){
        strMobile = inputPhone.getText().toString();
        if(strMobile.length() == 0){
            if (language.equals("En")){

                inputPhone.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            }else {

                inputPhone.setError(getResources().getString(R.string.signup_msg_enter_mobile_ar));
            }
            inputPhone.setError(getResources().getString(R.string.signup_msg_enter_mobile));
            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        else if(strMobile.length() != 9){
            if (language.equals("En")){

                inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile));
            }else {
                inputPhone.setError(getResources().getString(R.string.signup_msg_invalid_mobile_ar));
            }

            Constants.requestEditTextFocus(inputPhone, SignUpActivity.this);
            return false;
        }
        return true;
    }

    private boolean nameValidations(){
        strFirstname = inputFirstname.getText().toString().trim();
        strLastname = inputLastname.getText().toString().trim();

        if(strFirstname.length() == 0){
            if (language.equals("En")){

                inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            }else {

                inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname_ar));
            }

            Constants.requestEditTextFocus(inputFirstname, SignUpActivity.this);
            return false;
        }
        if(strLastname.length() == 0){
            if (language.equals("En")){


                inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            }else {


                inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname_ar));
            }


            Constants.requestEditTextFocus(inputLastname, SignUpActivity.this);
            return false;
        }
        return true;
    }

//    private boolean phonenumberValidations(){
//
//
//        return true;
//    }


    private boolean emailValidations(){

        strEmail = inputEmail.getText().toString();
        strPassword = inputPassword.getText().toString();
        String confirmPwd = inputConfirm.getText().toString();


        if(strEmail.length() == 0){
            if (language.equals("En")){

                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email));
            }else {

                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }
            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }
        else if(!Constants.isValidEmail(strEmail)){
            if (language.equals("En")){

                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email));
            }else {

                inputLayoutEmail.setError(getResources().getString(R.string.signup_msg_invalid_email_ar));
            }

            Constants.requestEditTextFocus(inputEmail, SignUpActivity.this);
            return false;
        }

        else if(strPassword.length() == 0){
            if (language.equals("En")){

                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            }else {

                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
            }

            Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(strPassword.length() < 4){
            if (language.equals("En")){

                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            }else {

                inputLayoutPassword.setError(getResources().getString(R.string.signup_msg_invalid_password_ar));
            }

       Constants.requestEditTextFocus(inputPassword, SignUpActivity.this);
            return false;
        }
        else if(confirmPwd.length() == 0){
            if (language.equals("En")){

                inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_enter_password));
              }else {

                inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_enter_password_ar));
             }

            Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        else if(!confirmPwd.equals(strPassword)){
            if (language.equals("En")){


                inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_valid_password));
            }else {

                inputLayoutConfirm.setError(getResources().getString(R.string.signup_msg_valid_password_ar));
            }

          Constants.requestEditTextFocus(inputConfirm, SignUpActivity.this);
            return false;
        }
        return true;
    }

    private class verifyMobileApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyMobileJson();
            Constants.showLoadingDialog(SignUpActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyMobileResponse> call = apiService.verfiyMobileNumber(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyMobileResponse>() {
                @Override
                public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyMobileResponse VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyMobileResponse.getData().getOTP());
                           showVerifyDialog();
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), SignUpActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private class verifyEmailApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareVerifyEmailJson();

        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(SignUpActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<VerifyEmailResponse> call = apiService.VerifyEmail(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<VerifyEmailResponse>() {
                @Override
                public void onResponse(Call<VerifyEmailResponse> call, Response<VerifyEmailResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyEmailResponse VerifyEmailResponse = response.body();
                        try {
                            if (VerifyEmailResponse.getStatus()) {
                                Log.d(TAG, "onResponse: " + VerifyEmailResponse.getData().getEmail());
                                emailLayout.setVisibility(View.GONE);
                                Animation slideUp1 = AnimationUtils.loadAnimation(SignUpActivity.this, R.anim.enter_from_right);
                                phoneLayout.setVisibility(View.VISIBLE);
                                phoneLayout.startAnimation(slideUp1);
                            } else {
                                String failureResponse = VerifyEmailResponse.getMessage();
                                if (language.equalsIgnoreCase("En")){
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), SignUpActivity.this);
                                }else {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok_ar), SignUpActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }


                }

                @Override
                public void onFailure(Call<VerifyEmailResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }



                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SignUpActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                }
            });
            return null;
        }
    }

    private String prepareVerifyEmailJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("EmailId", strEmail);
            parentObj.put("FlagId", "4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

    private String prepareVerifyMobileJson() {
            JSONObject parentObj = new JSONObject();
            try {
                parentObj.put("MobileNo", "+966" + strMobile);
                parentObj.put("FlagId", "1");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
            return parentObj.toString();
        }

        private void showVerifyDialog() {
            Intent intent = new Intent(SignUpActivity.this, VerifyOtpDialog.class);
            intent.putExtra("FirstName", strFirstname);
            intent.putExtra("LastName", strLastname);
            intent.putExtra("EmailId", strEmail);
            intent.putExtra("MobileNo", strMobile);
            intent.putExtra("Password", strPassword);
            intent.putExtra("Language", "en");
            intent.putExtra("DeviceToken", SplashScreen.regId);
            intent.putExtra("DeviceVersion", Constants.getDeviceType(SignUpActivity.this));
            intent.putExtra("DeviceType", "andr");
            intent.putExtra("screen", "register");
            startActivity(intent);
        }
}




