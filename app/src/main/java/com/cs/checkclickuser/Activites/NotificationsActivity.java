package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.NotificationsAdapter;
import com.cs.checkclickuser.Models.Notifications;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotificationsActivity extends AppCompatActivity {

    private ImageView back_btn;
    RecyclerView listview;
    String TAG = "TAG";
    String  userId;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    List<Notifications.Data> notificationsArrayList = new ArrayList<>();
    NotificationsAdapter mAdapter;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_notifications);
        } else {
            setContentView(R.layout.activity_notifications_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        listview=(RecyclerView)findViewById(R.id.list_item) ;
        back_btn=(ImageView)findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        new GetstoreApi().execute(userId);
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(NotificationsActivity.this);

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<Notifications> call = apiService.getNotifications(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<Notifications>() {
                @Override
                public void onResponse(Call<Notifications> call, Response<Notifications> response) {
                    Log.d(TAG, "product servies responce: "+ response);

                    if (response.isSuccessful()) {
                        Notifications stores = response.body();
                        Constants.notificationCount = 0;

                        if (stores.getStatus()) {
                            notificationsArrayList = stores.getData();
                        }
                    }

                    mAdapter = new NotificationsAdapter(NotificationsActivity.this, notificationsArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NotificationsActivity.this);
                    listview.setLayoutManager(new GridLayoutManager(NotificationsActivity.this,1 ));
                    listview.setAdapter(mAdapter);

                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<Notifications> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t);
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(NotificationsActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(NotificationsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(NotificationsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(NotificationsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(NotificationsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();

                }
            });
            return "";
        }
    }
    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FlagId", 0);
            parentObj.put("UserId", userId) ;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

}
