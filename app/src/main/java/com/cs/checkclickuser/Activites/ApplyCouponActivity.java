package com.cs.checkclickuser.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cs.checkclickuser.Adapter.ApplyCouponAdapter;
import com.cs.checkclickuser.Adapter.CouponVoucherAdapter;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ApplyCouponActivity extends Activity {
    View rootView;
    ImageView back_Btn;
    RecyclerView couponlist;
    ApplyCouponAdapter mCouponAdapter;
    public static final String TAG = "TAG";
    private ArrayList<CartResponce.CouponList> couponArrayList = new ArrayList<>();
    String  userId;
    SharedPreferences userPrefs;
    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.coupon_fragment);
        } else {
            setContentView(R.layout.coupon_fragment_ar);
        }

        couponArrayList = (ArrayList<CartResponce.CouponList>) getIntent().getSerializableExtra("coupons");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        back_Btn=(ImageView)findViewById(R.id.back_btn);
        couponlist=(RecyclerView)findViewById(R.id.couponslist);

        back_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mCouponAdapter = new ApplyCouponAdapter(ApplyCouponActivity.this, ApplyCouponActivity.this, couponArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ApplyCouponActivity.this);
        couponlist.setLayoutManager(new GridLayoutManager(ApplyCouponActivity.this, 1));
        couponlist.setAdapter(mCouponAdapter);
    }
}
