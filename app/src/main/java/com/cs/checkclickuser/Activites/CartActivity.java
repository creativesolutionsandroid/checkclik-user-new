package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.CartDetailsListAdapter;
import com.cs.checkclickuser.Listeners.UpdateCartListener;
import com.cs.checkclickuser.Models.ApplyCoupon;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.CartResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BranchColor;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CartActivity extends AppCompatActivity {

    private CartDetailsListAdapter mcartAdapter;
    private Button checkoutBtn, addAnotherBtn;
    private TextView couponText, minimumOrderText, totalprice,deliverycharges,vatTitle,Vat,subtotal,discount,nettotal, shipment_cod, priceDetailsTitle, netAmountTitle, discountTitle;
    private Button applyCouponBtn;
    private ArrayList<CartResponce.CartList> cartArryLists ;
    private ArrayList<CartResponce.CouponList> couponLists ;
    int storePos = 0;
    private static String TAG = "TAG";
    ListView iteamlist;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId;
    ImageView backBtn;
    UpdateCartListener listener;
    String strComment = "";
    EditText commentsEt;
    ImageView emptyCart;
    NestedScrollView scrollView;
    RelativeLayout bottomLayout;
    int couponId;


    SharedPreferences LanguagePrefs;
    String language;

    public static int COUPON_INTENT = 1;
    private String freeDeliveryText;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.cart_activity);
        } else {

            setContentView(R.layout.cart_activity_ar);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

        commentsEt =(EditText) findViewById(R.id.comments);
        couponText =(TextView)findViewById(R.id.coupon);
        totalprice =(TextView)findViewById(R.id.totalprice);
        deliverycharges =(TextView)findViewById(R.id.deliverycharges);
        minimumOrderText =(TextView)findViewById(R.id.min_order_text);
        Vat =(TextView)findViewById(R.id.Vat);
        vatTitle = (TextView) findViewById(R.id.Vattext);
        subtotal =(TextView)findViewById(R.id.subtotal);
        discount =(TextView)findViewById(R.id.discount);
        nettotal =(TextView)findViewById(R.id.nettotal);
        shipment_cod =(TextView)findViewById(R.id.shipment_cod);
        priceDetailsTitle =(TextView)findViewById(R.id.pricetext);
        netAmountTitle =(TextView)findViewById(R.id.nettotaltext);
        discountTitle =(TextView)findViewById(R.id.discounttext);
        applyCouponBtn =(Button) findViewById(R.id.apply_coupon_btn);

        iteamlist =(ListView)findViewById(R.id.iteamlist);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        emptyCart = (ImageView) findViewById(R.id.empty_cart);
        bottomLayout = (RelativeLayout) findViewById(R.id.bottom_layout);

        scrollView = (NestedScrollView) findViewById(R.id.scrollview);

        checkoutBtn = (Button) findViewById(R.id.checkout_btn);
        addAnotherBtn = (Button) findViewById(R.id.add_another_btn);

        listener = new UpdateCartListener() {
            @Override
            public void onCartUpdated() {
                new GetCartApi().execute();
            }
        };

        new GetCartApi().execute();

        emptyCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cartArryLists != null && cartArryLists.size() > 0) {
                    Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                    intent.putExtra("cart", cartArryLists);
                    if (commentsEt.getText().toString().length() > 0) {
                        intent.putExtra("comments", commentsEt.getText().toString());
                    } else {
                        intent.putExtra("comments", "");
                    }
                    startActivity(intent);
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.cart_empty), getResources().getString(R.string.app_name),
                                getResources().getString(R.string.ok), CartActivity.this);
                    }else {
                        Constants.showOneButtonAlertDialog(getResources().getString(R.string.cart_empty_ar), getResources().getString(R.string.app_name),
                                getResources().getString(R.string.ok_ar), CartActivity.this);
                    }

                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addAnotherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartArryLists != null && cartArryLists.get(0).getBranchId() != 0) {
                    new GetBranchDetailsApi().execute();
                }
            }
        });

        applyCouponBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                if (cartArryLists.get(0).getCouponId() == 0) {
                    if (couponLists.size() > 0) {
                        Intent intent = new Intent(CartActivity.this, ApplyCouponActivity.class);
                        intent.putExtra("coupons", couponLists);
                        startActivityForResult(intent, COUPON_INTENT);
                    }
                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CartActivity.this, getResources().getString(R.string.no_coupons_available), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CartActivity.this, getResources().getString(R.string.no_coupons_available_ar), Toast.LENGTH_SHORT).show();
                        }

                    }
                }
                else {
                    // Remove coupon
                    couponId = 0;
                    new AddCouponToCart().execute();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == COUPON_INTENT && resultCode == RESULT_OK) {
            int pos = data.getIntExtra("coupon_position", 0);
            couponId = couponLists.get(pos).getId();
            new AddCouponToCart().execute();
        }
    }

    private String prepareAddCouponToCartJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId",  userId);
            parentObj.put("CouponId",  couponId);
            parentObj.put("Remarks",  "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareAddCouponToCartJSON: "+parentObj);
        return parentObj.toString();
    }

    private class AddCouponToCart extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareAddCouponToCartJSON();
            Constants.showLoadingDialog(CartActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ApplyCoupon> call = apiService.applyCoupon(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ApplyCoupon>() {
                @Override
                public void onResponse(Call<ApplyCoupon> call, Response<ApplyCoupon> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            new GetCartApi().execute();
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), CartActivity.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ApplyCoupon> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CartActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(CartActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CartActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private class GetCartApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(CartActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CartResponce> call = apiService.getcart(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CartResponce>() {
                @Override
                public void onResponse(Call<CartResponce> call, Response<CartResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if(response.isSuccessful()) {
                        if(response.body().getStatus()) {
                            Constants.closeLoadingDialog();
                            cartArryLists = response.body().getData().getCartList();
                            couponLists = response.body().getData().getCouponList();

                            if(cartArryLists.size() > 0) {
                                emptyCart.setVisibility(View.GONE);
                                BranchColor branchColor = new BranchColor();
                                Constants.appColor = branchColor.BranchColor(cartArryLists.get(0).getBranchColor());

                                mcartAdapter = new CartDetailsListAdapter(CartActivity.this, cartArryLists, listener);
                                iteamlist.setAdapter(mcartAdapter);
                                mcartAdapter.notifyDataSetChanged();

                                int icback_btnId = getResources().getIdentifier("ic_back_"+Constants.appColor, "drawable", getPackageName());
                                backBtn.setImageDrawable(getResources().getDrawable(icback_btnId));

                                priceDetailsTitle.setTextColor(Color.parseColor("#"+Constants.appColor));
                                netAmountTitle.setTextColor(Color.parseColor("#"+Constants.appColor));
                                discountTitle.setTextColor(Color.parseColor("#"+Constants.appColor));
                                applyCouponBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
                                checkoutBtn.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
                                addAnotherBtn.setTextColor(Color.parseColor("#"+Constants.appColor));

                                if (cartArryLists.get(0).getCouponId() != 0) {
                                    couponId = cartArryLists.get(0).getCouponId();
                                    if (language.equals("En")){
                                        applyCouponBtn.setText("" + getResources().getString(R.string.remove));
                                    }else {
                                        applyCouponBtn.setText("" + getResources().getString(R.string.remove_ar));
                                    }

                                    for (int i = 0; i < couponLists.size(); i++) {
                                        if (couponLists.get(i).getId() == couponId) {
                                            couponText.setText(couponLists.get(i).getCouponCode());
                                        }
                                    }
                                }
                                else {
                                    if (language.equals("En")){
                                        applyCouponBtn.setText("" + getResources().getString(R.string.apply_coupon));
                                    }else {
                                        applyCouponBtn.setText("" + getResources().getString(R.string.apply_coupon_ar));
                                    }

                                    couponText.setText("");
                                }

                                updatePrices();
                            }
                            else {
                                emptyCart.setVisibility(View.VISIBLE);
                                scrollView.setVisibility(View.GONE);
                                bottomLayout.setVisibility(View.GONE);
                            }
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
//                            String failureResponse = response.body().getMessage();
//                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
//                                    getResources().getString(R.string.ok), CartActivity.this);
                            emptyCart.setVisibility(View.VISIBLE);
                            scrollView.setVisibility(View.GONE);
                            bottomLayout.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CartResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CartActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CartActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CartActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CartActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CartActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                    emptyCart.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.GONE);
                    bottomLayout.setVisibility(View.GONE);
                }
            });
            return "";
        }
    }

    private void updatePrices() {
        double totalAmount = 0, vatAmount = 0, discountAmount = 0, nettotalAmount = 0;

        totalAmount = cartArryLists.get(0).getSubTotal();
        vatAmount = cartArryLists.get(0).getVAT();
//            subtotalAmount = subtotalAmount + cartArryLists.get(i).getTotalItemPrice() -
//                    cartArryLists.get(i).getTotalVatAmount();
        discountAmount = cartArryLists.get(0).getDiscountAmount();
        nettotalAmount = cartArryLists.get(0).getGrandTotal();

        totalprice.setText("SAR " + Constants.priceFormat1.format(totalAmount));
        Vat.setText("SAR " + Constants.priceFormat1.format(vatAmount));
        subtotal.setText("SAR " + Constants.priceFormat1.format(totalAmount - discountAmount));
        discount.setText("SAR " + Constants.priceFormat1.format(discountAmount));
        nettotal.setText("SAR " + Constants.priceFormat1.format(nettotalAmount));
        vatTitle.setText("" + getResources().getString(R.string.tax_vat) + "(" + cartArryLists.get(0).getVatPercentage() + "%)");

        String freeDeliveryText = "<p>Shop for <span style=\"color: #"+ Constants.appColor +";\"><a style=\"color: #"+ Constants.appColor +";\"> SAR "+ Constants.priceFormat1.format(cartArryLists.get(0).getFreeDeliveryAboveValue() - nettotalAmount) +"</a></span> more to avoid delivery fee.</p>";
        String freeDeliveryTextAr = "<p>Shop for <span style=\"color: #"+ Constants.appColor +";\"><a style=\"color: #"+ Constants.appColor +";\"> SAR "+ Constants.priceFormat1.format(cartArryLists.get(0).getFreeDeliveryAboveValue() - nettotalAmount) +"</a></span> to avoid delivery fee.</p>";
        String minOrderText = "<p>Minimum order greater than <span style=\"color: #"+ Constants.appColor +";\"><a style=\"color: #"+ Constants.appColor +";\"> SAR "+ Constants.priceFormat1.format(cartArryLists.get(0).getMinimumOrderValue()) +".</p>";
        String minOrderTextAr = "<p> الحد الأدنى للطلب أكبر من<span style=\"color: #"+ Constants.appColor +";\"><a style=\"color: #"+ Constants.appColor +";\"> SAR "+ Constants.priceFormat1.format(cartArryLists.get(0).getMinimumOrderValue()) +".</p>";

        if ((nettotalAmount) < cartArryLists.get(0).getMinimumOrderValue()) {
            minimumOrderText.setVisibility(View.VISIBLE);
            checkoutBtn.setAlpha(0.5f);
            checkoutBtn.setEnabled(false);
            if (language.equalsIgnoreCase("En")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    minimumOrderText.setText(Html.fromHtml(minOrderText, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    minimumOrderText.setText(Html.fromHtml(minOrderText));
                }
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    minimumOrderText.setText(Html.fromHtml(minOrderTextAr, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    minimumOrderText.setText(Html.fromHtml(minOrderTextAr));
                }
            }
        }
        else if ((nettotalAmount) < cartArryLists.get(0).getFreeDeliveryAboveValue()) {
            if (language.equalsIgnoreCase("En")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    minimumOrderText.setText(Html.fromHtml(freeDeliveryText, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    minimumOrderText.setText(Html.fromHtml(freeDeliveryText));
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    minimumOrderText.setText(Html.fromHtml(freeDeliveryTextAr, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    minimumOrderText.setText(Html.fromHtml(freeDeliveryTextAr));
                }

                checkoutBtn.setAlpha(1.0f);
                checkoutBtn.setEnabled(true);
            }
        }
        else {
            minimumOrderText.setVisibility(View.GONE);
            checkoutBtn.setAlpha(1.0f);
            checkoutBtn.setEnabled(true);
        }
    }

    private String prepareGetStoresJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId",  userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class GetBranchDetailsApi extends AsyncTask<String, Integer, String> {
        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetBranchDetailsJSON();
            Constants.showLoadingDialog(CartActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchDetails> call = apiService.getBranchDetails(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchDetails>() {
                @Override
                public void onResponse(Call<BranchDetails> call, Response<BranchDetails> response) {
                    Log.i("TAG", "product servies responce " + response);

                    if (response.isSuccessful()) {
                        BranchDetails branchResponse = response.body();
                        if (branchResponse.getData().getBranch() != null &&
                                branchResponse.getData().getBranch().size() > 0) {
                            Intent intent = new Intent(CartActivity.this, ProductStoresActivityStep1.class);
                            intent.putExtra("stores", branchResponse.getData().getBranch());
                            intent.putExtra("pos", 0);
                            intent.putExtra("type", branchResponse.getData().getBranch().get(0).getStoreType());
                            intent.putExtra("class", "vendor_app_intent");
                            startActivity(intent);
                            finish();
                        }
                        else {
                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(CartActivity.this, getResources().getString(R.string.store_details_not_available), Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(CartActivity.this, getResources().getString(R.string.store_details_not_available_ar), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BranchDetails> call, Throwable t) {
                    Log.e("TAG", "product servies responce " + t.toString());
                    Constants.closeLoadingDialog();
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(CartActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CartActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CartActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(CartActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CartActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                }
            });
//            Constants.closeLoadingDialog();
            return "";
        }
    }

    private String prepareGetBranchDetailsJSON(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("BranchId",  cartArryLists.get(0).getBranchId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("TAG", "prepareBranchId: "+parentObj.toString());
        return parentObj.toString();
    }
}
