package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.cs.checkclickuser.Adapter.FillterMainAdapter;
import com.cs.checkclickuser.Adapter.FillterSubAdapter;
import com.cs.checkclickuser.Fragements.ProductFragment;
import com.cs.checkclickuser.Fragements.ServiceFragment;
import com.cs.checkclickuser.Models.FilterSubResponce;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FillterActivity extends AppCompatActivity {
    private static final String TAG = "TAG";
    ListView maincategory,subcategory;

    private ArrayList<ProductlistResponce.FilterMainCategory> mainCategories =new ArrayList<>();
    private ArrayList<ProductlistResponce.FilterSubCategoryCat> subCategories =new ArrayList<>();
    private ArrayList<FilterSubResponce> filteredSubCategories =new ArrayList<>();
    ImageView backbtn;
    Button apply_btn,clear_btn;
    private FillterMainAdapter mMainAdapter;
    private FillterSubAdapter mSubAdapter;
    public static int mainFilterPosition = 0;
    String selectedValue;
    private int type = 1;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.fillter_activity);
        } else {
            setContentView(R.layout.fillter_activity_ar);
        }

        mainCategories = (ArrayList<ProductlistResponce.FilterMainCategory>) getIntent().getSerializableExtra("Fillterarry");
        subCategories = (ArrayList<ProductlistResponce.FilterSubCategoryCat>) getIntent().getSerializableExtra("Fillterarry1");
        type = getIntent().getIntExtra("type", 1);

        backbtn=(ImageView) findViewById(R.id.back_btn);
        maincategory=(ListView)findViewById(R.id.maincategory);
        subcategory=(ListView)findViewById(R.id.subcategory);
        clear_btn=(Button)findViewById(R.id.clear_btn);
        apply_btn=(Button)findViewById(R.id.apply_btn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fetchSubCategories();

        mMainAdapter = new FillterMainAdapter(FillterActivity.this,mainCategories);
        maincategory.setAdapter(mMainAdapter);

        maincategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mainFilterPosition = position;
                mMainAdapter.notifyDataSetChanged();
                fetchSubCategories();
            }
        });

        subcategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(type == 1) {
                    if (mainFilterPosition == 0) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ProductFragment.filterStatus = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 1) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ProductFragment.filterPaymentType = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 2) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ProductFragment.filterRatings = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 3) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ProductFragment.filterDistance = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 4) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ProductFragment.filterSort = filteredSubCategories.get(i).getValue();
                    }
                }
                else if (type == 2) {
                    if (mainFilterPosition == 0) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ServiceFragment.filterStatus = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 1) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ServiceFragment.filterPaymentType = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 2) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ServiceFragment.filterRatings = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 3) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ServiceFragment.filterDistance = filteredSubCategories.get(i).getValue();
                    } else if (mainFilterPosition == 4) {
                        selectedValue = filteredSubCategories.get(i).getValue();
                        ServiceFragment.filterSort = filteredSubCategories.get(i).getValue();
                    }
                }

                mSubAdapter = new FillterSubAdapter(FillterActivity.this,filteredSubCategories, selectedValue);
                subcategory.setAdapter(mSubAdapter);
                mSubAdapter.notifyDataSetChanged();
            }
        });

        clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void fetchSubCategories(){
        filteredSubCategories.clear();
        for (int i = 0; i < subCategories.size(); i++) {
            if(mainCategories.get(mainFilterPosition).getCategoryId() == subCategories.get(i).getCategoryId()) {
                FilterSubResponce subResponce=new FilterSubResponce();
                subResponce.setCategoryId(subCategories.get(i).getCategoryId());
                subResponce.setName(subCategories.get(i).getName());
                subResponce.setType(subCategories.get(i).getType());
                subResponce.setTypeId(subCategories.get(i).getTypeId());
                subResponce.setValue(subCategories.get(i).getValue());
                filteredSubCategories.add(subResponce);
            }
        }

        fetchSelectedValue();
        mSubAdapter = new FillterSubAdapter(FillterActivity.this,filteredSubCategories, selectedValue);
        subcategory.setAdapter(mSubAdapter);
        mSubAdapter.notifyDataSetChanged();
    }

    private void fetchSelectedValue(){
        if(type == 1) {
            if (mainFilterPosition == 0) {
                selectedValue = ProductFragment.filterStatus;
            } else if (mainFilterPosition == 1) {
                selectedValue = ProductFragment.filterPaymentType;
            } else if (mainFilterPosition == 2) {
                selectedValue = ProductFragment.filterRatings;
            } else if (mainFilterPosition == 3) {
                selectedValue = ProductFragment.filterDistance;
            } else if (mainFilterPosition == 4) {
                selectedValue = ProductFragment.filterSort;
            }
        }
        else if(type == 2) {
            if (mainFilterPosition == 0) {
                selectedValue = ServiceFragment.filterStatus;
            } else if (mainFilterPosition == 1) {
                selectedValue = ServiceFragment.filterPaymentType;
            } else if (mainFilterPosition == 2) {
                selectedValue = ServiceFragment.filterRatings;
            } else if (mainFilterPosition == 3) {
                selectedValue = ServiceFragment.filterDistance;
            } else if (mainFilterPosition == 4) {
                selectedValue = ServiceFragment.filterSort;
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (type == 1) {
            ProductFragment.filterStatus = "";
            ProductFragment.filterPaymentType = "";
            ProductFragment.filterRatings = "";
            ProductFragment.filterDistance = "";
            ProductFragment.filterSort = "";
            setResult(RESULT_OK);
            finish();
        }
        else if (type == 2) {
            ServiceFragment.filterStatus = "";
            ServiceFragment.filterPaymentType = "";
            ServiceFragment.filterRatings = "";
            ServiceFragment.filterDistance = "";
            ServiceFragment.filterSort = "";
            setResult(RESULT_OK);
            finish();
        }
//        super.onBackPressed();
    }
}
