package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.checkclickuser.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MyAccountActivity extends AppCompatActivity {

    TextView firstname,lastname,email,mobilenumber,password;
    SharedPreferences.Editor userPrefsEditor;
    SharedPreferences userPrefs;
    ImageView back_btn;
    RelativeLayout firstNameLayout, lastNameLayout, emailLayout, mobileLayout, passwordLayout;

    SharedPreferences LanguagePrefs;
    String language;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.myaccount_activity);
        } else {
            setContentView(R.layout.myaccount_activity_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        firstname=(TextView)findViewById(R.id.firstname);
        lastname=(TextView)findViewById(R.id.lastname);
        email=(TextView)findViewById(R.id.email);
        mobilenumber=(TextView)findViewById(R.id.mobile);
        password=(TextView)findViewById(R.id.password);
        back_btn=(ImageView)findViewById(R.id.back_btn);

        firstNameLayout = (RelativeLayout) findViewById(R.id.firstname_layout);
        lastNameLayout = (RelativeLayout) findViewById(R.id.lastname_layout);
        emailLayout = (RelativeLayout) findViewById(R.id.email_layout);
        mobileLayout = (RelativeLayout) findViewById(R.id.mobile_number_layout);
        passwordLayout = (RelativeLayout) findViewById(R.id.password_layout);

        firstname.setText(userPrefs.getString("FirstName",""));
        lastname.setText(userPrefs.getString("LastName",""));
        email.setText(userPrefs.getString("EmailId",""));
        mobilenumber.setText(userPrefs.getString("MobileNo",""));

        passwordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccountActivity.this, ChangeaPasswordActivity.class);
                startActivity(i);
            }
        });
        firstNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Name");
                startActivity(intent);
            }
        });

        lastNameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Name");
                startActivity(intent);
            }
        });

        emailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Email");
                startActivity(intent);
            }
        });

        mobileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                intent.putExtra("changename","Change Mobile Number");
                startActivity(intent);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
