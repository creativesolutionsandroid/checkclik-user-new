package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Models.BasicResponse;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.PRODUCTS_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.RETURN_IMAGE_URL;

public class OrderRetunDetailsActivity extends AppCompatActivity {

    TextView invoiceNumber, qty, requestDate, status;
    ImageView productImage1, productImage2, productImage3, productImage4;
    ImageView backbtn;

    SharedPreferences userPrefs, LanguagePrefs;
    String userId, language;
    int returnCount = 1;

    private ArrayList<OrderPendingResponce.Items> retunArrayList = new ArrayList<>();
    int productPos;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_retun_details);
        } else {
            setContentView(R.layout.order_retun_details_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "");

        retunArrayList = (ArrayList<OrderPendingResponce.Items>) getIntent().getSerializableExtra("Order");
        productPos = getIntent().getIntExtra("Orderpos", 0);

        invoiceNumber = (TextView) findViewById(R.id.invoice_number);
        qty = (TextView) findViewById(R.id.qty);
        requestDate = (TextView) findViewById(R.id.request_date);
        status = (TextView) findViewById(R.id.status);
//        price = (TextView) findViewById(R.id.price);
        backbtn=(ImageView)findViewById(R.id.back_btn);

        productImage1 = (ImageView) findViewById(R.id.image1);
        productImage2 = (ImageView) findViewById(R.id.image2);
        productImage3 = (ImageView) findViewById(R.id.image3);
        productImage4 = (ImageView) findViewById(R.id.image4);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        invoiceNumber.setText(retunArrayList.get(productPos).getReturnRequest().get(0).getInvoice());
        qty.setText(""+retunArrayList.get(productPos).getReturnRequest().get(0).getQty());
        requestDate.setText(retunArrayList.get(productPos).getReturnRequest().get(0).getRequestDate());
        status.setText(retunArrayList.get(productPos).getReturnRequest().get(0).getStatusIdText());

        for (int i = 0; i < retunArrayList.get(productPos).getReturnRequest().get(0).getItemImage().size(); i++) {
            if (i == 0) {
                Glide.with(this)
                        .load(RETURN_IMAGE_URL + retunArrayList.get(productPos).getReturnRequest().get(0).getItemImage().get(i).getImage())
                        .into(productImage1);
            }
            else if (i == 1) {
                productImage2.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(RETURN_IMAGE_URL + retunArrayList.get(productPos).getReturnRequest().get(0).getItemImage().get(i).getImage())
                        .into(productImage2);
            }
            else if (i == 2) {
                productImage3.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(RETURN_IMAGE_URL + retunArrayList.get(productPos).getReturnRequest().get(0).getItemImage().get(i).getImage())
                        .into(productImage3);
            }
            else if (i == 3) {
                productImage4.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(RETURN_IMAGE_URL + retunArrayList.get(productPos).getReturnRequest().get(0).getItemImage().get(i).getImage())
                        .into(productImage4);
            }
        }
    }
}
