package com.cs.checkclickuser.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.CouponVoucherAdapter;
import com.cs.checkclickuser.Models.CouponResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CouponVoucherActivity extends Activity {
    View rootView;
    ImageView back_Btn;
    RecyclerView couponlist;
    CouponVoucherAdapter mCouponAdapter;
    public static final String TAG = "TAG";
    private ArrayList<CouponResponce.Data> couponArrayList = new ArrayList<>();
    String  userId;
    SharedPreferences userPrefs;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.coupon_fragment);
        } else {
            setContentView(R.layout.coupon_fragment_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        back_Btn=(ImageView)findViewById(R.id.back_btn);
        couponlist=(RecyclerView)findViewById(R.id.couponslist);

        back_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        new categoryApi().execute();

    }

    private class categoryApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparecatJson();
            Constants.showLoadingDialog(CouponVoucherActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(CouponVoucherActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<CouponResponce> call = apiService.getcoupons(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<CouponResponce>() {
                @Override
                public void onResponse(Call<CouponResponce> call, Response<CouponResponce> response) {
                    Log.d(TAG, "onResponse: "+response);
                    if (response.isSuccessful()) {
                        CouponResponce CouponResponce = response.body();
                        try {
                            if (response.isSuccessful()) {
                                    CouponResponce coupons = response.body();

                                    if (coupons.getStatus()) {
                                        couponArrayList = coupons.getData();
                                        Log.d(TAG, "coupon arry size: "+couponArrayList.size());

                                        if (couponArrayList.size() > 0) {
                                            mCouponAdapter = new CouponVoucherAdapter(CouponVoucherActivity.this, couponArrayList);
                                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CouponVoucherActivity.this);
                                            couponlist.setLayoutManager(new GridLayoutManager(CouponVoucherActivity.this, 1));
                                            couponlist.setAdapter(mCouponAdapter);
                                        }
                                        else {
                                            if (language.equals("En")){
                                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.no_records_found), getResources().getString(R.string.error),
                                                        getResources().getString(R.string.ok), CouponVoucherActivity.this);

                                            }else {
                                                Constants.showOneButtonAlertDialog(getResources().getString(R.string.no_records_found_ar), getResources().getString(R.string.error_ar),
                                                        getResources().getString(R.string.ok_ar), CouponVoucherActivity.this);

                                            }
                                                                                    }
                                    }
                                }
                            else {
                                if (language.equals("En")){
                                    String failureResponse = CouponResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), CouponVoucherActivity.this);
                                }else {
                                    String failureResponse = CouponResponce.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), CouponVoucherActivity.this);
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equals("En")){
                                Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equals("En")){
                            Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();

                }

                @Override
                public void onFailure(Call<CouponResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        if (language.equals("En")){
                            Toast.makeText(CouponVoucherActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CouponVoucherActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }



                    } else {
                        if (language.equals("En")){
                            Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(CouponVoucherActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String preparecatJson() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareVerifyMobileJson: "+parentObj);
        return parentObj.toString();
    }

}
