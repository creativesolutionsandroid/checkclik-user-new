package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.ChangeEmailResponce;
import com.cs.checkclickuser.Models.ChangeMobileOtp;
import com.cs.checkclickuser.Models.ChangePasswordResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EditProfileActivity extends AppCompatActivity {

    TextView tapbarname;
    LinearLayout changename,changeemail,changephone;
    private static final String TAG = "TAG";
    Button changedone,emaildone,changephonedone;
    EditText firstnameed, lastnameed,email,phone;
    String strFirstname,strLastname,stremail,strphone;
    ImageView back_btn;
    private TextInputLayout inputLayoutfirstname, inputLayoutlastname,inputemailLayout;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId,fistname,lastname ;

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (language.equals("En")){
            setContentView(R.layout.edit_profileactivity);
        }else {
            setContentView(R.layout.edit_profileactivity_ar);
        }


        tapbarname = (TextView) findViewById(R.id.tapbarname);
        changename= (LinearLayout)findViewById(R.id.changenamelayout);
        changeemail=(LinearLayout)findViewById(R.id.changeemaillayout);
        changephone=(LinearLayout)findViewById(R.id.changephonellayout);
        changedone=(Button)findViewById(R.id.changedone);
        emaildone=(Button)findViewById(R.id.emaildone);
        changephonedone=(Button)findViewById(R.id.phonedone);
        firstnameed =(EditText)findViewById(R.id.fullname);
        lastnameed =(EditText)findViewById(R.id.lastname);
        email =(EditText)findViewById(R.id.signin_input_email);
        phone =(EditText)findViewById(R.id.signin_input_phone);
        back_btn=(ImageView)findViewById(R.id.back_btn);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        fistname=userPrefs.getString("name" ,null);
        lastname=userPrefs.getString("lastname" ,null);

        inputLayoutfirstname = (TextInputLayout) findViewById(R.id.layout_firstname);
        inputLayoutlastname = (TextInputLayout) findViewById(R.id.layout_last);
        inputemailLayout = (TextInputLayout) findViewById(R.id.layout_email_input_);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        emaildone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailValidation()){
                    new ChangeemailApi().execute();
                }
            }
        });

        Intent intent = getIntent();

        if (intent.getStringExtra("changename").equals("Change Name")) {
            if (language.equals("En")){
                tapbarname.setText("" + getResources().getString(R.string.change_name));
            }else {
                tapbarname.setText("" + getResources().getString(R.string.change_name_ar));
            }

            changename.setVisibility(View.VISIBLE);
            firstnameed.setText(fistname);
            lastnameed.setText(lastname);
        }
        else if (intent.getStringExtra("changename").equals("Change Email")){
            if (language.equals("En")){
                tapbarname.setText("" + getResources().getString(R.string.change_email));
            }else {
                tapbarname.setText("" + getResources().getString(R.string.change_email_ar));
            }


            changeemail.setVisibility(View.VISIBLE);
        } else if (intent.getStringExtra("changename").equals("Change Mobile Number")){
            if (language.equals("En")){
                tapbarname.setText("" + getResources().getString(R.string.change_mobile_number));
            }else {
                tapbarname.setText("" + getResources().getString(R.string.change_mobile_number_ar));
            }

            changephone.setVisibility(View.VISIBLE);
        }

        changedone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                new ChangeNameApi().execute();
            }
        });

        changephonedone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phoneValidation()){
                    new ChangephoneResponse().execute();
                }
            }
        });
    }

    private boolean phoneValidation() {
        strphone=phone.getText().toString();
        if (strphone.length()==0){
            if (language.equals("En")){
                phone.setError(getResources().getString(R.string.signup_msg_enter_phone));
            }else {
                phone.setError(getResources().getString(R.string.signup_msg_enter_phone_ar));
            }

            Constants.requestEditTextFocus(phone, EditProfileActivity.this);
            return false;
        }
        else if (strphone.length()<9){
            if (language.equals("En")){
                phone.setError(getResources().getString(R.string.signup_msg_enter_valid_phone));
            }else {
                phone.setError(getResources().getString(R.string.signup_msg_enter_valid_phone_ar));
            }

            Constants.requestEditTextFocus(phone, EditProfileActivity.this);
            return false;
        }
        return true;
    }

    private boolean emailValidation() {
        stremail=email.getText().toString();
        if (stremail.length()==0){
            if (language.equals("En")){
                inputemailLayout.setError(getResources().getString(R.string.signup_msg_enter_email));
            }else {
                inputemailLayout.setError(getResources().getString(R.string.signup_msg_enter_email_ar));
            }

            return false;
        }
        return true;
    }


    private boolean nameValidations(){
        strFirstname = firstnameed.getText().toString();
        strLastname = lastnameed.getText().toString();

        if(strFirstname.length() == 0){
            inputLayoutfirstname.setError(getResources().getString(R.string.signup_msg_enter_firstname));
            Constants.requestEditTextFocus(firstnameed, EditProfileActivity.this);
            return false;
        }

        if(strLastname.length() == 0){
            inputLayoutlastname.setError(getResources().getString(R.string.signup_msg_enter_lastname));
            Constants.requestEditTextFocus(lastnameed, EditProfileActivity.this);
            return false;
        }

        if (stremail.length()==0){
            inputemailLayout.setError(getResources().getString(R.string.edit_msg_enter_email));
        }
        return true;
    }

    private class ChangeNameApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparenameJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordResponce> call = apiService.getchagepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponce>() {
                @Override
                public void onResponse(Call<ChangePasswordResponce> call, Response<ChangePasswordResponce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangePasswordResponce resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
//                                String userId = resetPasswordResponse.getData().getMobileNo();
//                                userPrefEditor.putString("userId", userId);
//                                userPrefEditor.commit();
                                finish();
                                Toast.makeText(EditProfileActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                            }
                            else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                        }
                        Constants.closeLoadingDialog();
                    }

                }

                @Override
                public void onFailure(Call<ChangePasswordResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();

                    }
                    Constants.showLoadingDialog(EditProfileActivity.this);

                }
            });
            return null;
        }
    }

    private String preparenameJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FirstName", strFirstname) ;
            parentObj.put("LastName", strLastname) ;
            parentObj.put("UserId", userId) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private class ChangeemailApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = preparemailJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeEmailResponce> call = apiService.getemailchange(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeEmailResponce>() {
                @Override
                public void onResponse(Call<ChangeEmailResponce> call, Response<ChangeEmailResponce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangeEmailResponce resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
                                userPrefsEditor.putString("email", email.toString());

                                finish();
                                Toast.makeText(EditProfileActivity.this, R.string.reset_success_msg_email, Toast.LENGTH_SHORT).show();
                            }
                            else {

                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<ChangeEmailResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(EditProfileActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.showLoadingDialog(EditProfileActivity.this);

                }
            });
            return null;
        }
    }

    private String preparemailJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("EmailId", stremail) ;
            parentObj.put("Id", userId) ;
            parentObj.put("FlagId", 2) ;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "preparechangeemail: "+parentObj);
        return parentObj.toString();
    }

    private class ChangephoneResponse extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(EditProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangeMobileOtp> call = apiService.getmobilechangeotp(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangeMobileOtp>() {
                @Override
                public void onResponse(Call<ChangeMobileOtp> call, Response<ChangeMobileOtp> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangeMobileOtp resetPasswordResponse = response.body();
                        try {
                            if (resetPasswordResponse.getStatus()) {
                                Log.d(TAG, "OTP: " + resetPasswordResponse.getData().getOTP());
                                showVerifyDialog(resetPasswordResponse.getData().getOTP());
                            } else {
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(resetPasswordResponse.getMessage(), getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), EditProfileActivity.this);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }


                        }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<ChangeMobileOtp> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(EditProfileActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(EditProfileActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(EditProfileActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(EditProfileActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }

            });
            return null;
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("MobileNo","966"+strphone);
            parentObj.put("FlagId",1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG","prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private void showVerifyDialog(String otp) {
        Intent intent = new Intent(EditProfileActivity.this, VerifyOtpDialog.class);
        intent.putExtra("screen","changemobile");
        intent.putExtra("MobileNo2","966"+strphone);
        intent.putExtra("OTP2",otp);
        startActivity(intent);
    }
}
