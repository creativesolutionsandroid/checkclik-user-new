package com.cs.checkclickuser.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Models.ChangePasswordResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangeaPasswordActivity extends AppCompatActivity {

    private TextInputLayout inputLayoutcurrentpassword,inputLayoutnewpassword,inputLayoutconfirampassword;
    SharedPreferences.Editor userPrefEditor;
    SharedPreferences userPrefs;
    String response, userId,strPhone,strPassword,strNewpassword;
    Button submit;
    TextView back_btn;
    String TAG;


    SharedPreferences LanguagePrefs;
    String language;

    EditText oldPassword, newPassword, confirmPassword;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.change_passwordactivity);
        } else {
            setContentView(R.layout.change_passwordactivity_ar);
        }


        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);
        strPhone = userPrefs.getString("mobile", null);

        oldPassword =(EditText)findViewById(R.id.currentpassword);
        newPassword =(EditText)findViewById(R.id.newpassword);
        confirmPassword =(EditText)findViewById(R.id.confirampassword);
        submit=(Button)findViewById(R.id.submit);
        back_btn=(TextView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String oldPwd = oldPassword.getText().toString();
                String newPwd = newPassword.getText().toString();
                String confirmPwd = confirmPassword.getText().toString();
                strPassword = oldPwd;
                strNewpassword = newPwd;
                if (oldPwd.length() == 0) {
                    if (language.equals("En")){
                        oldPassword.setError("" + getResources().getString(R.string.please_enter_old_password));
                    }else {
                        oldPassword.setError("" + getResources().getString(R.string.please_enter_old_password_ar));
                    }
                    }
                 else if (newPwd.length() == 0) {
                    if (language.equals("En")){
                        newPassword.setError("" + getResources().getString(R.string.please_enter_new_password));
                    }else {
                        newPassword.setError("" + getResources().getString(R.string.please_enter_new_password_ar));;
                    }
                } else if (confirmPwd.length() == 0) {
                    if (language.equals("En")){
                        confirmPassword.setError("" + getResources().getString(R.string.please_retype_password));
                    }else {
                        confirmPassword.setError("" + getResources().getString(R.string.please_retype_password_ar));
                    }
                } else if (!newPwd.equals(confirmPwd)) {
                    if (language.equals("En")){
                        confirmPassword.setError("" + getResources().getString(R.string.passwords_not_match));
                    }else {
                        confirmPassword.setError("" + getResources().getString(R.string.passwords_not_match_ar));
                    }
                }
                else {
                    new ChangePasswordResponse().execute(userId, oldPwd, newPwd);
                }

            }
        });

    }
    private class ChangePasswordResponse extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(ChangeaPasswordActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ChangePasswordResponce> call = apiService.getchagepassword(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ChangePasswordResponce>() {
                @Override
                public void onResponse(Call<ChangePasswordResponce> call, Response<ChangePasswordResponce> response) {
                    Log.d("TAG", "onResponse: "+response);
                    if(response.isSuccessful()){
                        ChangePasswordResponce resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getMobileNo();
                                userPrefEditor.putString("userId", userId);
                                userPrefEditor.commit();
                                finish();
                                if (language.equalsIgnoreCase("En")){
                                    Toast.makeText(ChangeaPasswordActivity.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(ChangeaPasswordActivity.this, R.string.reset_success_msg_ar, Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {

                                if (language.equalsIgnoreCase("En")){
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), ChangeaPasswordActivity.this);
                                }else {
                                    String failureResponse = resetPasswordResponse.getMessage();
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok_ar), ChangeaPasswordActivity.this);
                                }

                                }

                        } catch (Exception e) {
                            e.printStackTrace();

                            if (language.equalsIgnoreCase("En")){
                                Toast.makeText(ChangeaPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ChangeaPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                        Constants.closeLoadingDialog();
                    }

                }

                @Override
                public void onFailure(Call<ChangePasswordResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ChangeaPasswordActivity.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ChangeaPasswordActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ChangeaPasswordActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ChangeaPasswordActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ChangeaPasswordActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                   Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }
    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Id",userId);
            parentObj.put("NewPassword",strNewpassword);
            parentObj.put("Password", strPassword);
            parentObj.put("FlagId", 6);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("TAG","prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

}
