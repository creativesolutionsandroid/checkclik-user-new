package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.ProductItemCategoriesAdapter;
import com.cs.checkclickuser.Models.ProductstoreResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.BarCodeScanner;
import com.cs.checkclickuser.Utils.Constants;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;


public class ProductItemCategoriesActivityStep3 extends AppCompatActivity {

    private ProductstoreResponce.Data storeArrayList = new ProductstoreResponce.Data();
    private ArrayList<ProductstoreResponce.SubCategory> subCategoryArrayList = new ArrayList<>();
    private ArrayList<ProductstoreResponce.SubCategory> subCategoryArrayListFiltered = new ArrayList<>();

    private int storePos;
    private ImageView back_btn, cart, search, notification;
    public ProductItemCategoriesAdapter miteamlistadaper;
    private TextView branchname;
    private String TAG = "TAG";
    private RecyclerView storeslist;
    private SharedPreferences userPrefs;
    private int type;

    private RelativeLayout searchLayout;
    private EditText productSearch;
    private boolean isSearchVisible = false;
    private ImageView cartStatusIcon, notifictionStatusIcon;

    private ImageView scanner;
    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
        if (Constants.notificationCount != 0) {
            notifictionStatusIcon.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_iteam_activity);
        } else {
            setContentView(R.layout.product_iteam_activity_ar);
        }



        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        search = (ImageView) findViewById(R.id.search);
        notification = (ImageView) findViewById(R.id.noti);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        branchname = (TextView) findViewById(R.id.branchname);
        storeslist = (RecyclerView) findViewById(R.id.list_item);
        cart = (ImageView) findViewById(R.id.cart);
        productSearch = (EditText) findViewById(R.id.product_search);
        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        searchLayout = (RelativeLayout) findViewById(R.id.search_layout);

        storeArrayList = (ProductstoreResponce.Data) getIntent().getSerializableExtra("stores");
        storePos = getIntent().getIntExtra("pos", 0);
        type = getIntent().getIntExtra("type", 1);

        int icback_btnId = getResources().getIdentifier("ic_back_"+Constants.appColor, "drawable", getPackageName());
        back_btn.setImageDrawable(getResources().getDrawable(icback_btnId));

        int icnotificationId = getResources().getIdentifier("ic_noti_"+Constants.appColor, "drawable", getPackageName());
        notification.setImageDrawable(getResources().getDrawable(icnotificationId));

        int iccartId = getResources().getIdentifier("ic_cart_"+Constants.appColor, "drawable", getPackageName());
        cart.setImageDrawable(getResources().getDrawable(iccartId));

        int icsearchId = getResources().getIdentifier("search_"+Constants.appColor, "drawable", getPackageName());
        search.setImageDrawable(getResources().getDrawable(icsearchId));

        copyData();

        scanner=(ImageView) findViewById(R.id.cameraicon);

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductItemCategoriesActivityStep3.this);
                } else {
                    Intent intent = new Intent(ProductItemCategoriesActivityStep3.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(ProductItemCategoriesActivityStep3.this);
                    }
                } else {
                    BarCodeScanner.showScannerDialog(ProductItemCategoriesActivityStep3.this);
                }
            }
        });

        if (language.equals("En")){
            branchname.setText(storeArrayList.getMainCategory().get(storePos).getNameEn());
        }else {
            branchname.setText(storeArrayList.getMainCategory().get(storePos).getNameAr());
        }


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductItemCategoriesActivityStep3.this);
                } else {
                    Intent intent = new Intent(ProductItemCategoriesActivityStep3.this, CartActivity.class);
                    startActivity(intent);
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSearchVisible) {
                    searchLayout.setVisibility(View.VISIBLE);
                    isSearchVisible = true;
                    productSearch.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

                }
                else {
                    searchLayout.setVisibility(View.GONE);
                    isSearchVisible = false;
                    hideKeyboard(ProductItemCategoriesActivityStep3.this);
                }
            }
        });

        productSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length() > 0) {
                    String searchText = editable.toString();
                        filterSubCategory(searchText);
                }
                else {
                    setAdapter();
                }
            }
        });

        productSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        productSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(ProductItemCategoriesActivityStep3.this);
                    return true;
                }
                return false;
            }
        });
    }

    private void filterSubCategory(String searchText){
        Log.d(TAG, "filterSubCategory: "+subCategoryArrayList.size());
        subCategoryArrayListFiltered = null;
        subCategoryArrayListFiltered = new ArrayList<>();
        Log.d(TAG, "filterSubCategory: "+subCategoryArrayList.size());
        for (int i = 0; i < subCategoryArrayList.size(); i++) {
            if(subCategoryArrayList.get(i).getNameEn().toLowerCase().contains(searchText)||
                    subCategoryArrayList.get(i).getNameAr().contains(searchText)) {
                ProductstoreResponce.SubCategory subCategory = new ProductstoreResponce.SubCategory();
                subCategory.setBranchId(subCategoryArrayList.get(i).getBranchId());
                subCategory.setCategoryId(subCategoryArrayList.get(i).getCategoryId());
                subCategory.setId(subCategoryArrayList.get(i).getId());
                subCategory.setImage(subCategoryArrayList.get(i).getImage());
                subCategory.setIsActive(subCategoryArrayList.get(i).getIsActive());
                subCategory.setNameAr(subCategoryArrayList.get(i).getNameAr());
                subCategory.setNameEn(subCategoryArrayList.get(i).getNameEn());
                subCategory.setProductCount(subCategoryArrayList.get(i).getProductCount());
                subCategoryArrayListFiltered.add(subCategory);
            }
        }

        miteamlistadaper = new ProductItemCategoriesAdapter(ProductItemCategoriesActivityStep3.this, subCategoryArrayListFiltered, storeArrayList, storePos, getIntent().getIntExtra("storeId", 0), type);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemCategoriesActivityStep3.this);
        storeslist.setLayoutManager(new GridLayoutManager(ProductItemCategoriesActivityStep3.this, 1));
        storeslist.setAdapter(miteamlistadaper);
        miteamlistadaper.notifyDataSetChanged();
    }

    private void setAdapter(){
        subCategoryArrayListFiltered.clear();
        subCategoryArrayListFiltered =
                (ArrayList<ProductstoreResponce.SubCategory>) subCategoryArrayList.clone();

        miteamlistadaper = new ProductItemCategoriesAdapter(ProductItemCategoriesActivityStep3.this, subCategoryArrayListFiltered, storeArrayList, storePos, getIntent().getIntExtra("storeId", 0), type);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemCategoriesActivityStep3.this);
        storeslist.setLayoutManager(new GridLayoutManager(ProductItemCategoriesActivityStep3.this, 1));
        storeslist.setAdapter(miteamlistadaper);
        miteamlistadaper.notifyDataSetChanged();
    }

    private void copyData(){
        ProductstoreResponce.SubCategory subCategory = new ProductstoreResponce.SubCategory();
        subCategory.setProductCount(storeArrayList.getSubCategory().get(storePos).getProductCount());
        subCategory.setNameEn(storeArrayList.getSubCategory().get(storePos).getNameEn());
        subCategory.setNameAr(storeArrayList.getSubCategory().get(storePos).getNameAr());
        subCategory.setIsActive(storeArrayList.getSubCategory().get(storePos).getIsActive());
        subCategory.setImage(storeArrayList.getSubCategory().get(storePos).getImage());
        subCategory.setId(storeArrayList.getSubCategory().get(storePos).getId());
        subCategory.setCategoryId(storeArrayList.getSubCategory().get(storePos).getCategoryId());
        subCategory.setBranchId(storeArrayList.getSubCategory().get(storePos).getBranchId());
        subCategoryArrayList.add(subCategory);

        subCategoryArrayListFiltered = subCategoryArrayList;

        miteamlistadaper = new ProductItemCategoriesAdapter(ProductItemCategoriesActivityStep3.this, subCategoryArrayListFiltered, storeArrayList, storePos, getIntent().getIntExtra("storeId", 0), type);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductItemCategoriesActivityStep3.this);
        storeslist.setLayoutManager(new GridLayoutManager(ProductItemCategoriesActivityStep3.this, 1));
        storeslist.setAdapter(miteamlistadaper);
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProductItemCategoriesActivityStep3.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(ProductItemCategoriesActivityStep3.this);
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied_Ar), Toast.LENGTH_LONG).show();
                    }

                }
                break;
        }
    }
}

