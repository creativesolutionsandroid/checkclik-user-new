package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.SearchFiltersSubTitleAdapter;
import com.cs.checkclickuser.Adapter.SearchFiltersTitleAdapter;
import com.cs.checkclickuser.Adapter.SearchItemAdapter;
import com.cs.checkclickuser.Adapter.SearchListAdapter;
import com.cs.checkclickuser.Models.SearchListResponce;
import com.cs.checkclickuser.Models.SearchProductListResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.BarCodeScanner;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;

public class SearchActivity extends AppCompatActivity {

    TextView resultsCount;
    ImageView back_btn;
    EditText inputsearch;
    public static final String TAG = "TAG";
    public static String searchTex;
    String inputStr;
    SearchListAdapter mSearchlistAdapter;
    private ArrayList<String> searchlistarray = new ArrayList<>();
    ListView searchlistview;
    ListView filterslistview;
    SearchProductListResponce.Data seachproductArrayList;
    private RecyclerView productItemList;
    private SearchItemAdapter mServiceDealsAdapter;
    RelativeLayout productsLayout;
    String searchTextLocal = "";
    TextView filters;
    RelativeLayout filtersLayout;
    BubbleLayout bubbleLayout;
    View closeFilters;
    SharedPreferences LanguagePrefs;
    String language = "En";



    private ImageView scanner;
    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.search_ativity);
        } else {
            setContentView(R.layout.search_ativity_ar);
        }



        resultsCount = (TextView) findViewById(R.id.results_count);
        filters = (TextView) findViewById(R.id.filters);
        back_btn = (ImageView)findViewById(R.id.back_btn);
        inputsearch = (EditText)findViewById(R.id.search);
        searchlistview = (ListView)findViewById(R.id.searchlist);
        productItemList = (RecyclerView) findViewById(R.id.productlist);
        productsLayout = (RelativeLayout) findViewById(R.id.results_layout);

        scanner=(ImageView) findViewById(R.id.cameraicon);

//        final BubbleLayout bubbleLayout = (BubbleLayout) LayoutInflater.from(this).inflate(R.layout.bubble_search_filters, null);
//        final PopupWindow popupWindow = BubblePopupHelper.create(this, bubbleLayout);
        filterslistview = (ListView) findViewById(R.id.filetrs_list);
        filtersLayout = (RelativeLayout) findViewById(R.id.filters_layout);
        closeFilters = (View) findViewById(R.id.filters_close);

        closeFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filtersLayout.setVisibility(View.GONE);
            }
        });

        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersLayout.setVisibility(View.VISIBLE);
            }
        });

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(SearchActivity.this);
                    }
                } else {
                    BarCodeScanner.showScannerDialog(SearchActivity.this);
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        inputsearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        inputsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    searchTex = inputsearch.getText().toString();
//                    if (searchTex.length() >= 3) {
                        hideKeyboard(SearchActivity.this);
//                        String networkStatus = NetworkUtil.getConnectivityStatusString(SearchActivity.this);
//                        if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
//                            new SearchApi().execute();
//                        }
//                        else {
//                            Toast.makeText(getApplicationContext().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
//                        }
//                    }
                    return true;
                }
                return false;
            }
        });

        inputsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                productsLayout.setVisibility(View.GONE);
                searchlistview.setVisibility(View.VISIBLE);
                if(inputsearch.getText().toString().length() > 0) {
                    searchTextLocal = inputsearch.getText().toString();
                }
                else {
                    searchTextLocal = "";
                }

                String networkStatus = NetworkUtil.getConnectivityStatusString(SearchActivity.this);
                if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                    new SearchApi().execute();
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(getApplicationContext().getApplicationContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getApplicationContext().getApplicationContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (getIntent().getStringExtra("text") != null && !getIntent().getStringExtra("text").equalsIgnoreCase("")) {
            inputsearch.setText(getIntent().getStringExtra("text"));
        }

        searchlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                searchTextLocal = searchlistarray.get(i);
                inputsearch.setText(searchTextLocal);
                inputsearch.setSelection(inputsearch.length());
                hideKeyboard(SearchActivity.this);
                inputStr = prepareGetStoresJSON(i);
                new GetProductsList().execute();
            }
        });
    }

    private class SearchApi extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SearchListResponce> call = apiService.getsearcresult(searchTextLocal,language);
            call.enqueue(new Callback<SearchListResponce>() {
                @Override
                public void onResponse(Call<SearchListResponce> call, Response<SearchListResponce> response) {
                    Log.i("TAG", "store servies responce " + response);
                    if (response.isSuccessful()) {
                        SearchListResponce products = response.body();
                        if (products.getStatus()) {
                            searchlistarray = products.getData();

                            mSearchlistAdapter = new SearchListAdapter(SearchActivity.this, searchlistarray);
                            searchlistview.setAdapter(mSearchlistAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(Call<SearchListResponce> call, Throwable t) {

                }
            });
            return null;
        }
    }

    private class GetProductsList extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(SearchActivity.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<SearchProductListResponce> call = apiService.getsearchlist(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<SearchProductListResponce>() {
                @Override
                public void onResponse(Call<SearchProductListResponce> call, Response<SearchProductListResponce> response) {
                    Log.d(TAG, "vonResponse: "+response);
                    if(response.isSuccessful()) {
                        SearchProductListResponce products = response.body();
                        if (products.getStatus()) {
                            seachproductArrayList = products.getData();
                            Constants.closeLoadingDialog();

                            if (language.equalsIgnoreCase("En")){
                                resultsCount.setText("" + getResources().getString(R.string.over) + " "+seachproductArrayList.getResults().size() + " " + getResources().getString(R.string.results));
                            }else {
                                resultsCount.setText("" + getResources().getString(R.string.over_ar) + " "+seachproductArrayList.getResults().size() + " " + getResources().getString(R.string.results_ar));
                            }


                            mServiceDealsAdapter = new SearchItemAdapter(SearchActivity.this, seachproductArrayList);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchActivity.this);
                            productItemList.setLayoutManager(new GridLayoutManager(SearchActivity.this, 2));
                            productItemList.setAdapter(mServiceDealsAdapter);

                            if (seachproductArrayList.getFacets().size() > 0) {
                                SearchFiltersTitleAdapter mAdapter = new SearchFiltersTitleAdapter(SearchActivity.this, seachproductArrayList.getFacets());
                                filterslistview.setAdapter(mAdapter);
                            }

                            productsLayout.setVisibility(View.VISIBLE);
                            searchlistview.setVisibility(View.GONE);
                        }
                        else {
                            //status false case
                            Constants.closeLoadingDialog();
                            if (language.equalsIgnoreCase("En")){
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok), SearchActivity.this);

                            }else {
                                String failureResponse = response.body().getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                        getResources().getString(R.string.ok_ar), SearchActivity.this);

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<SearchProductListResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(SearchActivity.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SearchActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SearchActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(SearchActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(SearchActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON(int position){
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Query",  searchlistarray.get(position));
            if(getIntent().getIntExtra("Branch_id", 0) > 0) {
                parentObj.put("BranchId", getIntent().getIntExtra("Branch_id", 0));
            }
            parentObj.put("Start",  0);
            parentObj.put("Rows",  20);
            parentObj.put("Location", Constants.Latitude+","+Constants.Longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: "+parentObj);
        return parentObj.toString();
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(SearchActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(SearchActivity.this);
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied_Ar), Toast.LENGTH_LONG).show();
                    }

                }
                break;
        }
    }
}
