package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
//import com.cometchat.pro.constants.CometChatConstants;
//import com.cometchat.pro.core.AppSettings;
//import com.cometchat.pro.core.CometChat;
//import com.cometchat.pro.exceptions.CometChatException;
//import com.cometchat.pro.models.Group;
//import com.cometchat.pro.models.User;
import com.cs.checkclickuser.Adapter.BranchListAdapter;
import com.cs.checkclickuser.Adapter.BranchPopupAdapter;
import com.cs.checkclickuser.Adapter.ProductReviewsAdapter;
import com.cs.checkclickuser.Contracts.StringContract;
import com.cs.checkclickuser.Models.BasicResponse;
import com.cs.checkclickuser.Models.BranchDetails;
import com.cs.checkclickuser.Models.BranchDetailsResponse;
import com.cs.checkclickuser.Models.BranchGroupId;
import com.cs.checkclickuser.Models.FavStoresResponce;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.Models.ProductStoreReviews;
import com.cs.checkclickuser.Models.ProductlistResponce;
import com.cs.checkclickuser.Models.UrlEncryptResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.AppConfig;
import com.cs.checkclickuser.Utils.BranchColor;
import com.cs.checkclickuser.Utils.CacheData;
import com.cs.checkclickuser.Utils.CommonUtils;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.CustomListView;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;
import com.daasuu.bl.ArrowDirection;
import com.daasuu.bl.BubbleLayout;
import com.daasuu.bl.BubblePopupHelper;
import com.gmail.samehadar.iosdialog.CamomileSpinner;
import com.gmail.samehadar.iosdialog.utils.DialogUtils;
import com.lovejjfg.shadowcircle.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;
import static com.cs.checkclickuser.Utils.Constants.closeLoadingDialog;
import static com.cs.checkclickuser.Utils.Constants.priceFormat1;

public class ProductStoresActivityStep1 extends AppCompatActivity {

    ImageView backbtn, share, favourite, cart, notifaction, stroeback, branchdown, chat, singleicon;
    TextView storetime, branchtext;
    ImageView card, cash, storeStatusImage;
    TextView deliveryTimeTitle, deliveryChargeTitle, pickupTitle;
    ImageView callImage, emailImage, facebook, twitter, google, maroof;

    TextView mainstore, reviews, storestatus, distance, deliverytime,
            minimumorder, dcharge, orderdone, businesssince, pickip, storeservice, about, textview, reviewsText, terms;
    CircleImageView storeimage;
    int storePos;
    String TAG = "TAG";
    Button bt_continue;
    View about_line, reviews_line, terms_line;
    private ProductReviewsAdapter reviewsAdapter;
    private CustomListView reviewsListView;
    LinearLayout aboutLayout;
    BranchPopupAdapter mpoupAdapter;
//    Spinner branchspinner;

    private ArrayList<HomePageResponse.Featuredstores> featuredstores = new ArrayList<>();
    public ArrayList<ProductlistResponce.Stores> productArrayList = new ArrayList<>();
    public List<ProductStoreReviews.Data> reviewsList = new ArrayList<>();
    public ArrayList<ProductlistResponce.JOtherBranch> branchresponcelist = new ArrayList<>();
    public ArrayList<BranchDetailsResponse.Branch> branchDetailsFromBarcode = new ArrayList<>();
    public ArrayList<FavStoresResponce.StoreList> branchDetailsFromFavStores = new ArrayList<>();
    public ArrayList<BranchDetails.Branch> branchDetailsFromVendorAppIntent = new ArrayList<>();

    private BranchListAdapter branchadapter;
    ArrayList<String> branchlistaray = new ArrayList<>();

    SharedPreferences userPrefs;
    int type;
    private RatingBar ratingBar;
    public static RelativeLayout hidden_layout;
    public static int branch_pos = -1;

    private RelativeLayout linksLayout;
    private ScrollView scrollView;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    private String groupChatId = "";
    private ImageView cartStatusIcon, notifictionStatusIcon;

    private ImageView icLocation, icClock, icDeliveryCharges, icMinOrder, icDeliveryTime, icPayment, icOrdersDone, icBusinessSince,
            icPickup, icShippingCharges;
    private LinearLayout facebookLayout, twitterLayout, instaLaoyut, maroofLayout;

    UrlEncryptResponse.Data encryptData;
    File file12;

    private static final int STORAGE_REQUEST = 2;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int FAV_REQUEST = 3;


    SharedPreferences LanguagePrefs;
    String language;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.product_storesdetails);
        } else {
            setContentView(R.layout.product_storesdetails_ar);

        }

        int currentapiVersion1 = Build.VERSION.SDK_INT;
        if (currentapiVersion1 >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        userPrefs = getSharedPreferences("USER_PREFS", MODE_PRIVATE);

        storePos = getIntent().getIntExtra("pos", 0);
        type = getIntent().getIntExtra("type", 1);
        branch_pos = -1;

        scrollView = (ScrollView) findViewById(R.id.scrollview);

        notifictionStatusIcon = (ImageView) findViewById(R.id.notification_status);
        cartStatusIcon = (ImageView) findViewById(R.id.cart_status);

        facebook = (ImageView) findViewById(R.id.facebook);
        twitter = (ImageView) findViewById(R.id.twitter);
        maroof = (ImageView) findViewById(R.id.maroof);
        google = (ImageView) findViewById(R.id.google);
        callImage = (ImageView) findViewById(R.id.phone);
        emailImage = (ImageView) findViewById(R.id.gmail);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        singleicon = (ImageView) findViewById(R.id.singleicon);
        share = (ImageView) findViewById(R.id.share_btn);
        favourite = (ImageView) findViewById(R.id.fav);
        cart = (ImageView) findViewById(R.id.cart);
        chat = (ImageView) findViewById(R.id.chat);
        notifaction = (ImageView) findViewById(R.id.noti);
        stroeback = (ImageView) findViewById(R.id.storebackimage);
        storeimage = (CircleImageView) findViewById(R.id.ac_storepic);
        card = (ImageView) findViewById(R.id.card);
        cash = (ImageView) findViewById(R.id.cash);
        branchdown = (ImageView) findViewById(R.id.dropdown);
        bt_continue = (Button) findViewById(R.id.bt_continue);

        icLocation = (ImageView) findViewById(R.id.pin);
        icClock = (ImageView) findViewById(R.id.clock);
        icDeliveryCharges = (ImageView) findViewById(R.id.ic_delivery_charges);
        icMinOrder = (ImageView) findViewById(R.id.ic_min_order);
        icDeliveryTime = (ImageView) findViewById(R.id.ic_delivery_time);
        icPayment = (ImageView) findViewById(R.id.ic_payment);
        icOrdersDone = (ImageView) findViewById(R.id.ic_orders_done);
        icBusinessSince = (ImageView) findViewById(R.id.ic_business_since);
        icPickup = (ImageView) findViewById(R.id.ic_pickup);
        icShippingCharges = (ImageView) findViewById(R.id.ic_shipping_charges);

        facebookLayout = (LinearLayout) findViewById(R.id.facebook_layout);
        twitterLayout = (LinearLayout) findViewById(R.id.twitter_layout);
        instaLaoyut = (LinearLayout) findViewById(R.id.instagram_layout);
        maroofLayout = (LinearLayout) findViewById(R.id.maroof_layout);

        mainstore = (TextView) findViewById(R.id.mainstore);
        branchtext = (TextView) findViewById(R.id.branchtext);
//        storerating = (TextView) findViewById(R.id.revies_count);
        distance = (TextView) findViewById(R.id.distance);
        reviews = (TextView) findViewById(R.id.revies);
        storestatus = (TextView) findViewById(R.id.storestatus);
        deliverytime = (TextView) findViewById(R.id.delivery_time);
        minimumorder = (TextView) findViewById(R.id.minimumorder);
        dcharge = (TextView) findViewById(R.id.dcharge);
        orderdone = (TextView) findViewById(R.id.orderdone);
        businesssince = (TextView) findViewById(R.id.business);
        pickip = (TextView) findViewById(R.id.pickip);
        storeservice = (TextView) findViewById(R.id.storeservice);
        about = (TextView) findViewById(R.id.about);
        reviewsText = (TextView) findViewById(R.id.reviews);
        terms = (TextView) findViewById(R.id.terms);
        about = (TextView) findViewById(R.id.about);
        textview = (TextView) findViewById(R.id.textview);

        storetime = (TextView) findViewById(R.id.storetime);
        deliveryTimeTitle = (TextView) findViewById(R.id.delivery_time_title);
        deliveryChargeTitle = (TextView) findViewById(R.id.delivery_charge_title);
        pickupTitle = (TextView) findViewById(R.id.pickip_title);

        storeStatusImage = (ImageView) findViewById(R.id.opengreen);
        about_line = (View) findViewById(R.id.about_line);
        reviews_line = (View) findViewById(R.id.reviews_line);
        terms_line = (View) findViewById(R.id.terms_line);
//        branchspinner = (Spinner) findViewById(R.id.branhspinner);

        ratingBar = (RatingBar) findViewById(R.id.rating_bar);

        hidden_layout = (RelativeLayout) findViewById(R.id.hidden_layout);
        linksLayout = (RelativeLayout) findViewById(R.id.links_layout);

        aboutLayout = (LinearLayout) findViewById(R.id.about_content);

        reviewsListView = (CustomListView) findViewById(R.id.reviews_listview);

        if (getIntent().getStringExtra("class").equals("product") || getIntent().getStringExtra("class").equals("service")) {
            productArrayList = (ArrayList<ProductlistResponce.Stores>) getIntent().getSerializableExtra("stores");
            initView();
        } else if (getIntent().getStringExtra("class").equals("home")) {
            featuredstores = (ArrayList<HomePageResponse.Featuredstores>) getIntent().getSerializableExtra("stores");
            convertFeaturedproductArrayListToStores();
        } else if (getIntent().getStringExtra("class").equals("search")) {
            branchDetailsFromBarcode = (ArrayList<BranchDetailsResponse.Branch>) getIntent().getSerializableExtra("stores");
            convertBarcodeBranchDetailsToStores();
        }
        else if (getIntent().getStringExtra("class").equals("fav")) {
            branchDetailsFromFavStores = (ArrayList<FavStoresResponce.StoreList>) getIntent().getSerializableExtra("stores");
            convertFavouriteBranchDetailsToStores();
        }
        else if (getIntent().getStringExtra("class").equals("vendor_app_intent")) {
            branchDetailsFromVendorAppIntent = (ArrayList<BranchDetails.Branch>) getIntent().getSerializableExtra("stores");
            convertVendorAppBranchDetailsToStores();
        }

        notifaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductStoresActivityStep1.this);
                } else {
                    Intent intent = new Intent(ProductStoresActivityStep1.this, NotificationsActivity.class);
                    startActivity(intent);
                }
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductStoresActivityStep1.this);
                } else {
                    Intent intent = new Intent(ProductStoresActivityStep1.this, CartActivity.class);
                    startActivity(intent);
                }
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductStoresActivityStep1.this);
                } else {
                    checkCometChatLoginStatus();
//                    startActivity(new Intent(ProductStoresActivityStep1.this, CometChatActivity.class));
                }
            }
        });

        branchdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productArrayList.get(storePos).getJOtherBranch() != null && productArrayList.get(storePos).getJOtherBranch().size() > 0) {
                    setspinner();
                }
            }
        });

        branchtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (productArrayList.get(storePos).getJOtherBranch() != null  && productArrayList.get(storePos).getJOtherBranch().size() > 0) {
                    setspinner();
                }
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                about_line.setVisibility(View.VISIBLE);
                terms_line.setVisibility(View.INVISIBLE);
                reviews_line.setVisibility(View.INVISIBLE);

                aboutLayout.setVisibility(View.VISIBLE);
                linksLayout.setVisibility(View.VISIBLE);
                reviewsListView.setVisibility(View.GONE);
                textview.setText(productArrayList.get(storePos).getDescriptionEn());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    textview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
                }
                scrollView.smoothScrollTo(0, (textview.getTop() + 200));
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                about_line.setVisibility(View.INVISIBLE);
                terms_line.setVisibility(View.VISIBLE);
                reviews_line.setVisibility(View.INVISIBLE);

                aboutLayout.setVisibility(View.VISIBLE);
                linksLayout.setVisibility(View.GONE);
                reviewsListView.setVisibility(View.GONE);
                if (language.equals("En")){
                    textview.setText(productArrayList.get(storePos).getTermsAndConditionsEn());
                }else {
                    textview.setText(productArrayList.get(storePos).getTermsAndConditionsAr());
                }

                scrollView.smoothScrollTo(0, (textview.getTop() + 200));
            }
        });

        reviewsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                about_line.setVisibility(View.INVISIBLE);
                terms_line.setVisibility(View.INVISIBLE);
                reviews_line.setVisibility(View.VISIBLE);

                aboutLayout.setVisibility(View.GONE);
                reviewsListView.setVisibility(View.VISIBLE);

                if (branch_pos == -1) {
                    if (reviewsList != null && reviewsList.size() > 0) {
                        reviewsAdapter = new ProductReviewsAdapter(ProductStoresActivityStep1.this, reviewsList);
                        reviewsListView.setAdapter(reviewsAdapter);
                        scrollView.smoothScrollTo(0, (reviewsListView.getTop() + 200));
                    } else {
                        new getReviewsApi().execute();
                    }
                } else {
                    new getReviewsApi().execute();
                }
            }
        });

        bt_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductStoresActivityStep1.this, ProductMaincatActivityStep2.class);
                if (branch_pos == -1) {
                    i.putExtra("stores", productArrayList);
                    i.putExtra("pos", storePos);
                    i.putExtra("type", type);
                } else {
                    i.putExtra("stores", branchresponcelist);
                    i.putExtra("pos", branch_pos);
                    i.putExtra("type", type);
                }
                startActivityForResult(i, FAV_REQUEST);
            }
        });

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userPrefs.getString("userId", "").equals("")) {
                    Constants.showTwoButtonAlertDialog(ProductStoresActivityStep1.this);
                } else {
                    new InsertStoreFav().execute();
                }
            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        hidden_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hidden_layout.setVisibility(View.GONE);
            }
        });

        callImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        String mobile = productArrayList.get(storePos).getContactNo();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                        if (ActivityCompat.checkSelfPermission(ProductStoresActivityStep1.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    String mobile = productArrayList.get(storePos).getContactNo();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                    startActivity(intent);
                }
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                else {
                    new GetEncrytptURLApi().execute();
                }
            }
        });

        emailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openGmail();
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(ProductStoresActivityStep1.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Facebook");
                fbIntent.putExtra("url", productArrayList.get(storePos).getFacebook());
                startActivity(fbIntent);
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(ProductStoresActivityStep1.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Twitter");
                fbIntent.putExtra("url", productArrayList.get(storePos).getTwitterHandle());
                startActivity(fbIntent);
            }
        });

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(ProductStoresActivityStep1.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Google");
                fbIntent.putExtra("url", productArrayList.get(storePos).getYoutube());
                startActivity(fbIntent);
            }
        });

        maroof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent fbIntent = new Intent(ProductStoresActivityStep1.this, WebViewActivity.class);
                fbIntent.putExtra("title", "Maroof");
                fbIntent.putExtra("url", productArrayList.get(storePos).getMaroof());
                startActivity(fbIntent);
            }
        });
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProductStoresActivityStep1.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    String mobile = productArrayList.get(storePos).getContactNo();
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: "+mobile));
                    startActivity(intent);
                } else {
                    if(language.equalsIgnoreCase("En")){
                    Toast.makeText(ProductStoresActivityStep1.this, getResources().getString(R.string.call_phone_permission_denied), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(ProductStoresActivityStep1.this, getResources().getString(R.string.call_phone_permission_denied_ar), Toast.LENGTH_LONG).show();
                    }

                }
                break;

            case STORAGE_REQUEST:
                new GetEncrytptURLApi().execute();
                break;
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setspinner() {

        branchresponcelist = productArrayList.get(storePos).getJOtherBranch();
        branchlistaray.clear();

        if(branchresponcelist!= null) {
            for (int i = 0; i < branchresponcelist.size(); i++) {
                branchlistaray.add(branchresponcelist.get(i).getBranchEn());
            }
        }

        try {
            final BubbleLayout bubbleLayout;
//            if (language.equalsIgnoreCase("En")) {
            bubbleLayout = (BubbleLayout) LayoutInflater.from(ProductStoresActivityStep1.this).inflate(R.layout.branch_list_bubble, null);
//            } else {
//                bubbleLayout = (BubbleLayout) LayoutInflater.from(getActivity()).inflate(R.layout.bubble_location_popup_arabic, null);
//            }
            final PopupWindow popupWindow = BubblePopupHelper.create(ProductStoresActivityStep1.this, bubbleLayout);
            hidden_layout.setVisibility(View.VISIBLE);

            RecyclerView mbubble_List = (RecyclerView) bubbleLayout.findViewById(R.id.branch_list);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ProductStoresActivityStep1.this);
            mbubble_List.setLayoutManager(mLayoutManager);
            branchadapter = new BranchListAdapter(ProductStoresActivityStep1.this, branchlistaray, popupWindow);
            mbubble_List.setAdapter(branchadapter);

            int[] location = new int[2];
            branchdown.getLocationOnScreen(location);
            bubbleLayout.setArrowDirection(ArrowDirection.TOP);

            popupWindow.showAtLocation(branchdown, Gravity.NO_GRAVITY,  branchdown.getWidth(), branchdown.getHeight() + location[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initView() {

        if (branch_pos == -1) {
            updateBranchColor(storePos);
            final DecimalFormat distanceFormat = new DecimalFormat("##,##,###.#");
            final DecimalFormat priceFormat = new DecimalFormat("##,##,##0.##");
            if (productArrayList.get(storePos).getCartItemCount() != 0) {
                cartStatusIcon.setVisibility(View.VISIBLE);
            }
            if (productArrayList.get(storePos).getNotificationCount() != 0) {
                notifictionStatusIcon.setVisibility(View.VISIBLE);
            }
            if (type == 1) {
                if (productArrayList.get(storePos).getInStoreService().equalsIgnoreCase("true")) {
                   if (language.equalsIgnoreCase("En")){
                    pickip.setText(getResources().getString(R.string.available));
                   }else {
                        pickip.setText(getResources().getString(R.string.available_ar));
                   }

                } else if (productArrayList.get(storePos).getInStoreService().equalsIgnoreCase("false")) {
                   if (language.equalsIgnoreCase("En")){
                    pickip.setText(getResources().getString(R.string.unavailable));
                   }else {
                       pickip.setText(getResources().getString(R.string.unavailable_ar));
                   }
                }

                if (productArrayList.get(storePos).getShippingProvider() == 0) {
                    if (language.equals("En")){
                    storeservice.setText(getResources().getString(R.string.available));
                    }else {
                        storeservice.setText(getResources().getString(R.string.available_ar));
                    }
                } else if (productArrayList.get(storePos).getShippingProvider() == 1) {
                     if (language.equals("En")){
                         storeservice.setText(getResources().getString(R.string.unavailable));
                    }else {
                         storeservice.setText(getResources().getString(R.string.unavailable_ar));
                    }
                }

                if (productArrayList.get(storePos).getIsDelivery()) {
                    dcharge.setText("SAR " + priceFormat.format(productArrayList.get(storePos).getDeliveryCharges()));
                    if (productArrayList.get(storePos).getIsDeliveryCompany() == 0) {
                        if (language.equals("En")){
                            deliveryChargeTitle.setText(getResources().getString(R.string.shipping_charges));
                            deliverytime.setText(getResources().getString(R.string.unavailable));
                        } else {
                            deliveryChargeTitle.setText(getResources().getString(R.string.shipping_charges_ar));
                            deliverytime.setText(getResources().getString(R.string.unavailable_ar));
                        }
                    }
                    else {
                        deliverytime.setText(productArrayList.get(storePos).getDeliveryTime());
                    }

                } else if (!productArrayList.get(storePos).getIsDelivery()) {
                      if (language.equals("En")){
                          deliverytime.setText(getResources().getString(R.string.unavailable));
                    }else {
                          deliverytime.setText(getResources().getString(R.string.unavailable_ar));
                    }
                      if (language.equals("En")){
                          dcharge.setText(getResources().getString(R.string.unavailable));
                    }else {
                          dcharge.setText(getResources().getString(R.string.unavailable_ar));
                    }
                }
            } else if (type == 2) {
                if (productArrayList.get(storePos).getIsDelivery()) {
                    if (language.equals("En")){
                           pickip.setText(getResources().getString(R.string.available));
                    }else {
                          pickip.setText(getResources().getString(R.string.available_ar));
                    }

                } else if (!productArrayList.get(storePos).getIsDelivery()) {

                }

                if (productArrayList.get(storePos).getInStoreService().equalsIgnoreCase("true")) {
                     if (language.equals("En")){
                           storeservice.setText(getResources().getString(R.string.available));
                    }else {
                          storeservice.setText(getResources().getString(R.string.available_ar));
                    }

                } else if (productArrayList.get(storePos).getInStoreService().equalsIgnoreCase("false")) {
                    if (language.equals("En")){
                           storeservice.setText(getResources().getString(R.string.unavailable));
                    }else {
                         storeservice.setText(getResources().getString(R.string.unavailable_ar));
                    }

                }
                deliverytime.setText(productArrayList.get(storePos).getDeliveryTime());
                dcharge.setText("SAR " + priceFormat.format(productArrayList.get(storePos).getDeliveryCharges()));
            }

            if (!productArrayList.get(storePos).getFavoriteId()) {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
            } else {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
            }

            if (language.equals("En")){
                mainstore.setText(productArrayList.get(storePos).getStoreEn());
                branchtext.setText(productArrayList.get(storePos).getBranchEn());
                textview.setText(productArrayList.get(storePos).getDescriptionEn());
            }else {
                mainstore.setText(productArrayList.get(storePos).getStoreAr());
                branchtext.setText(productArrayList.get(storePos).getBranchAr());
                textview.setText(productArrayList.get(storePos).getDescriptionAr());
            }

            ratingBar.setRating(productArrayList.get(storePos).getReviews());

            DecimalFormat decimalFormat1 = new DecimalFormat("0");

//        storerating.setText("" + productArrayList.get(storePos).getRatings());
            distance.setText(distanceFormat.format(productArrayList.get(storePos).getDistance()) + " KM");
            if (productArrayList.get(storePos).getReviewsCount() == 0) {
                if (language.equals("En")){
                reviews.setText("(" + getResources().getString(R.string.no_reviews) + ")");
                }else {
                    reviews.setText("(" + getResources().getString(R.string.no_reviews_ar) + ")");
                }

            } else if (productArrayList.get(storePos).getReviewsCount() == 1) {
                if (language.equals("En")){
                reviews.setText("(" + decimalFormat1.format(productArrayList.get(storePos).getReviewsCount()) + " " + getResources().getString(R.string.review) + ")");
                }else {
                    reviews.setText("(" + decimalFormat1.format(productArrayList.get(storePos).getReviewsCount()) + " " + getResources().getString(R.string.review_ar) + ")");
                }

            } else {
                 if (language.equals("En")){
                reviews.setText("(" + decimalFormat1.format(productArrayList.get(storePos).getReviewsCount()) + " " + getResources().getString(R.string.reviews) + ")");
                }else {
                    reviews.setText("(" + decimalFormat1.format(productArrayList.get(storePos).getReviewsCount()) + " " + getResources().getString(R.string.review_ar) + ")");
                }

            }

            storestatus.setText(productArrayList.get(storePos).getBranchStatus());

            minimumorder.setText("SAR " + priceFormat.format(productArrayList.get(storePos).getMinimumOrderValue()));
            orderdone.setText("" + productArrayList.get(storePos).getOrderDone());
            storetime.setText("(" + productArrayList.get(storePos).getShift() + ")");

            if (productArrayList.get(storePos).getInStoreService().equals("false")) {
               if (language.equals("En")){
                storeservice.setText("" + getResources().getString(R.string.unavailable));
               }else {
                   storeservice.setText("" + getResources().getString(R.string.unavailable_ar));
               }

            } else {
                if (language.equals("En")){
                storeservice.setText("" + getResources().getString(R.string.available));
               }else {
                  storeservice.setText("" + getResources().getString(R.string.available_ar));
               }

            }

            if (productArrayList.get(storePos).getIsCashAllowed().equals("true") && (productArrayList.get(storePos).getIsCreditCardAllowed().equals("true"))) {
                card.setVisibility(View.VISIBLE);
                cash.setVisibility(View.VISIBLE);
            } else if (productArrayList.get(storePos).getIsCreditCardAllowed().equals("false")) {
                card.setVisibility(View.GONE);
            } else if (productArrayList.get(storePos).getIsCashAllowed().equals("false")) {
                card.setVisibility(View.GONE);
            }

            if (productArrayList.get(storePos).getBranchStatus().equalsIgnoreCase("open")) {
                storestatus.setTextColor(Color.parseColor("#7CD322"));
                storeStatusImage.setImageDrawable(getResources().getDrawable(R.drawable.status_green2x));
            } else {
                storestatus.setTextColor(Color.parseColor("#E74C3C"));
                storeStatusImage.setImageDrawable(getResources().getDrawable(R.drawable.status_red));
            }

            if (productArrayList.get(storePos).getVendorType() == 1) {
                singleicon.setImageDrawable(getResources().getDrawable(R.drawable.groupusersicon2x));
            }
            else {
                singleicon.setImageDrawable(getResources().getDrawable(R.drawable.singleicon2x));
            }

            String openDateStr = productArrayList.get(storePos).getBusinessSince();
            String[] dateSplit = openDateStr.split("-");
            businesssince.setText(dateSplit[0]);

            CamomileSpinner spinner = (CamomileSpinner) findViewById(R.id.spinner);
            spinner.start();

            spinner.recreateWithParams(
                    this,
                    DialogUtils.getColor(this, R.color.black),
                    120,
                    true
            );

            Glide.with(ProductStoresActivityStep1.this)
                    .load(Constants.STORE_IMAGE_URL + productArrayList.get(storePos).getLogo())
                    .into(storeimage);

            Glide.with(ProductStoresActivityStep1.this)
                    .load(STORE_IMAGE_URL + productArrayList.get(storePos).getBackground())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            spinner.setVisibility(View.GONE);
                            return false;
                        }
                    }).into(stroeback);

            if (type == 2) {
                Log.d(TAG, "type: " + type);
                if (language.equals("En")){
                    deliveryTimeTitle.setText(getResources().getString(R.string.min_booking_time));
                    deliveryChargeTitle.setText(getResources().getString(R.string.transportation_charges));
                    pickupTitle.setText(getResources().getString(R.string.home_service));
                }else {
                    deliveryTimeTitle.setText(getResources().getString(R.string.min_booking_time_ar));
                    deliveryChargeTitle.setText(getResources().getString(R.string.transportation_charges_ar));
                    pickupTitle.setText(getResources().getString(R.string.home_service_ar));
                }

            }
        } else {
            updateBranchColor(branch_pos);

            about_line.setVisibility(View.INVISIBLE);
            terms_line.setVisibility(View.INVISIBLE);
            reviews_line.setVisibility(View.INVISIBLE);

            aboutLayout.setVisibility(View.GONE);
            reviewsListView.setVisibility(View.GONE);

            final DecimalFormat distanceFormat = new DecimalFormat("##,##,###.#");
            final DecimalFormat priceFormat = new DecimalFormat("##,##,##0.00");

            if (type == 1) {
                if (branchresponcelist.get(branch_pos).getInStoreService()) {
                   if (language.equalsIgnoreCase("En")){
                    pickip.setText(getResources().getString(R.string.available));
                   }else {
                       pickip.setText(getResources().getString(R.string.available_ar));
                   }

                } else if (!branchresponcelist.get(branch_pos).getInStoreService()) {
                    if (language.equalsIgnoreCase("En")){
                    pickip.setText(getResources().getString(R.string.unavailable));
                    }else {
                         pickip.setText(getResources().getString(R.string.unavailable_ar));
                    }
                }

                if (branchresponcelist.get(branch_pos).getShippingProvider() == 0) {
                    if (language.equalsIgnoreCase("En")){
                    storeservice.setText(getResources().getString(R.string.available));
                    }else {
                        storeservice.setText(getResources().getString(R.string.available_ar));
                    }

                } else if (branchresponcelist.get(branch_pos).getShippingProvider() == 1) {
                    if (language.equalsIgnoreCase("En")){
                    storeservice.setText(getResources().getString(R.string.unavailable));
                    }else {
                        storeservice.setText(getResources().getString(R.string.unavailable_ar));
                    }
                }

                if (branchresponcelist.get(branch_pos).getIsDelivery()) {
                    deliverytime.setText(branchresponcelist.get(branch_pos).getDeliveryTime());
                    dcharge.setText("SAR " + priceFormat.format(branchresponcelist.get(branch_pos).getDeliveryCharges()));
                } else if (!branchresponcelist.get(branch_pos).getIsDelivery()) {
                    if (language.equalsIgnoreCase("En")){
                    deliverytime.setText(getResources().getString(R.string.unavailable));
                    dcharge.setText(getResources().getString(R.string.unavailable));
                    }else {
                        deliverytime.setText(getResources().getString(R.string.unavailable_ar));
                    dcharge.setText(getResources().getString(R.string.unavailable_ar));
                    }

                }
            } else if (type == 2) {
                if (branchresponcelist.get(branch_pos).getIsDelivery()) {
                    if (language.equals("En")){
                    pickip.setText(getResources().getString(R.string.available));
                    }else {
                        pickip.setText(getResources().getString(R.string.available_ar));
                    }

                } else if (!branchresponcelist.get(branch_pos).getIsDelivery()) {
                     if (language.equals("En")){
                    pickip.setText(getResources().getString(R.string.unavailable));
                    }else {
                        pickip.setText(getResources().getString(R.string.unavailable_ar));
                    }
                }

                if (branchresponcelist.get(branch_pos).getInStoreService()) {
                     if (language.equals("En")){
                    storeservice.setText(getResources().getString(R.string.available));
                    }else {
                         storeservice.setText(getResources().getString(R.string.available_ar));
                    }


                } else if (!branchresponcelist.get(branch_pos).getInStoreService()) {
                    if (language.equals("En")){
                    storeservice.setText(getResources().getString(R.string.unavailable));
                    }else {
                       storeservice.setText(getResources().getString(R.string.unavailable_ar));
                    }

                }
                deliverytime.setText(branchresponcelist.get(branch_pos).getDeliveryTime());
                dcharge.setText("SAR " + priceFormat.format(productArrayList.get(branch_pos).getDeliveryCharges()));
            }

            if (branchresponcelist.get(branch_pos).getFavorite()) {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
            } else {
                favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
            }

            if (language.equals("En")){
                mainstore.setText(branchresponcelist.get(branch_pos).getStoreEn());
                branchtext.setText(branchresponcelist.get(branch_pos).getBranchEn());
            }else {
                mainstore.setText(branchresponcelist.get(branch_pos).getStoreAr());
                branchtext.setText(branchresponcelist.get(branch_pos).getBranchAr());
            }


            ratingBar.setRating(branchresponcelist.get(branch_pos).getReviews());

            DecimalFormat decimalFormat1 = new DecimalFormat("0");

//        storerating.setText("" + branchresponcelist.get(branch_pos).getRatings());
            distance.setText(distanceFormat.format(branchresponcelist.get(branch_pos).getDistance()) + " KM");
            if (branchresponcelist.get(branch_pos).getReviews() == 0) {
                if (language.equals("En")){
                reviews.setText("(" + getResources().getString(R.string.no_reviews) + ")");
                }else {
                    reviews.setText("(" + getResources().getString(R.string.no_reviews_ar) + ")");
                }

            } else {
                if (language.equals("En")){
                reviews.setText("(" + decimalFormat1.format(branchresponcelist.get(branch_pos).getRatings()) + " " + getResources().getString(R.string.reviews) + ")");
                }else {
                    reviews.setText("(" + decimalFormat1.format(branchresponcelist.get(branch_pos).getRatings()) + " " + getResources().getString(R.string.review_ar) + ")");
                }

            }
            storestatus.setText(branchresponcelist.get(branch_pos).getBranchStatus());

            minimumorder.setText("SAR " + priceFormat.format(productArrayList.get(storePos).getMinimumOrderValue()));
            orderdone.setText("" + branchresponcelist.get(branch_pos).getOrderDone());
            storetime.setText("(" + branchresponcelist.get(branch_pos).getShift() + ")");

            if (!branchresponcelist.get(branch_pos).getInStoreService()) {
                if (language.equals("En")){
                    storeservice.setText("" + getResources().getString(R.string.unavailable));
                }else {
                    storeservice.setText("" + getResources().getString(R.string.unavailable_ar));
                }

            } else {
                if (language.equals("En")){
                    storeservice.setText("" + getResources().getString(R.string.available));
                }else {
                   storeservice.setText("" + getResources().getString(R.string.available_ar));
                }

            }

            if (branchresponcelist.get(branch_pos).getIsCashAllowed() && (branchresponcelist.get(branch_pos).getIsCreditCardAllowed())) {
                card.setVisibility(View.VISIBLE);
                cash.setVisibility(View.VISIBLE);
            } else if (!branchresponcelist.get(branch_pos).getIsCreditCardAllowed()) {
                card.setVisibility(View.GONE);
            } else if (!branchresponcelist.get(branch_pos).getIsCashAllowed()) {
                card.setVisibility(View.GONE);
            }

            if (branchresponcelist.get(branch_pos).getBranchStatus().equalsIgnoreCase("open")) {
                storestatus.setTextColor(Color.parseColor("#7CD322"));
                storeStatusImage.setImageDrawable(getResources().getDrawable(R.drawable.status_green2x));
            } else {
                storestatus.setTextColor(Color.parseColor("#E74C3C"));
                storeStatusImage.setImageDrawable(getResources().getDrawable(R.drawable.status_red));
            }

            if (branchresponcelist.get(branch_pos).getVendorType() == 1) {
                singleicon.setImageDrawable(getResources().getDrawable(R.drawable.groupusersicon2x));
            }
            else {
                singleicon.setImageDrawable(getResources().getDrawable(R.drawable.singleicon2x));
            }

            String openDateStr = branchresponcelist.get(branch_pos).getBusinessSince();
            String[] dateSplit = openDateStr.split("-");
            businesssince.setText(dateSplit[0]);

            Glide.with(ProductStoresActivityStep1.this)
                    .load(Constants.STORE_IMAGE_URL + branchresponcelist.get(branch_pos).getLogo())
                    .into(storeimage);

            Glide.with(ProductStoresActivityStep1.this)
                    .load(STORE_IMAGE_URL + branchresponcelist.get(branch_pos).getBackground())
                    .into(stroeback);

            if (type == 2) {
                Log.d(TAG, "type: " + type);

               if (language.equals("En")){
                   deliveryTimeTitle.setText(getResources().getString(R.string.min_booking_time));
                   deliveryChargeTitle.setText(getResources().getString(R.string.transportation_charges));
                pickupTitle.setText(getResources().getString(R.string.home_service));
               }else {
                   deliveryTimeTitle.setText(getResources().getString(R.string.min_booking_time_ar));
                   deliveryChargeTitle.setText(getResources().getString(R.string.transportation_charges_ar));
                   pickupTitle.setText(getResources().getString(R.string.home_service_ar));
               }

            }

        }
    }

    private void convertFeaturedproductArrayListToStores() {
        ProductlistResponce.Stores stores = new ProductlistResponce.Stores();
        stores.setShift(featuredstores.get(storePos).getShift());
        stores.setFavoriteId(featuredstores.get(storePos).getFavoriteid());
        stores.setVendorType(featuredstores.get(storePos).getVendortype());
        stores.setStoreType(featuredstores.get(storePos).getStoretype());
        stores.setLongitude(featuredstores.get(storePos).getLongitude());
        stores.setLatitude(featuredstores.get(storePos).getLatitude());
        stores.setWeekday(featuredstores.get(storePos).getWeekday());
        stores.setEndTime(featuredstores.get(storePos).getEndtime());
        stores.setStartTime(featuredstores.get(storePos).getStarttime());
        stores.setAddress(featuredstores.get(storePos).getAddress());
        stores.setLogo(featuredstores.get(storePos).getLogo());
        stores.setBackground(featuredstores.get(storePos).getBackground());
        stores.setMaroof(featuredstores.get(storePos).getMaroof());
        stores.setYoutube(featuredstores.get(storePos).getYoutube());
        stores.setSnapchat(featuredstores.get(storePos).getSnapchat());
        stores.setInstagram(featuredstores.get(storePos).getInstagram());
        stores.setLinkedIn(featuredstores.get(storePos).getLinkedin());
        stores.setTwitterHandle(featuredstores.get(storePos).getTwitterhandle());
        stores.setFacebook(featuredstores.get(storePos).getFacebook());
        stores.setBusinessSince(featuredstores.get(storePos).getBusinesssince());
        stores.setIsDelivery(featuredstores.get(storePos).getIsdelivery());
        stores.setInStoreService(featuredstores.get(storePos).getInstoreservice());
        stores.setShippingProvider(featuredstores.get(storePos).getShippingprovider());
        stores.setOrderDone(featuredstores.get(storePos).getOrderdone());
        stores.setBranchColor(featuredstores.get(storePos).getBranchcolor());
        stores.setDeliveryCharges(featuredstores.get(storePos).getDeliverycharges());
        stores.setIsCreditCardAllowed(featuredstores.get(storePos).getIscreditcardallowed());
        stores.setIsCashAllowed(featuredstores.get(storePos).getIscashallowed());
        stores.setDeliveryTime(featuredstores.get(storePos).getDeliverytime());
        stores.setRatings(featuredstores.get(storePos).getRatings());
        stores.setReviewsCount(featuredstores.get(storePos).getReviewscount());
        stores.setCategoryAr(featuredstores.get(storePos).getCategoryar());
        stores.setCategoryEn(featuredstores.get(storePos).getCategoryen());
        stores.setTermsAndConditionsAr(featuredstores.get(storePos).getTermsandconditionsar());
        stores.setTermsAndConditionsEn(featuredstores.get(storePos).getTermsandconditionsen());
        stores.setStoreImage(featuredstores.get(storePos).getStoreimage());
        stores.setBranchLogoImage(featuredstores.get(storePos).getBranchlogoimage());
        stores.setBackgroundImage(featuredstores.get(storePos).getBackgroundimage());
        stores.setMinimumOrderValue(featuredstores.get(storePos).getMinimumordervalue());
        stores.setStoreAr(featuredstores.get(storePos).getStorear());
        stores.setStoreEn(featuredstores.get(storePos).getStoreen());
        stores.setBranchAr(featuredstores.get(storePos).getBranchar());
        stores.setBranchEn(featuredstores.get(storePos).getBranchen());
        stores.setBranchId(featuredstores.get(storePos).getBranchid());
        stores.setStoreId(featuredstores.get(storePos).getStoreid());
        stores.setBranchStatus(featuredstores.get(storePos).getBranchstatus());
        stores.setWeekNo(featuredstores.get(storePos).getWeekno());
        stores.setCurrentDateTime(featuredstores.get(storePos).getCurrentdatetime());
        stores.setEndDateTime(featuredstores.get(storePos).getEnddatetime());
        stores.setStarDateTime(featuredstores.get(storePos).getStardatetime());
        stores.setDistance(featuredstores.get(storePos).getDistance());
        stores.setActualDate(featuredstores.get(storePos).getActualdate());
        productArrayList.add(stores);
        storePos = 0;
        initView();
    }

    private void convertBarcodeBranchDetailsToStores() {
        ProductlistResponce.Stores stores = new ProductlistResponce.Stores();
        stores.setShift(branchDetailsFromBarcode.get(0).getShift());
        stores.setFavoriteId(branchDetailsFromBarcode.get(0).getFavorite());
        stores.setVendorType(branchDetailsFromBarcode.get(0).getVendorType());
        stores.setStoreType(branchDetailsFromBarcode.get(0).getStoreType());
        stores.setLongitude(branchDetailsFromBarcode.get(0).getLongitude());
        stores.setLatitude(branchDetailsFromBarcode.get(0).getLatitude());
        stores.setWeekday(branchDetailsFromBarcode.get(0).getWeekday());
        stores.setEndTime(branchDetailsFromBarcode.get(0).getEndTime());
        stores.setStartTime(branchDetailsFromBarcode.get(0).getStartTime());
        stores.setAddress(branchDetailsFromBarcode.get(0).getAddress());
        stores.setLogo(branchDetailsFromBarcode.get(0).getLogo());
        stores.setBackground(branchDetailsFromBarcode.get(0).getBackground());
        stores.setMaroof(branchDetailsFromBarcode.get(0).getMaroof());
        stores.setYoutube(branchDetailsFromBarcode.get(0).getYoutube());
        stores.setSnapchat(branchDetailsFromBarcode.get(0).getSnapchat());
        stores.setInstagram(branchDetailsFromBarcode.get(0).getInstagram());
        stores.setLinkedIn(branchDetailsFromBarcode.get(0).getLinkedIn());
        stores.setTwitterHandle(branchDetailsFromBarcode.get(0).getTwitterHandle());
        stores.setFacebook(branchDetailsFromBarcode.get(0).getFacebook());
        stores.setBusinessSince(branchDetailsFromBarcode.get(0).getBusinessSince());
        stores.setIsDelivery(branchDetailsFromBarcode.get(0).getIsDelivery());
        stores.setInStoreService(branchDetailsFromBarcode.get(0).getInStoreService());
        stores.setShippingProvider(branchDetailsFromBarcode.get(0).getShippingProvider());
        stores.setOrderDone(branchDetailsFromBarcode.get(0).getOrderDone());
        stores.setBranchColor(branchDetailsFromBarcode.get(0).getBranchColor());
        stores.setDeliveryCharges(branchDetailsFromBarcode.get(0).getDeliveryCharges());
        stores.setIsCreditCardAllowed(branchDetailsFromBarcode.get(0).getIsCreditCardAllowed());
        stores.setIsCashAllowed(branchDetailsFromBarcode.get(0).getIsCashAllowed());
        stores.setDeliveryTime(branchDetailsFromBarcode.get(0).getDeliveryTime());
        stores.setRatings(branchDetailsFromBarcode.get(0).getRatings());
        stores.setReviewsCount(branchDetailsFromBarcode.get(0).getReviewsCount());
        stores.setCategoryAr(branchDetailsFromBarcode.get(0).getCategoryAr());
        stores.setCategoryEn(branchDetailsFromBarcode.get(0).getCategoryEn());
        stores.setTermsAndConditionsAr(branchDetailsFromBarcode.get(0).getTermsAndConditionsAr());
        stores.setTermsAndConditionsEn(branchDetailsFromBarcode.get(0).getTermsAndConditionsEn());
        stores.setStoreImage(branchDetailsFromBarcode.get(0).getStoreImage());
        stores.setBranchLogoImage(branchDetailsFromBarcode.get(0).getBranchLogoImage());
        stores.setBackgroundImage(branchDetailsFromBarcode.get(0).getBackgroundImage());
        stores.setMinimumOrderValue(branchDetailsFromBarcode.get(0).getMinimumOrderValue());
        stores.setStoreAr(branchDetailsFromBarcode.get(0).getStoreAr());
        stores.setStoreEn(branchDetailsFromBarcode.get(0).getStoreEn());
        stores.setBranchAr(branchDetailsFromBarcode.get(0).getBranchAr());
        stores.setBranchEn(branchDetailsFromBarcode.get(0).getBranchEn());
        stores.setBranchId(branchDetailsFromBarcode.get(0).getBranchId());
        stores.setStoreId(branchDetailsFromBarcode.get(0).getStoreId());
        stores.setBranchStatus(branchDetailsFromBarcode.get(0).getBranchStatus());
        stores.setWeekNo(branchDetailsFromBarcode.get(0).getWeekNo());
        stores.setCurrentDateTime(branchDetailsFromBarcode.get(0).getCurrentDateTime());
        stores.setEndDateTime(branchDetailsFromBarcode.get(0).getEndDateTime());
        stores.setStarDateTime(branchDetailsFromBarcode.get(0).getStarDateTime());
        stores.setDistance(branchDetailsFromBarcode.get(0).getDistance());
        stores.setActualDate(branchDetailsFromBarcode.get(0).getCurrentDateTime());
        productArrayList.add(stores);
        storePos = 0;
        initView();
    }

    private void convertFavouriteBranchDetailsToStores() {
        ProductlistResponce.Stores stores = new ProductlistResponce.Stores();
        stores.setShift(branchDetailsFromFavStores.get(0).getShift());
//        stores.setFavoriteId(branchDetailsFromBarcode.get(0).getFavorite());
        stores.setVendorType(branchDetailsFromFavStores.get(0).getVendorType());
        stores.setStoreType(branchDetailsFromFavStores.get(0).getStoreType());
        stores.setLongitude(branchDetailsFromFavStores.get(0).getLongitude());
        stores.setLatitude(branchDetailsFromFavStores.get(0).getLatitude());
        stores.setWeekday(branchDetailsFromFavStores.get(0).getWeekday());
        stores.setEndTime(branchDetailsFromFavStores.get(0).getEndTime());
        stores.setStartTime(branchDetailsFromFavStores.get(0).getStartTime());
        stores.setAddress(branchDetailsFromFavStores.get(0).getAddress());
        stores.setLogo(branchDetailsFromFavStores.get(0).getLogo());
        stores.setBackground(branchDetailsFromFavStores.get(0).getBackground());
        stores.setMaroof(branchDetailsFromFavStores.get(0).getMaroof());
        stores.setYoutube(branchDetailsFromFavStores.get(0).getYoutube());
        stores.setSnapchat(branchDetailsFromFavStores.get(0).getSnapchat());
        stores.setInstagram(branchDetailsFromFavStores.get(0).getInstagram());
        stores.setLinkedIn(branchDetailsFromFavStores.get(0).getLinkedIn());
        stores.setTwitterHandle(branchDetailsFromFavStores.get(0).getTwitterHandle());
        stores.setFacebook(branchDetailsFromFavStores.get(0).getFacebook());
        stores.setBusinessSince(branchDetailsFromFavStores.get(0).getBusinessSince());
        stores.setIsDelivery(branchDetailsFromFavStores.get(0).getIsDelivery());
        stores.setInStoreService(""+branchDetailsFromFavStores.get(0).getInStoreService());
        stores.setShippingProvider(branchDetailsFromFavStores.get(0).getShippingProvider());
        stores.setOrderDone(branchDetailsFromFavStores.get(0).getOrderDone());
        stores.setBranchColor(branchDetailsFromFavStores.get(0).getBranchColor());
        stores.setDeliveryCharges(branchDetailsFromFavStores.get(0).getDeliveryCharges());
        stores.setIsCreditCardAllowed(branchDetailsFromFavStores.get(0).getIsCreditCardAllowed());
        stores.setIsCashAllowed(branchDetailsFromFavStores.get(0).getIsCashAllowed());
        stores.setDeliveryTime(branchDetailsFromFavStores.get(0).getDeliveryTime());
        stores.setRatings(branchDetailsFromFavStores.get(0).getRatings());
        stores.setReviewsCount(branchDetailsFromFavStores.get(0).getReviewsCount());
        stores.setCategoryAr(branchDetailsFromFavStores.get(0).getCategoryAr());
        stores.setCategoryEn(branchDetailsFromFavStores.get(0).getCategoryEn());
        stores.setTermsAndConditionsAr(branchDetailsFromFavStores.get(0).getTermsAndConditionsAr());
        stores.setTermsAndConditionsEn(branchDetailsFromFavStores.get(0).getTermsAndConditionsEn());
        stores.setStoreImage(branchDetailsFromFavStores.get(0).getStoreImage());
        stores.setBranchLogoImage(branchDetailsFromFavStores.get(0).getBranchLogoImage());
        stores.setBackgroundImage(branchDetailsFromFavStores.get(0).getBackgroundImage());
        stores.setMinimumOrderValue(branchDetailsFromFavStores.get(0).getMinimumOrderValue());
        stores.setStoreAr(branchDetailsFromFavStores.get(0).getStoreAr());
        stores.setStoreEn(branchDetailsFromFavStores.get(0).getStoreEn());
        stores.setBranchAr(branchDetailsFromFavStores.get(0).getBranchAr());
        stores.setBranchEn(branchDetailsFromFavStores.get(0).getBranchEn());
        stores.setBranchId(branchDetailsFromFavStores.get(0).getBranchId());
        stores.setStoreId(branchDetailsFromFavStores.get(0).getStoreId());
        stores.setBranchStatus(branchDetailsFromFavStores.get(0).getBranchStatus());
        stores.setWeekNo(branchDetailsFromFavStores.get(0).getWeekNo());
        stores.setCurrentDateTime(branchDetailsFromFavStores.get(0).getCurrentDateTime());
        stores.setEndDateTime(branchDetailsFromFavStores.get(0).getEndDateTime());
        stores.setStarDateTime(branchDetailsFromFavStores.get(0).getStarDateTime());
        stores.setDistance(branchDetailsFromFavStores.get(0).getDistance());
        stores.setActualDate(branchDetailsFromFavStores.get(0).getCurrentDateTime());
        productArrayList.add(stores);
        storePos = 0;
        initView();
    }

    private void convertVendorAppBranchDetailsToStores() {
        ProductlistResponce.Stores stores = new ProductlistResponce.Stores();
        stores.setShift(branchDetailsFromVendorAppIntent.get(0).getShift());
        stores.setFavoriteId(branchDetailsFromVendorAppIntent.get(0).getFavoriteId());
        stores.setVendorType(branchDetailsFromVendorAppIntent.get(0).getVendorType());
        stores.setStoreType(branchDetailsFromVendorAppIntent.get(0).getStoreType());
        stores.setLongitude(branchDetailsFromVendorAppIntent.get(0).getLongitude());
        stores.setLatitude(branchDetailsFromVendorAppIntent.get(0).getLatitude());
        stores.setWeekday(branchDetailsFromVendorAppIntent.get(0).getWeekday());
        stores.setEndTime(branchDetailsFromVendorAppIntent.get(0).getEndTime());
        stores.setStartTime(branchDetailsFromVendorAppIntent.get(0).getStartTime());
        stores.setAddress(branchDetailsFromVendorAppIntent.get(0).getAddress());
        stores.setLogo(branchDetailsFromVendorAppIntent.get(0).getLogo());
        stores.setBackground(branchDetailsFromVendorAppIntent.get(0).getBackground());
        stores.setMaroof(branchDetailsFromVendorAppIntent.get(0).getMaroof());
        stores.setYoutube(branchDetailsFromVendorAppIntent.get(0).getYoutube());
        stores.setSnapchat(branchDetailsFromVendorAppIntent.get(0).getSnapchat());
        stores.setInstagram(branchDetailsFromVendorAppIntent.get(0).getInstagram());
        stores.setLinkedIn(branchDetailsFromVendorAppIntent.get(0).getLinkedIn());
        stores.setTwitterHandle(branchDetailsFromVendorAppIntent.get(0).getTwitterHandle());
        stores.setFacebook(branchDetailsFromVendorAppIntent.get(0).getFacebook());
        stores.setBusinessSince(branchDetailsFromVendorAppIntent.get(0).getBusinessSince());
        stores.setIsDelivery(branchDetailsFromVendorAppIntent.get(0).getIsDelivery());
        stores.setInStoreService(branchDetailsFromVendorAppIntent.get(0).getInStoreService());
        stores.setShippingProvider(branchDetailsFromVendorAppIntent.get(0).getShippingProvider());
        stores.setOrderDone(branchDetailsFromVendorAppIntent.get(0).getOrderDone());
        stores.setBranchColor(branchDetailsFromVendorAppIntent.get(0).getBranchColor());
        stores.setDeliveryCharges(branchDetailsFromVendorAppIntent.get(0).getDeliveryCharges());
        stores.setIsCreditCardAllowed(branchDetailsFromVendorAppIntent.get(0).getIsCreditCardAllowed());
        stores.setIsCashAllowed(branchDetailsFromVendorAppIntent.get(0).getIsCashAllowed());
        stores.setDeliveryTime(branchDetailsFromVendorAppIntent.get(0).getDeliveryTime());
        stores.setRatings(branchDetailsFromVendorAppIntent.get(0).getRatings());
        stores.setReviewsCount(branchDetailsFromVendorAppIntent.get(0).getReviewsCount());
        stores.setCategoryAr(branchDetailsFromVendorAppIntent.get(0).getCategoryAr());
        stores.setCategoryEn(branchDetailsFromVendorAppIntent.get(0).getCategoryEn());
        stores.setTermsAndConditionsAr(branchDetailsFromVendorAppIntent.get(0).getTermsAndConditionsAr());
        stores.setTermsAndConditionsEn(branchDetailsFromVendorAppIntent.get(0).getTermsAndConditionsEn());
        stores.setStoreImage(branchDetailsFromVendorAppIntent.get(0).getStoreImage());
        stores.setBranchLogoImage(branchDetailsFromVendorAppIntent.get(0).getBranchLogoImage());
        stores.setBackgroundImage(branchDetailsFromVendorAppIntent.get(0).getBackgroundImage());
        stores.setMinimumOrderValue(branchDetailsFromVendorAppIntent.get(0).getMinimumOrderValue());
        stores.setStoreAr(branchDetailsFromVendorAppIntent.get(0).getStoreAr());
        stores.setStoreEn(branchDetailsFromVendorAppIntent.get(0).getStoreEn());
        stores.setBranchAr(branchDetailsFromVendorAppIntent.get(0).getBranchAr());
        stores.setBranchEn(branchDetailsFromVendorAppIntent.get(0).getBranchEn());
        stores.setBranchId(branchDetailsFromVendorAppIntent.get(0).getBranchId());
        stores.setStoreId(branchDetailsFromVendorAppIntent.get(0).getStoreId());
        stores.setBranchStatus(branchDetailsFromVendorAppIntent.get(0).getBranchStatus());
        stores.setWeekNo(branchDetailsFromVendorAppIntent.get(0).getWeekNo());
        stores.setCurrentDateTime(branchDetailsFromVendorAppIntent.get(0).getCurrentDateTime());
        stores.setEndDateTime(branchDetailsFromVendorAppIntent.get(0).getEndDateTime());
        stores.setStarDateTime(branchDetailsFromVendorAppIntent.get(0).getStarDateTime());
        stores.setDistance(branchDetailsFromVendorAppIntent.get(0).getDistance());
        stores.setActualDate(branchDetailsFromVendorAppIntent.get(0).getCurrentDateTime());
        productArrayList.add(stores);
        storePos = 0;
        initView();
    }

    private class InsertStoreFav extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<com.cs.checkclickuser.Models.Response> call = apiService.NewAddGetDeleteUserFavorite(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<com.cs.checkclickuser.Models.Response>() {
                @Override
                public void onResponse(Call<com.cs.checkclickuser.Models.Response> call, Response<com.cs.checkclickuser.Models.Response> response) {
                    Log.i("TAG", "product servies responce " + response);
                    if (response.isSuccessful()) {
                        com.cs.checkclickuser.Models.Response stores = response.body();
                        if (stores.getStatus()) {
                            if (branch_pos == -1) {
                                if (!productArrayList.get(storePos).getFavoriteId()) {
                                    productArrayList.get(storePos).setFavoriteId(true);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                } else {
                                    productArrayList.get(storePos).setFavoriteId(false);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                                }
                            }
                            else {
                                if (!productArrayList.get(branch_pos).getFavoriteId()) {
                                    productArrayList.get(branch_pos).setFavoriteId(true);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                                } else {
                                    productArrayList.get(branch_pos).setFavoriteId(false);
                                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                                }
                            }

                            CacheData.productStoresArrayList = productArrayList;

                            if (language.equalsIgnoreCase("En")){
                            Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductStoresActivityStep1.this);
                            }else {
                                Constants.showOneButtonAlertDialog(stores.getMessage(), getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok_ar), ProductStoresActivityStep1.this);
                            }
                        }
                    } else {
                        Toast.makeText(ProductStoresActivityStep1.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<com.cs.checkclickuser.Models.Response> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductStoresActivityStep1.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            if (branch_pos == -1) {
                if (productArrayList.get(storePos).getFavoriteId()) {
                    parentObj.put("Type", IdConstants.TYPE_DELETE_FAV);
                } else {
                    parentObj.put("Type", IdConstants.TYPE_INSERT_FAV);
                }
            }
            else {
                if (productArrayList.get(branch_pos).getFavoriteId()) {
                    parentObj.put("Type", IdConstants.TYPE_DELETE_FAV);
                } else {
                    parentObj.put("Type", IdConstants.TYPE_INSERT_FAV);
                }
            }
            parentObj.put("StatusId", IdConstants.STATUS_ID_STORE);
            parentObj.put("Ids", productArrayList.get(storePos).getBranchId());
            parentObj.put("UserId", userPrefs.getString("userId", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareGetStoresJSON: " + parentObj.toString());
        return parentObj.toString();
    }

    private class getReviewsApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetReviewsJSON();
            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductStoresActivityStep1.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ProductStoreReviews> call = apiService.ProductStoreReviews(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ProductStoreReviews>() {
                @Override
                public void onResponse(Call<ProductStoreReviews> call, Response<ProductStoreReviews> response) {
                    if (response.isSuccessful()) {
                        ProductStoreReviews VerifyMobileResponse = response.body();
                        try {
                            if (VerifyMobileResponse.getStatus()) {
                                reviewsList = VerifyMobileResponse.getData();
                                reviewsAdapter = new ProductReviewsAdapter(ProductStoresActivityStep1.this, VerifyMobileResponse.getData());
                                reviewsListView.setAdapter(reviewsAdapter);
                                scrollView.smoothScrollTo(0, (reviewsListView.getTop() + 150));
                            } else {
                                String failureResponse = VerifyMobileResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ProductStoresActivityStep1.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ProductStoreReviews> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if(language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareGetReviewsJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            if (branch_pos == -1) {
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
                parentObj.put("Type", 1);
                parentObj.put("StatusId", 1);
            } else {
                parentObj.put("BranchId", branchresponcelist.get(branch_pos).getBranchId());
                parentObj.put("Type", 1);
                parentObj.put("StatusId", 1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (branch_pos != -1) {
            initView();
        }

        if (Constants.cartCount != 0) {
            cartStatusIcon.setVisibility(View.VISIBLE);
        }
    }

    private void openGmail(){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{productArrayList.get(storePos).getEmailId()});
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
            String className = null;
            for (final ResolveInfo info : matches) {
                if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                    className = info.activityInfo.name;

                    if(className != null && !className.isEmpty()){
                        break;
                    }
                }
            }
            i.setClassName("com.google.android.gm", className);
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                if (language.equals("En")){
                Toast.makeText(this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (language.equals("En")){
            Toast.makeText(this, getResources().getString(R.string.there_are_no_email_apps_installed), Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this, getResources().getString(R.string.there_are_no_email_apps_installed_ar), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class getBranchGroupIdApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareBranchGroupIdJSON();
//            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductStoresActivityStep1.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BranchGroupId> call = apiService.getBranchGroupId(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BranchGroupId>() {
                @Override
                public void onResponse(Call<BranchGroupId> call, Response<BranchGroupId> response) {
                    if (response.isSuccessful()) {
                        BranchGroupId BranchGroupIdResponse = response.body();
                        try {
                            if (BranchGroupIdResponse.getStatus()) {
                                // groupChatId already created
                                groupChatId = BranchGroupIdResponse.getData().getGroupChatId();
                                launchGroupChat();
                            } else {
                                // groupChatId doesn't exist
                                createGroupChat();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Constants.closeLoadingDialog();
                            if (language.equals("En")){
                               Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                       if (language.equals("En")){
                               Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        Constants.closeLoadingDialog();
                    }
                }

                @Override
                public void onFailure(Call<BranchGroupId> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                         if (language.equals("En")){
                              Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareBranchGroupIdJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            if (branch_pos == -1) {
                parentObj.put("StoreId", productArrayList.get(storePos).getStoreId());
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
            }
            else {
                parentObj.put("StoreId", productArrayList.get(storePos).getStoreId());
                parentObj.put("BranchId", productArrayList.get(storePos).getJOtherBranch().get(branch_pos).getBranchId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private void checkCometChatLoginStatus(){
        Constants.showLoadingDialog(ProductStoresActivityStep1.this);
//        AppSettings appSettings = new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(AppConfig.AppDetails.REGION).build();
//
//        CometChat.init(ProductStoresActivityStep1.this, StringContract.AppDetails.APP_ID, appSettings, new CometChat.CallbackListener<String>() {
//            @Override
//            public void onSuccess(String successMessage) {
//                Log.d(TAG, "Initialization completed successfully");
//            }
//            @Override
//            public void onError(CometChatException e) {
//                Log.d(TAG, "Initialization failed with exception: " + e.getMessage());
//            }
//        });
//
//        // checking cometchat login user
//        if(CometChat.getLoggedInUser() == null) {
//            if(!userPrefs.getString("chatUserId", "").equals("")) {
//                CometChat.login(userPrefs.getString("chatUserId", ""), StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
//                    @Override
//                    public void onSuccess(User user) {
//                        new getBranchGroupIdApi().execute();
//                    }
//
//                    @Override
//                    public void onError(CometChatException e) {
//                        e.printStackTrace();
//                    }
//
//                });
//            }
//            else {
//                if (language.equalsIgnoreCase("En")){
//                Toast.makeText(ProductStoresActivityStep1.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
//                }else {
//                    Toast.makeText(ProductStoresActivityStep1.this, getResources().getString(R.string.cannot_reach_server_ar), Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        }
//        else {
//            new getBranchGroupIdApi().execute();
//        }

        closeLoadingDialog();
    }

    private void createGroupChat(){
        String timeStamp = new SimpleDateFormat("yyyyMMMddHHmmssSSS").format(new Date());
        int pos = 0;
        if(branch_pos == -1) {
            pos = storePos;
        }
        else {
            pos = branch_pos;
        }

        String groupId = "CC" + userPrefs.getString("userId", "") + timeStamp +
                productArrayList.get(pos).getBranchId() +"android";

        String groupName = productArrayList.get(pos).getBranchEn().toUpperCase(Locale.ENGLISH);
        String icon = Constants.STORE_IMAGE_URL + productArrayList.get(pos).getBranchLogoImage();

//        Group group = new Group(groupId, groupName, CometChatConstants.GROUP_TYPE_PUBLIC.toLowerCase(), null, icon, "");
//
//        Log.d(TAG, "createGroupChat: "+group.toString());
//        CometChat.createGroup(group, new CometChat.CallbackListener<Group>() {
//            @Override
//            public void onSuccess(Group group) {
//
//                groupChatId = groupId;
//
//                new saveBranchGroupIdApi().execute();
//
//                Log.d("createGroup", "onSuccess: "+group);
//            }
//
//            @Override
//            public void onError(CometChatException e) {
//                Log.d("createGroup", "onError: "+e.getMessage());
//            }
//
//        });
    }

    private class saveBranchGroupIdApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareSaveBranchGroupIdJSON();
//            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductStoresActivityStep1.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BasicResponse> call = apiService.SaveGroupUserChatId(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (response.isSuccessful()) {
                        BasicResponse BranchGroupIdResponse = response.body();
                        try {
                            if (BranchGroupIdResponse.getStatus()) {
                                launchGroupChat();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Constants.closeLoadingDialog();
                            if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        Constants.closeLoadingDialog();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        Constants.closeLoadingDialog();
                        }

                    }
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equals("En")){
                        Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                             Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareSaveBranchGroupIdJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userPrefs.getString("userId", ""));
            if (branch_pos == -1) {
                parentObj.put("StoreId", productArrayList.get(storePos).getStoreId());
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
            }
            else {
                parentObj.put("StoreId", productArrayList.get(branch_pos).getStoreId());
                parentObj.put("BranchId", productArrayList.get(branch_pos).getBranchId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    private void launchGroupChat(){

//        CometChat.getGroup(groupChatId, new CometChat.CallbackListener<Group>() {
//            @Override
//            public void onSuccess(Group group) {
//                Constants.closeLoadingDialog();
//                CommonUtils.startActivityIntent(group, ProductStoresActivityStep1.this, true, null);
//            }
//
//            @Override
//            public void onError(CometChatException e) {
//                Log.d("createGroup", "onError: "+e.getMessage());
//            }
//        });
    }

    private void updateBranchColor(int position){
        BranchColor branchColor = new BranchColor();
        if (branch_pos == -1) {
            Constants.appColor = branchColor.BranchColor(productArrayList.get(storePos).getBranchColor());
        }
        else {
            Constants.appColor = branchColor.BranchColor(productArrayList.get(storePos).getJOtherBranch().get(branch_pos).getBranchColor());
        }

        int iclocationId = getResources().getIdentifier("locationicon_"+Constants.appColor, "drawable", getPackageName());
        icLocation.setImageDrawable(getResources().getDrawable(iclocationId));

        int icdropDonwId = getResources().getIdentifier("dropdown_"+Constants.appColor, "drawable", getPackageName());
        branchdown.setImageDrawable(getResources().getDrawable(icdropDonwId));

        int icClockId = getResources().getIdentifier("timeicon_"+Constants.appColor, "drawable", getPackageName());
        icClock.setImageDrawable(getResources().getDrawable(icClockId));

        int icDeliveryChargesId = getResources().getIdentifier("delivery_charge_"+Constants.appColor, "drawable", getPackageName());
        icDeliveryCharges.setImageDrawable(getResources().getDrawable(icDeliveryChargesId));

        int icMinOrderId = getResources().getIdentifier("min_order_"+Constants.appColor, "drawable", getPackageName());
        icMinOrder.setImageDrawable(getResources().getDrawable(icMinOrderId));

        int icDeliveryTimeId = getResources().getIdentifier("delivery_time_"+Constants.appColor, "drawable", getPackageName());
        icDeliveryTime.setImageDrawable(getResources().getDrawable(icDeliveryTimeId));

        int icPaymentId = getResources().getIdentifier("payment_"+Constants.appColor, "drawable", getPackageName());
        icPayment.setImageDrawable(getResources().getDrawable(icPaymentId));

        int icOrdersDoneId = getResources().getIdentifier("ordersdone_"+Constants.appColor, "drawable", getPackageName());
        icOrdersDone.setImageDrawable(getResources().getDrawable(icOrdersDoneId));

        int icBusinessSinceId = getResources().getIdentifier("business_since_"+Constants.appColor, "drawable", getPackageName());
        icBusinessSince.setImageDrawable(getResources().getDrawable(icBusinessSinceId));

        int icPickupId = getResources().getIdentifier("pickup_"+Constants.appColor, "drawable", getPackageName());
        icPickup.setImageDrawable(getResources().getDrawable(icPickupId));

        int icShippingChargesId = getResources().getIdentifier("shipping_"+Constants.appColor, "drawable", getPackageName());
        icShippingCharges.setImageDrawable(getResources().getDrawable(icShippingChargesId));

        int icPhoneId = getResources().getIdentifier("phone_"+Constants.appColor, "drawable", getPackageName());
        callImage.setImageDrawable(getResources().getDrawable(icPhoneId));

        int icEmailId = getResources().getIdentifier("mail_"+Constants.appColor, "drawable", getPackageName());
        emailImage.setImageDrawable(getResources().getDrawable(icEmailId));

        int circleId = getResources().getIdentifier("circle_"+Constants.appColor, "drawable", getPackageName());
        facebookLayout.setBackground(getResources().getDrawable(circleId));
        twitterLayout.setBackground(getResources().getDrawable(circleId));
        instaLaoyut.setBackground(getResources().getDrawable(circleId));
        maroofLayout.setBackground(getResources().getDrawable(circleId));

        about.setTextColor(Color.parseColor("#"+Constants.appColor));
        terms.setTextColor(Color.parseColor("#"+Constants.appColor));
        reviewsText.setTextColor(Color.parseColor("#"+Constants.appColor));

        about_line.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        terms_line.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        reviews_line.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
        bt_continue.setBackgroundColor(Color.parseColor("#"+Constants.appColor));
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(ProductStoresActivityStep1.this, perm));
    }

    private class GetEncrytptURLApi extends AsyncTask<String, Integer, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareURLEncryptJSON();
            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<UrlEncryptResponse> call = apiService.UrlEncrypt(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<UrlEncryptResponse>() {
                @Override
                public void onResponse(Call<UrlEncryptResponse> call, Response<UrlEncryptResponse> response) {
                    Log.d(TAG, "vonResponse: " + response);
                    if (response.isSuccessful()) {
                        if (response.body().getStatus() && response.body().getMessage().equals("Success")) {
                            encryptData = response.body().getData();
                            String imageName = "";
//                            if (branch_pos == -1) {
//                                imageName = productArrayList.get(storePos).getLogo();
//                            }
//                            else {
//                                imageName = productArrayList.get(branch_pos).getLogo();
//                            }
//                            if (canAccessStorage() && imageName != null &&
//                                    imageName.length() > 0) {
//                                new DownloadImage().execute();
//                            }
//                            else {
                                prepareEncryptURL();
//                            }
                            Constants.closeLoadingDialog();
                        } else {
                            //status false case
                            Constants.closeLoadingDialog();
                            String failureResponse = response.body().getMessage();
                            Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.app_name),
                                    getResources().getString(R.string.ok), ProductStoresActivityStep1.this);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UrlEncryptResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ProductStoresActivityStep1.this);
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(ProductStoresActivityStep1.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }
                    Constants.closeLoadingDialog();
                }
            });
            return "";
        }
    }

    private String prepareURLEncryptJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("Type", IdConstants.URL_SHARE_TYPE_BRANCH);
            if (branch_pos == -1) {
                parentObj.put("BranchId", productArrayList.get(storePos).getBranchId());
            }
            else {
                parentObj.put("BranchId", productArrayList.get(branch_pos).getBranchId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    private void prepareEncryptURL(){
        String storeName = productArrayList.get(storePos).getStoreEn();
        String branchName, branchAddress;
        if (branch_pos == -1) {
            if (language.equals("En")){
                branchName = productArrayList.get(storePos).getBranchEn();
            }else {
                branchName = productArrayList.get(storePos).getBranchAr();
            }

            branchAddress = productArrayList.get(storePos).getAddress();
        }
        else {
            if (language.equals("En")){
                branchName = productArrayList.get(branch_pos).getBranchEn();
            }else {
                branchName = productArrayList.get(branch_pos).getBranchAr();
            }

            branchAddress = productArrayList.get(branch_pos).getAddress();
        }
        String encyptedURL = Constants.UNIVERSAL_LINKS_URL +
                encryptData.getStoreName() + "/" + IdConstants.URL_SHARE_TYPE_BRANCH + "/" + encryptData.getEncryId();

        encyptedURL = encyptedURL.replace(" ", "-");
        String shareBody = storeName + "(" + branchName + "), " + branchAddress +" \n\n "+encyptedURL;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, storeName);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share using.."));
    }

    private class DownloadImage extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Constants.showLoadingDialog(ProductStoresActivityStep1.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String extStorageDirectory = Environment.getExternalStorageDirectory()
                        .toString();
                File folder = new File(extStorageDirectory, "CheckClik");
                folder.mkdir();
                file12 = new File(folder, "CheckClik_"+System.currentTimeMillis()+".png");
                try {
                    file12.createNewFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                FileOutputStream f = new FileOutputStream(file12);

                String imageName = "";
                if (branch_pos == -1) {
                    imageName = productArrayList.get(storePos).getLogo();
                }
                else {
                    imageName = productArrayList.get(branch_pos).getLogo();
                }

                URL u = new URL(Constants.STORE_IMAGE_URL+imageName);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
//                c.setDoOutput(true);
                c.connect();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                Log.d(TAG, "doInBackground: "+buffer.length);
                while ((len1 = in.read(buffer)) > 0) {
                    f.write(buffer, 0, len1);
                }
                f.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Constants.closeLoadingDialog();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (file12.exists()) {
                prepareEncryptURL();
                Constants.closeLoadingDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FAV_REQUEST && resultCode == RESULT_OK) {
            if (branch_pos == -1) {
                productArrayList.get(storePos).setFavoriteId(data.getBooleanExtra("fav", false));
                if (!productArrayList.get(storePos).getFavoriteId()) {
                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                } else {
                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                }
            } else {
                productArrayList.get(storePos).getJOtherBranch().get(branch_pos).setFavorite(data.getBooleanExtra("fav", false));
                branchresponcelist.get(branch_pos).setFavorite(data.getBooleanExtra("fav", false));
                if (!branchresponcelist.get(branch_pos).getFavorite()) {
                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.favwhite));
                } else {
                    favourite.setImageDrawable(getResources().getDrawable(R.drawable.fav_selected));
                }
            }
            CacheData.productStoresArrayList = productArrayList;
        }
    }
}

