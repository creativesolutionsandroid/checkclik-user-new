package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.OrderDetailsHistoryListAdapter;
import com.cs.checkclickuser.Adapter.OrderDetailsListAdapter;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.IdConstants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.STORE_IMAGE_URL;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView oredernumber, storename, placedon, seheduled, oredertotal, paymentinfo, deliveryadress,
            addressTitle, deliveryChargesTitle, vatTitle, total, deliverycharges, vat, subtotal, shipment_cod, discount, nettotal, orderType, deliveryType;
    ImageView storeimage, backbtn, phone;
    Button cancal_Btn;
    int storePos, productPos;
    boolean oerdertype;
    ScrollView scrollView;
    LinearLayout deliveryChargesLayout, shipmentCODLayout;

    String Language = "En";
    ListView iteamlist;
    public LayoutInflater inflater;
    public static final String TAG = "TAG";
    private ArrayList<OrderPendingResponce.Data> pendingArrayList = new ArrayList<>();
    public OrderDetailsListAdapter mlistadapter;

    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;
    SharedPreferences userPrefs, LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId, language;
    int branchid;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_details_activity);
        } else {
            setContentView(R.layout.order_details_activity_arabic);
        }

        this.inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();
        userId = userPrefs.getString("userId", null);

//        oredernumber = (TextView) findViewById(R.id.ordernumer);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        storename = (TextView) findViewById(R.id.storename);
        placedon = (TextView) findViewById(R.id.placeon);
        seheduled = (TextView) findViewById(R.id.seheduled);
        oredertotal = (TextView) findViewById(R.id.ordertotal);
        paymentinfo = (TextView) findViewById(R.id.paymenttype);
        deliveryadress = (TextView) findViewById(R.id.adress);
        addressTitle = (TextView) findViewById(R.id.deliverytext);
        deliveryChargesTitle = (TextView) findViewById(R.id.deliverychtext);
        vatTitle = (TextView) findViewById(R.id.Vattext);
        total = (TextView) findViewById(R.id.totalprice);
        deliverycharges = (TextView) findViewById(R.id.deliverycharges);
        vat = (TextView) findViewById(R.id.Vat);
        subtotal = (TextView) findViewById(R.id.subtotal);
        shipment_cod = (TextView) findViewById(R.id.shipment_cod);
        discount = (TextView) findViewById(R.id.discount);
        nettotal = (TextView) findViewById(R.id.nettotal);
        orderType = (TextView) findViewById(R.id.delivery_typetext);
        deliveryType = (TextView) findViewById(R.id.delivery_type);
        storeimage = (ImageView) findViewById(R.id.storeimage);
        iteamlist = (ListView) findViewById(R.id.iteamlist);
        deliveryChargesLayout = (LinearLayout) findViewById(R.id.delivery_charges_layout);
        shipmentCODLayout = (LinearLayout) findViewById(R.id.shipment_cod_layout);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        phone = (ImageView) findViewById(R.id.phone);
        cancal_Btn = (Button) findViewById(R.id.cancelbtn);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancal_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetcancelApi().execute();
            }
        });
//        cancal_Btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent =new Intent(OrderDetailsActivity.this,SearchActivity.class);
//            }
//        });

        Intent intent = getIntent();
        pendingArrayList = (ArrayList<OrderPendingResponce.Data>) getIntent().getSerializableExtra("arrylist");
        storePos = getIntent().getIntExtra("Orderpos", productPos);

        if (language.equalsIgnoreCase("En")) {
            storename.setText(pendingArrayList.get(storePos).getStoreNameEn());
            oredertotal.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getGrandTotal())
                    + " (" + pendingArrayList.get(storePos).getItemsCount() + " " + getResources().getString(R.string.items) + ")");

            if (pendingArrayList.get(storePos).getPaymentMode().equals("CC")) {
                paymentinfo.setText("" + getResources().getString(R.string.credit_card_str) + " (" + pendingArrayList.get(storePos).getPaymentType() + ")");
            } else {
                paymentinfo.setText("" + getResources().getString(R.string.cash_on_delivery));
            }

            if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_PICKUP) {
                deliveryType.setText(R.string.pickup);
                addressTitle.setText(R.string.pickup_address);
                deliveryChargesLayout.setVisibility(View.GONE);
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_DELIVERY) {
                deliveryType.setText(R.string.delivery);
                addressTitle.setText(R.string.delivery_address);
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_SHIPPING) {
                deliveryType.setText(R.string.shipping);
                addressTitle.setText(R.string.delivery_address);
                deliveryChargesTitle.setText("Shipment Charges");
                if (pendingArrayList.get(storePos).getPaymentMode().equalsIgnoreCase("COD")) {
                    shipmentCODLayout.setVisibility(View.VISIBLE);
                }
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.SERVICE_HOME) {
                deliveryType.setText(R.string.home);
                orderType.setText(R.string.service_option);
                addressTitle.setText(R.string.home_address);
                deliveryChargesTitle.setText("" + getResources().getString(R.string.transportation_charges));
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.SERVICE_STORE) {
                deliveryType.setText(R.string.store);
                orderType.setText(R.string.service_option);
                addressTitle.setText(R.string.store_service);
                deliveryChargesLayout.setVisibility(View.GONE);
            }
        } else {
            storename.setText(pendingArrayList.get(storePos).getStoreNameAr());
            oredertotal.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getGrandTotal())
                    + " (" + getResources().getString(R.string.items_ar) + " " + pendingArrayList.get(storePos).getItemsCount() + ")");

            if (pendingArrayList.get(storePos).getPaymentMode().equals("CC")) {
                paymentinfo.setText("" + "(" + pendingArrayList.get(storePos).getPaymentType() + ") " + getResources().getString(R.string.credit_card_str_ar));
            } else {
                paymentinfo.setText("" + getResources().getString(R.string.cash_on_delivery_ar));
            }

            if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_PICKUP) {
                deliveryType.setText(R.string.pickup_ar);
                addressTitle.setText(R.string.pickup_address_ar);
                deliveryChargesLayout.setVisibility(View.GONE);
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_DELIVERY) {
                deliveryType.setText(R.string.delivery_ar);
                addressTitle.setText(R.string.delivery_address_ar);
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_SHIPPING) {
                deliveryType.setText(R.string.shipping_ar);
                addressTitle.setText(R.string.delivery_address_ar);
                deliveryChargesTitle.setText(R.string.shipment_charges_ar);
                if (pendingArrayList.get(storePos).getPaymentMode().equalsIgnoreCase("COD")) {
                    shipmentCODLayout.setVisibility(View.VISIBLE);
                }
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.SERVICE_HOME) {
                deliveryType.setText(R.string.home_ar);
                orderType.setText(R.string.service_option_ar);
                addressTitle.setText(R.string.home_address_ar);
                deliveryChargesTitle.setText("" + getResources().getString(R.string.transportation_charges_ar));
            } else if (pendingArrayList.get(storePos).getOrderType() == IdConstants.SERVICE_STORE) {
                deliveryType.setText(R.string.store_ar);
                orderType.setText(R.string.service_option_ar);
                addressTitle.setText(R.string.store_service_ar);
                deliveryChargesLayout.setVisibility(View.GONE);
            }
        }

        Glide.with(OrderDetailsActivity.this)
                .load(STORE_IMAGE_URL + pendingArrayList.get(storePos).getBranchLogoImage())
                .into(storeimage);

        if (pendingArrayList.get(productPos).getOrderStatus() == 1 || pendingArrayList.get(productPos).getOrderStatus() == 2
                || pendingArrayList.get(productPos).getOrderStatus() == 5 || pendingArrayList.get(productPos).getOrderStatus() == 7
                || pendingArrayList.get(productPos).getOrderStatus() == 8 || pendingArrayList.get(productPos).getOrderStatus() == 9
                || pendingArrayList.get(productPos).getOrderStatus() == 10) {
            cancal_Btn.setVisibility(View.VISIBLE);
        } else {
//            if(pendingArrayList.get(productPos).getord)
            cancal_Btn.setVisibility(View.GONE);
        }

        deliveryadress.setText(pendingArrayList.get(storePos).getAddress());
        total.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getAcceptedTotal()));
        deliverycharges.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getDeliveryFee()));
        shipment_cod.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getShimentCODFee()));
        vat.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getAcceptedVAT()));

        subtotal.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getAcceptedTotal() -
                pendingArrayList.get(storePos).getDiscountAmount()));

        nettotal.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getAcceptedGrandTotal()));
        discount.setText("SAR " + Constants.priceFormat1.format(pendingArrayList.get(storePos).getDiscountAmount()));

        if (language.equalsIgnoreCase("En")) {
            vatTitle.setText("" + getResources().getString(R.string.tax_vat) + "(" + pendingArrayList.get(storePos).getVatPercentage() + "%)");
        } else {
            vatTitle.setText("" + "(%" + pendingArrayList.get(storePos).getVatPercentage() + ")" + getResources().getString(R.string.tax_vat_ar));
        }

        mlistadapter = new OrderDetailsListAdapter(OrderDetailsActivity.this, pendingArrayList.get(storePos).getItems(),
                pendingArrayList.get(storePos).getOrderStatus(), pendingArrayList.get(storePos).getOrderType(), pendingArrayList.get(storePos).getBranchId(), language);
        iteamlist.setAdapter(mlistadapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        }, 200);

        String date = pendingArrayList.get(storePos).getOrderDate();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        try {
            Date datetime = sdf.parse(date);
            date = sdf1.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        placedon.setText("" + date);

        String date1 = pendingArrayList.get(storePos).getExpectingDelivery();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-ddHH'T'mm:ss", Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        date1.replace(date1, "yyyy-MM-dd");
        try {
            Date datetime = format.parse(date1);
            date1 = sdf3.format(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (pendingArrayList.get(storePos).getOrderType() == IdConstants.PRODUCT_SHIPPING) {
            seheduled.setText("" + date1);
        } else {
            seheduled.setText("" + date1 + " " + pendingArrayList.get(storePos).getTimeSlot());
        }

        phone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: " + pendingArrayList.get(storePos).getMobile()));
                        if (ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                            return;
                        }
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderDetailsActivity.this, perm));
    }

    private class GetcancelApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(OrderDetailsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderDetailsActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<OrderCancelResponce> call = apiService.getcancl(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<OrderCancelResponce>() {
                @Override
                public void onResponse(Call<OrderCancelResponce> call, Response<OrderCancelResponce> response) {
                    Log.d(TAG, "ordercancel: " + response);
                    if (response.isSuccessful()) {
                        OrderCancelResponce SaveuserRatingResponce = response.body();
                        try {
                            if (SaveuserRatingResponce.getStatus()) {
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                //                          status false case
                                String failureResponse = SaveuserRatingResponce.getMessage();
                                if (language.equalsIgnoreCase("En")) {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), OrderDetailsActivity.this);
                                } else {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), OrderDetailsActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Log.d(TAG, "onResponse: " + response.errorBody());
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<OrderCancelResponce> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("UserId", userId);
            parentObj.put("Reason", "");
            parentObj.put("OrderId", pendingArrayList.get(storePos).getOrderId());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

}
