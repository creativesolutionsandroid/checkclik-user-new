package com.cs.checkclickuser.Activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.Constants;

public class OrderCompletedActivity extends AppCompatActivity {


    SharedPreferences LanguagePrefs;
    String language;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_order_completed);
        } else {
            setContentView(R.layout.activity_order_completed_ar);
        }


        TextView invoiceText = (TextView) findViewById(R.id.invoice_text);
        Button continueBtn = (Button) findViewById(R.id.continue_btn);
        View view = (View) findViewById(R.id.view);

        invoiceText.setText("Order Id:\n" + getIntent().getStringExtra("orderId"));

        invoiceText.setTextColor(Color.parseColor("#"+ Constants.appColor));
        continueBtn.setBackgroundColor(Color.parseColor("#"+ Constants.appColor));
        view.setBackgroundColor(Color.parseColor("#"+ Constants.appColor));

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderCompletedActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent(OrderCompletedActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}