package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cs.checkclickuser.Adapter.PendingOrderItemsList;
import com.cs.checkclickuser.Models.BasicResponse;
import com.cs.checkclickuser.Models.OrderCancelResponce;
import com.cs.checkclickuser.Models.OrderPendingResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderRetunActivity extends AppCompatActivity {

    TextView storename, iteamcount, qty, returnCountText, done, price;
    ImageView productimage, back_btn, increase, decrease, addimage, addimage1, addimage2, addimage3;
    RelativeLayout doneBtn;

    private ArrayList<OrderPendingResponce.Items> retunArrayList = new ArrayList<>();
    int productPos;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };
    String imageResponse;
    private static final int CAMERA_REQUEST = 4;
    private static final int STORAGE_REQUEST = 5;
    private static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    boolean isCamera = false;
    int QTY = 0;
    private byte[] image1Bytes;
    private byte[] image2Bytes;
    private byte[] image3Bytes;
    private byte[] image4Bytes;
    private String image1Str = "", image2Str = "", image3Str = "", image4Str = "", logoStr = "", vatStr = "", crStr = "";
    private boolean isImagesUploaded = false;
    String image1Name = "", image2Name, image3Name, image4Name;
    private int image1Id = 0, image2Id = 0, image3Id = 0, image4Id = 0;
    Uri imageUri;
    private static final int PICK_IMAGE_FROM_CAMERA = 3;

    int imageSelected = 0;
    Bitmap thumbnail1 = null, thumbnail2 = null, thumbnail3 = null, thumbnail4 = null;
    SharedPreferences userPrefs, LanguagePrefs;
    SharedPreferences.Editor userPrefsEditor;
    String userId, language;
    int returnCount = 1;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_retun_activity);
        } else {
            setContentView(R.layout.order_retun_activity_arabic);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", "");

        storename = (TextView) findViewById(R.id.storename);
        iteamcount = (TextView) findViewById(R.id.iteamcount);
        qty = (TextView) findViewById(R.id.products);
        returnCountText = (TextView) findViewById(R.id.qtycount);
        done = (TextView) findViewById(R.id.done);
        price = (TextView) findViewById(R.id.price);
        productimage = (ImageView) findViewById(R.id.storeimage);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        decrease = (ImageView) findViewById(R.id.decrease);
        increase = (ImageView) findViewById(R.id.increase);
        addimage = (ImageView) findViewById(R.id.addpic);
        addimage1 = (ImageView) findViewById(R.id.addpic1);
        addimage2 = (ImageView) findViewById(R.id.addpic2);
        addimage3 = (ImageView) findViewById(R.id.addpic3);
        doneBtn = (RelativeLayout) findViewById(R.id.done_btn);

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isImagesUploaded) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderRetunActivity.this, getResources().getString(R.string.please_upload_atleast), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(OrderRetunActivity.this, getResources().getString(R.string.please_upload_atleast_ar), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    new returnOrderApi().execute();
                }
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (returnCount < retunArrayList.get(productPos).getQty()) {
                    returnCount = returnCount + 1;
                    returnCountText.setText("" + returnCount);
                }
            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (returnCount > 1) {
                    returnCount = returnCount - 1;
                    returnCountText.setText("" + returnCount);
                }
            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelected = 1;
                openCamera();
            }
        });
        addimage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelected = 2;
                openCamera();
            }
        });
        addimage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelected = 3;
                openCamera();
            }
        });
        addimage3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelected = 4;
                openCamera();
            }
        });

        retunArrayList = (ArrayList<OrderPendingResponce.Items>) getIntent().getSerializableExtra("Order");
        productPos = getIntent().getIntExtra("Orderpos", 0);

        if (language.equalsIgnoreCase("En")) {
            storename.setText(retunArrayList.get(productPos).getNameEn());
            qty.setText("" + getResources().getString(R.string.qty_caps) + "  " + retunArrayList.get(productPos).getQty());
            iteamcount.setText("" + retunArrayList.get(productPos).getQty() + " " + getResources().getString(R.string.pics));
        } else {
            storename.setText(retunArrayList.get(productPos).getNameAr());
            qty.setText("" + retunArrayList.get(productPos).getQty() + "  " + getResources().getString(R.string.qty_caps_ar));
            iteamcount.setText("" + getResources().getString(R.string.pics_ar) + " " + retunArrayList.get(productPos).getQty());
        }
        price.setText("SAR " + Constants.priceFormat1.format(retunArrayList.get(productPos).getPrice()));

        Glide.with(this)
                .load(Constants.PRODUCTS_IMAGE_URL + retunArrayList.get(productPos).getImage())
                .into(productimage);

        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else if (!canAccessStorage()) {
                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
            }
        }
    }

    public void increaseInteger(View view) {
        QTY = QTY + 1;
        display(QTY);

    }

    public void decreaseInteger(View view) {
        QTY = QTY - 1;
        display(QTY);
    }

    private void display(int number) {
        TextView displayInteger = (TextView) findViewById(
                R.id.qtycount);
        displayInteger.setText("" + number);
    }

    private boolean canAccessStorage() {
        return (hasPermission1(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    private boolean canAccessCamera() {
        return (hasPermission1(Manifest.permission.CAMERA));
    }

    private boolean hasPermission1(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OrderRetunActivity.this, perm));
    }

    public void openCamera() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera = true;
                requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
            } else {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "New Picture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                imageUri = getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                startActivityForResult(intent, PICK_IMAGE_FROM_CAMERA);
            }
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            Log.d("TAG", "onActivityResult: ");
            convertCameraImages();
        }
    }

    private void convertCameraImages() {
        String path = getRealPathFromURI(imageUri);
        Log.d("TAG", "convertCameraImages: " + path);
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inScaled = false;

        if (imageSelected == 1) {
            isImagesUploaded = true;
            thumbnail1 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail1, 800,
                    800, true);
            thumbnail1 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail1);
            addimage.setImageDrawable(drawable);
            StoreByteImage(thumbnail1, 1);
        } else if (imageSelected == 2) {
            isImagesUploaded = true;
            thumbnail2 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail2, 800,
                    800, true);
            thumbnail2 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail2);
            addimage1.setImageDrawable(drawable);
            StoreByteImage(thumbnail2, 2);
        } else if (imageSelected == 3) {
            isImagesUploaded = true;
            thumbnail3 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail3, 800,
                    800, true);
            thumbnail3 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail3);
            addimage2.setImageDrawable(drawable);
            StoreByteImage(thumbnail3, 3);
        } else if (imageSelected == 4) {
            isImagesUploaded = true;
            thumbnail4 = BitmapFactory.decodeFile(path, bmOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(thumbnail4, 800,
                    800, true);
            thumbnail4 = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            Drawable drawable = new BitmapDrawable(getResources(), thumbnail4);
            addimage3.setImageDrawable(drawable);
            StoreByteImage(thumbnail4, 4);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void StoreByteImage(Bitmap bitmap, int num) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(new Date());
        String pictureName = "";
        pictureName = "IMG_" + retunArrayList.get(productPos).getOrderId() + "_" + timeStamp + ".jpg";

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        if (num == 1) {
            image1Name = pictureName;
            image1Bytes = stream.toByteArray();
            image1Str = Base64.encodeToString(image1Bytes, Base64.DEFAULT);
        } else if (num == 2) {
            image2Name = pictureName;
            image2Bytes = stream.toByteArray();
            image2Str = Base64.encodeToString(image2Bytes, Base64.DEFAULT);
        } else if (num == 3) {
            image3Name = pictureName;
            image3Bytes = stream.toByteArray();
            image3Str = Base64.encodeToString(image3Bytes, Base64.DEFAULT);
        } else if (num == 4) {
            image4Name = pictureName;
            image4Bytes = stream.toByteArray();
            image4Str = Base64.encodeToString(image4Bytes, Base64.DEFAULT);
        }

    }

    private class returnOrderApi extends AsyncTask<String, String, String> {

        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(OrderRetunActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            final String networkStatus = NetworkUtil.getConnectivityStatusString(OrderRetunActivity.this);
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<BasicResponse> call = apiService.returnRequest(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    Log.d("TAG", "ordercancel: " + response);
                    if (response.isSuccessful()) {
                        BasicResponse SaveuserRatingResponce = response.body();
                        try {
                            if (SaveuserRatingResponce.getStatus()) {
                                if (language.equalsIgnoreCase("En")) {
                                    Constants.showOneButtonAlertDialog(SaveuserRatingResponce.getMessage(), getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), OrderRetunActivity.this);
                                } else {
                                    Constants.showOneButtonAlertDialog(SaveuserRatingResponce.getMessage(), getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), OrderRetunActivity.this);
                                }
                                finish();
                            } else {
                                //                          status false case
                                String failureResponse = SaveuserRatingResponce.getMessage();
                                if (language.equalsIgnoreCase("En")) {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                            getResources().getString(R.string.ok), OrderRetunActivity.this);
                                } else {
                                    Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error_ar),
                                            getResources().getString(R.string.ok_ar), OrderRetunActivity.this);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (language.equalsIgnoreCase("En")) {
                                Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Log.d("TAG", "onResponse: " + response.errorBody());
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    Log.d("TAG", "onFailure: " + t.toString());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderRetunActivity.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderRetunActivity.this, R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (language.equalsIgnoreCase("En")) {
                            Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderRetunActivity.this, R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }
                    }
                    Constants.closeLoadingDialog();
                }
            });
            return null;
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();
        JSONArray itemsArray = new JSONArray();
        JSONObject itemsObj = new JSONObject();
        JSONArray imagesArray = new JSONArray();
        try {
            parentObj.put("UserId", userId);
            parentObj.put("Reason", "");
            parentObj.put("OrderId", retunArrayList.get(productPos).getOrderId());
            parentObj.put("FullName", userPrefs.getString("FirstName", ""));
            parentObj.put("AccountNo", "12345678910");
            parentObj.put("IbanNo", "IBAN123456");
            parentObj.put("BankName", "NCP");

            itemsObj.put("OrderItemId", retunArrayList.get(productPos).getOrderItemId());
            itemsObj.put("Qty", returnCount);

            if (!image1Str.equals("")) {
                JSONObject imagesObj = new JSONObject();
                imagesObj.put("FileName", image1Name);
                imagesObj.put("Base64FileData", image1Str);
                imagesArray.put(imagesObj);
            }

            if (!image2Str.equals("")) {
                JSONObject imagesObj = new JSONObject();
                imagesObj.put("FileName", image2Name);
                imagesObj.put("Base64FileData", image2Str);
                imagesArray.put(imagesObj);
            }

            if (!image3Str.equals("")) {
                JSONObject imagesObj = new JSONObject();
                imagesObj.put("FileName", image3Name);
                imagesObj.put("Base64FileData", image3Str);
                imagesArray.put(imagesObj);
            }

            if (!image4Str.equals("")) {
                JSONObject imagesObj = new JSONObject();
                imagesObj.put("FileName", image4Name);
                imagesObj.put("Base64FileData", image4Str);
                imagesArray.put(imagesObj);
            }

            itemsObj.put("Images", imagesArray);
            itemsArray.put(itemsObj);
            parentObj.put("jItems", itemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("TAG", "prepareBranchId: " + parentObj);
        return parentObj.toString();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case STORAGE_REQUEST:

                if (canAccessStorage()) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_FROM_GALLERY);
                } else {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(OrderRetunActivity.this, getResources().getString(R.string.storage_permission_denied), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(OrderRetunActivity.this, "تم رفض  التخزين ، غير قادر على تحديد الصور", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case CAMERA_REQUEST:
                if (!canAccessStorage()) {
                    requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                }
                break;
        }
    }
}
