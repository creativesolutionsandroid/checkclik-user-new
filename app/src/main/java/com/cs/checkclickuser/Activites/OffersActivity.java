package com.cs.checkclickuser.Activites;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.checkclickuser.Adapter.OffersCategoryAdapter;
import com.cs.checkclickuser.Adapter.RecommondedAdapter;
import com.cs.checkclickuser.Models.HomePageResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Utils.BarCodeScanner;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cs.checkclickuser.Utils.Constants.hideKeyboard;


public class OffersActivity extends AppCompatActivity {

    private RecyclerView listView;
    ImageView back_btn;
    private OffersCategoryAdapter offersCategoryAdapter;
    private RecommondedAdapter recommondedAdapter;
    private LinearLayout offersLayout, recommendedLayout;
    private View offersLine, recommendedLine;
    private EditText productSearch;

    private ArrayList<HomePageResponse.Subcategorylist> subcategorylist = new ArrayList<>();
    private ArrayList<HomePageResponse.Subcategorylist> subcategorylistFiltered = new ArrayList<>();
    private ArrayList<HomePageResponse.Featuredstores> featuredstores = new ArrayList<>();
    private ArrayList<HomePageResponse.Featuredstores> featuredstoresFiltered = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers> offers = new ArrayList<>();
    private ArrayList<HomePageResponse.Offers2> offers2 = new ArrayList<>();

    private boolean isOffersSelected;

    private ImageView scanner;
    private static final int CAMERA_REQUEST = 2;
    private static final String[] CAMERA_PERMS = {
            Manifest.permission.CAMERA
    };

    SharedPreferences LanguagePrefs;
    String language;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.activity_view_all);
        } else {
            setContentView(R.layout.activity_view_all_ar);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        subcategorylist = (ArrayList<HomePageResponse.Subcategorylist>) getIntent().getSerializableExtra("subcategorylist");
        featuredstores = (ArrayList<HomePageResponse.Featuredstores>) getIntent().getSerializableExtra("featuredstores");
        offers = (ArrayList<HomePageResponse.Offers>) getIntent().getSerializableExtra("offers");
        offers2 = (ArrayList<HomePageResponse.Offers2>) getIntent().getSerializableExtra("offers2");

        subcategorylistFiltered = (ArrayList<HomePageResponse.Subcategorylist>) subcategorylist.clone();
        featuredstoresFiltered = (ArrayList<HomePageResponse.Featuredstores>) featuredstores.clone();

        productSearch = (EditText) findViewById(R.id.product_search);
        listView = (RecyclerView) findViewById(R.id.product_listview);

        back_btn=(ImageView) findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        offersLayout = (LinearLayout) findViewById(R.id.offers_layout);
        recommendedLayout = (LinearLayout) findViewById(R.id.recommended_layout);

        offersLine = (View) findViewById(R.id.offers_line);
        recommendedLine = (View) findViewById(R.id.recommended_line);

        scanner=(ImageView) findViewById(R.id.cameraicon);

        scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessCamera()) {
                        requestPermissions(CAMERA_PERMS, CAMERA_REQUEST);
                    } else {
                        BarCodeScanner.showScannerDialog(OffersActivity.this);
                    }
                } else {
                    BarCodeScanner.showScannerDialog(OffersActivity.this);
                }
            }
        });

        if(getIntent().getIntExtra("selected", 1) == 1) {
            offersLine.setVisibility(View.VISIBLE);
            recommendedLine.setVisibility(View.INVISIBLE);
            isOffersSelected = true;

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(OffersActivity.this, 2);
            listView.setLayoutManager(mLayoutManager);
            offersCategoryAdapter = new OffersCategoryAdapter(OffersActivity.this, subcategorylistFiltered, offers, offers2);
            listView.setAdapter(offersCategoryAdapter);
        }
        else if(getIntent().getIntExtra("selected", 1) == 2) {
            offersLine.setVisibility(View.INVISIBLE);
            recommendedLine.setVisibility(View.VISIBLE);
            isOffersSelected = false;

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OffersActivity.this);
            listView.setLayoutManager(mLayoutManager);
            recommondedAdapter = new RecommondedAdapter(OffersActivity.this, featuredstoresFiltered);
            listView.setAdapter(recommondedAdapter);
        }

        offersLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offersLine.setVisibility(View.VISIBLE);
                recommendedLine.setVisibility(View.INVISIBLE);
                isOffersSelected = true;

                if(productSearch.getText().toString().length() > 0) {
                    String searchText = productSearch.getText().toString();
                    filterOffersSubCategory(searchText);
                }
                else {
                    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(OffersActivity.this, 2);
                    listView.setLayoutManager(mLayoutManager);
                    offersCategoryAdapter = new OffersCategoryAdapter(OffersActivity.this, subcategorylistFiltered, offers, offers2);
                    listView.setAdapter(offersCategoryAdapter);
                    offersCategoryAdapter.notifyDataSetChanged();
                }
            }
        });

        recommendedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                offersLine.setVisibility(View.INVISIBLE);
                recommendedLine.setVisibility(View.VISIBLE);
                isOffersSelected = false;

                if(productSearch.getText().toString().length() > 0) {
                    String searchText = productSearch.getText().toString();
                    filterRecommendedStores(searchText);
                }
                else {
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OffersActivity.this);
                    listView.setLayoutManager(mLayoutManager);
                    recommondedAdapter = new RecommondedAdapter(OffersActivity.this, featuredstoresFiltered);
                    listView.setAdapter(recommondedAdapter);
                    recommondedAdapter.notifyDataSetChanged();
                }
            }
        });

        productSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().length() > 0) {
                    String searchText = editable.toString();
                    if(isOffersSelected) {
                        filterOffersSubCategory(searchText);
                    }
                    else {
                        filterRecommendedStores(searchText);
                    }
                }
                else {
                    subcategorylistFiltered = (ArrayList<HomePageResponse.Subcategorylist>) subcategorylist.clone();
                    featuredstoresFiltered = (ArrayList<HomePageResponse.Featuredstores>) featuredstores.clone();
                    if(isOffersSelected) {
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(OffersActivity.this, 2);
                        listView.setLayoutManager(mLayoutManager);
                        offersCategoryAdapter = new OffersCategoryAdapter(OffersActivity.this, subcategorylistFiltered, offers, offers2);
                        listView.setAdapter(offersCategoryAdapter);
                        offersCategoryAdapter.notifyDataSetChanged();
                    }
                    else {
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OffersActivity.this);
                        listView.setLayoutManager(mLayoutManager);
                        recommondedAdapter = new RecommondedAdapter(OffersActivity.this, featuredstoresFiltered);
                        listView.setAdapter(recommondedAdapter);
                        recommondedAdapter.notifyDataSetChanged();
                    }
                }
            }
        });

        productSearch.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        productSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard(OffersActivity.this);

                    return true;
                }
                return false;
            }
        });
    }

    private void filterOffersSubCategory(String searchText){
        subcategorylistFiltered.clear();
        for (int i = 0; i < subcategorylist.size(); i++) {
            if(subcategorylist.get(i).getNameEn().toLowerCase().contains(searchText.toLowerCase())
            || subcategorylist.get(i).getNameAr().contains(searchText)) {
                HomePageResponse.Subcategorylist subcategory = new HomePageResponse.Subcategorylist();
                subcategory.setId(subcategorylist.get(i).getId());
                subcategory.setUniCode(subcategorylist.get(i).getUniCode());
                subcategory.setNameEn(subcategorylist.get(i).getNameEn());
                subcategory.setNameAr(subcategorylist.get(i).getNameAr());
                subcategory.setColorCode(subcategorylist.get(i).getColorCode());
                subcategorylistFiltered.add(subcategory);
            }

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(OffersActivity.this, 2);
            listView.setLayoutManager(mLayoutManager);
            offersCategoryAdapter = new OffersCategoryAdapter(OffersActivity.this, subcategorylistFiltered, offers, offers2);
            listView.setAdapter(offersCategoryAdapter);
            offersCategoryAdapter.notifyDataSetChanged();
        }

    }

    private void filterRecommendedStores(String searchText){
        featuredstoresFiltered.clear();
        for (int i = 0; i < featuredstores.size(); i++) {
            if(featuredstores.get(i).getStoreen().toLowerCase().contains(searchText.toLowerCase())
            || featuredstores.get(i).getStorear().contains(searchText)) {
                HomePageResponse.Featuredstores featuredstoresList = new HomePageResponse.Featuredstores();
                featuredstoresList.setActualdate(featuredstores.get(i).getActualdate());
                featuredstoresList.setAddress(featuredstores.get(i).getAddress());
                featuredstoresList.setBackground(featuredstores.get(i).getBackground());
                featuredstoresList.setBackgroundimage(featuredstores.get(i).getBackgroundimage());
                featuredstoresList.setBranchar(featuredstores.get(i).getBranchar());
                featuredstoresList.setBranchcolor(featuredstores.get(i).getBranchcolor());
                featuredstoresList.setBranchen(featuredstores.get(i).getBranchen());
                featuredstoresList.setBranchid(featuredstores.get(i).getBranchid());
                featuredstoresList.setBranchlogoimage(featuredstores.get(i).getBranchlogoimage());
                featuredstoresList.setBranchstatus(featuredstores.get(i).getBranchstatus());
                featuredstoresList.setBusinesssince(featuredstores.get(i).getBusinesssince());
                featuredstoresList.setCategoryar(featuredstores.get(i).getCategoryar());
                featuredstoresList.setCategoryen(featuredstores.get(i).getCategoryen());
                featuredstoresList.setCurrentdatetime(featuredstores.get(i).getCurrentdatetime());
                featuredstoresList.setDeliverycharges(featuredstores.get(i).getDeliverycharges());
                featuredstoresList.setDeliverytime(featuredstores.get(i).getDeliverytime());
                featuredstoresList.setDistance(featuredstores.get(i).getDistance());
                featuredstoresList.setEnddatetime(featuredstores.get(i).getEnddatetime());
                featuredstoresList.setEndtime(featuredstores.get(i).getEndtime());
                featuredstoresList.setFacebook(featuredstores.get(i).getFacebook());
                featuredstoresList.setFavoriteid(featuredstores.get(i).getFavoriteid());
                featuredstoresList.setInstagram(featuredstores.get(i).getInstagram());
                featuredstoresList.setInstoreservice(featuredstores.get(i).getInstoreservice());
                featuredstoresList.setIscashallowed(featuredstores.get(i).getIscashallowed());
                featuredstoresList.setIscreditcardallowed(featuredstores.get(i).getIscreditcardallowed());
                featuredstoresList.setLatitude(featuredstores.get(i).getLatitude());
                featuredstoresList.setIsdelivery(featuredstores.get(i).getIsdelivery());
                featuredstoresList.setLinkedin(featuredstores.get(i).getLinkedin());
                featuredstoresList.setLogo(featuredstores.get(i).getLogo());
                featuredstoresList.setLongitude(featuredstores.get(i).getLongitude());
                featuredstoresList.setMaroof(featuredstores.get(i).getMaroof());
                featuredstoresList.setMinimumordervalue(featuredstores.get(i).getMinimumordervalue());
                featuredstoresList.setOrderdone(featuredstores.get(i).getOrderdone());
                featuredstoresList.setRatings(featuredstores.get(i).getRatings());
                featuredstoresList.setReviews(featuredstores.get(i).getReviews());
                featuredstoresList.setReviewscount(featuredstores.get(i).getReviewscount());
                featuredstoresList.setShift(featuredstores.get(i).getShift());
                featuredstoresList.setShippingprovider(featuredstores.get(i).getShippingprovider());
                featuredstoresList.setSnapchat(featuredstores.get(i).getSnapchat());
                featuredstoresList.setStardatetime(featuredstores.get(i).getStardatetime());
                featuredstoresList.setStarttime(featuredstores.get(i).getStarttime());
                featuredstoresList.setStorear(featuredstores.get(i).getStorear());
                featuredstoresList.setStoreen(featuredstores.get(i).getStoreen());
                featuredstoresList.setStoreid(featuredstores.get(i).getStoreid());
                featuredstoresList.setStoreimage(featuredstores.get(i).getStoreimage());
                featuredstoresList.setStoretype(featuredstores.get(i).getStoretype());
                featuredstoresList.setTermsandconditionsar(featuredstores.get(i).getTermsandconditionsar());
                featuredstoresList.setTermsandconditionsen(featuredstores.get(i).getTermsandconditionsen());
                featuredstoresList.setTwitterhandle(featuredstores.get(i).getTwitterhandle());
                featuredstoresList.setVendortype(featuredstores.get(i).getVendortype());
                featuredstoresList.setWeekday(featuredstores.get(i).getWeekday());
                featuredstoresList.setWeekno(featuredstores.get(i).getWeekno());
                featuredstoresList.setYoutube(featuredstores.get(i).getYoutube());
                featuredstoresFiltered.add(featuredstoresList);
            }

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(OffersActivity.this);
            listView.setLayoutManager(mLayoutManager);
            recommondedAdapter = new RecommondedAdapter(OffersActivity.this, featuredstoresFiltered);
            listView.setAdapter(recommondedAdapter);
            recommondedAdapter.notifyDataSetChanged();
        }
    }

    private boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(OffersActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case CAMERA_REQUEST:
                if(canAccessCamera()) {
                    BarCodeScanner.showScannerDialog(OffersActivity.this);
                }
                else {
                    if (language.equalsIgnoreCase("En")){
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(this, getResources().getString(R.string.camera_permission_denied_Ar), Toast.LENGTH_LONG).show();
                    }

                }
                break;
        }
    }
}
