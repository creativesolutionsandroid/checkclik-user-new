package com.cs.checkclickuser;

import android.app.Application;
import androidx.annotation.NonNull;
import android.util.Log;

//import com.cometchat.pro.core.AppSettings;
//import com.cometchat.pro.core.CometChat;
//import com.cometchat.pro.exceptions.CometChatException;
import com.cs.checkclickuser.Contracts.StringContract;
import com.cs.checkclickuser.Utils.AppConfig;
import com.cs.checkclickuser.Utils.FakeCrashLibrary;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import static com.cs.checkclickuser.Adapter.ServiceItemGridAdapter.TAG;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/GothamBook.ttf")
                .setFontAttrId(R.attr.fontPath)
                .disableCustomViewInflation()
                .build()
        );

//        AppSettings appSettings = new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(AppConfig.AppDetails.REGION).build();
//
//        CometChat.init(this, StringContract.AppDetails.APP_ID, appSettings, new CometChat.CallbackListener<String>() {
//
//            @Override
//            public void onSuccess(String s) {
////                Toast.makeText(MyApplication.this, "SetUp Complete", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(CometChatException e) {
//                Log.d(TAG, "onError: "+e.getMessage());
//            }
//
//        });
    }
}
