package com.cs.checkclickuser.Listeners;

public interface UpdateCartListener {

    void onCartUpdated();

}
