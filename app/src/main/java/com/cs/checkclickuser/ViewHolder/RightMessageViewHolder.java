package com.cs.checkclickuser.ViewHolder;


import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.cs.checkclickuser.CustomView.CircleImageView;
import com.cs.checkclickuser.R;


public class RightMessageViewHolder extends RecyclerView.ViewHolder{
    public TextView textMessage;
    public TextView messageTimeStamp;
    public CircleImageView messageStatus;

    public RightMessageViewHolder(View itemView) {
        super(itemView);
        textMessage = itemView.findViewById(R.id.textViewMessage);
        messageStatus = itemView.findViewById(R.id.img_message_status);
        messageTimeStamp = itemView.findViewById(R.id.timestamp);
    }

}
