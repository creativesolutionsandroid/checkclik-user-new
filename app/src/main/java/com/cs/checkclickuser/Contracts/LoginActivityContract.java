package com.cs.checkclickuser.Contracts;


import com.cs.checkclickuser.Base.BasePresenter;

public interface LoginActivityContract {

    interface LoginActivityView {

        void startCometChatActivity();
    }

    interface LoginActivityPresenter extends BasePresenter<LoginActivityView> {

        void Login(String uid);

        void loginCheck();
    }
}
