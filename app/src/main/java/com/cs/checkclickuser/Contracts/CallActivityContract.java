package com.cs.checkclickuser.Contracts;

import android.content.Context;

import com.cs.checkclickuser.Base.BasePresenter;


public interface CallActivityContract {

    interface CallActivityView{

    }

    interface CallActivityPresenter extends BasePresenter<CallActivityView> {

        void removeCallListener(String listener);

        void addCallListener(Context context, String listener);

    }
}
