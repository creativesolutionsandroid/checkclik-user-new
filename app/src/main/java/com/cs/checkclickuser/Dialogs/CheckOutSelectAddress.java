package com.cs.checkclickuser.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.cs.checkclickuser.Adapter.ApplyCouponAdapter;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cs.checkclickuser.Activites.AddAdressActivity;
import com.cs.checkclickuser.Adapter.CheckoutAdressAdapter;
import com.cs.checkclickuser.Models.ManageAdressResponce;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutSelectAddress extends BottomSheetDialogFragment {

    View rootView;
    RecyclerView adresslistview;
    private ArrayList<ManageAdressResponce.Data> addressArrayList = new ArrayList<>();
    private CheckoutAdressAdapter mAddressAdapter;
    SharedPreferences userPrefs;
    String userId;
    RelativeLayout addnew;

    SharedPreferences LanguagePrefs;
    String language;

    public static CheckOutSelectAddress newInstance() {
        return new CheckOutSelectAddress();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        LanguagePrefs = getActivity().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            rootView = inflater.inflate(R.layout.checkout_select_address, container, false);
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        } else {
            rootView = inflater.inflate(R.layout.checkout_select_address_ar, container, false);
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        }

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        addnew = (RelativeLayout) rootView.findViewById(R.id.addnew);
        adresslistview = (RecyclerView) rootView.findViewById(R.id.adresslist);

        addnew.setBackgroundColor(Color.parseColor("#"+Constants.appColor));

        addnew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddAdressActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    private class GetstoreApi extends AsyncTask<String, Integer, String> {
        String inputStr;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareGetStoresJSON();
            Constants.showLoadingDialog(getActivity());

        }

        @Override
        protected String doInBackground(final String... strings) {
            APIInterface apiService =
                    ApiClient.getClient().create(APIInterface.class);

            Call<ManageAdressResponce> call = apiService.getaddress(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ManageAdressResponce>() {
                @Override
                public void onResponse(Call<ManageAdressResponce> call, Response<ManageAdressResponce> response) {
                    if (response.isSuccessful()) {
                        ManageAdressResponce stores = response.body();

                        if (stores.getStatus()) {
                            addressArrayList = stores.getData();
                        }
                    }

                    if (addressArrayList != null) {
                        mAddressAdapter = new CheckoutAdressAdapter(getContext(),
                                getArguments().getInt("id", 0), addressArrayList, true, getActivity() ,
                                getArguments().getString("country", ""));
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                        adresslistview.setLayoutManager(new GridLayoutManager(getContext(), 1));
                        adresslistview.setAdapter(mAddressAdapter);
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ManageAdressResponce> call, Throwable t) {
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(getContext());
                    if (networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(getContext(), R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getContext(), R.string.str_connection_error_ar, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (language.equalsIgnoreCase("En")){
                            Toast.makeText(getContext(), R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getContext(), R.string.cannot_reach_server_ar, Toast.LENGTH_SHORT).show();
                        }

                    }

                    Constants.closeLoadingDialog();

                }
            });
            return "";
        }
    }

    private String prepareGetStoresJSON() {
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("FlagId", 1);
            parentObj.put("UserId", userId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return parentObj.toString();
    }

    @Override
    public void onResume() {
        super.onResume();
        new GetstoreApi().execute();
    }
}
