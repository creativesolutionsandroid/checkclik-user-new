package com.cs.checkclickuser.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.cs.checkclickuser.Activites.SignInActivity;
import com.cs.checkclickuser.Models.ResetPasswordResponse;
import com.cs.checkclickuser.R;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;
import com.cs.checkclickuser.Utils.Constants;
import com.cs.checkclickuser.Utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ResetPasswordDialog extends AppCompatActivity implements View.OnClickListener {

    private int currentStage = 1;
    TextInputLayout inputLayoutphone, inputLayoutConfirmPassword,inputLayoutnewPassword;
    EditText  inputPassword,inputConfirmPassword;
    String strPassword, strConfirmPassword, otp;
    Button buttonSend;
    String strMobile;
    View rootView;
    ImageView backbtn;
    SharedPreferences LanguagePrefs;
    String language;

    private static String TAG = "TAG";
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefsEditor;
    public static ResetPasswordDialog newInstance() {
        return new ResetPasswordDialog();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguagePrefs = getApplication().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = LanguagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.dialog_reset_password);
        } else {
            setContentView(R.layout.dialog_reset_password);

        }



        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        strMobile = getIntent().getStringExtra("mobile");
        otp = getIntent().getStringExtra("otp");

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefsEditor = userPrefs.edit();

        inputPassword= (EditText) findViewById(R.id.reset_input_password);
        inputConfirmPassword= (EditText) findViewById(R.id.reset_input_retype_password);
        backbtn = (ImageView) findViewById(R.id.back_btn);
        buttonSend = (Button) findViewById(R.id.reset_submit_button);
        inputConfirmPassword.addTextChangedListener(new TextWatcher(inputConfirmPassword));

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

        buttonSend.setOnClickListener(this);

    }

//    @Override
//    public void onActivityCreated(Bundle arg0) {
//        super.onActivityCreated(arg0);
//        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_btn:
                if(currentStage == 1){
//                    finish();
                }

            case R.id.reset_submit_button:
                if(validations()) {
                    String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordDialog.this);
                    if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        new ResetPasswordApi().execute();
                    }
                    else{
                        Toast.makeText(ResetPasswordDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private boolean validations(){
        strPassword = inputPassword.getText().toString();
        strConfirmPassword = inputConfirmPassword.getText().toString();

        if (strPassword.length() == 0){

            inputPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        else if (strPassword.length() < 4 || strPassword.length() > 20){
            inputPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (strConfirmPassword.length() == 0){
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_enter_password));
            return false;
        }
        else if (strConfirmPassword.length() < 4 || strConfirmPassword.length() > 20){
            inputConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
            return false;
        }
        else if (!strPassword.equals(strConfirmPassword)){
            inputConfirmPassword.setError(getResources().getString(R.string.reset_alert_passwords_not_match));
            return false;
        }
        return true;
    }

    private void clearErrors(){
        inputLayoutphone.setErrorEnabled(false);
        inputLayoutphone.setErrorEnabled(false);
    }

    private class TextWatcher implements android.text.TextWatcher {
        private View view;

        private TextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutnewPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
                case R.id.input_confirm_password:
                    clearErrors();
                    if(editable.length() > 20){
                        inputLayoutConfirmPassword.setError(getResources().getString(R.string.signup_msg_invalid_password));
                    }
                    break;
            }
        }
    }

    private String prepareResetPasswordJson(){
        JSONObject parentObj = new JSONObject();

        try {
            parentObj.put("MobileNo", strMobile);
            parentObj.put("Password",strPassword);
            parentObj.put("OTPCode", otp);
            parentObj.put("FlagId", 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "prepareResetPasswordJson: "+parentObj.toString());
        return parentObj.toString();
    }

    private class ResetPasswordApi extends AsyncTask<String, Integer, String> {

        String inputStr;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inputStr = prepareResetPasswordJson();
            Constants.showLoadingDialog(ResetPasswordDialog.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            APIInterface apiService = ApiClient.getClient().create(APIInterface.class);

            Call<ResetPasswordResponse> call = apiService.getrestPass(
                    RequestBody.create(MediaType.parse("application/json"), inputStr));
            call.enqueue(new Callback<ResetPasswordResponse>() {
                @Override
                public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                    Log.d(TAG, "resetonResponse: "+response);
                    if(response.isSuccessful()){
                        ResetPasswordResponse resetPasswordResponse = response.body();
                        try {
                            if(resetPasswordResponse.getStatus()){
//                                status true case
                                String userId = resetPasswordResponse.getData().getMobileNo();
                                userPrefsEditor.putString("userId", userId);
                                userPrefsEditor.putString("MobileNo", resetPasswordResponse.getData().getMobileNo());
                                userPrefsEditor.commit();
                                Toast.makeText(ResetPasswordDialog.this, R.string.reset_success_msg, Toast.LENGTH_SHORT).show();
//                                ForgotPasswordActivity.isResetSuccessful = true;
                                Intent intent = new Intent(ResetPasswordDialog.this, SignInActivity.class);
//                                intent.putExtra("type", "1");
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("reset", true);
                                startActivity(intent);
                                finish();
                            }
                            else {
//                                status false case
                                String failureResponse = resetPasswordResponse.getMessage();
                                Constants.showOneButtonAlertDialog(failureResponse, getResources().getString(R.string.error),
                                        getResources().getString(R.string.ok), ResetPasswordDialog.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(ResetPasswordDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Toast.makeText(ResetPasswordDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }

                    Constants.closeLoadingDialog();
                }

                @Override
                public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                    Log.d(TAG, "resetonFailure: "+t.toString());
                    final String networkStatus = NetworkUtil.getConnectivityStatusString(ResetPasswordDialog.this);
                    if(networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                        Toast.makeText(ResetPasswordDialog.this, R.string.str_connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(ResetPasswordDialog.this, R.string.cannot_reach_server, Toast.LENGTH_SHORT).show();
                    }
                    Constants.closeLoadingDialog();

                }
            });
            return null;
        }
    }
}

