package com.cs.checkclickuser.Dialogs;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;


import com.cs.checkclickuser.Adapter.ProductDetilsSizeAdapter;
import com.cs.checkclickuser.Models.ProductVariantResponse;
import com.cs.checkclickuser.R;

import java.util.ArrayList;

public class ProductDetailsCartDailog extends BottomSheetDialogFragment implements View.OnClickListener {
    View rootView;
    private static String TAG = "TAG";
    RecyclerView listview;
    private ArrayList<ProductVariantResponse.VariantsList> variantsLists = new ArrayList<>();
    ProductDetilsSizeAdapter mSizeAdapter;

    public static ProductDetailsCartDailog newInstance() {
        return new ProductDetailsCartDailog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.productdetails_dailog, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        variantsLists = (ArrayList<ProductVariantResponse.VariantsList>)getArguments().getSerializable("stores");

        listview=(RecyclerView)rootView.findViewById(R.id.listview);

        mSizeAdapter = new ProductDetilsSizeAdapter(getActivity(), variantsLists);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        listview.setAdapter(mSizeAdapter);

        return rootView;
    }

    @Override
    public void onClick(View v) {

    }
}
