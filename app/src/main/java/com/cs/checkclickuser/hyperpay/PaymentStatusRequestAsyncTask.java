package com.cs.checkclickuser.hyperpay;

import android.os.AsyncTask;
import android.util.Log;


import com.cs.checkclickuser.Models.MyCards;
import com.cs.checkclickuser.Rest.APIInterface;
import com.cs.checkclickuser.Rest.ApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Represents an async task to request a payment status from the server.
 */
public class PaymentStatusRequestAsyncTask extends AsyncTask<String, Void, MyCards> {

    private PaymentStatusRequestListener listener;
    private MyCards cardsResponse = null;

    public PaymentStatusRequestAsyncTask(PaymentStatusRequestListener listener) {
        this.listener = listener;
    }

    @Override
    protected MyCards doInBackground(String... params) {
        if (params.length != 5) {
            return null;
        }

//         prepare input json
        JSONObject parentObj = new JSONObject();
        try {
            parentObj.put("userId", params[0]);
            parentObj.put("resourcePath", params[1]);
            parentObj.put("isApplePay", params[2]);
            parentObj.put("isVisaMaster", params[3]);
            parentObj.put("isMadaCard", params[4]);
            parentObj.put("paymentBy", "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final APIInterface apiService =
                ApiClient.getClient().create(APIInterface.class);

        Call<MyCards> call = apiService.getPaymentStatus(
                RequestBody.create(MediaType.parse("application/json"), parentObj.toString()));
        call.enqueue(new Callback<MyCards>() {
            @Override
            public void onResponse(Call<MyCards> call, Response<MyCards> response) {
                Log.d("TAG", "onResponse: " + response);
                if (response.isSuccessful()) {
                    MyCards checkOutIdResponse = response.body();
                    try {
                        if (checkOutIdResponse.getStatus()) {
                            cardsResponse = checkOutIdResponse;

                            if (listener != null) {
                                listener.onPaymentStatusReceived(cardsResponse);
                            }
                        } else {
                            // status false case
                            if (listener != null) {
                                listener.onErrorOccurred();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onErrorOccurred();
                        }
                    }
                } else {
                    if (listener != null) {
                        listener.onErrorOccurred();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyCards> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.toString());
                if (listener != null) {
                    listener.onErrorOccurred();
                }
            }
        });

        return null;
    }

    @Override
    protected void onPostExecute(MyCards paymentStatus) {

    }
}
