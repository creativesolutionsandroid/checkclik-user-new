package com.cs.checkclickuser.hyperpay;


import com.cs.checkclickuser.Models.MyCards;

public interface PaymentStatusRequestListener {

    void onErrorOccurred();
    void onPaymentStatusReceived(MyCards paymentStatus);
}
