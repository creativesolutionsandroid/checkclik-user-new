package com.cs.checkclickuser.hyperpay;


public interface CheckoutIdRequestListener {

    void onCheckoutIdReceived(String checkoutId);
}
